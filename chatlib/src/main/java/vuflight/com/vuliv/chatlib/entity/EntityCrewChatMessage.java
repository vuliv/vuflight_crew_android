package vuflight.com.vuliv.chatlib.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MB0000004 on 09-Aug-18.
 */

public class EntityCrewChatMessage {
    @SerializedName("crewId")
    private int crewId;

    @SerializedName("message")
    private String message;

    @SerializedName("crewName")
    private String crewName;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("isOtherMsg")
    private boolean isOtherMsg;

    public EntityCrewChatMessage(String message, boolean isOtherMsg) {
        this.message = message;
        this.isOtherMsg = isOtherMsg;
    }

    public EntityCrewChatMessage(String message, boolean isOtherMsg, String crewName, String createdAt) {
        this.message = message;
        this.isOtherMsg = isOtherMsg;
        this.crewName = crewName;
        this.createdAt = createdAt;
    }

    public boolean isOtherMsg() {
        return isOtherMsg;
    }

    public void setOtherMsg(boolean otherMsg) {
        isOtherMsg = otherMsg;
    }


    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public int getCrewId() {
        return crewId;
    }

    public void setCrewId(int crewId) {
        this.crewId= crewId;
    }
}
