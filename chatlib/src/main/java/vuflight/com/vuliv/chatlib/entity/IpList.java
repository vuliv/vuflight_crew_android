package vuflight.com.vuliv.chatlib.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isCrew")
    @Expose
    private Boolean isCrew;
    @SerializedName("ip")
    @Expose
    private String ip;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsCrew() {
        return isCrew;
    }

    public void setIsCrew(Boolean isCrew) {
        this.isCrew = isCrew;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

}