package vuflight.com.vuliv.chatlib.observer;


import vuflight.com.vuliv.chatlib.entity.EntityCrewChatMessage;

/**
 * Created by MB0000004 on 10-Aug-18.
 */

public interface IChatMessageObserver {
    void onMessageReceived(EntityCrewChatMessage entityCrewChatMessage);
}
