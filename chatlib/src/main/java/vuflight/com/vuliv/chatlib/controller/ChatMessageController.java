package vuflight.com.vuliv.chatlib.controller;


import android.widget.Toast;

import java.util.HashSet;
import java.util.Set;

import vuflight.com.vuliv.chatlib.entity.EntityCrewChatMessage;
import vuflight.com.vuliv.chatlib.observer.IChatMessageObserver;

/**
 * Created by MB0000004 on 10-Aug-18.
 */

public class ChatMessageController {

    private static ChatMessageController instance;
    private Set<IChatMessageObserver> iChatMessageObserverList = new HashSet<>();


    public Set getAllObserver() {
        return iChatMessageObserverList;
    }

    public static ChatMessageController getInstance() {
        if (instance == null) {
            instance = new ChatMessageController();
        }
        return instance;
    }

    public void registerObserver(IChatMessageObserver chatMessageObserver) {
        iChatMessageObserverList.add(chatMessageObserver);
    }

    public void removeObserver(IChatMessageObserver chatMessageObserver) {
        iChatMessageObserverList.remove(chatMessageObserver);
    }

    public void setChatMessage(EntityCrewChatMessage entityCrewChatMessage) {
        for (IChatMessageObserver chatMessageObserver : iChatMessageObserverList) {
            chatMessageObserver.onMessageReceived(entityCrewChatMessage);
        }
    }

    // void notifyObservers();
}
