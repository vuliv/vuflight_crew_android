package vuflight.com.vuliv.chatlib.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MB0000004 on 09-Aug-18.
 */

public class EntityCrewChatStatus {

    @SerializedName("hasReceived")
    private boolean hasReceived;

    public boolean hasReceived() {
        return hasReceived;
    }

    public void setHasReceived(boolean hasReceived) {
        this.hasReceived = hasReceived;
    }
}
