package vuflight.com.vuliv.chatlib.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class EntityIpDetail {

    @SerializedName("ipList")
    @Expose
    private List<IpList> ipList = new ArrayList<>();

    public List<IpList> getIpList() {
        return ipList;
    }

    public void setIpList(List<IpList> ipList) {
        this.ipList = ipList;
    }

}