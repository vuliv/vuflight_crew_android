package com;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.vuliv.network.utility.StringUtils;
import com.vuliv.network.utility.Utility;

import io.fabric.sdk.android.Fabric;

/**
 * Created by user on 19-09-2017.
 */

public class TheAplication extends Application{

    public static TheAplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Fabric.with(this, new Crashlytics());
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(this);
        config.threadPriority(Thread.MAX_PRIORITY);
        config.threadPoolSize(10);
        config.denyCacheImageMultipleSizesInMemory();
//		config.memoryCacheExtraOptions(500, 500);
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        ImageLoader.getInstance().init(config.build());
    }

    public static TheAplication getInstance() {
        return instance;
    }
}
