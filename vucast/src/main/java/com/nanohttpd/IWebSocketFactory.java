package com.nanohttpd;

import com.nanohttpd.protocols.http.IHTTPSession;
import com.nanohttpd.protocols.websockets.WebSocket;

/**
 * Created by user on 28-07-2017.
 */

public interface IWebSocketFactory {

    WebSocket openWebSocket(IHTTPSession handshake);
}
