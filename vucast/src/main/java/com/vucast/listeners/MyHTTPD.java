package com.vucast.listeners;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nanohttpd.protocols.http.IHTTPSession;
import com.nanohttpd.protocols.http.NanoHTTPD;
import com.nanohttpd.protocols.http.request.Method;
import com.nanohttpd.protocols.http.response.Response;
import com.nanohttpd.protocols.http.response.Status;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.vucast.R;
import com.vucast.constants.Constants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.service.DataService;
import com.vucast.utility.DominosAPI;
import com.vuliv.network.service.SharedPrefController;
import com.vucast.utility.StringUtils;
import com.vucast.utility.Utility;
import com.vucast.utility.WebPageCreater;
import com.vuliv.network.database.sharedpref.SettingHelper;
import com.vuliv.network.database.tables.EntityTableFolderItems;
import com.vuliv.network.database.tables.EntityTableTracking;
import com.vuliv.network.entity.EntityServerPing;
import com.vuliv.network.server.DataController;
import com.vuliv.network.server.TrackingController;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nanohttpd.protocols.http.response.Response.newChunkedResponse;
import static com.nanohttpd.protocols.http.response.Response.newFixedLengthResponse;
import static com.vuliv.network.utility.Constants.USERMACS;


/**
 * Created by user on 31-07-2017.
 */

public class MyHTTPD extends NanoHTTPD implements Constants {

    private static final String TAG = "MyHTTPD";

    final String MIME_PLAINTEXT = "text/plain",
            MIME_HTML = "text/html",
            MIME_JS = "application/javascript",
            MIME_CSS = "text/css",
            MIME_PNG = "image/png",
            MIME_JPEG = "image/jpeg",
            MIME_DEFAULT_BINARY = "application/octet-stream",
            MIME_XML = "text/xml",
            MIME_JSON = "application/json",
            MIME_TYPE_AUDIO = "audio/mp3",
            MIME_TYPE_PHOTO = "image/jpeg",
            MIME_TYPE_VIDEO = "video/mp4";
    FileInputStream fileInputStream = null;
    Context context;
    TypedArray images;

    public MyHTTPD(int port, Context context) {
        super(port);
        this.context = context;
        images = context.getResources().obtainTypedArray(R.array.avatar_images);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected Response serve(IHTTPSession session) {
        Log.wtf(TAG, session.getRemoteIpAddress() + " -> " + session.getUri());
        Response response = newFixedLengthResponse(Status.NOT_FOUND, MIME_PLAINTEXT, "File not found");
        String currentUri = session.getUri();
        String userAgent = null;
        try {
            userAgent = session.getHeaders().get("user-agent");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (currentUri != null && currentUri.startsWith("/vuflight/")) {
            String jsonString = DominosAPI.getInstance().process(currentUri, session.getQueryParameterString());
            response = new Response(Status.OK, MIME_JSON, jsonString, jsonString.length());
            /// TODO: Sumit Agrawal has made changes here
        } else if (currentUri != null && currentUri.startsWith("/ping")) {
            EntityServerPing entityServerPing = new EntityServerPing();
            entityServerPing.setIpAddress(Utility.getIp(context));
            entityServerPing.setName(SharedPrefController.getUserName(context));
            entityServerPing.setIcon(SharedPrefController.getUserAvatar(context));
            entityServerPing.setRegistrationId(SettingHelper.getRegistrationKey(context));
            entityServerPing.setSessionId(DataController.getInstance().getSessionId(context));
            entityServerPing.setPartner(SettingHelper.getPartnerId(context));
            entityServerPing.setSessionTime(Constants.CLIENT_SESSION_TIME);
            entityServerPing.setInActiveSessionTime(Constants.CLIENT_INACTIVE_SESSION_TIME);
            entityServerPing.setDataSize(Constants.CLIENT_DATA_SIZE);
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String jsonString = gson.toJson(entityServerPing, EntityServerPing.class);

            response = new Response(Status.OK, MIME_JSON, jsonString, jsonString.length());
        }/* else if (currentUri != null && currentUri.startsWith("/list")) {
//            response = processListPage();
        } else if (currentUri != null && currentUri.startsWith("/user.png")) {
            Bitmap albumArtBitMap = BitmapFactory.decodeResource(context.getResources(), images.getResourceId(SharedPrefController.getUserAvatar(context), 0));
            InputStream inputstream = Utility.encodeToInputstream(albumArtBitMap, Bitmap.CompressFormat.JPEG, 0);
            try {
                response = new Response(Status.OK, MIME_JPEG, inputstream, inputstream.available());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*//* else if (currentUri != null && currentUri.startsWith("/player")) {
            response = processPlayerPage(session.getParameters());
        } *//*else if (currentUri != null && currentUri.startsWith("/messagePlay")) {
            currentUri = currentUri.replace("/messagePlay", "");
            response = getVideoForIOS(session, currentUri);
        } */ else if ((currentUri != null) && (currentUri.startsWith("/contentUrl"))) {
            String str7 = currentUri.replaceFirst("/contentUrl/", "");
            str7 = str7.replaceFirst(".mp4", "");
//            String[] split = str7.split("/");
            response = getVideoForIOS(session, DataController.getInstance().getVideoDefaultPath("/" + str7));
        } else if ((currentUri != null) && (currentUri.startsWith("/contentThumbnail"))) {
            String str6 = currentUri.replace("/contentThumbnail/", "");
            try {
//                String[] split = str6.split("/");
                DisplayImageOptions localDisplayImageOptions2 = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build();
                ImageSize localImageSize2 = new ImageSize(300, 200);
                InputStream localInputStream3 = Utility.bitmapToInputStream(ImageLoader.getInstance().loadImageSync(DataController.getInstance().getThumbnailDefaultStreamPath("/" + str6), localImageSize2, localDisplayImageOptions2));
                Response localResponse4 = new Response(Status.OK, MIME_JPEG, localInputStream3, localInputStream3.available());
                response = localResponse4;
            } catch (Exception localException4) {
                localException4.printStackTrace();
            }
        }/* else  if ((currentUri != null) && (currentUri.startsWith("/contentLogo"))) {
            String str6 = currentUri.replace("/contentLogo", "");
            try
            {
                DisplayImageOptions localDisplayImageOptions2 = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).bitmapConfig(Bitmap.Config.RGB_565).build();
                ImageSize localImageSize2 = new ImageSize(200, 200);
                InputStream localInputStream3 = Utility.bitmapToInputStream(ImageLoader.getInstance().loadImageSync(DataController.getInstance().getLogoDefaultStreamPath(str6), localImageSize2, localDisplayImageOptions2));
                Response localResponse4 = new Response(Status.OK, "image/png", localInputStream3, localInputStream3.available());
                response = localResponse4;
            } catch (Exception localException4) {
                localException4.printStackTrace();
            }
        } else if (currentUri != null && currentUri.startsWith("/messageVideoImage")) {
            currentUri = currentUri.replace("/messageVideoImage/", "");
            try {
                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build();
                ImageSize targetSize = new ImageSize(300, 200);
                Bitmap bitmap = ImageLoader.getInstance().loadImageSync("content://media/external/video/media/" + currentUri, targetSize, options);
                InputStream inputStream = Utility.bitmapToInputStream(bitmap);
                response = new Response(Status.OK, MIME_PNG, inputStream, inputStream.available());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (currentUri != null && currentUri.startsWith("/messagePhotoImage")) {
            currentUri = currentUri.replace("/messagePhotoImage", "");
            try {
//                response = streamImage(session, currentUri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (currentUri != null && currentUri.startsWith("/messageAudioImage")) {
            currentUri = currentUri.replace("/messageAudioImage", "");
            try {
//                Bitmap thumb = Utility.getAlbumArt(context, currentUri);
//                InputStream inputstream = Utility.encodeToInputstream(thumb, Bitmap.CompressFormat.PNG, 0);
//                response = new Response(Status.OK, MIME_PNG, inputstream, inputstream.available());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } */ else if (currentUri != null && currentUri.startsWith("/api/tracking")) {
            try {
                String id = session.getHeaders().get("id");
                String duration = session.getHeaders().get("duration");
                String model = session.getHeaders().get("model");
                String androidId = session.getHeaders().get("device_id");
                EntityTableTracking entityTableTracking = new EntityTableTracking();
                entityTableTracking.setCampId(Integer.parseInt(id));
                entityTableTracking.setDuration(Integer.parseInt(duration));
                entityTableTracking.setUserAgent(userAgent);
                entityTableTracking.setModel((!StringUtils.isEmpty(model) && "undefined".equalsIgnoreCase(model)) ? null : model);
                entityTableTracking.setAndroidId((!StringUtils.isEmpty(androidId) && "undefined".equalsIgnoreCase(androidId)) ? null : androidId);
                entityTableTracking.setTimestamp(System.currentTimeMillis());
                entityTableTracking.setIp(session.getRemoteIpAddress());
                entityTableTracking.setSessionId(DataController.getInstance().getSessionId(context));
                entityTableTracking.setName(DataController.getInstance().contentMap.get(id).getName());
                entityTableTracking.setTrackingType(EntityTableTracking.TRACKING_TYPE_EVENT);
                String macAddress = null;
                if (!USERMACS.containsKey(session.getRemoteIpAddress())) {
                    com.vuliv.network.utility.Utility.readAddresses();
                    if (USERMACS.containsKey(session.getRemoteIpAddress())) {
                        macAddress = USERMACS.get(session.getRemoteIpAddress());
                    }
                } else {
                    macAddress = USERMACS.get(session.getRemoteIpAddress());
                }
                entityTableTracking.setMac(macAddress);
                DataController.getInstance().insertTracking(context, entityTableTracking);
            } catch (Exception e) {
                e.printStackTrace();
            }
            TrackingController.getInstance().sendTracking(context);
            String jsonString = "{\"status\":\"OK\"}";
            response = new Response(Status.OK, MIME_JSON, jsonString, jsonString.length());
        } else if (currentUri != null && currentUri.startsWith("/api/feedback")) {
            try {
                String message = session.getHeaders().get("message");
                String model = session.getHeaders().get("model");
                String androidId = session.getHeaders().get("device_id");
                EntityTableTracking entityTableTracking = new EntityTableTracking();

                entityTableTracking.setUser(EntityTableTracking.TRACKING_USER_CLIENT);
                entityTableTracking.setStatus(EntityTableTracking.TRACKING_STATUS_REVIEW);
                entityTableTracking.setContentId(Integer.valueOf(message));

                entityTableTracking.setUserAgent(userAgent);
                entityTableTracking.setModel((!StringUtils.isEmpty(model) && "undefined".equalsIgnoreCase(model)) ? null : model);
                entityTableTracking.setAndroidId((!StringUtils.isEmpty(androidId) && "undefined".equalsIgnoreCase(androidId)) ? null : androidId);
                entityTableTracking.setTimestamp(System.currentTimeMillis());
                entityTableTracking.setIp(session.getRemoteIpAddress());
                entityTableTracking.setSessionId(DataController.getInstance().getSessionId(context));
                entityTableTracking.setTrackingType(EntityTableTracking.TRACKING_TYPE_INITIALIZE);
                String macAddress = null;
                if (!USERMACS.containsKey(session.getRemoteIpAddress())) {
                    com.vuliv.network.utility.Utility.readAddresses();
                    if (USERMACS.containsKey(session.getRemoteIpAddress())) {
                        macAddress = USERMACS.get(session.getRemoteIpAddress());
                    }
                } else {
                    macAddress = USERMACS.get(session.getRemoteIpAddress());
                }
                entityTableTracking.setMac(macAddress);
                DataController.getInstance().insertTracking(context, entityTableTracking);
            } catch (Exception e) {
                e.printStackTrace();
            }
            TrackingController.getInstance().sendTracking(context);
            String jsonString = "{\"status\":\"OK\"}";
            response = new Response(Status.OK, MIME_JSON, jsonString, jsonString.length());
        } else if (currentUri != null && currentUri.startsWith("/api")) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(DataService.getInstance().getContent(),
                    new TypeToken<ArrayList<EntityMediaDetail>>() {
                    }.getType());
            response = new Response(Status.OK, MIME_JSON, jsonString, jsonString.length());
        }/* else if (currentUri != null && currentUri.startsWith("/player1")) {
            try {
                String folder = session.getParameters().get("f").get(0);
                String id = session.getParameters().get("id").get(0);
                if (!StringUtils.isEmpty(folder) && !StringUtils.isEmpty(id)) {
                    return processPlayer1Page(folder, id);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/ else if (currentUri != null && currentUri.startsWith("/player")) {
            try {
                String folder = session.getParameters().get("f").get(0);
                String id = session.getParameters().get("id").get(0);
                String platform = null;
                try {
                    platform = session.getParameters().get("platform").get(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                platform = !StringUtils.isEmpty(platform) ? platform : EntityTableFolderItems.PLATFORM_WEB;
                if (!StringUtils.isEmpty(folder) && !StringUtils.isEmpty(id)) {
                    return processPlayerPage(folder, id, platform);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (currentUri != null && currentUri.startsWith("/invite")) {
            return processInvitePage(session);
        } else if (currentUri != null && currentUri.startsWith("/VuFlight.apk")) {
            try {
                InputStream in = context.getAssets().open("VuFlight.apk");
                return new Response(Status.OK, "", in, in.available());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (currentUri != null && currentUri.startsWith("/0.2index.html")) {
            String platform = null;
            try {
                platform = session.getParameters().get("platform").get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            platform = !StringUtils.isEmpty(platform) ? platform : EntityTableFolderItems.PLATFORM_WEB;
            return processHomePage(session, platform, userAgent);
        }/*  else if (currentUri != null && currentUri.startsWith("/dominosvideo.html")) {
            String platform = null;
            try {
                platform = session.getParameters().get("platform").get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            platform = !StringUtils.isEmpty(platform) ? platform : EntityTableFolderItems.PLATFORM_WEB;
            return processHomePage(session, platform, userAgent);
        }  */ else if (currentUri != null && currentUri.startsWith("/dominosvideo.html")) {
            String platform = null;
            try {
                platform = session.getParameters().get("platform").get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            platform = !StringUtils.isEmpty(platform) ? platform : EntityTableFolderItems.PLATFORM_WEB;
            return processDominosPage(session, platform, userAgent);
        } else if (currentUri != null && currentUri.startsWith("/TnC.html")) {
            try {
                return processTnCPage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } /*else if (currentUri != null && currentUri.startsWith("/invite")) {
            return processInvitePage(session);
        } else if (currentUri != null && currentUri.startsWith("/VuScreen.apk")) {
            try {
                InputStream in = context.getAssets().open("VuScreen.apk");
                return new Response(Status.OK, "", in, in.available());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/ else if (currentUri != null && currentUri.startsWith("/")) {
            String platform = null;
            try {
                platform = session.getParameters().get("platform").get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            platform = !StringUtils.isEmpty(platform) ? platform : EntityTableFolderItems.PLATFORM_WEB;
            return processHomePage(session, platform, userAgent);
        } else {
            Log.d(TAG, "Not serving request for: " + session.getUri());
        }

        return response;
    }

    private Response getFullResponse(String mimeType, String fileName) throws FileNotFoundException {
        cleanupStreams();
        fileInputStream = new FileInputStream(fileName);
        return newChunkedResponse(Status.OK, mimeType, fileInputStream);
    }

    private void cleanupStreams() {
        try {
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Response getPartialResponse(String mimeType, String rangeHeader, String fileName) throws IOException {
        File file = new File(fileName);
        String rangeValue = rangeHeader.trim().substring("bytes=".length());
        long fileLength = file.length();
        long start, end;
        if (rangeValue.startsWith("-")) {
            end = fileLength - 1;
            start = fileLength - 1 - Long.parseLong(rangeValue.substring("-".length()));
        } else {
            String[] range = rangeValue.split("-");
            start = Long.parseLong(range[0]);
            end = range.length > 1 ? Long.parseLong(range[1]) : fileLength - 1;
        }
        if (end > fileLength - 1) {
            end = fileLength - 1;
        }
        if (start <= end) {
            long contentLength = end - start + 1;
            cleanupStreams();
            fileInputStream = new FileInputStream(file);
            //noinspection ResultOfMethodCallIgnored
            fileInputStream.skip(start);
            Response response = newChunkedResponse(Status.PARTIAL_CONTENT, mimeType, fileInputStream);
            response.addHeader("Content-Length", contentLength + "");
            response.addHeader("Content-Range", "bytes " + start + "-" + end + "/" + fileLength);
            response.addHeader("Content-Type", mimeType);
            return response;
        } else {
            return newFixedLengthResponse(Status.RANGE_NOT_SATISFIABLE, "text/html", rangeHeader);
        }
    }

    private Response processInvitePage(IHTTPSession session) {
        Response response = null;
        final String PATH = "web";
        String filename = "/";
        String uri = session.getUri();
        filename = uri;
        if (filename.equals("/"))
            filename = "/invite.html";

        InputStream in = null;

        try {
            in = context.getApplicationContext().getAssets().open(PATH + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mime = MIME_HTML;

        if (uri.endsWith(".html")) mime = MIME_HTML;
        else if (uri.endsWith(".css")) mime = MIME_CSS;
        else if (uri.endsWith(".js")) mime = MIME_JS;
        else if (uri.endsWith(".png")) mime = MIME_PNG;
        else if (uri.endsWith(".xml")) mime = MIME_XML;
        else if (uri.endsWith(".json")) mime = MIME_JSON;

        try {
            if (filename.equalsIgnoreCase("/invite.html")) {
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line).append('\n');
                }
                in = new ByteArrayInputStream(total.toString().getBytes(StandardCharsets.UTF_8));
            }

            response = new Response(Status.OK, mime, in, in.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private Response processHomePage(IHTTPSession session, String platform, String userAgent) {
        Response response = null;
        final String PATH = "web";
        String filename = "/";
        String uri = session.getUri();
        filename = uri;
        if (filename.equals("/"))
            filename = "/index.html";

        InputStream in = null;

        try {
            in = context.getApplicationContext().getAssets().open(PATH + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mime = MIME_HTML;

        if (uri.endsWith(".html")) mime = MIME_HTML;
        else if (uri.endsWith(".css")) mime = MIME_CSS;
        else if (uri.endsWith(".js")) mime = MIME_JS;
        else if (uri.endsWith(".png")) mime = MIME_PNG;
        else if (uri.endsWith(".xml")) mime = MIME_XML;
        else if (uri.endsWith(".json")) mime = MIME_JSON;

        try {
            if (filename.equalsIgnoreCase("/index.html")) {
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line).append('\n');
                }

                String paggeString = total.toString().replace("_REPLACE_DIV_", WebPageCreater.getInstance().getWebPage(context, platform, userAgent));

                in = new ByteArrayInputStream(paggeString.getBytes(StandardCharsets.UTF_8));
            }

            response = new Response(Status.OK, mime, in, in.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private Response processDominosPage(IHTTPSession session, String platform, String userAgent) {
        Response response = null;
        final String PATH = "web";
        String filename = "/";
        String uri = session.getUri();
        filename = uri;
        if (filename.equals("/"))
            filename = "/dominosvideo.html";

        InputStream in = null;

        try {
            in = context.getApplicationContext().getAssets().open(PATH + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mime = MIME_HTML;

        if (uri.endsWith(".html")) mime = MIME_HTML;
        else if (uri.endsWith(".css")) mime = MIME_CSS;
        else if (uri.endsWith(".js")) mime = MIME_JS;
        else if (uri.endsWith(".png")) mime = MIME_PNG;
        else if (uri.endsWith(".xml")) mime = MIME_XML;
        else if (uri.endsWith(".json")) mime = MIME_JSON;

        try {
            if (filename.equalsIgnoreCase("/index.html")) {
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line).append('\n');
                }

                in = new ByteArrayInputStream(total.toString().getBytes(StandardCharsets.UTF_8));
            }

            response = new Response(Status.OK, mime, in, in.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private Response processPlayerPage(String folderId, String id, String platform) {
        Response response = null;
        final String PATH = "web";
        String filename = "/player.html";
        InputStream in = null;

        try {
            in = context.getApplicationContext().getAssets().open(PATH + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mime = MIME_HTML;

        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }

            String paggeString = total.toString().replace("_REPLACE_DIV_", WebPageCreater.getInstance().getPlayerPage(folderId, id, context, platform));
            paggeString = paggeString.replace("_REPLACE_FIRST_COOKIE_", WebPageCreater.getInstance().playFirstVideo(folderId, id, context, platform));
            in = new ByteArrayInputStream(paggeString.getBytes(StandardCharsets.UTF_8));

            response = new Response(Status.OK, mime, in, in.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

/*
    private Response processPlayer1Page (String folderId, String id) {
        Response response = null;
        final String PATH = "web";
        String filename = "/player1.html";
        InputStream in = null;

        try {
            in = context.getApplicationContext().getAssets().open(PATH + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mime = MIME_HTML;

        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }

            String paggeString = total.toString().replace("_REPLACE_DIV_", WebPageCreater.getInstance().getPlayer1Page(folderId, id, context));
            paggeString = paggeString.replace("_REPLACE_FIRST_COOKIE_", WebPageCreater.getInstance().playFirstVideo(folderId, id, context));
            in = new ByteArrayInputStream(paggeString.getBytes(StandardCharsets.UTF_8));

            response = new Response(Status.OK, mime, in, in.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
*/

    private Response processTnCPage() {
        Response response = null;
        final String PATH = "web";
        String filename = "/TnC.html";
        InputStream in = null;

        try {
            in = context.getApplicationContext().getAssets().open(PATH + filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mime = MIME_HTML;

        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            StringBuilder paggeString = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                paggeString.append(line).append('\n');
            }
            in = new ByteArrayInputStream(paggeString.toString().getBytes(StandardCharsets.UTF_8));
            response = new Response(Status.OK, mime, in, in.available());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    private Response getVideoForIOS(IHTTPSession session, String currentUri) {
        Map<String, String> headers = session.getHeaders();
//        Map<String, String> parms = session.getParms();
        Method method = session.getMethod();
//        String uri = session.getUri();
        Map<String, String> files = new HashMap<>();

        if (Method.POST.equals(method) || Method.PUT.equals(method)) {
            try {
                session.parseBody(files);
            } catch (IOException e) {
                return getResponse("Internal Error IO Exception: " + e.getMessage());
            } catch (ResponseException e) {
                return new Response(e.getStatus(), MIME_PLAINTEXT, e.getMessage(), e.getMessage().length());
            }
        }

//        uri = uri.trim().replace(File.separatorChar, '/');
//        if (uri.indexOf('?') >= 0) {
//            uri = uri.substring(0, uri.indexOf('?'));
//        }

        File f = new File(currentUri);
        return serveFile(currentUri, headers, f);
    }

    private Response serveFile(String uri, Map<String, String> header, File file) {
        Response res;
        String mime = getMimeTypeForFile(uri);
        try {
            // Calculate etag
            String etag = Integer.toHexString((file.getAbsolutePath() +
                    file.lastModified() + "" + file.length()).hashCode());

            // Support (simple) skipping:
            long startFrom = 0;
            long endAt = -1;
            String range = header.get("range");
            if (range != null) {
                if (range.startsWith("bytes=")) {
                    range = range.substring("bytes=".length());
                    int minus = range.indexOf('-');
                    try {
                        if (minus > 0) {
                            startFrom = Long.parseLong(range.substring(0, minus));
                            endAt = Long.parseLong(range.substring(minus + 1));
                        }
                    } catch (NumberFormatException ignored) {
                    }
                }
            }

            // Change return code and add Content-Range header when skipping is requested
            long fileLen = file.length();
            if (range != null && startFrom >= 0) {
                if (startFrom >= fileLen) {
                    res = createResponse(Status.RANGE_NOT_SATISFIABLE, MIME_PLAINTEXT, "");
                    res.addHeader("Content-Range", "bytes 0-0/" + fileLen);
                    res.addHeader("ETag", etag);
                } else {
                    if (endAt < 0) {
                        endAt = fileLen - 1;
                    }
                    long newLen = endAt - startFrom + 1;
                    if (newLen < 0) {
                        newLen = 0;
                    }

                    final long dataLen = newLen;
                    FileInputStream fis = new FileInputStream(file) {
                        @Override
                        public int available() throws IOException {
                            return (int) dataLen;
                        }
                    };
                    fis.skip(startFrom);

                    res = createResponse(Status.PARTIAL_CONTENT, mime, fis);
                    res.addHeader("Content-Length", "" + dataLen);
                    res.addHeader("Content-Range", "bytes " + startFrom + "-" +
                            endAt + "/" + fileLen);
                    res.addHeader("ETag", etag);
                }
            } else {
                if (etag.equals(header.get("if-none-match")))
                    res = createResponse(Status.NOT_MODIFIED, mime, "");
                else {
                    res = createResponse(Status.OK, mime, new FileInputStream(file));
                    res.addHeader("Content-Length", "" + fileLen);
                    res.addHeader("ETag", etag);
                }
            }
        } catch (IOException ioe) {
            res = getResponse("Forbidden: Reading file failed");
        }

        return (res == null) ? getResponse("Error 404: File not found") : res;
    }

    // Announce that the file server accepts partial content requests
    private Response createResponse(Status status, String mimeType, InputStream message) {
        Response res = null;
        try {
            res = new Response(status, mimeType, message, message.available());
        } catch (IOException e) {
            e.printStackTrace();
        }
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    // Announce that the file server accepts partial content requests
    private Response createResponse(Status status, String mimeType, String message) {
        Response res = new Response(status, mimeType, message, message.length());
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    private Response getResponse(String message) {
        return createResponse(Status.OK, "text/plain", message);
    }

    private Response createFileDownloadResponse(String url) {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
