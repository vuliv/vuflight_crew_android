package com.vucast.listeners;

import android.util.Log;
import android.widget.Switch;

import com.vucast.constants.Constants;
import com.vucast.service.WebServerService;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.FramedataImpl1;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Collections;

/**
 * Created by user on 02-08-2017.
 */

public class WebSocketServerSend extends WebSocketServer implements Constants {

    private static final String TAG = WebSocketServerSend.class.getCanonicalName();
    private static int counter = 0;
    private Switch switchControls;
//    public boolean songPrepared = false;

    public WebSocketServerSend(int port, Draft d, Switch switchControls) throws UnknownHostException {
        super(new InetSocketAddress(port), Collections.singletonList(d));
        this.switchControls = switchControls;
    }

    public WebSocketServerSend(InetSocketAddress address, Draft d) {
        super(address, Collections.singletonList(d));
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        counter++;
        CLIENTS.add(conn);
        WebServerService.getInstance().messageParticularControlsVisibility(conn, switchControls != null ? switchControls.isChecked() : true);
        System.out.println("Opened connection number" + counter + " - " + CLIENTS.size());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        CLIENTS.remove(conn);
        counter++;
        System.out.println("closed " + CLIENTS.size());
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        CLIENTS.remove(conn);
        counter++;
        System.out.println("Error: " + CLIENTS.size());
        ex.printStackTrace();
    }

    @Override
    public void onStart() {
        System.out.println("Server started!");
    }

    @Override
    public void onMessage(WebSocket conn, final String message) {
        Log.wtf(TAG, " MESSAGE: " + message);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer blob) {
        conn.send(blob);
    }

    @Override
    public void onFragment(WebSocket conn, Framedata frame) {
        FramedataImpl1 builder = (FramedataImpl1) frame;
        builder.setTransferemasked(false);
        conn.sendFrame(frame);
    }
}
