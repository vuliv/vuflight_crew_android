package com.vucast.listeners;

/**
 * Created by MX0002681 on 28-09-2015.
 */
public interface InterfaceCallback {
    public void performOkClick();
    public void performCancelClick();
}
