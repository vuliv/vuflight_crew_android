/**
 *
 */
package com.vucast.listeners;

/**
 * @author Ankit Jun 26, 2015 IUniversalCallback.java
 */
public interface IVideoStateCallback {
    void videoStart();

    void videoPause();

    void videoStop();

    void videoBuffering();

    void videoEnd();

    void videoPreparing();

    void setOrientation();

    void share();
}
