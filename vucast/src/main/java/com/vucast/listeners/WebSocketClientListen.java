package com.vucast.listeners;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.Gson;
import com.vucast.callback.IConnectionCallback;
import com.vucast.callback.IPlayCallback;
import com.vucast.constants.Constants;
import com.vucast.constants.DataConstants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.utility.Utility;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.FramedataImpl1;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.nio.ByteBuffer;

/**
 * Created by user on 02-08-2017.
 */

public class WebSocketClientListen extends WebSocketClient implements Constants, OnPreparedListener {
    Context context;
    VideoView videoView;
    private boolean isPrepared = false;
    private String playingVideoUrl;
    private IPlayCallback iPlayCallback;
    private IConnectionCallback iConnectionCallback;

    public WebSocketClientListen(URI serverUri, Draft protocolDraft, Context context, VideoView videoView, IPlayCallback iPlayCallback, IConnectionCallback iConnectionCallback) {
        super(serverUri, protocolDraft);
        this.context = context;
        this.iPlayCallback = iPlayCallback;
        this.iConnectionCallback = iConnectionCallback;
        this.videoView = videoView;
        videoView.setOnPreparedListener(this);
    }

    @Override
    public void onMessage(final String message) {
        if (message != null && message.startsWith(SOCKET_MESSAGE_PLAY)) {
            String replace = message.substring(SOCKET_MESSAGE_PLAY.length());
            if (!TextUtils.isEmpty(replace)) {
                playingVideoUrl = replace;
                isPrepared = false;
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iPlayCallback.playVideo(playingVideoUrl, null, null);
                    }
                });
            } else if (isPrepared) {
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        videoView.start();
                    }
                });
            }
        } else if (message != null && message.startsWith(SOCKET_VIDEO_PLAY)) {
            String replace = message.substring(SOCKET_VIDEO_PLAY.length());
            if (!TextUtils.isEmpty(replace)) {
                playingVideoUrl = replace;
                isPrepared = false;
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iPlayCallback.playVideo(playingVideoUrl, null, null);
                    }
                });
            } else if (isPrepared) {
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        videoView.start();
                    }
                });
            }
        } else if (message != null && message.startsWith(SOCKET_AUDIO_PLAY)) {
            String replace = message.substring(SOCKET_AUDIO_PLAY.length());
            if (!TextUtils.isEmpty(replace)) {
                final EntityMediaDetail entityMediaDetail = new Gson().fromJson(replace, EntityMediaDetail.class);
                playingVideoUrl = entityMediaDetail.getPath();
                isPrepared = false;
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iPlayCallback.playVideo(playingVideoUrl, Utility.getThumbnailUrl(entityMediaDetail), entityMediaDetail);
                    }
                });
            } else if (isPrepared) {
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        videoView.start();
                    }
                });
            }
        } else if (message != null && message.startsWith(SOCKET_PHOTO)) {
            String replace = message.substring(SOCKET_PHOTO.length());
            if (!TextUtils.isEmpty(replace)) {
                final EntityMediaDetail entityMediaDetail = new Gson().fromJson(replace, EntityMediaDetail.class);
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iPlayCallback.playVideo(entityMediaDetail.getPath(), Utility.getThumbnailUrl(entityMediaDetail), entityMediaDetail);
                    }
                });
            }
        } else if (message != null && message.startsWith(SOCKET_MESSAGE_PAUSE)) {
            if (isPrepared) {
                DataConstants.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        videoView.pause();
                    }
                });
            }
        } else if (message != null && message.startsWith(SOCKET_MESSAGE_CONTROLS)) {
            String replace = message.replace(SOCKET_MESSAGE_CONTROLS, "");
            if (!TextUtils.isEmpty(replace)) {
                try {
                    DataConstants.HAVE_CONTROLS = Boolean.parseBoolean(replace);
                    DataConstants.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            videoView.getVideoControls().setVisibility(DataConstants.HAVE_CONTROLS ? View.VISIBLE : View.GONE);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (message != null && message.startsWith(SOCKET_MESSAGE_VOLUME_DOWN)) {
            Utility.VolDown(context);
        } else if (message != null && message.startsWith(SOCKET_MESSAGE_VOLUME_UP)) {
            Utility.VolUp(context);
        }
    }

    @Override
    public void onMessage(ByteBuffer blob) {
        getConnection().send(blob);
    }

    @Override
    public void onError(Exception ex) {
//        System.out.println("Error: ");
//        iConnectionCallback.connectionStatus(isConnected());
        ex.printStackTrace();
    }

    @Override
    public void onOpen(ServerHandshake handshake) {
        iConnectionCallback.connectionStatus(isConnected());
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        iConnectionCallback.connectionStatus(isConnected());
//        System.out.println("Closed: " + code + " " + reason);
    }

    @Override
    public void onWebsocketMessageFragment(WebSocket conn, Framedata frame) {
        FramedataImpl1 builder = (FramedataImpl1) frame;
        builder.setTransferemasked(true);
        getConnection().sendFrame(frame);
    }

    @Override
    public void onPrepared() {
        isPrepared = true;
        videoView.start();
    }

    public boolean isConnected() {
        return isOpen();
    }
}