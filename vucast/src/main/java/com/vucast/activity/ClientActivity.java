package com.vucast.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.listener.VideoControlsButtonListener;
import com.devbrackets.android.exomedia.listener.VideoControlsSeekListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;
import com.vucast.R;
import com.vucast.adapter.AdapterVideoList;
import com.vucast.callback.IConnectionCallback;
import com.vucast.callback.IClickCallback;
import com.vucast.callback.IPlayCallback;
import com.vucast.constants.Constants;
import com.vucast.constants.DataConstants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.service.WebClientService;
import com.vucast.utility.StringUtils;
import com.vucast.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import static com.vucast.constants.DataConstants.HAVE_CONTROLS;

/**
 * Created by user on 01-08-2017.
 */

public class ClientActivity extends AppCompatActivity implements Constants, OnPreparedListener, IPlayCallback, IConnectionCallback {

    public static final String SERVER_SSID = "SSIDName";

    private VideoView videoView;
    private ImageView imageViewer;
    private RecyclerView rvPlaylist;
    private Toolbar mToolbar;
    private String mServerName = "", mSSID = "";
    private int index;
    private ImageView previewImageView;

    private ThinDownloadManager mDownloadManager;
    private long updatedTime;
    private Set<EntityMediaDetail> mDownloadQueue = new HashSet<>();
    private LinearLayoutManager mLinearLayoutManager;
    boolean isSongPlaying = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        mDownloadManager = new ThinDownloadManager();
        updatedTime = System.currentTimeMillis();

        videoView = (VideoView) findViewById(R.id.video_view);
        rvPlaylist = (RecyclerView) findViewById(R.id.rv_playlist);
        imageViewer = (ImageView) findViewById(R.id.image_viewer);
        previewImageView = videoView.getPreviewImageView();
        mLinearLayoutManager = new LinearLayoutManager(this);
        rvPlaylist.setLayoutManager(mLinearLayoutManager);

        mSSID = getIntent().getStringExtra(SERVER_SSID);
        String[] splits = mSSID.split("-");
        index = 0;
        if (splits.length > 2) {
            mServerName = splits[0];
            mServerName = mServerName.replace("\"", "");

            String avatarIndex = splits[1];
            avatarIndex = avatarIndex.replace("\"", "");
            index = Integer.parseInt(avatarIndex);
        }

        TypedArray images = getResources().obtainTypedArray(R.array.avatar_images);

        mToolbar = (Toolbar) findViewById(R.id.client_toolbar);
        mToolbar.setTitle("  " + mServerName);
        mToolbar.setSubtitle("  " + mSSID.replace("\"", ""));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable myIcon = getResources().getDrawable(images.getResourceId(index, 0));
        Bitmap bitmap = ((BitmapDrawable) myIcon).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 50, true));
        mToolbar.setLogo(d);

        WebClientService.getInstance().startClient(this, videoView, this, this);
        refreshClicked();
        HAVE_CONTROLS = true;

        if (Utility.isOrientationLandscape(this)) {
            rvPlaylist.setVisibility(View.GONE);
            mToolbar.setVisibility(View.GONE);
        } else {
            rvPlaylist.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDownloadQueue!= null) mDownloadQueue.clear();
        if (mDownloadManager != null) mDownloadManager.cancelAll();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_client, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.action_invite) {
            inviteClick();
            return true;
        } else if (itemId == R.id.action_refresh) {
            refreshClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Invite button is clicked
     */
    public void inviteClick() {
        WebClientService.getInstance().startClient(this, videoView, this, this);
        StringBuilder message = new StringBuilder();
        message.append("Hey! checkout this amazing video. Connect to \"");
        message.append(mSSID);
        message.append("\" WiFi and click http://");
        message.append(DataConstants.IP_ADDRESS);
        message.append(":" + Constants.SERVER_PORT);

        Log.i("Invite", message.toString());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    /**
     * Refresh button is clicked
     */
    public void refreshClicked() {
        WebClientService.getInstance().startClient(this, videoView, this, this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                String requestData = Utility.getRequestData("http://" + DataConstants.IP_ADDRESS + ":" + Constants.SERVER_PORT + "/api");
                try {
                    Gson gson = new Gson();
                    final ArrayList<EntityMediaDetail> details = gson.fromJson(requestData,
                            new TypeToken<ArrayList<EntityMediaDetail>>() {
                            }.getType());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                rvPlaylist.setAdapter(new AdapterVideoList(ClientActivity.this, details, videoView,
                                        true, ClientActivity.this, null, mClickCallback));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView != null && videoView.isPlaying()) {
            isSongPlaying = true;
            videoView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (videoView != null && isSongPlaying) {
            isSongPlaying = false;
            videoView.start();
        }
    }

    @Override
    public void onBackPressed() {
        Utility.showAlertDialog(this, getString(R.string.receive_stop_message),
                getString(R.string.share_stop_positive),
                getString(R.string.share_stop_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(RESULT_OK);
                        finish();
                    }
                }, null);
    }

    @Override
    public void playVideo(String url, String thumbnail, EntityMediaDetail entityMediaDetail) {
        if (entityMediaDetail != null && Constants.TYPE_PHOTO.equalsIgnoreCase(entityMediaDetail.getType())) {
            try {
                videoView.pause();
            } catch (Exception e) {
                e.printStackTrace();
            }
            imageViewer.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            Glide.with(ClientActivity.this)
                    .setDefaultRequestOptions(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.thumb_music))
                    .load(url)
                    .into(imageViewer);
            return;
        }
        imageViewer.setVisibility(View.GONE);
        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoURI(Uri.parse(url));
        if (StringUtils.isEmpty(thumbnail)) {
            previewImageView.setVisibility(View.GONE);
        } else {
            previewImageView.setVisibility(View.VISIBLE);
            Glide.with(ClientActivity.this)
                    .setDefaultRequestOptions(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.thumb_music))
                    .load(thumbnail)
                    .into(previewImageView);
        }
    }

    @Override
    public void onPrepared() {
        videoView.start();
    }

    @Override
    public void connectionStatus(boolean isConnected) {
        Snackbar snackbar = Snackbar.make(rvPlaylist, isConnected ? R.string.successfully_connected : R.string.disconnected, Snackbar.LENGTH_LONG);
        snackbar.show();
    }



    /**
     * Download this file
     * @param entityMediaDetail
     */
    private void downloadFile(final EntityMediaDetail entityMediaDetail) {
        if (mDownloadQueue!= null) mDownloadQueue.add(entityMediaDetail);

        Log.i("Download", "downloadFile");
        String hostPath = entityMediaDetail.getPath();
        String fileName = hostPath.substring(hostPath.lastIndexOf('/') + 1, hostPath.length());
        final String savedPath = Constants.SAVE_FILE_PATH + "/" + entityMediaDetail.getType() + "/" + fileName;

        File folder = new File(Constants.SAVE_FILE_PATH + "/" + entityMediaDetail.getType());
        if (!folder.exists())
            folder.mkdirs();

        Uri downloadUri = Uri.parse(Constants.DOWNLOAD_FILE_URL + entityMediaDetail.getPath().replace(" ", "+"));
        final Uri destinationUri = Uri.parse(savedPath);

        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri)
                .setPriority(DownloadRequest.Priority.HIGH)
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        // Set file's actual size and downloaded size
                        entityMediaDetail.setDownloadedSize(entityMediaDetail.getActualSize());
                        // Scan Android database
                        Utility.scanFile(ClientActivity.this, savedPath);
                        // Update size views
                        updateViews(true, entityMediaDetail);

                        // Remove downloaded file from queue
                        mDownloadQueue.remove(entityMediaDetail);
                        // Download next file
                        if (mDownloadQueue.size() > 0) {
                            Iterator<EntityMediaDetail> iterator = mDownloadQueue.iterator();
                            while (iterator.hasNext()) {
                                EntityMediaDetail nextFile = iterator.next();
                                downloadFile(nextFile);
                                break;
                            }
                        }
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes,
                                           long downloadedBytes, int progress) {
                        // Set file progress
                        entityMediaDetail.setDownloadedSize(downloadedBytes);
                        // Update size views
                        updateViews(false, entityMediaDetail);
                    }
                });

        // Updated file
        entityMediaDetail.setDownloadId(Long.valueOf(downloadRequest.getDownloadId()));
        entityMediaDetail.setSavedPath(savedPath);
        entityMediaDetail.setDownloadedSize(0);
        // Update size views
        updateViews(true, entityMediaDetail);

        mDownloadManager.add(downloadRequest);
    }

    /**
     * Update download progress
     */
    private void updateViews(boolean forceUpdate, final EntityMediaDetail entityMediaDetail) {
        if (!forceUpdate) {
            if (Math.abs(System.currentTimeMillis() - updatedTime) < 100) return;
        }
        updatedTime = System.currentTimeMillis();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                long downloadedSize = entityMediaDetail.getDownloadedSize();
                long actualSize = entityMediaDetail.getActualSize();
                final long progress = (long) ((downloadedSize * 100) / actualSize);
                Log.wtf("Download", "Progress: " + progress +
                        ", Title: " + entityMediaDetail.getTitle() +
                        ", id: " + entityMediaDetail.getDownloadId());

                RecyclerView.Adapter adapter = rvPlaylist.getAdapter();
                if (adapter != null && adapter instanceof AdapterVideoList) {
                    ((AdapterVideoList) adapter).updateRow(entityMediaDetail);
                }
            }
        });
    }

    private IClickCallback mClickCallback = new IClickCallback() {
        @Override
        public void click(Object object) {
            if (object instanceof EntityMediaDetail) {
                EntityMediaDetail entityMediaDetail = (EntityMediaDetail) object;
                if (mDownloadQueue != null && mDownloadQueue.size() > 0) {
                    Log.wtf("Download", "Queue " + ", Title: " + entityMediaDetail.getTitle());

                    if (mDownloadQueue!= null) mDownloadQueue.add(entityMediaDetail);
                    // Add in download queue
                    entityMediaDetail.setDownloadedSize(0);
                    // Update size views
                    updateViews(true, entityMediaDetail);
                    // Notify user
                    Snackbar snackbar = Snackbar.make(rvPlaylist, R.string.added_in_queue, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }

                downloadFile(entityMediaDetail);
            }
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (Utility.isOrientationLandscape(this)) {
            rvPlaylist.setVisibility(View.GONE);
            mToolbar.setVisibility(View.GONE);
        } else {
            rvPlaylist.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }
}
