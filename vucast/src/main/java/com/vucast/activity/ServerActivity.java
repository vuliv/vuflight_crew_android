package com.vucast.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.listener.VideoControlsButtonListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.vucast.R;
import com.vucast.adapter.AdapterVideoList;
import com.vucast.callback.IPlayCallback;
import com.vucast.constants.Constants;
import com.vucast.constants.DataConstants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.exoplayer.ExoPlayerControllerView;
import com.vucast.service.DataService;
import com.vucast.service.WebServerService;
import com.vucast.utility.StringUtils;
import com.vucast.utility.Utility;
import com.vuliv.network.activity.ParentActivity;

import static com.vucast.activity.ClientActivity.SERVER_SSID;

public class ServerActivity extends ParentActivity implements Constants, OnPreparedListener, VideoControlsButtonListener, IPlayCallback {

    private VideoView videoView;
    private ImageView imageViewer;
    private boolean isPrepared = false;
    private RecyclerView rvPlaylist;
    private Switch switchControls;
    private String filePath = "";
    private EntityMediaDetail entityMediaDetail;
    private String mServerName = "", mSSID = "";
    private Toolbar mToolbar;
    private int index = 0;
    private ImageView previewImage;
    private ExoPlayerControllerView mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataConstants.IP_ADDRESS = Utility.getIp(this);
        init();
        Toast.makeText(this, Utility.getIp(this), Toast.LENGTH_LONG).show();
        if (Utility.isOrientationLandscape(this)) {
            rvPlaylist.setVisibility(View.GONE);
            mToolbar.setVisibility(View.GONE);
        } else {
            rvPlaylist.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WebServerService.getInstance().stopServer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_server, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.action_invite) {
            btn_share_click();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        String message = getString(R.string.share_stop_message);
        String positiveText = getString(R.string.share_stop_positive);
        String negativeText = getString(R.string.share_stop_negative);
        Utility.showAlertDialog(this, message, positiveText, negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(RESULT_OK);
                        finish();
                    }
                }, null);
    }

    private void init() {
        // extract intent values
        mSSID = getIntent().getStringExtra(SERVER_SSID);
        String[] splits = mSSID.split("-");
        index = 0;
        if (splits.length > 2) {
            mServerName = splits[0];
            mServerName = mServerName.replace("\"", "");

            String avatarIndex = splits[1];
            avatarIndex = avatarIndex.replace("\"", "");
            index = Integer.parseInt(avatarIndex);
        }

        setViews();
        setListeners();

        if (DataService.getInstance().getContent().size() > 0) {
            entityMediaDetail = DataService.getInstance().getContent().get(0);
            filePath = entityMediaDetail.getPath();
        }
        rvPlaylist.setAdapter(new AdapterVideoList(this, DataService.getInstance().getContent(), videoView,
                false, ServerActivity.this, switchControls, null));
        rvPlaylist.setLayoutManager(new LinearLayoutManager(this));

        try {
            WebServerService.getInstance().startServer(ServerActivity.this, switchControls);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void setViews() {
        videoView = (VideoView) findViewById(R.id.video_view);
        rvPlaylist = (RecyclerView) findViewById(R.id.rv_playlist);
        switchControls = (Switch) findViewById(R.id.switch_controls);
        imageViewer = (ImageView) findViewById(R.id.image_viewer);
        previewImage = videoView.getPreviewImageView();

        TypedArray images = getResources().obtainTypedArray(R.array.avatar_images);

        mToolbar = (Toolbar) findViewById(R.id.server_toolbar);
        mToolbar.setTitle("  " + mServerName);
        mToolbar.setSubtitle("  " + mSSID.replace("\"", ""));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable myIcon = getResources().getDrawable(images.getResourceId(index, 0));
        Bitmap bitmap = ((BitmapDrawable) myIcon).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 50, true));
        mToolbar.setLogo(d);
    }

    private void setListeners() {
        videoView.setOnPreparedListener(this);
        videoView.getVideoControls().setButtonListener(this);
        switchControls.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                WebServerService.getInstance().messageControlsVisibility(b);
            }
        });
    }

    public void playPlayback() {
        if (Utility.sendMessage(switchControls)) WebServerService.getInstance().messagePlay();
        if (isPrepared) {
            videoView.start();
        } else {
            playVideo(filePath, Constants.TYPE_MUSIC.equalsIgnoreCase(entityMediaDetail.getType()) ? (Utility.getThumbnailUrl(entityMediaDetail)) : null, entityMediaDetail);
            if (Utility.sendMessage(switchControls)) {
                WebServerService.getInstance().messagePlayExactContent(entityMediaDetail);
            }
        }
    }

    public void pausePlayback() {
        if (isPrepared) {
            videoView.pause();
        }
        if (Utility.sendMessage(switchControls)) WebServerService.getInstance().messagePause();
    }

    @Override
    public void onPrepared() {
        isPrepared = true;
        videoView.start();
    }

    @Override
    public boolean onPlayPauseClicked() {
        if (videoView.isPlaying()) {
            pausePlayback();
        } else {
            playPlayback();
        }
        return true;
    }

    @Override
    public boolean onPreviousClicked() {
        return false;
    }

    @Override
    public boolean onNextClicked() {
        return false;
    }

    @Override
    public boolean onRewindClicked() {
        return false;
    }

    @Override
    public boolean onFastForwardClicked() {
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (Utility.sendMessage(switchControls)) WebServerService.getInstance().messageVolumeUp(ServerActivity.this);
                    Utility.VolUp(ServerActivity.this);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (Utility.sendMessage(switchControls)) WebServerService.getInstance().messageVolumeDown(ServerActivity.this);
                    Utility.VolDown(ServerActivity.this);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    public void btn_share_click() {
        Toast.makeText(this, Utility.getIp(this), Toast.LENGTH_LONG).show();
        StringBuilder message = new StringBuilder();
        message.append("Hey! checkout this amazing video. Connect to \"");
        message.append(mSSID);
        message.append("\" WiFi and click http://");
        message.append(Utility.getIp(this));
        message.append(":" + Constants.SERVER_PORT);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView != null && videoView.isPlaying()) {
            videoView.pause();
        }
    }

    @Override
    public void playVideo(String url, String thumbnail, EntityMediaDetail entityMediaDetail) {
        if (entityMediaDetail != null && Constants.TYPE_PHOTO.equalsIgnoreCase(entityMediaDetail.getType())) {
            try {
                videoView.pause();
            } catch (Exception e) {
                e.printStackTrace();
            }
            imageViewer.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            Glide.with(ServerActivity.this)
                    .setDefaultRequestOptions(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.thumb_music))
                    .load(url)
                    .into(imageViewer);
            return;
        }
        imageViewer.setVisibility(View.GONE);
        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoURI(Uri.parse(url));
        if (StringUtils.isEmpty(thumbnail)) {
            previewImage.setVisibility(View.GONE);
        } else {
            previewImage.setVisibility(View.VISIBLE);
            Glide.with(ServerActivity.this)
                    .setDefaultRequestOptions(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.thumb_music))
                    .load(thumbnail)
                    .into(previewImage);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (Utility.isOrientationLandscape(this)) {
            rvPlaylist.setVisibility(View.GONE);
            mToolbar.setVisibility(View.GONE);
        } else {
            rvPlaylist.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }
}
