package com.vucast.adapter;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.google.gson.Gson;
import com.vucast.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vucast.callback.IClickCallback;
import com.vucast.callback.IPlayCallback;
import com.vucast.constants.Constants;
import com.vucast.constants.DataConstants;
import com.vucast.entity.EntityMediaDetail;
import com.bumptech.glide.Glide;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.vucast.service.WebServerService;
import com.vucast.utility.Utility;

import java.io.File;
import java.util.ArrayList;

import static com.vucast.constants.Constants.CLIENTS;
import static com.vucast.constants.Constants.SOCKET_AUDIO_PLAY;
import static com.vucast.constants.Constants.SOCKET_MESSAGE_PLAY;
import static com.vucast.constants.Constants.SOCKET_VIDEO_PLAY;

/**
 * Created by user on 20-08-2017.
 */

public class AdapterVideoList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<EntityMediaDetail> list = new ArrayList<>();
    VideoView videoView;
    boolean clientStream;
    private IPlayCallback iPlayCallback;
    private final IClickCallback mClickCallback;
    private Switch controlSwitch;

    public AdapterVideoList(Context context, ArrayList<EntityMediaDetail> list, VideoView videoView,
                            boolean clientStream, IPlayCallback iPlayCallback, Switch controlSwitch,
                            IClickCallback clickCallback) {
        this.context = context;
        this.list.addAll(list);
        this.videoView = videoView;
        this.clientStream = clientStream;
        this.iPlayCallback = iPlayCallback;
        this.controlSwitch = controlSwitch;
        this.mClickCallback = clickCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_list, parent, false);
        RecyclerView.ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final EntityMediaDetail entityMediaDetail = list.get(position);

            // Handle thumbnail
            if (clientStream) {
                String thumbnail = Utility.getThumbnailUrl(entityMediaDetail);

                Glide.with(context)
                .setDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.thumb_music))
                .load(thumbnail)
                .into(((ViewHolder) holder).ivBanner);

                // Handle download views
                long downloadedSize = entityMediaDetail.getDownloadedSize();
                long actualSize = entityMediaDetail.getActualSize();
                if (downloadedSize == -1) {
                    ((ViewHolder) holder).ivDownload.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).ivCheck.setVisibility(View.GONE);
                    ((ViewHolder) holder).progressBar.setVisibility(View.GONE);
                } else if (downloadedSize == 0) {
                    ((ViewHolder) holder).ivDownload.setVisibility(View.GONE);
                    ((ViewHolder) holder).ivCheck.setVisibility(View.GONE);
                    ((ViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).progressBar.setProgress(0);
                } else if (downloadedSize == actualSize) {
                    ((ViewHolder) holder).ivDownload.setVisibility(View.GONE);
                    ((ViewHolder) holder).ivCheck.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).progressBar.setVisibility(View.GONE);
                } else if (downloadedSize < actualSize) {
                    ((ViewHolder) holder).ivDownload.setVisibility(View.GONE);
                    ((ViewHolder) holder).ivCheck.setVisibility(View.GONE);
                    ((ViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
                    final long progress = (long) ((downloadedSize * 100) / actualSize);
                    ((ViewHolder) holder).progressBar.setProgress(progress);
                }
            } else {
                String thumbnail = "file://" + entityMediaDetail.getPath();
                if (Constants.TYPE_MUSIC.equals(entityMediaDetail.getType())) {
                    thumbnail = entityMediaDetail.getAudioThumbnail();
                } else if (Constants.TYPE_VIDEO.equals(entityMediaDetail.getType())) {
                    thumbnail = "file://" + entityMediaDetail.getPath();
                }
                Glide.with(context)
                .setDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.thumb_music))
                .load(thumbnail)
                .into(((ViewHolder) holder).ivBanner);

                // Handle download views
                ((ViewHolder) holder).ivDownload.setVisibility(View.GONE);
                ((ViewHolder) holder).ivCheck.setVisibility(View.GONE);
                ((ViewHolder) holder).progressBar.setVisibility(View.GONE);
            }

            // Handle title and subtitles
            ((ViewHolder) holder).tvTitle.setText(entityMediaDetail.getTitle());
            if (Constants.TYPE_PHOTO.equalsIgnoreCase(entityMediaDetail.getType())) {
                ((ViewHolder) holder).tvSubtitle.setText(R.string.image);
            } else {
                ((ViewHolder) holder).tvSubtitle.setText(Utility.getDurationFromMilli(entityMediaDetail.getDuration()));
            }

            // Handle click listeners
            ((ViewHolder) holder).llRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clientStream) {
                        if (!DataConstants.HAVE_CONTROLS) return;
                        String thumbnail = Utility.getThumbnailUrl(entityMediaDetail);
                        iPlayCallback.playVideo(Constants.VIDEO_URL + entityMediaDetail.getPath().replace(" ", "+"), Constants.TYPE_MUSIC.equalsIgnoreCase(entityMediaDetail.getType()) ? thumbnail : null, entityMediaDetail);
                    } else {
                        String filePath = entityMediaDetail.getPath();
                        if (!Uri.parse(filePath).equals(videoView.getVideoUri())) {
                            String thumbnail = Utility.getThumbnailUrl(entityMediaDetail);
                            iPlayCallback.playVideo(filePath, Constants.TYPE_MUSIC.equalsIgnoreCase(entityMediaDetail.getType()) ? thumbnail : null, entityMediaDetail);
                        } else {
                            videoView.setVisibility(View.VISIBLE);
                            videoView.start();
                        }
                        if (Utility.sendMessage(controlSwitch)) {
                            WebServerService.getInstance().messagePlayExactContent(entityMediaDetail);
                        }
                    }
                }
            });

            ((ViewHolder) holder).ivDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mClickCallback != null) mClickCallback.click(entityMediaDetail);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * Update single row of list
     * @param entityMediaDetail
     */
    public void updateRow(EntityMediaDetail entityMediaDetail) {
        int size = list.size();
        for (int i=0; i<size; i++) {
            EntityMediaDetail entityMediaDetail1 = list.get(i);
            if (entityMediaDetail1.getPath().equals(entityMediaDetail.getPath())) {
                Log.i("Download", "Update row: " + i);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivBanner;
        public TextView tvTitle;
        public TextView tvSubtitle;
        private RelativeLayout llRoot;
        public ImageView ivDownload;
        public ImageView ivCheck;
        public CircularProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);
            ivBanner = (ImageView) v.findViewById(R.id.ivBanner);
            ivDownload = (ImageView) v.findViewById(R.id.ivDownload);
            ivCheck = (ImageView) v.findViewById(R.id.ivCheck);
            progressBar = (CircularProgressBar) v.findViewById(R.id.item_download_progress);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ivBanner.setClipToOutline(true);
            }
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvSubtitle = (TextView) v.findViewById(R.id.tvSubtitle);
            llRoot = (RelativeLayout) v.findViewById(R.id.llRoot);
        }
    }
}
