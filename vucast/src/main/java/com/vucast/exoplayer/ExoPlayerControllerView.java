package com.vucast.exoplayer;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.vucast.R;
import com.vucast.listeners.IVideoStateCallback;
import com.vucast.listeners.InterfaceCallback;

import java.lang.ref.WeakReference;
import java.util.Formatter;
import java.util.Locale;

public class ExoPlayerControllerView extends FrameLayout implements View.OnClickListener {

    private static final String TAG = "VideoControllerView";

//    private MediaController.MediaPlayerControl mPlayer;
    private VideoView mPlayer;
    private Context mContext;
    private ViewGroup mAnchor;
    private View mRoot;
    public FrameLayout playMediaRoot;
    private SeekBar mProgress;
    private TextView mEndTime, mCurrentTime;
    private boolean mShowing;
    private boolean mDragging;
    private static final int sDefaultTimeout = 3000;
    private static final int FADE_OUT = 1;
    private static final int SHOW_PROGRESS = 2;
    private boolean mUseFastForward;
    private boolean mFromXml;
    private boolean mListenersSet;
    private OnClickListener mNextListener, mPrevListener;
    StringBuilder mFormatBuilder;
    Formatter mFormatter;
    private PlayPauseButton mPauseButton;
//    private ImageView mFfwdButton;
//    private ImageView mRewButton;
    private ImageButton mNextButton;
    private ImageButton mPrevButton;
//    private ImageButton mShareButton;
//    private ImageButton mMenuButton;
//    private ImageButton mSettingsButton;
//    private ImageView mFullscreenButton;
    private Handler mHandler = new MessageHandler(this);
//    private boolean mShowShare, mShowMenu;
//    private InterfaceCallback mInterfaceCallback;
//    private OnClickListener mInterfaceCallbackMenu;
//    private IVideoStateCallback iVideoStateCallback;

    private boolean isManuallyPause;

    public ExoPlayerControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRoot = null;
        mContext = context;
        mUseFastForward = true;
        mFromXml = true;
        Log.i(TAG, TAG);
    }

    public ExoPlayerControllerView(Context context, boolean useFastForward) {
        super(context);
        mContext = context;
        mUseFastForward = useFastForward;
        Log.i(TAG, TAG);
    }

    public ExoPlayerControllerView(Context context) {
        this(context, true);
        Log.i(TAG, TAG);
    }

    @Override
    public void onFinishInflate() {
        if (mRoot != null)
            initControllerView(mRoot);
    }

    public boolean isManuallyPause() {
        return isManuallyPause;
    }

    public void setMediaPlayer(VideoView player) {
        mPlayer = player;
        updatePausePlay();
        updateFullScreen();
    }

    public VideoView getMediaPlayer() {
        return mPlayer;
    }

    /**
     * Устанавливает якорь на ту вьюху на которую вы хотите разместить контролы
     * Это может быть например VideoView или SurfaceView
     */
    public void setAnchorView(ViewGroup view
                              /*boolean showShare, InterfaceCallback interfaceCallback, boolean showMenu,
                              OnClickListener interfaceCallbackMenu,
                              IVideoStateCallback iVideoStateCallback*/) {
//        mShowShare = showShare;
//        mShowMenu = showMenu;
//        mInterfaceCallback = interfaceCallback;
//        mInterfaceCallbackMenu = interfaceCallbackMenu;
        mAnchor = view;
//        this.iVideoStateCallback = iVideoStateCallback;
        LayoutParams frameParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        removeAllViews();
        View v = makeControllerView();
        addView(v, frameParams);
    }

    /**
     * Создает вьюху которая будет находится поверх вашего VideoView или другого контролла
     */
    protected View makeControllerView() {
        LayoutInflater inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRoot = inflate.inflate(R.layout.play_media_controller, null);
        playMediaRoot = (FrameLayout) mRoot.findViewById(R.id.play_media_root);
        initControllerView(mRoot);

        return mRoot;
    }

    private void initControllerView(View v) {
        mPauseButton = (PlayPauseButton) v.findViewById(R.id.pause);
        if (mPauseButton != null) {
            mPauseButton.setPlayed(true);
            mPauseButton.startAnimation();
            mPauseButton.requestFocus();
            mPauseButton.setOnClickListener(mPauseListener);
        }

        /*mFullscreenButton = (ImageView) v.findViewById(R.id.fullscreen);
        if (mFullscreenButton != null) {
            if (iVideoStateCallback == null) {
                mFullscreenButton.setVisibility(GONE);
            } else {
                mFullscreenButton.requestFocus();
                mFullscreenButton.setOnClickListener(mFullscreenListener);
            }
        }

        mFfwdButton = (ImageView) v.findViewById(R.id.ffwd);
        if (mFfwdButton != null) {
            mFfwdButton.setOnClickListener(mFfwdListener);
            if (!mFromXml) {
                mFfwdButton.setVisibility(mUseFastForward ? View.VISIBLE : View.GONE);
            }
        }

        mRewButton = (ImageView) v.findViewById(R.id.rew);
        if (mRewButton != null) {
            mRewButton.setOnClickListener(mRewListener);
            if (!mFromXml) {
                mRewButton.setVisibility(mUseFastForward ? View.VISIBLE : View.GONE);
            }
        }*/

        // По дефолту вьюха будет спрятана, и показываться будет после события onTouch()
        mNextButton = (ImageButton) v.findViewById(R.id.next);
        if (mNextButton != null && !mFromXml && !mListenersSet) {
            mNextButton.setVisibility(View.GONE);
        }
        mPrevButton = (ImageButton) v.findViewById(R.id.prev);
        if (mPrevButton != null && !mFromXml && !mListenersSet) {
            mPrevButton.setVisibility(View.GONE);
        }

        /*mShareButton = (ImageButton) v.findViewById(R.id.ibShare);
        mMenuButton = (ImageButton) v.findViewById(R.id.ibMenu);
        mSettingsButton = (ImageButton) v.findViewById(R.id.ibSettings);
        mMenuButton.setOnClickListener(mInterfaceCallbackMenu);
        mShareButton.setOnClickListener(this);
        mSettingsButton.setOnClickListener(this);
        *//*if (mShareButton != null && !mFromXml && !mListenersSet) {
            mShareButton.setVisibility(View.GONE);
        } else *//*
        if (mShowShare) {
            mShareButton.setVisibility(View.VISIBLE);
        } else {
            mShareButton.setVisibility(View.GONE);
        }
        if (mShowMenu) {
            mMenuButton.setVisibility(View.VISIBLE);
        } else {
            mMenuButton.setVisibility(View.GONE);
        }*/

        mProgress = (SeekBar) v.findViewById(R.id.mediacontroller_progress);
        if (mProgress != null) {
            if (mProgress instanceof SeekBar) {
                SeekBar seeker = (SeekBar) mProgress;
                seeker.setPadding(2, 0, 2, 0);
                seeker.setOnSeekBarChangeListener(mSeekListener);
            }
            mProgress.setMax(1000);
        }

        mEndTime = (TextView) v.findViewById(R.id.time);
        mCurrentTime = (TextView) v.findViewById(R.id.time_current);
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());

        installPrevNextListeners();
    }

    /**
     * Показывает контроллер на экране
     * Он будет убран через 3 секуунды
     */
    public void show() {
        show(sDefaultTimeout);
    }

    /**
     * Отключить паузу или seek button, если поток не может быть приостановлена
     * Это требует интерфейс управления MediaPlayerControlExt
     */
    private void disableUnsupportedButtons() {
        if (mPlayer == null) {
            return;
        }

       /* try {
            if (mPauseButton != null && !mPlayer.canPause()) {
                mPauseButton.setEnabled(false);
            }
            if (mRewButton != null && !mPlayer.canSeekBackward()) {
                mRewButton.setEnabled(false);
            }
            if (mFfwdButton != null && !mPlayer.canSeekForward()) {
                mFfwdButton.setEnabled(false);
            }
        } catch (IncompatibleClassChangeError ex) {
            //выводите в лог что хотите из ex
        }*/
    }

    /**
     * Показывает контроллы на экране
     * Он исчезнет автоматически после своего таймаута
     */
    public void show(int timeout) {
        if (!mShowing && mAnchor != null) {
            setProgress();
            if (mPauseButton != null) {
                mPauseButton.requestFocus();
            }
            disableUnsupportedButtons();

            LayoutParams tlp = new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM
            );

            mAnchor.addView(this, tlp);
            mShowing = true;
        }
        updatePausePlay();
        updateFullScreen();

        mHandler.sendEmptyMessage(SHOW_PROGRESS);

        Message msg = mHandler.obtainMessage(FADE_OUT);
        if (timeout != 0) {
            mHandler.removeMessages(FADE_OUT);
            mHandler.sendMessageDelayed(msg, timeout);
        }
    }

    public boolean isShowing() {
        return mShowing;
    }

    /**
     * Удаляем контроллы с экрана.
     */
    public void hide() {
        if (mAnchor == null) {
            return;
        }

        try {
            mAnchor.removeView(this);
            mHandler.removeMessages(SHOW_PROGRESS);
        } catch (IllegalArgumentException ex) {
            Log.w("MediaController", "already removed");
        }
        mShowing = false;
    }

    private String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private int setProgress() {
        if (mPlayer == null || mDragging) {
            return 0;
        }

        int position = (int) mPlayer.getCurrentPosition();
        int duration = (int) mPlayer.getDuration();
        if (mProgress != null) {
            if (duration > 0) {
                // use long to avoid overflow
                long pos = 1000L * position / duration;
                mProgress.setProgress((int) pos);
            }
            int percent = mPlayer.getBufferPercentage();
            mProgress.setSecondaryProgress(percent * 10);
        }

        if (mEndTime != null)
            mEndTime.setText(stringForTime(duration - position));
        if (mCurrentTime != null)
            mCurrentTime.setText(stringForTime(position));

        return position;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        show(sDefaultTimeout);
        return true;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent ev) {
        show(sDefaultTimeout);
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mPlayer == null) {
            return true;
        }

        int keyCode = event.getKeyCode();
        final boolean uniqueDown = event.getRepeatCount() == 0
                && event.getAction() == KeyEvent.ACTION_DOWN;
        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                || keyCode == KeyEvent.KEYCODE_SPACE) {
            if (uniqueDown) {
                doPauseResume();
                show(sDefaultTimeout);
                if (mPauseButton != null) {
                    mPauseButton.requestFocus();
                }
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY) {
            if (uniqueDown && !mPlayer.isPlaying()) {
                mPlayer.start();
                updatePausePlay();
                show(sDefaultTimeout);
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP
                || keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE) {
            if (uniqueDown && mPlayer.isPlaying()) {
                mPlayer.pause();
                updatePausePlay();
                show(sDefaultTimeout);
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
                || keyCode == KeyEvent.KEYCODE_VOLUME_UP
                || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
            return super.dispatchKeyEvent(event);
        } else if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU) {
            if (uniqueDown) {
                hide();
            }
            return true;
        }

        show(sDefaultTimeout);
        return super.dispatchKeyEvent(event);
    }

    private OnClickListener mPauseListener = new OnClickListener() {
        public void onClick(View v) {
            doPauseResume();
            show(sDefaultTimeout);
        }
    };

    /*private OnClickListener mFullscreenListener = new OnClickListener() {
        public void onClick(View v) {
            doToggleFullscreen();
            show(sDefaultTimeout);
        }
    };*/

    public void updatePausePlay() {
        if (mRoot == null || mPauseButton == null || mPlayer == null) {
            return;
        }

      /*  if (mPlayer.isPlaying() && mPauseButton.isPlayed()) {
            mPauseButton.setPlayed(false);
            mPauseButton.startAnimation();
        } else {
            mPauseButton.setPlayed(true);
            mPauseButton.startAnimation();
        }*/
        /*if (mPlayer.isPlaying()) {
            mPauseButton.setImageResource(R.drawable.pause);
        } else {
            mPauseButton.setImageResource(R.drawable.play);
        }*/
    }

    public void updateFullScreen() {
        if (mRoot == null || /*mFullscreenButton == null ||*/ mPlayer == null) {
            return;
        }

        /*if (mPlayer.isFullScreen()) {
            mFullscreenButton.setImageResource(R.drawable.play);
        }
        else {
            mFullscreenButton.setImageResource(R.drawable.pause);
        }*/
    }

    public void doPauseResume() {
        if (mPlayer == null) {
            return;
        }

        if (mPlayer.isPlaying()) {
            isManuallyPause = true;
            mPlayer.pause();
        } else {
            isManuallyPause = false;
            mPlayer.start();
        }

        if (mPauseButton.isPlayed()) {
            mPauseButton.setPlayed(false);
            mPauseButton.startAnimation();
        } else {
            mPauseButton.setPlayed(true);
            mPauseButton.startAnimation();
        }
        updatePausePlay();
    }

    public void showPauseIcon(){
        mPauseButton.setPlayed(false);
        mPauseButton.startAnimation();
    }

    public void showPlayIcon(){
        mPauseButton.setPlayed(true);
        mPauseButton.startAnimation();
    }

    public void pauseVideo() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
            updatePausePlay();
        }
    }

    public void playVideo() {
        if (mPlayer != null && !mPlayer.isPlaying()) {
            mPlayer.start();
            updatePausePlay();
        }
    }

    /*private void doToggleFullscreen() {
        if (mPlayer == null) {
            return;
        }
        iVideoStateCallback.setOrientation();
//        mPlayer.toggleFullScreen();
    }*/

    private OnSeekBarChangeListener mSeekListener = new OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar bar) {
            show(3600000);

            mDragging = true;
            mHandler.removeMessages(SHOW_PROGRESS);
        }

        public void onProgressChanged(SeekBar bar, int progress, boolean fromuser) {
            if (mPlayer == null) {
                return;
            }

            if (!fromuser) {
                return;
            }

            long duration = mPlayer.getDuration();
            long newposition = (duration * progress) / 1000L;
            mPlayer.seekTo((int) newposition);
            if (mCurrentTime != null)
                mCurrentTime.setText(stringForTime((int) newposition));
        }

        public void onStopTrackingTouch(SeekBar bar) {
            mDragging = false;
            setProgress();
            updatePausePlay();
            show(sDefaultTimeout);

            mHandler.sendEmptyMessage(SHOW_PROGRESS);
        }
    };

    @Override
    public void setEnabled(boolean enabled) {
        if (mPauseButton != null) {
            mPauseButton.setEnabled(enabled);
        }
        /*if (mFfwdButton != null) {
            mFfwdButton.setEnabled(enabled);
        }
        if (mRewButton != null) {
            mRewButton.setEnabled(enabled);
        }*/
        if (mNextButton != null) {
            mNextButton.setEnabled(enabled && mNextListener != null);
        }
        if (mPrevButton != null) {
            mPrevButton.setEnabled(enabled && mPrevListener != null);
        }
        if (mProgress != null) {
            mProgress.setEnabled(enabled);
        }
        disableUnsupportedButtons();
        super.setEnabled(enabled);
    }

    private OnClickListener mRewListener = new OnClickListener() {
        public void onClick(View v) {
            if (mPlayer == null) {
                return;
            }

            int pos = (int) mPlayer.getCurrentPosition();
            pos -= 10000; // милисекунд
            mPlayer.seekTo(pos);
            setProgress();

            show(sDefaultTimeout);
        }
    };

    private OnClickListener mFfwdListener = new OnClickListener() {
        public void onClick(View v) {
            if (mPlayer == null) {
                return;
            }

            int pos = (int) mPlayer.getCurrentPosition();
            pos += 10000; // милисекунд
            mPlayer.seekTo(pos);
            setProgress();

            show(sDefaultTimeout);
        }
    };

    private void installPrevNextListeners() {
        if (mNextButton != null) {
            mNextButton.setOnClickListener(mNextListener);
            mNextButton.setEnabled(mNextListener != null);
        }

        if (mPrevButton != null) {
            mPrevButton.setOnClickListener(mPrevListener);
            mPrevButton.setEnabled(mPrevListener != null);
        }
    }

    public void setPrevNextListeners(OnClickListener next, OnClickListener prev) {
        mNextListener = next;
        mPrevListener = prev;
        mListenersSet = true;

        if (mRoot != null) {
            installPrevNextListeners();

            if (mNextButton != null && !mFromXml) {
                mNextButton.setVisibility(View.VISIBLE);
            }
            if (mPrevButton != null && !mFromXml) {
                mPrevButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        /*switch (v.getId()){
            case R.id.ibShare :
                mInterfaceCallback.performOkClick();
                break;
            case R.id.ibSettings :
                break;
        }*/
    }

/*    public interface MediaPlayerControl {
        void    start();
        void    pause();
        int     getDuration();
        int     getCurrentPosition();
        void    seekTo(int pos);
        boolean isPlaying();
        int     getBufferPercentage();
        boolean canPause();
        boolean canSeekBackward();
        boolean canSeekForward();
        boolean isFullScreen();
        void    toggleFullScreen();
    }*/

    private static class MessageHandler extends Handler {
        private final WeakReference<ExoPlayerControllerView> mView;

        MessageHandler(ExoPlayerControllerView view) {
            mView = new WeakReference<ExoPlayerControllerView>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            ExoPlayerControllerView view = mView.get();
            if (view == null || view.mPlayer == null) {
                return;
            }

            int pos;
            switch (msg.what) {
                case FADE_OUT:
                    view.hide();
                    break;
                case SHOW_PROGRESS:
                    pos = view.setProgress();
                    if (!view.mDragging && view.mShowing && view.mPlayer.isPlaying()) {
                        msg = obtainMessage(SHOW_PROGRESS);
                        sendMessageDelayed(msg, 1000 - (pos % 1000));
                    }
                    break;
            }
        }
    }

    /**
     * update share button
     *
     * @param enable
     */
    public void updateShare(boolean enable) {
        /*if(enable) {
            mShareButton.setAlpha(1.0F);
        } else {
            mShareButton.setAlpha(0.5F);
        }
        mShareButton.setEnabled(enable);*/
    }
}