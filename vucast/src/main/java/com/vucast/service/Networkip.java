//package com.vucast.service;
//
//import android.app.Activity;
//import android.database.Cursor;
//import android.net.DhcpInfo;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.AsyncTask;
//import android.text.format.Formatter;
//
//import java.net.InetAddress;
//import java.util.HashMap;
//
///**
// * Created by user on 12-09-2017.
// */
//
//public class Networkip extends Activity{
//    private static final int BUF = 8192;
//    private static final String MAC_RE = "^%s\\s+0x1\\s+0x2\\s+([:0-9a-fA-F]+)\\s+\\*\\s+\\w+$";
//    private static final String TAG = "HardwareAddress";
//    private final String CMD = "/system/bin/ping -A -q -n -w 3 -W 2 -c 3 ";
//    private Button ScanButton;
//    private Button ScanButton2;
//    private boolean bHotspot = false;
//    private boolean bOnAdLoaded = false;
//    private boolean bWifi;
//    String hostIP;
//    private int iaddress1;
//    private int iaddress2;
//    private int iaddress3;
//    private int iaddress4;
//    ArrayList<HashMap<String, Object>> list;
//    private SimpleAdapter listAdapter = null;
//    private WifiManager mCheckWifi;
//    private MyTask mTask;
//    private Long realipaddress;
//    private String szHotspotIP;
//    String szgatewayIP;
//
//    private void WifiDeviceInfo()
//    {
//        WifiManager localWifiManager = (WifiManager)getSystemService("wifi");
//        WifiInfo localWifiInfo = localWifiManager.getConnectionInfo();
//        DhcpInfo localDhcpInfo = localWifiManager.getDhcpInfo();
//        if (localWifiManager.getWifiState() == 3)
//        {
//            this.bWifi = true;
//            int i = localWifiInfo.getIpAddress();
//            this.realipaddress = Long.valueOf(localWifiInfo.getIpAddress());
//            String str1 = localWifiInfo.getMacAddress();
//            String str2 = (str1.substring(0, 2) + str1.substring(3, 5) + str1.substring(6, 8)).toUpperCase();
//            this.szgatewayIP = Formatter.formatIpAddress(localDhcpInfo.gateway);
//            String str3 = "";
//            if (i != 0)
//            {
//                String str4 = Formatter.formatIpAddress(i);
//                Cursor localCursor = this.database.rawQuery("SELECT * FROM mac where _mac=?", new String[] { str2 });
//                if (localCursor != null)
//                {
//                    localCursor.moveToNext();
//                    if (localCursor.getCount() > 0)
//                        str3 = localCursor.getString(2);
//                }
//                localCursor.close();
//                HashMap localHashMap2 = new HashMap();
//                localHashMap2.put("image", Integer.valueOf(2130837531));
//                localHashMap2.put("text1", str4);
//                localHashMap2.put("text2", str1);
//                localHashMap2.put("text3", str3);
//                this.list.add(localHashMap2);
//                this.listAdapter.notifyDataSetChanged();
//            }
//            return;
//        }
//        if (getWifiApIpAddress() != null)
//        {
//            this.bWifi = true;
//            this.bHotspot = true;
//            return;
//        }
//        this.bWifi = false;
//        Toast.makeText(getApplicationContext(), "Please Enable Wifi or Phone Hotspot !!!", 1).show();
//        HashMap localHashMap1 = new HashMap();
//        localHashMap1.put("image", Integer.valueOf(2130837531));
//        localHashMap1.put("text1", "0.0.0.0");
//        localHashMap1.put("text2", "00:00:00:00:00:00");
//        localHashMap1.put("text3", " ");
//        this.list.add(localHashMap1);
//        this.listAdapter.notifyDataSetChanged();
//    }
//
//    public static Long ipToInt(String paramString)
//    {
//        String[] arrayOfString = paramString.split("\\.");
//        long l = 0L;
//        for (int i = 0; ; i++)
//        {
//            if (i >= arrayOfString.length)
//                return Long.valueOf(l);
//            int j = i;
//            l = (long) (l + Integer.parseInt(arrayOfString[i]) % 256 * Math.pow(256.0D, j));
//        }
//    }
//
//    public static int[] rangeFromCidr(String paramString)
//    {
//        String[] arrayOfString = paramString.split("/");
//        int i = Integer.parseInt(arrayOfString[1]);
//        int[] arrayOfInt = new int[2];
//        arrayOfInt[0] = (InetRange.ipToInt(arrayOfString[0]) & -2147483648 >> i - 1);
//        arrayOfInt[1] = InetRange.ipToInt(arrayOfString[0]);
//        return arrayOfInt;
//    }
//
//    public String getCurrentIpAddr()
//    {
//        WifiManager localWifiManager = (WifiManager)getSystemService("wifi");
//        new StringBuilder("MAC     = ").append(localWifiManager.getConnectionInfo().getMacAddress()).toString();
//        int i = localWifiManager.getConnectionInfo().getIpAddress();
//        StringBuilder localStringBuilder = new StringBuilder("WiFi IP = ");
//        Object[] arrayOfObject = new Object[4];
//        arrayOfObject[0] = Integer.valueOf(i & 0xFF);
//        arrayOfObject[1] = Integer.valueOf(0xFF & i >> 8);
//        arrayOfObject[2] = Integer.valueOf(0xFF & i >> 16);
//        arrayOfObject[3] = Integer.valueOf(0xFF & i >> 24);
//        return String.format("%d.%d.%d.%d", arrayOfObject);
//    }
//
//    public String getIpAddr()
//    {
//        int i = ((WifiManager)getSystemService("wifi")).getConnectionInfo().getIpAddress();
//        Object[] arrayOfObject = new Object[4];
//        arrayOfObject[0] = Integer.valueOf(i & 0xFF);
//        arrayOfObject[1] = Integer.valueOf(0xFF & i >> 8);
//        arrayOfObject[2] = Integer.valueOf(0xFF & i >> 16);
//        arrayOfObject[3] = Integer.valueOf(255);
//        return String.format("%d.%d.%d.%d", arrayOfObject);
//    }
//
//    public String getWifiApIpAddress()
//    {
//        try
//        {
//            while (true)
//            {
//                Enumeration localEnumeration1 = NetworkInterface.getNetworkInterfaces();
//                while (true)
//                    if (localEnumeration1.hasMoreElements())
//                    {
//                        NetworkInterface localNetworkInterface = (NetworkInterface)localEnumeration1.nextElement();
//                        if (!localNetworkInterface.getName().contains("wlan"))
//                            continue;
//                        Enumeration localEnumeration2 = localNetworkInterface.getInetAddresses();
//                        if (!localEnumeration2.hasMoreElements())
//                            continue;
//                        InetAddress localInetAddress = (InetAddress)localEnumeration2.nextElement();
//                        if ((localInetAddress.isLoopbackAddress()) || (localInetAddress.getAddress().length != 4))
//                            break;
//                        this.realipaddress = ipToInt(localInetAddress.getHostAddress());
//                        HashMap localHashMap = new HashMap();
//                        localHashMap.put("image", Integer.valueOf(2130837531));
//                        localHashMap.put("text1", localInetAddress.getHostAddress());
//                        localHashMap.put("text2", "Hotspot Mode");
//                        this.list.add(localHashMap);
//                        this.listAdapter.notifyDataSetChanged();
//                        String str = localInetAddress.getHostAddress();
//                        return str;
//                    }
//            }
//        }
//        catch (SocketException localSocketException)
//        {
//        }
//        return null;
//    }
//
//    protected void onCreate(Bundle paramBundle)
//    {
//        WifiDeviceInfo();
//    }
//
//    static class InetRange
//    {
//        public static String intToIp(int paramInt)
//        {
//            int i = (0xFF000000 & paramInt) >>> 24;
//            int j = (0xFF0000 & paramInt) >>> 16;
//            int k = (0xFF00 & paramInt) >>> 8;
//            int m = paramInt & 0xFF;
//            return i + '.' + j + '.' + k + '.' + m;
//        }
//
//        public static int ipToInt(String paramString)
//        {
//            try
//            {
//                byte[] arrayOfByte = InetAddress.getByName(paramString).getAddress();
//                int i = (0xFF & arrayOfByte[0]) << 24;
//                int j = (0xFF & arrayOfByte[1]) << 16;
//                int k = (0xFF & arrayOfByte[2]) << 8;
//                int m = arrayOfByte[3];
//                return m & 0xFF | (k | (i | j));
//            }
//            catch (Exception localException)
//            {
//                localException.printStackTrace();
//            }
//            return 0;
//        }
//    }
//
//    private class MyTask extends AsyncTask<Void, Integer, Void>
//    {
//        private boolean bListView = false;
//        private int itest;
//        String szFname;
//        String szGetF;
//        String szGetIP;
//        String szGetMac;
//
//        private MyTask()
//        {
//        }
//
//        protected Void doInBackground(Void[] paramArrayOfVoid)
//        {
//            int[] arrayOfInt = Networkip.rangeFromCidr(Networkip.this.getIpAddr() + "/24");
//            if (Networkip.this.bHotspot)
//            {
//                Object[] arrayOfObject3 = new Object[4];
//                arrayOfObject3[0] = Long.valueOf(0xFF & Networkip.access$6(Networkip.this).longValue());
//                arrayOfObject3[1] = Long.valueOf(0xFF & Networkip.access$6(Networkip.this).longValue() >> 8);
//                arrayOfObject3[2] = Long.valueOf(0xFF & Networkip.access$6(Networkip.this).longValue() >> 16);
//                arrayOfObject3[3] = Integer.valueOf(255);
//                arrayOfInt = Networkip.rangeFromCidr(String.format("%d.%d.%d.%d", arrayOfObject3) + "/24");
//            }
//            this.itest = 0;
//            int i = arrayOfInt[0];
//            String str1;
//            while (true)
//            {
//                if (i > arrayOfInt[1])
//                    return null;
//                if (isCancelled())
//                    return null;
//                Networkip.this.iaddress4 = i;
//                this.szFname = "";
//                str1 = Networkip.InetRange.intToIp(i);
//                Networkip.this.hostIP = str1;
//                try
//                {
//                    InetAddress localInetAddress = InetAddress.getByName(str1);
//                    if (localInetAddress.isReachable(100))
//                    {
//                        System.out.printf("Address %s is reachable\n", new Object[] { localInetAddress });
//                        Object[] arrayOfObject2 = new Object[1];
//                        arrayOfObject2[0] = str1.replace(".", "\\.");
//                        Pattern localPattern2 = Pattern.compile(String.format("^%s\\s+0x1\\s+0x2\\s+([:0-9a-fA-F]+)\\s+\\*\\s+\\w+$", arrayOfObject2));
//                        BufferedReader localBufferedReader2 = new BufferedReader(new FileReader("/proc/net/arp"), 8192);
//                        label296: String str5 = localBufferedReader2.readLine();
//                        if (str5 == null);
//                        while (true)
//                        {
//                            localBufferedReader2.close();
//                            this.itest = (1 + this.itest);
//                            Integer[] arrayOfInteger = new Integer[1];
//                            arrayOfInteger[0] = Integer.valueOf(i);
//                            publishProgress(arrayOfInteger);
//                            i++;
//                            break;
//                            Matcher localMatcher2 = localPattern2.matcher(str5);
//                            if (!localMatcher2.matches())
//                                break label296;
//                            String str6 = localMatcher2.group(1);
//                            System.out.printf("Address %s is reachable reachable reachable\n", new Object[] { str1 });
//                            String str7 = (str6.substring(0, 2) + str6.substring(3, 5) + str6.substring(6, 8)).toUpperCase();
//                            Log.e("HardwareAddress", "else");
//                            Log.e("HardwareAddress", str7);
//                            Cursor localCursor2 = Networkip.this.database.rawQuery("SELECT * FROM mac where _mac=?", new String[] { str7 });
//                            if (localCursor2 == null)
//                                break label573;
//                            localCursor2.moveToNext();
//                            int j = localCursor2.getCount();
//                            if (j > 0)
//                            {
//                                Log.d("TEST database", Integer.toString(j));
//                                this.szFname = localCursor2.getString(2);
//                            }
//                            localCursor2.close();
//                            this.bListView = true;
//                            this.szGetIP = str1;
//                            this.szGetMac = str6;
//                            this.szGetF = this.szFname;
//                        }
//                    }
//                }
//                catch (IOException localIOException1)
//                {
//                    while (true)
//                    {
//                        localIOException1.printStackTrace();
//                        continue;
//                        label573: this.szFname = "Unknown !";
//                    }
//                }
//            }
//            while (true)
//            {
//                try
//                {
//                    Object[] arrayOfObject1 = new Object[1];
//                    arrayOfObject1[0] = str1.replace(".", "\\.");
//                    localPattern1 = Pattern.compile(String.format("^%s\\s+0x1\\s+0x2\\s+([:0-9a-fA-F]+)\\s+\\*\\s+\\w+$", arrayOfObject1));
//                    BufferedReader localBufferedReader1 = new BufferedReader(new FileReader("/proc/net/arp"), 8192);
//                    str2 = localBufferedReader1.readLine();
//                    if (str2 != null)
//                        continue;
//                    localBufferedReader1.close();
//                }
//                catch (UnknownHostException localUnknownHostException)
//                {
//                    Pattern localPattern1;
//                    String str2;
//                    localUnknownHostException.printStackTrace();
//                    break;
//                    Matcher localMatcher1 = localPattern1.matcher(str2);
//                    if (!localMatcher1.matches())
//                        continue;
//                    String str3 = localMatcher1.group(1);
//                    Networkip.this.hostIP = str1;
//                    String str4 = (str3.substring(0, 2) + str3.substring(3, 5) + str3.substring(6, 8)).toUpperCase();
//                    Cursor localCursor1 = Networkip.this.database.rawQuery("SELECT * FROM mac where _mac=?", new String[] { str4 });
//                    if (localCursor1 == null)
//                        break label849;
//                    localCursor1.moveToNext();
//                    if (localCursor1.getCount() <= 0)
//                        continue;
//                    this.szFname = localCursor1.getString(2);
//                    localCursor1.close();
//                    this.bListView = true;
//                    this.szGetIP = str1;
//                    this.szGetMac = str3;
//                    this.szGetF = this.szFname;
//                    continue;
//                }
//                catch (IOException localIOException2)
//                {
//                    localIOException2.printStackTrace();
//                }
//                break;
//                label849: this.szFname = "Unknown !";
//            }
//        }
//
//        protected void onCancelled()
//        {
//            Networkip.this.ScanButton.setEnabled(true);
//            Networkip.this.ScanButton.setText("SEARCH");
//            Networkip.this.searchprogressBar.setProgress(0);
//            Networkip.this.searchprogressBar.setEnabled(false);
//            Networkip.this.searchprogressBar.setVisibility(4);
//            super.onCancelled();
//        }
//
//        protected void onPostExecute(Void paramVoid)
//        {
//            super.onPostExecute(paramVoid);
//            Networkip.this.ScanButton.setEnabled(true);
//            Networkip.this.ScanButton.setText("SEARCH");
//            Networkip.this.searchprogressBar.setProgress(0);
//            Networkip.this.searchprogressBar.setEnabled(false);
//            Networkip.this.searchprogressBar.setVisibility(4);
//            if (Networkip.this.bOnAdLoaded)
//                Networkip.this.displayInterstitial();
//        }
//
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//            Networkip.this.searchprogressBar.setEnabled(true);
//            Networkip.this.searchprogressBar.setProgress(0);
//            Networkip.this.searchprogressBar.setVisibility(0);
//        }
//
//        protected void onProgressUpdate(Integer[] paramArrayOfInteger)
//        {
//            super.onProgressUpdate(paramArrayOfInteger);
//            if (this.bListView)
//            {
//                HashMap localHashMap = new HashMap();
//                localHashMap.put("image", Integer.valueOf(2130837544));
//                localHashMap.put("text1", this.szGetIP);
//                localHashMap.put("text2", this.szGetMac);
//                localHashMap.put("text3", this.szGetF);
//                Networkip.this.list.add(localHashMap);
//                Networkip.this.listAdapter.notifyDataSetChanged();
//                this.bListView = false;
//            }
//            int i = this.itest;
//            Networkip.this.searchprogressBar.setProgress(i);
//            Networkip.this.ScanButton.setText(Networkip.this.hostIP);
//        }
//    }
//
//    class StartButtonListener
//            implements View.OnClickListener
//    {
//        StartButtonListener()
//        {
//        }
//
//        public void onClick(View paramView)
//        {
//            Networkip.this.ScanButton.setEnabled(false);
//            int i = Networkip.this.list.size();
//            do
//            {
//                if (i > 1)
//                {
//                    Networkip.this.list.remove(-1 + Networkip.this.list.size());
//                    Networkip.this.listAdapter.notifyDataSetChanged();
//                }
//                i = Networkip.this.list.size();
//            }
//            while (i > 1);
//            Networkip.this.searchprogressBar.setVisibility(0);
//            Networkip.this.mTask = new Networkip.MyTask(Networkip.this, null);
//            Networkip.this.mTask.execute(new Void[0]);
//        }
//    }
//}
