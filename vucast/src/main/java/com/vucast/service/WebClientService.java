package com.vucast.service;

import android.content.Context;

import com.vucast.callback.IConnectionCallback;
import com.vucast.callback.IPlayCallback;
import com.vucast.constants.Constants;
import com.vucast.constants.DataConstants;
import com.vucast.listeners.WebSocketClientListen;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_6455;

import java.net.URI;

/**
 * Created by user on 28-08-2017.
 */

public class WebClientService {

    private static WebClientService mInstance = new WebClientService();
    private WebSocketClientListen webSocketClientListen;
    private WebClientService(){}

    public static WebClientService getInstance() {
        return mInstance;
    }

    public void startClient (Context context, VideoView videoView, IPlayCallback iPlayCallback, IConnectionCallback iConnectionCallback) {
        startClient(context, videoView, false, iPlayCallback, iConnectionCallback);
    }

    public void reStartClient (Context context, VideoView videoView, IPlayCallback iPlayCallback, IConnectionCallback iConnectionCallback) {
        startClient(context, videoView, true, iPlayCallback, iConnectionCallback);
    }

    public void stopClient () {
        if (webSocketClientListen != null && webSocketClientListen.isOpen()) {
            webSocketClientListen.close();
        }
    }

    private void startClient (Context context, VideoView videoView, boolean recreateConnection, IPlayCallback iPlayCallback, IConnectionCallback iConnectionCallback) {
        if (!recreateConnection) {
            if ((webSocketClientListen != null && webSocketClientListen.isOpen())) {
                iConnectionCallback.connectionStatus(isConnected());
                return;
            }
        }
        stopClient();
        Draft d = new Draft_6455();
        String clientname = "tootallnate/websocket";
        String protocol = "ws";
        String serverlocation = protocol + "://" + DataConstants.IP_ADDRESS + ":" + Constants.SOCKET_PORT;

        URI uri = URI.create(serverlocation + "/updateReports?agent=" + clientname);

        webSocketClientListen = new WebSocketClientListen(uri, d, context, videoView, iPlayCallback, iConnectionCallback);
        Thread t = new Thread(webSocketClientListen);
        t.start();
    }

    public boolean isConnected () {
        if (webSocketClientListen != null) return webSocketClientListen.isConnected();
        return false;
    }

}
