package com.vucast.service;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.vucast.constants.Constants;
import com.vucast.entity.EntityWiFiDetail;
import com.vucast.utility.StringUtils;
import com.vucast.utility.Utility;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by user on 11-09-2017.
 */

public class DetectServer {

    private static final String TAG = DetectServer.class.getCanonicalName();
    DhcpInfo dhcpInfo;
    private ArrayList<String> ipAddresses = new ArrayList<>();
    private static String digitsRegex = "[0-9]+";
    private ArrayList<EntityWiFiDetail> wiFiDetails = new ArrayList<>();

    private static DetectServer mInstance = new DetectServer();

    private DetectServer() {
    }

    public static DetectServer getInstance() {
        return mInstance;
    }

    public void scanDevices(final Context context) {
        Log.wtf(TAG, "IP:- " + Utility.getIp(context));
        dhcpInfo = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getDhcpInfo();
        wiFiDetails.clear();
        ipAddresses.clear();
        Log.wtf(TAG, "**********START***********");
        scanInetAddresses();
        Log.wtf(TAG, "**********END***********");
        int size = ipAddresses.size();
        for (int j = 0; j < size; j++) {
            String ipAddress = ipAddresses.get(j);
            Log.wtf(TAG, "IP: " + ipAddress);
            String requestData = Utility.getRequestData(String.format(Constants.SERVER_PING_URL, ipAddress));
            if (requestData != null) {
                Gson gson = new Gson();
                try {
                    wiFiDetails.add(gson.fromJson(requestData, EntityWiFiDetail.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Log.wtf(TAG, requestData);
        }
    }

    private void scanInetAddresses() {
        final int timeout = 200;
        byte[] ip = new byte[0];
        try {
            ip = InetAddress.getByName(Utility.getIp(dhcpInfo.dns1)).getAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        for (int j = 0; j < 255; j++) {
            ip[3] = (byte) j;
            try {
                InetAddress checkAddress = InetAddress.getByName(Utility.getIp(getByteToIp(ip)));
                /*Log.wtf(TAG, "scanInetAddresses IP - " + checkAddress.getCanonicalHostName());
                if (!StringUtils.isEmpty(checkAddress.getCanonicalHostName()) && !Character.isDigit(checkAddress.getCanonicalHostName().charAt(0))) {
                    Log.wtf(TAG, "Connected IP - " + checkAddress.getCanonicalHostName() + " : " + checkAddress.getHostAddress());
                    inetAddresses.add(checkAddress);
                    continue;
                } else {
                    Log.wtf(TAG, "scanInetAddresses IP - " + checkAddress.getCanonicalHostName());
                }*/
                String hostAddress = checkAddress.getHostAddress();
                Log.wtf(TAG, "IP:- scanInetAddresses " + hostAddress);
                if (checkAddress.isReachable(timeout)) {
                    Log.wtf(TAG, "Connected IP - " + hostAddress);
                    ipAddresses.add(hostAddress);
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
                Log.wtf(TAG, "IP:- scanInetAddresses " + e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.wtf(TAG, "IP:- scanInetAddresses " + e.getMessage());
            }
        }
    }

    public ArrayList<EntityWiFiDetail> getWiFiDetails() {
        return wiFiDetails;
    }

//    public static Long ipToInt(String paramString) {
//        String[] arrayOfString = paramString.split("\\.");
//        long l = 0L;
//        for (int i = 0; ; i++) {
//            if (i >= arrayOfString.length)
//                return Long.valueOf(l);
//            int j = i;
//            l = (long) (l + Integer.parseInt(arrayOfString[i]) % 256 * Math.pow(256.0D, j));
//        }
//    }

    public static int[] rangeFromCidr(String paramString) {
        String[] arrayOfString = paramString.split("/");
        int i = Integer.parseInt(arrayOfString[1]);
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = (ipToInt(arrayOfString[0]) & -2147483648 >> i - 1);
        arrayOfInt[1] = ipToInt(arrayOfString[0]);
        return arrayOfInt;
    }

    public static int ipToInt(String paramString) {
        try {
            byte[] arrayOfByte = InetAddress.getByName(paramString).getAddress();
            int i = (0xFF & arrayOfByte[0]) << 24;
            int j = (0xFF & arrayOfByte[1]) << 16;
            int k = (0xFF & arrayOfByte[2]) << 8;
            int m = arrayOfByte[3];
            return m & 0xFF | (k | (i | j));
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return 0;
    }

    private static int getByteToIp (byte[] arrayOfByte) {
        int i = (0xFF & arrayOfByte[3]) << 24;
        int j = (0xFF & arrayOfByte[2]) << 16;
        int k = (0xFF & arrayOfByte[1]) << 8;
        int m = arrayOfByte[0];
        return m & 0xFF | (k | (i | j));
    }

}