package com.vucast.service;

import android.content.Context;
import android.widget.Switch;

import com.google.gson.Gson;
import com.vucast.constants.Constants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.listeners.MyHTTPD;
import com.vucast.listeners.WebSocketServerSend;
import com.vucast.utility.Utility;

import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.drafts.Draft_6455;

import static com.vucast.constants.Constants.CLIENTS;
import static com.vucast.constants.Constants.SOCKET_AUDIO_PLAY;
import static com.vucast.constants.Constants.SOCKET_PHOTO;
import static com.vucast.constants.Constants.SOCKET_VIDEO_PLAY;

/**
 * Created by user on 29-08-2017.
 */

public class WebServerService {

    private static WebServerService mInstance = new WebServerService();
    private MyHTTPD server = null;
    WebSocketServerSend webSocketServerSend = null;
    private WebServerService(){}
    public static WebServerService getInstance () { return mInstance; }

    private void startServer  (Context context, boolean recreateConnection, Switch switchControls) {
        if (!recreateConnection) {
            if ((server != null && server.isAlive())) {
                return;
            }
        }
        stopServer();
        server = new MyHTTPD(Constants.SERVER_PORT, context);
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        WebSocketImpl.DEBUG = false;
        try {
            webSocketServerSend = new WebSocketServerSend(Constants.SOCKET_PORT, new Draft_6455(), switchControls);
        } catch (Exception e) {
            e.printStackTrace();
        }
        webSocketServerSend.setConnectionLostTimeout(0);
        webSocketServerSend.start();
    }

    public void startServer (Context context, Switch switchControls) {
        startServer(context, false, switchControls);
    }

    public void restartServer (Context context, Switch switchControls) {
        startServer(context, true, switchControls);
    }

    public void stopServer () {
        if (server != null && server.isAlive()) {
            server.stop();
        }
        if (webSocketServerSend != null) {
            try {
                webSocketServerSend.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void messageVolumeDown (Context context) {
        for (int i = 0; i < Constants.CLIENTS.size(); i++) {
            Constants.CLIENTS.get(i).send(Constants.SOCKET_MESSAGE_VOLUME_DOWN);
        }
    }

    public void messageVolumeUp (Context context) {
        for (int i = 0; i < Constants.CLIENTS.size(); i++) {
            Constants.CLIENTS.get(i).send(Constants.SOCKET_MESSAGE_VOLUME_UP);
        }
    }

    public void messagePause () {
        for (int i = 0; i < Constants.CLIENTS.size(); i++) {
            Constants.CLIENTS.get(i).send(Constants.SOCKET_MESSAGE_PAUSE);
        }
    }

    public void messagePlay() {
        for (int i = 0; i < Constants.CLIENTS.size(); i++) {
            Constants.CLIENTS.get(i).send(Constants.SOCKET_MESSAGE_PLAY);
        }
    }

    public void messageControlsVisibility (boolean b) {
        for (int i = 0; i < Constants.CLIENTS.size(); i++) {
            messageParticularControlsVisibility(Constants.CLIENTS.get(i), b);
        }
    }

    public void messageParticularControlsVisibility (WebSocket webSocket, boolean b) {
        webSocket.send(Constants.SOCKET_MESSAGE_CONTROLS + b);
    }

    public void messagePlayExactContent (EntityMediaDetail entityMediaDetail) {
        if (Constants.TYPE_MUSIC.equalsIgnoreCase(entityMediaDetail.getType())) {
            EntityMediaDetail entityMediaDetailMusic = new EntityMediaDetail();
            entityMediaDetailMusic.setPath(Constants.VIDEO_URL + entityMediaDetail.getPath().replace(" ", "+"));
            entityMediaDetailMusic.setAudioId(entityMediaDetail.getAudioId());
            entityMediaDetailMusic.setType(Constants.TYPE_MUSIC);
            String json = new Gson().toJson(entityMediaDetailMusic, EntityMediaDetail.class);
            for (int i = 0; i < CLIENTS.size(); i++) {
                CLIENTS.get(i).send(SOCKET_AUDIO_PLAY + json);
            }
        } else if (Constants.TYPE_PHOTO.equalsIgnoreCase(entityMediaDetail.getType())) {
            EntityMediaDetail entityMediaDetailMusic = new EntityMediaDetail();
            entityMediaDetailMusic.setPath(Constants.PHOTO_IMAGE_URL + entityMediaDetail.getPath().replace(" ", "+"));
            entityMediaDetailMusic.setAudioId(entityMediaDetail.getAudioId());
            entityMediaDetailMusic.setType(Constants.TYPE_PHOTO);
            String json = new Gson().toJson(entityMediaDetailMusic, EntityMediaDetail.class);
            for (int i = 0; i < CLIENTS.size(); i++) {
                CLIENTS.get(i).send(SOCKET_PHOTO + json);
            }
        } else if (!Constants.TYPE_MUSIC.equalsIgnoreCase(entityMediaDetail.getType())) {
            for (int i = 0; i < CLIENTS.size(); i++) {
                CLIENTS.get(i).send(SOCKET_VIDEO_PLAY + Constants.VIDEO_URL + entityMediaDetail.getPath().replace(" ", "+"));
            }
        }
    }

}
