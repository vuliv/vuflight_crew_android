package com.vucast.service;

import com.vucast.entity.EntityMediaDetail;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by user on 29-08-2017.
 */

public class DataService {


    private static DataService mInstance = new DataService();
    private ArrayList<EntityMediaDetail> mediaDetailList = new ArrayList<EntityMediaDetail>();
    private TreeMap<String, ArrayList<EntityMediaDetail>> folderTreeMap = new TreeMap<>();
    private DataService(){}

    public static DataService getInstance() {
        return mInstance;
    }

    public void setContent (ArrayList<EntityMediaDetail> mediaDetailList) {
        this.mediaDetailList.clear();
        this.mediaDetailList.addAll(mediaDetailList);
    }

    public void setFolderContent (TreeMap<String, ArrayList<EntityMediaDetail>> mediaDetailList) {
        folderTreeMap = mediaDetailList;
    }

    public ArrayList<EntityMediaDetail> getContent () {
        mediaDetailList.clear();
        for (TreeMap.Entry<String, ArrayList<EntityMediaDetail>> entry : getFolderTreeMap().entrySet()) {
            mediaDetailList.addAll(entry.getValue());
        }
        return mediaDetailList;
    }

    public TreeMap<String, ArrayList<EntityMediaDetail>> getFolderTreeMap() {
        return folderTreeMap;
    }
}
