package com.vucast.callback;

/**
 * Created by user on 04-09-2017.
 */

public interface IConnectionCallback {

    void connectionStatus (boolean isConnected);

}
