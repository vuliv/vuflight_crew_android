package com.vucast.callback;

/**
 * Created by user on 31-08-2017.
 */

public interface IClickCallback {

    void click(Object object);

}
