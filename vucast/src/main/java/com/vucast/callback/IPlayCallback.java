package com.vucast.callback;

import com.vucast.entity.EntityMediaDetail;

/**
 * Created by user on 31-08-2017.
 */

public interface IPlayCallback {

    void playVideo (String url, String thumbnailUrl, EntityMediaDetail entityMediaDetail);

}
