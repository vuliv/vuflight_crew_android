package com.vucast.utility;

import com.vuliv.network.entity.EntityDominosRequest;

public interface OrderReceiver {
    void onOrderReceived(EntityDominosRequest entityDominosRequest,boolean isFromNotify);
}
