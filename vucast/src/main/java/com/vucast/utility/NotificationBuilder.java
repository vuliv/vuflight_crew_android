package com.vucast.utility;

public interface NotificationBuilder {
    void notification(String group, String name, String message,int crewId, String condition);
}
