package com.vucast.utility;

/**
 * Created by user on 30-08-2017.
 */

public class StringUtils {

    /**
     * Checks whether the passed string is having some text or just a null string.
     *
     * @param text
     * @return
     */
    public static boolean isEmpty(String text) {
        boolean isEmpty = true;
        if (text != null && text.trim().length() > 0 && !text.trim().equalsIgnoreCase("null")) {
            isEmpty = false;
        }
        return isEmpty;
    }
}
