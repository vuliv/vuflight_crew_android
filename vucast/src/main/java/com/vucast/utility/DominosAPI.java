package com.vucast.utility;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.TheAplication;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.vucast.R;
import com.vucast.constants.Constants;

import vuflight.com.vuliv.chatlib.controller.ChatMessageController;
import vuflight.com.vuliv.chatlib.entity.EntityCrewChatMessage;
import vuflight.com.vuliv.chatlib.entity.EntityCrewChatStatus;

import com.vuliv.network.entity.EntityDataResponse;
import com.vuliv.network.entity.EntityDominosRequest;
import com.vuliv.network.server.DataController;
import com.vuliv.network.server.TrackingController;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by user on 12-01-2018.
 */

public class DominosAPI {
    private static final String TAG = DominosAPI.class.getSimpleName();
    private static DominosAPI instance = null;
    private static Map<String, EntityDominosRequest> orderMap = new LinkedHashMap<String, EntityDominosRequest>();
    public static Map<String, EntityDominosRequest> orderIdMap = new LinkedHashMap<String, EntityDominosRequest>();
    public static ArrayList<String> orderList = new ArrayList<>();

    public static ArrayList<EntityDominosRequest.Data> clickedData;

    public static ServiceRequestObserver serviceRequestObserver = null;
    public static OrderReceiver orderReceiver = null;
    public static NotificationBuilder notificationBuilder = null;
    public static boolean IS_SERVICE_REQUEST_ACTIVITY_VISIBLE = false;
    public static boolean IS_ORDER_FRAGMENT_VISIBLE = false;

    public static class TempPrefrenceForChatAndServices {
        public static TempPrefrenceForChatAndServices tempPrefrenceForChatAndServices;
        public static final String IS_CREWCHAT_ACTIVITY_VISIBLE = "iscrewchatactivityvisible";
        public static final String IS_CREWCHAT_ACTIVITY_VISIBLE_CREW_NAME = "iscrewchatactivityvisiblecrewname";
        public static final String CREW_MESSAGES_MAP = "cerwmessagesmap";
        private SharedPreferences pref;

        private TempPrefrenceForChatAndServices() {
            pref = TheAplication.getInstance().getSharedPreferences("Chat_Prefrence", TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
        }

        public static TempPrefrenceForChatAndServices getInstance() {

            if (tempPrefrenceForChatAndServices == null) {
                synchronized (TempPrefrenceForChatAndServices.class) {
                    if (tempPrefrenceForChatAndServices == null) {
                        tempPrefrenceForChatAndServices = new TempPrefrenceForChatAndServices();
                    }
                }
            }
            return tempPrefrenceForChatAndServices;
        }

        public void setCrewChatActivityVisible(boolean visible) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(IS_CREWCHAT_ACTIVITY_VISIBLE, visible);
            editor.commit();
        }

        public boolean getCrewChatActivityVisible() {

            return pref.getBoolean(IS_CREWCHAT_ACTIVITY_VISIBLE, false);
        }

        public void setIsCrewchatActivityVisibleCrewName(String crewName) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(IS_CREWCHAT_ACTIVITY_VISIBLE_CREW_NAME, crewName);
            editor.commit();
        }

        public String getIsCrewchatActivityVisibleCrewName() {
            return pref.getString(IS_CREWCHAT_ACTIVITY_VISIBLE_CREW_NAME, null);
        }

        public void clearPreferance() {
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
        }

        public void saveCrewMessagesMap(HashMap<String, ArrayList<EntityCrewChatMessage>> crewChatMessages) {
            if (pref != null) {
                Gson gson = new Gson();
                String jsonString = gson.toJson(crewChatMessages);
                SharedPreferences.Editor editor = pref.edit();
                editor.remove(CREW_MESSAGES_MAP).commit();
                editor.putString(CREW_MESSAGES_MAP, jsonString);
                editor.commit();
            }
        }

        public HashMap<String, ArrayList<EntityCrewChatMessage>> loadCrewMessagesMap() {
            HashMap<String, ArrayList<EntityCrewChatMessage>> outputMap = new HashMap<String, ArrayList<EntityCrewChatMessage>>();
            try {
                if (pref != null) {
                    String jsonString = pref.getString(CREW_MESSAGES_MAP, (new JSONObject()).toString());
                    JSONObject jsonObject = new JSONObject(jsonString);
                    Iterator<String> keysItr = jsonObject.keys();
                    while (keysItr.hasNext()) {
                        String key = keysItr.next();
                        Gson gson = new Gson();
                        Type arrayListType = new TypeToken<ArrayList<EntityCrewChatMessage>>() {
                        }.getType();
                        ArrayList<EntityCrewChatMessage> entityCrewChatMessages = gson.fromJson(jsonObject.getString(key), arrayListType);
                        outputMap.put(key, entityCrewChatMessages);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return outputMap;
        }
    }

    public static DominosAPI getInstance() {
        if (instance == null) {
            synchronized (DominosAPI.class) {
                if (instance == null) {
                    instance = new DominosAPI();
                }
            }
        }
        return instance;
    }

    private DominosAPI() {
    }

    /**
     * This is used to fetch Chat Message from Server and display at Crew End
     *
     * @param queryParameterString
     * @return
     */
    private String showCrewChat(String queryParameterString) {
        EntityCrewChatMessage entityCrewChat = null;
        EntityCrewChatStatus entityCrewChatStatus = new EntityCrewChatStatus();
        String[] split = queryParameterString.split("=");
        try {
            String result = java.net.URLDecoder.decode(split[1], "UTF-8");
            entityCrewChat = new Gson().fromJson(result, EntityCrewChatMessage.class);

            final EntityCrewChatMessage entityCrewChatMessage1 = entityCrewChat;

            if (!TempPrefrenceForChatAndServices.getInstance().getCrewChatActivityVisible()) {
                entityCrewChatMessage1.setOtherMsg(false);
                ArrayList<EntityCrewChatMessage> entityCrewChatMessages = new ArrayList<>();
                HashMap<String, ArrayList<EntityCrewChatMessage>> integerArrayListHashMap = TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap();
                if (integerArrayListHashMap.containsKey(entityCrewChatMessage1.getCrewName())) {
                    entityCrewChatMessages = integerArrayListHashMap.get(entityCrewChatMessage1.getCrewName());
                } else {
                    entityCrewChatMessages = new ArrayList<>();
                }
                if (entityCrewChatMessages != null) {
                    entityCrewChatMessages.add(entityCrewChatMessage1);
                } else {
                    entityCrewChatMessages = new ArrayList<>();
                    entityCrewChatMessages.add(entityCrewChatMessage1);
                }
                integerArrayListHashMap.put(entityCrewChatMessage1.getCrewName(), entityCrewChatMessages);
                TempPrefrenceForChatAndServices.getInstance().saveCrewMessagesMap(integerArrayListHashMap);

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (notificationBuilder != null) {
                            notificationBuilder.notification("Crew_Chat", entityCrewChatMessage1.getCrewName(), entityCrewChatMessage1.getMessage(), entityCrewChatMessage1.getCrewId(), "ChatActivity");
                        }
                    }
                });


            }
            if (TempPrefrenceForChatAndServices.getInstance().getCrewChatActivityVisible()) {
                if (!TempPrefrenceForChatAndServices.getInstance().getIsCrewchatActivityVisibleCrewName().equals(entityCrewChatMessage1.getCrewName())) {
                    entityCrewChatMessage1.setOtherMsg(false);
                    ArrayList<EntityCrewChatMessage> entityCrewChatMessages = new ArrayList<>();
                    HashMap<String, ArrayList<EntityCrewChatMessage>> integerArrayListHashMap = TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap();
                    if (integerArrayListHashMap.containsKey(entityCrewChatMessage1.getCrewName())) {
                        entityCrewChatMessages = integerArrayListHashMap.get(entityCrewChatMessage1.getCrewName());
                    } else {
                        entityCrewChatMessages = new ArrayList<>();
                    }
                    if (entityCrewChatMessages != null) {
                        entityCrewChatMessages.add(entityCrewChatMessage1);
                    } else {
                        entityCrewChatMessages = new ArrayList<>();
                        entityCrewChatMessages.add(entityCrewChatMessage1);
                    }
                    integerArrayListHashMap.put(entityCrewChatMessage1.getCrewName(), entityCrewChatMessages);
                    TempPrefrenceForChatAndServices.getInstance().saveCrewMessagesMap(integerArrayListHashMap);

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (notificationBuilder != null) {
                                notificationBuilder.notification("Crew_Chat", entityCrewChatMessage1.getCrewName(), entityCrewChatMessage1.getMessage(), entityCrewChatMessage1.getCrewId(), "ChatActivity");
                            }
                        }
                    });

                } else {
                    ChatMessageController.getInstance().setChatMessage(entityCrewChat);
                }
            }
            entityCrewChatStatus.setHasReceived(true);
        } catch (Exception e) {
            e.printStackTrace();
            entityCrewChatStatus.setHasReceived(false);
        }
        return new Gson().toJson(entityCrewChatStatus, EntityCrewChatStatus.class);
    }

    /**
     * This is used to fetch passenger service request from Server to crew and will displays notification at Crew End
     *
     * @param queryParameterString
     * @return
     */
    private String serviceRequestFromServer(String queryParameterString) {
        ServiceRequestModel serviceRequestModel = null;
        Boolean isReceived = false;
        String[] split = queryParameterString.split("=");
        try {
            String result = java.net.URLDecoder.decode(split[1], "UTF-8");
            serviceRequestModel = new Gson().fromJson(result, ServiceRequestModel.class);
            if (IS_SERVICE_REQUEST_ACTIVITY_VISIBLE == true && serviceRequestObserver != null) {
                serviceRequestObserver.onRequestReceived(new Gson().toJson(serviceRequestModel));
            } else {
                if (notificationBuilder != null) {
                    notificationBuilder.notification("Service_Request", "Seat No. " + serviceRequestModel.getMsisdn(), serviceRequestModel.getServiceRequest(), 0, "ServiceRequest");
                }
            }
            if (serviceRequestObserver != null) {
                serviceRequestObserver.onRequestNotify(1);
            }
            isReceived = true;
        } catch (Exception e) {
            e.printStackTrace();
            isReceived = false;
        }
        return new Gson().toJson(isReceived, Boolean.class);
    }

    /**
     * This is used to fetch passenger order from Server to crew and will displays notification at Crew End
     *
     * @param queryParameterString
     * @return
     */
    private String orderByPassenger(String queryParameterString) {
        EntityDominosRequest entityDominosRequest = null;
        Boolean isReceived = false;
        String[] split = queryParameterString.split("=");
        try {
            String result = java.net.URLDecoder.decode(split[1], "UTF-8");
            entityDominosRequest = new Gson().fromJson(result, EntityDominosRequest.class);
            if (IS_ORDER_FRAGMENT_VISIBLE == true && orderReceiver != null) {
                orderReceiver.onOrderReceived(entityDominosRequest,true);
            } else {
                if (notificationBuilder != null) {
                    notificationBuilder.notification("Passenger_Order", "Seat No. " + entityDominosRequest.getMsisdn(), "Booked some orders", 0, "OrdersByPassenger");
                }
            }
            isReceived = true;
        } catch (Exception e) {
            e.printStackTrace();
            isReceived = false;
        }
        return new Gson().toJson(isReceived, Boolean.class);
    }

    public String process(String contentUri, String queryParameterString) {
        if (contentUri.equalsIgnoreCase("/vuflight/crewChat")) {
            return showCrewChat(queryParameterString);  // todo Sumit Agrawal has added this line
        } else if (contentUri.equalsIgnoreCase("/vuflight/notifyRequestServices")) {
            return serviceRequestFromServer(queryParameterString);  // todo Pankaj Sharma has added this line
        } else if (contentUri.equalsIgnoreCase("/vuflight/notifyOrderByPassengers")) {
            return orderByPassenger(queryParameterString);  // todo Pankaj Sharma has added this line
        } else if (contentUri.equalsIgnoreCase("/vuflight/feed")) {
            return getPizzas();
        } else if (contentUri.equalsIgnoreCase("/vuflight/product")) {
            return getProduct();
        } else if (contentUri.equalsIgnoreCase("/vuflight/getotp")) {
            EntityDominosRequest entityDominosRequest = null;
            String[] split = queryParameterString.split("=");
            try {
                String result = java.net.URLDecoder.decode(split[1], "UTF-8");
                entityDominosRequest = new Gson().fromJson(result, EntityDominosRequest.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (entityDominosRequest != null) {
                return getOtp(entityDominosRequest);
            }
        } else if (contentUri.equalsIgnoreCase("/vuflight/verifyotp")) {
            EntityDominosRequest entityDominosRequest = null;
            String[] split = queryParameterString.split("=");
            try {
                String result = java.net.URLDecoder.decode(split[1], "UTF-8");
                entityDominosRequest = new Gson().fromJson(result, EntityDominosRequest.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (entityDominosRequest != null) {
                return verifyOTP(entityDominosRequest);
            }
        } else if (contentUri.equalsIgnoreCase("/vuflight/orderservice")) {
            EntityDominosRequest entityDominosRequest = null;
            String[] split = queryParameterString.split("=");
            try {
                String result = java.net.URLDecoder.decode(split[1], "UTF-8");
                entityDominosRequest = new Gson().fromJson(result, EntityDominosRequest.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (entityDominosRequest != null) {
                return serviceRequestOTP(entityDominosRequest);
            }
        } else if (contentUri.equalsIgnoreCase("/vuflight/confirm")) {
            EntityDominosRequest entityDominosRequest = null;
            String[] split = queryParameterString.split("=");
            try {
                String result = java.net.URLDecoder.decode(split[1], "UTF-8");
                entityDominosRequest = new Gson().fromJson(result, EntityDominosRequest.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (entityDominosRequest != null) {
                return confirmOTP(entityDominosRequest);
            }
        }
        return "{\"status\":\"101\"}";
    }

    public String getPizzas() {
        return new Gson().toJson(DataController.getInstance().getDataResponse(), EntityDataResponse.class);
    }

    public String getProduct() {
        return new Gson().toJson(DataController.getInstance().getProductResponse(), EntityDataResponse.class);
    }

    public String getOtp(EntityDominosRequest request) {
        /*String msisdn = request.getMsisdn();
        request.setTimeStamp(System.currentTimeMillis());
        int price = 0;
        for (EntityDominosRequest.Data data : request.getData()) {
            for (EntityDataResponse.Dominos dominos : DataController.getInstance().getDataResponse().getInformation()) {
                if (data.getId().equalsIgnoreCase(dominos.getId())) {
                    data.setImage(dominos.getImage());
                    data.setVeg(dominos.isVeg());
                    for (EntityDataResponse.SizeAndPrice sizeAndPrice : dominos.getSizes()) {
                        if (data.getSize().equalsIgnoreCase(sizeAndPrice.getSize())) {
                            data.setPrice(data.getQuantity() * sizeAndPrice.getPrice());
                        }
                    }
                    data.setName(dominos.getName());
                    price += data.getPrice();
                }
            }
        }
        request.setPrice(price);
        OrderViewModel orderViewModel = new OrderViewModel(TheAplication.instance);
        Gson gsonObj = new Gson();
        // converts object to json string
        String jsonStr = gsonObj.toJson(request,EntityDominosRequest.class);
        Order order = new Order();
        order.setOrderJson(jsonStr);
        order.setPhoneNumber(msisdn);
        order.setTime(request.getTimeStamp());
        orderViewModel.insertorUpdateOrder(order);
        orderMap.put(msisdn, request);

        Intent i = new Intent();
        i.setAction(Constants.UPDATE_UI);
        LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(i);
        while(!orderMap.get(msisdn).isOtpSent());*/

        return "{\"status\":\"200\"}";
    }

    public String verifyOTP(EntityDominosRequest request) {
        /*OrderViewModel orderViewModel = new OrderViewModel(TheAplication.instance);
        String msisdn = request.getMsisdn();
        String otp = request.getOtp();
        Order order = orderViewModel.getOrder(msisdn);
        if (!StringUtils.isEmpty(otp)) {
            if (order != null) {
                String jsonStr = order.getOrderJson();
                Gson gsonObj = new Gson();
                EntityDominosRequest entity = gsonObj.fromJson(jsonStr,EntityDominosRequest.class);
                entity.setOtp(otp);
                order.setOrderJson(gsonObj.toJson(entity,EntityDominosRequest.class));
                orderViewModel.insertorUpdateOrder(order);
                Intent i = new Intent();
                i.setAction(Constants.UPDATE_UI);
                LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(i);
                while(StringUtils.isEmpty(orderMap.get(msisdn).getOtpVerified()));
                String response = null;
                if ("YES".equalsIgnoreCase(orderMap.get(msisdn).getOtpVerified())) {
                    ++orderId;
                    orderMap.get(msisdn).setOrderId(String.valueOf(orderId));
                    entity.setOrderId(String.valueOf(orderId));

                    response = "{\"status\":\"200\", \"orderID\":\"" + String.valueOf(orderId) + "\"}";
                } else {
                    response = "{\"status\":\"101\"}";
                }
                String updatedJson = gsonObj.toJson(entity,EntityDominosRequest.class);
                order.setOrderJson(updatedJson);
                orderViewModel.insertorUpdateOrder(order);
                Intent intent = new Intent();
                intent.setAction(Constants.UPDATE_UI);
                LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(intent);
                return response;
            }
        }*/
        return "{\"status\":\"101\"}";
    }

    public String confirmOTP(EntityDominosRequest request) {
        for (EntityDominosRequest.Data data : request.getData()) {
            request = instance.addRequestData(data, request, true);
        }
        instance.addRequest(request.getMsisdn(), request);

        Intent intent = new Intent();
        intent.setAction(Constants.UPDATE_ORDERED_ITEM);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.UPDATED_REQUEST_ITEM, request);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(intent);
//        OrderViewModel orderViewModel = new OrderViewModel(TheAplication.instance);
//        String msisdn = request.getMsisdn();
//        String otp = request.getOtp();
//        Order order = orderViewModel.getOrder(msisdn);
//        if (!StringUtils.isEmpty(otp)) {
//            if (order != null) {
//                String jsonStr = order.getOrderJson();
//                Gson gsonObj = new Gson();
//                EntityDominosRequest entity = gsonObj.fromJson(jsonStr,EntityDominosRequest.class);
//                entity.setOtp(otp);
//                order.setOrderJson(gsonObj.toJson(entity,EntityDominosRequest.class));
//                orderViewModel.insertorUpdateOrder(order);
//                Intent i = new Intent();
//                i.setAction(Constants.UPDATE_UI);
//                LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(i);
//                while(StringUtils.isEmpty(orderMap.get(msisdn).getOtpVerified()));
//                String response = null;
//                if ("YES".equalsIgnoreCase(orderMap.get(msisdn).getOtpVerified())) {
//                    ++orderId;
//                    orderMap.get(msisdn).setOrderId(String.valueOf(orderId));
//                    entity.setOrderId(String.valueOf(orderId));
//
//                    response = "{\"status\":\"200\", \"orderID\":\"" + String.valueOf(orderId) + "\"}";
//                } else {
//                    response = "{\"status\":\"101\"}";
//                }
//                String updatedJson = gsonObj.toJson(entity,EntityDominosRequest.class);
//                order.setOrderJson(updatedJson);
//                orderViewModel.insertorUpdateOrder(order);
//                Intent intent = new Intent();
//                intent.setAction(Constants.UPDATE_UI);
//                LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(intent);
//                return response;
//            }
//        }
        /*intent.setAction(Constants.UPDATE_UI);
        LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(intent);*/
        return "{\"status\":\"200\", \"orderID\":\"" + "\"}";
    }

    public String serviceRequestOTP(EntityDominosRequest request) {
        orderMap.put(request.getMsisdn() + "" + request.getServiceRequest(), request);
        Intent intent = new Intent();
        intent.setAction(Constants.UPDATE_UI);
        LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(intent);
        return "{\"status\":\"200\"}";
    }

    public void removeRequest(EntityDominosRequest request) {
        String removalKey = null;
        for (Map.Entry<String, EntityDominosRequest> entry : orderMap.entrySet()) {
            if (request.equals(entry.getValue())) {
                removalKey = entry.getKey();
                break;
            }
        }

        if (removalKey != null) {
            orderMap.remove(removalKey);
        }
    }

    public void addRequest(String seatNumber, EntityDominosRequest request) {
//      following code should bex executed for new request
        if (!request.isOtpSent()) {
            request.setOtpSent(true);
            request.setMsisdn(seatNumber);
            request.setKey(request.getMsisdn() + "" + request.getOrderId());
            instance.orderMap.put(request.getKey(), request);
            request.setOtpVerified("YES");
        }

        for (EntityDominosRequest.Data data : request.getData()) {
            request.setPrice(request.getPrice() + data.getPrice());
        }

        TrackingController.getInstance().startPurchaseTracking(TheAplication.instance, request);
    }

    public EntityDominosRequest addRequestData(EntityDominosRequest.Data requestData, EntityDominosRequest request, boolean check) {
        if (request == null) {
            request = new EntityDominosRequest();
        }

        ArrayList<EntityDominosRequest.Data> listOfData = request.getData();
        if (listOfData == null) {
            listOfData = new ArrayList<>();
            request.setData(listOfData);
        }

        if (check) {
            listOfData.add(requestData);
        }
        return request;
    }

    public Set<String> getOrderMapKeySet() {
        return orderMap.keySet();
    }

    public EntityDominosRequest getRequest(String key) {
        return orderMap.get(key);
    }

    public void addValueInOrderMap(String key, EntityDominosRequest request) {
        orderMap.put(key, request);
    }

    public Collection<EntityDominosRequest> getOrderMapValues() {
        return orderMap.values();
    }

    public void removeDataFromMap(String key) {
        orderMap.remove(key);
    }

    public boolean checkMapIsEmpty() {
        return orderMap.isEmpty();
    }

    public void clearOrders() {
        orderMap.clear();
    }
}

class ServiceRequestModel {

    @SerializedName("serviceId")
    String serviceId;

    @SerializedName("msisdn")
    String msisdn;

    @SerializedName("serviceType")
    String serviceRequest;

    @SerializedName("hasAccepted")
    boolean hasAccepted;


    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(String serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public boolean isHasAccepted() {
        return hasAccepted;
    }

    public void setHasAccepted(boolean hasAccepted) {
        this.hasAccepted = hasAccepted;
    }


}

