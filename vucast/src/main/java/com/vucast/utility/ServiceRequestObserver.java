package com.vucast.utility;

public interface ServiceRequestObserver {
    void onRequestReceived(String serviceRequestModel);
    void onRequestNotify(int notify);
}
