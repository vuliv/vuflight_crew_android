package com.vucast.utility;

import android.content.Context;

import com.vucast.constants.Constants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.service.DataService;
import com.vuliv.network.database.FolderItemsTableOperations;
import com.vuliv.network.database.tables.EntityTableFolderItems;
import com.vuliv.network.server.DataController;
import com.vuliv.network.utility.*;
import com.vuliv.network.utility.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by user on 21-09-2017.
 */

public class WebPageCreater {

    private static WebPageCreater mInstance = new WebPageCreater();
    private WebPageCreater(){}

    public static WebPageCreater getInstance () {return mInstance;}

    public synchronized String getWebPage (Context context, String platform, String userAgent) {
        StringBuilder stringBuilder = new StringBuilder();
        LinkedHashMap<String, List<EntityTableFolderItems>> folderTreeMap = DataController.getInstance().getData(context, platform);
        int count = 2;
        if (!StringUtils.isEmpty(userAgent) && userAgent.matches(".*Android.*")) {
            if (EntityTableFolderItems.PLATFORM_WEB.equalsIgnoreCase(platform)) {
                stringBuilder.append("<div class=\"row\">\n" +
                        "\t\t\t<div class=\"col s12 m6 offset-m3\">\n" +
                        "\t\t\t\t<div class=\"card\">\n" +
                        "\t\t\t\t\t<div class=\"card-image\">\n" +
                        "\t\t\t\t\t\t<a href=\"VuScreen.apk\"><img src=\"images/banner.jpg\"></a>\n" +
                        "\t\t\t\t\t</div>\n" +
                        "\t\t\t\t</div>\n" +
                        "\t\t\t</div>\n" +
                        "\t\t</div>");
            }
        }
        for (Map.Entry<String, List<EntityTableFolderItems>> entry : folderTreeMap.entrySet()) {
//            count++;
            if (entry.getValue() != null && entry.getValue().size() > 0) {
                stringBuilder.append("<div class=\"section\">");
                stringBuilder.append("\n\t<div class=\"tray-heading tray-color-Carrot\"><h2>");
                stringBuilder.append(entry.getKey());
                stringBuilder.append("</h2></div>");
                stringBuilder.append("\n<div class=\"row card\">");
                stringBuilder.append("\n\t\t<div class=\"carouselA js-flickity\" data-flickity-options='{\"freeScroll\": false, \"prevNextButtons\": false, \"pageDots\": true,\"contain\": true,\"groupCells\": true,\"dragThreshold\": 200, \"imagesLoaded\": true, \"autoPlay\": false, \"wrapAround\": true}'>");

                for (EntityTableFolderItems entityMediaDetail : entry.getValue()) {
                    stringBuilder.append("\n\t\t\t<div class=\"card-image carouselA-cell\">");
                    stringBuilder.append("\n\t\t\t <div class=\"carouselA-cell-image\">");
                    stringBuilder.append(String.format("\n\t\t\t\t<a href=\"/player.html?f=%s&id=%s\"><img src=\"", entityMediaDetail.getFolderId(), entityMediaDetail.getItemId()));
                    stringBuilder.append(Utility.getVideoThumbnailUrl(String.valueOf(entityMediaDetail.getFolderId()), entityMediaDetail.getItemId()));
                    stringBuilder.append("\"/>");
                    stringBuilder.append(String.format("<div class=\"cardicon\"><img src=\"%s\" class=\"\" alt=\"\" /></div>", Utility.getContentLogoUrl(com.vuliv.network.utility.StringUtils.getLogoName(entityMediaDetail.getLogo()))));
                    stringBuilder.append("<div class=\"carouselA-gradient\">");
                    stringBuilder.append(String.format("<span class=\"card-title truncate\">%s</span>", entityMediaDetail.getName()));
                    stringBuilder.append("</div>");

                    stringBuilder.append("</a>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                }
                stringBuilder.append("</div>");
                stringBuilder.append("</div>");
                stringBuilder.append("</div>");
            }
        }
        return stringBuilder.toString();
    }

    public synchronized String getPlayerPage (String folderId, String videoId, Context context, String platform) {
        StringBuilder stringBuilder = new StringBuilder();
        List<EntityTableFolderItems> entityMediaDetails = DataController.getInstance().getData(context, platform).get(DataController.getInstance().getFolderName(Integer.parseInt(folderId), platform));
        // Get ads from DB, shuffle them and pick first one
        List<EntityTableFolderItems> ads = DataController.getInstance().getAdItem(context);
        if (ads.size() == 0) {
            ads.add(new EntityTableFolderItems());
        }
        if (entityMediaDetails != null && entityMediaDetails.size() > 0) {
            for (EntityTableFolderItems entityMediaDetail : entityMediaDetails) {
                if (entityMediaDetail.getItemId().equalsIgnoreCase(videoId)) {
                    stringBuilder.append("<div class=\"section\">");
                    stringBuilder.append("<div class=\"row\">");
                    stringBuilder.append("<div class=\"col s12 m6 l6 push-l3 push-m3\">");
                    stringBuilder.append("<div class=\"card\">");
                    stringBuilder.append("<div class=\"card-image\">");
                    stringBuilder.append(String.format("<video id=\"my-video\" class=\"video-js vjs-fluid vjs-16-9\" controls preload=\"auto\"\n" +
                            "                    poster=\"%s\" data-setup=\"{}\">", Utility.getVideoThumbnailUrl(folderId, entityMediaDetail.getItemId())));
                    stringBuilder.append(String.format("<source src=\"%s\" type='video/mp4'>", Utility.getVideoUrl(folderId, entityMediaDetail.getItemId())));
                    stringBuilder.append("<p class=\"vjs-no-js\">");
                    stringBuilder.append("To view this video please enable JavaScript, and consider upgrading to a web browser that");
                    stringBuilder.append("<a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a>");
                    stringBuilder.append("</p>");
                    stringBuilder.append("</video>");
                    stringBuilder.append("<script src=\"js/video.js\"></script>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    break;
                }
            }
            stringBuilder.append("<div class=\"section\">");
            stringBuilder.append("\n\t<div class=\"tray-heading tray-color-Carrot\"><h2>");
            stringBuilder.append(DataController.getInstance().getFolderName(Integer.parseInt(folderId), platform));
            stringBuilder.append("</h2></div>");
            stringBuilder.append("\n<div class=\"row card\">");
            stringBuilder.append("\n\t\t<div class=\"carouselB js-flickity\" data-flickity-options='{\"freeScroll\": true, \"prevNextButtons\": false, \"pageDots\": false,\"contain\": true,\"groupCells\": true,\"dragThreshold\": 200, \"imagesLoaded\": true, \"autoPlay\": false, \"wrapAround\": false}'>");

            for (EntityTableFolderItems entityMediaDetail : entityMediaDetails) {
                stringBuilder.append("\n\t\t\t<div class=\"card-image carouselB-cell\">");
                stringBuilder.append("\n\t\t\t <div class=\"carouselB-cell-image\">");
                // Get ad detail
                Collections.shuffle(ads);
                EntityTableFolderItems adItem = ads.get(0);

                stringBuilder.append(String.format("\n\t\t\t\t<a nohref onclick=\"loadVideoUrl('%s', '%s', '%s', '%s', '%s'); return false;\"><img src=\"",
                        Utility.getVideoUrl(folderId, entityMediaDetail.getItemId()), Utility.getVideoThumbnailUrl(folderId, entityMediaDetail.getItemId()),
                        entityMediaDetail.getItemId(),
                        Utility.getVideoUrl(String.valueOf(adItem.getFolderId()), adItem.getItemId()), adItem.getItemId()));
                stringBuilder.append(Utility.getVideoThumbnailUrl(folderId, entityMediaDetail.getItemId()));
                stringBuilder.append("\"/>");
                stringBuilder.append(String.format("<div class=\"cardicon-small\"><img src=\"%s\" class=\"\" alt=\"\" /></div>", Utility.getContentLogoUrl(com.vuliv.network.utility.StringUtils.getLogoName(entityMediaDetail.getLogo()))));
                stringBuilder.append("<div class=\"carouselA-gradient\">");
                stringBuilder.append(String.format("<span class=\"card-title truncate\">%s</span>", entityMediaDetail.getName()));
                stringBuilder.append("</div>");
                stringBuilder.append("\n\t\t\t\t</a>");

                stringBuilder.append("\n\t\t\t</div>");
                stringBuilder.append("\n\t\t\t</div>");
            }
            stringBuilder.append("\n\t\t</div>");
            stringBuilder.append("\n</div>");
            stringBuilder.append("\n\t</div>");
        }

        return stringBuilder.toString();
    }

/*
    public synchronized String getPlayer1Page (String folderId, String videoId, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        List<EntityTableFolderItems> entityMediaDetails = DataController.getInstance().getData(context).get(DataController.getInstance().getFolderName(Integer.parseInt(folderId)));
        // Get ads from DB, shuffle them and pick first one
        List<EntityTableFolderItems> ads = DataController.getInstance().getAdItem(context);
        if (ads.size() == 0) {
            ads.add(new EntityTableFolderItems());
        }
        if (entityMediaDetails != null && entityMediaDetails.size() > 0) {
            for (EntityTableFolderItems entityMediaDetail : entityMediaDetails) {
                if (entityMediaDetail.getItemId().equalsIgnoreCase(videoId)) {
                    stringBuilder.append("<div class=\"section\">");
                    stringBuilder.append("<div class=\"row\">");
                    stringBuilder.append("<div class=\"col s12 m6 l6 push-l3 push-m3\">");
                    stringBuilder.append("<div class=\"card\">");
                    stringBuilder.append("<div class=\"card-image\">");
                    stringBuilder.append(String.format("<video id=\"my-video\" width=\"320\" height=\"240\" controls poster=\"%s\">\n", Utility.getVideoThumbnailUrl(folderId, entityMediaDetail.getItemId())));
                    stringBuilder.append(String.format("<source src=\"%s\" type=\"video/mp4\">", Utility.getVideoUrl(folderId, entityMediaDetail.getItemId())));
                    stringBuilder.append("</video>");
                    stringBuilder.append("<script src=\"js/video.js\"></script>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    stringBuilder.append("</div>");
                    break;
                }
            }
            stringBuilder.append("<div class=\"section\">");
            stringBuilder.append("\n\t<div class=\"tray-heading tray-color-Carrot\"><h2>");
            stringBuilder.append(DataController.getInstance().getFolderName(Integer.parseInt(folderId)));
            stringBuilder.append("</h2></div>");
            stringBuilder.append("\n<div class=\"row card\">");
            stringBuilder.append("\n\t\t<div class=\"carouselB js-flickity\" data-flickity-options='{\"freeScroll\": true, \"prevNextButtons\": false, \"pageDots\": false,\"contain\": true,\"groupCells\": true,\"dragThreshold\": 200, \"imagesLoaded\": true, \"autoPlay\": false, \"wrapAround\": false}'>");

            for (EntityTableFolderItems entityMediaDetail : entityMediaDetails) {
                stringBuilder.append("\n\t\t\t<div class=\"card-image carouselB-cell\">");
                stringBuilder.append("\n\t\t\t <div class=\"carouselB-cell-image\">");
                // Get ad detail
                Collections.shuffle(ads);
                EntityTableFolderItems adItem = ads.get(0);

                stringBuilder.append(String.format("\n\t\t\t\t<a nohref onclick=\"loadVideoUrl('%s', '%s', '%s', '%s', '%s'); return false;\"><img src=\"",
                        Utility.getVideoUrl(folderId, entityMediaDetail.getItemId()), Utility.getVideoThumbnailUrl(folderId, entityMediaDetail.getItemId()),
                        entityMediaDetail.getItemId(),
                        Utility.getVideoUrl(String.valueOf(adItem.getFolderId()), adItem.getItemId()), adItem.getItemId()));
                stringBuilder.append(Utility.getVideoThumbnailUrl(folderId, entityMediaDetail.getItemId()));
                stringBuilder.append("\"/>");
                stringBuilder.append(String.format("<div class=\"cardicon-small\"><img src=\"%s\" class=\"\" alt=\"\" /></div>", Utility.getContentLogoUrl(com.vuliv.network.utility.StringUtils.getLogoName(entityMediaDetail.getLogo()))));
                stringBuilder.append("<div class=\"carouselA-gradient\">");
                stringBuilder.append(String.format("<span class=\"card-title truncate\">%s</span>", entityMediaDetail.getName()));
                stringBuilder.append("</div>");
                stringBuilder.append("\n\t\t\t\t</a>");

                stringBuilder.append("\n\t\t\t</div>");
                stringBuilder.append("\n\t\t\t</div>");
            }
            stringBuilder.append("\n\t\t</div>");
            stringBuilder.append("\n</div>");
            stringBuilder.append("\n\t</div>");
        }

        return stringBuilder.toString();
    }
*/

    public synchronized String playFirstVideo (String folderId, String videoId, Context context, String platform) {
        StringBuilder stringBuilder = new StringBuilder();
        List<EntityTableFolderItems> entityMediaDetails = DataController.getInstance().getData(context, platform).get(DataController.getInstance().getFolderName(Integer.parseInt(folderId), platform));
        if (entityMediaDetails != null && entityMediaDetails.size() > 0) {
            for (EntityTableFolderItems entityMediaDetail : entityMediaDetails) {
                if (entityMediaDetail.getItemId().equalsIgnoreCase(videoId)) {
                    stringBuilder.append(String.format("setIdTemp('%s');",
                            entityMediaDetail.getItemId()));
                    break;
                }
            }
        }
        return stringBuilder.toString();
    }
}
