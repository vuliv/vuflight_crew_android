package com.vucast.utility;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Switch;

import com.vucast.R;
import com.vucast.constants.Constants;
import com.vucast.entity.EntityMediaDetail;
import com.vuliv.network.service.SharedPrefController;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by user on 01-08-2017.
 */

public class Utility {

    private static final String TAG = "Utility";

    public static String getDurationFromMilli(long millisecond) {
        return getDuration(millisecond / 1000);
    }

    public static String getDuration(long second) {
        if (second < 0)
            second = 0;
        long sec = second % 60;
        long min = (second / 60) % 60;
        long hour = second / 3600;

        String s = (sec < 10) ? "0" + sec : "" + sec;
        String m = ((min + "").length() < 2) ? "0" + min : "" + min;
        String h = "" + hour;

        String time = "";
        if (hour > 0) {
            time = h + ":" + m + ":" + s;
        } else {
            time = m + ":" + s;
        }
        return time;
    }

    public static boolean getRequest(String url) {
        Log.v(TAG, "URL : " + url);
        boolean received = false;
        try {
            URL mUrl = new URL(url);
            URLConnection con = mUrl.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            in.close();

            Log.v(TAG, "STATUS CODE : " + ((HttpURLConnection) con).getResponseCode());
            if (((HttpURLConnection) con).getResponseCode() == HttpURLConnection.HTTP_OK) {
                received = true;
            }
            ((HttpURLConnection) con).disconnect();
            return received;
        } catch (Exception e) {
            e.printStackTrace();
            Log.v(TAG, "Error : " + e.getMessage());
        }
        return received;
    }

    public static String getRequestData(String url) {
        Log.v(TAG, "URL : " + url);
        StringBuffer response = null;
        try {
            URL mUrl = new URL(url);
            URLConnection con;
            con = (HttpURLConnection) mUrl.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            if (((HttpURLConnection) con).getResponseCode() != HttpURLConnection.HTTP_OK) {
                response = null;
            }
            ((HttpURLConnection) con).disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            Log.v(TAG, "Error : " + e.getMessage());
        }
        return (response != null) ? response.toString() : null;
    }


    public static void VolUp(Context context) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
    }

    public static void VolDown(Context context) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
    }

    public void initWifiAPConfig() {
        WifiConfiguration wifiConfiguration = new WifiConfiguration();
        wifiConfiguration.SSID = "SomeName";
        wifiConfiguration.preSharedKey = "SomeKey1";
        wifiConfiguration.hiddenSSID = false;
        wifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        wifiConfiguration.allowedKeyManagement.set(4);
        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static InputStream encodeToInputstream(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, stream);
        return new ByteArrayInputStream(stream.toByteArray());
    }


    public static String getIp(Context context) {
//        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
//        String formatedIpAddress = String.format("%d.%d.%d.%d",
//                (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
//                (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
//        if (formatedIpAddress.equalsIgnoreCase("0.0.0.0")) {
            String formatedIpAddress = "192.168.43.1";
//        }
        return formatedIpAddress;
    }

    public static String getIp(int ipAddress) {
        String formatedIpAddress = String.format("%d.%d.%d.%d",
                (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        if (formatedIpAddress.equalsIgnoreCase("0.0.0.0")) {
            formatedIpAddress = "192.168.43.1";
        }
        return formatedIpAddress;
    }

    /**
     * Show alert dialog
     *
     * @param context
     * @param message
     * @param positiveText
     * @param negativeText
     * @param positiveListener
     * @param negativeListener
     */
    public static void showAlertDialog(Context context, String message, String positiveText, String negativeText,
                                       DialogInterface.OnClickListener positiveListener,
                                       DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, positiveListener);
        if (!StringUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText,
                    negativeListener == null ? new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    } : negativeListener);
        }
        builder.show();
    }

    public static Bitmap getAlbumArt(Context context, String album_id) {
        Bitmap albumArtBitMap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, Long.parseLong(album_id));

            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                albumArtBitMap = BitmapFactory.decodeFileDescriptor(fd, null,
                        options);
                pfd = null;
                fd = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            albumArtBitMap = BitmapFactory.decodeResource(context.getResources(), R.drawable.thumb_music);
        }

//        if (null != albumArtBitMap) {
        return albumArtBitMap;
//        }

//        return getDefaultAlbumArtEfficiently(context.getResources());
    }

//    public Bitmap getDefaultAlbumArtEfficiently(Resources resource) {
//
//        if (defaultBitmapArt == null) {
//
//            defaultBitmapArt = decodeSampledBitmapFromResource(resource,
//                    R.drawable.default_album_art, UtilFunctions
//                            .getUtilFunctions().dpToPixels(85, resource),
//                    UtilFunctions.getUtilFunctions().dpToPixels(85, resource));
//
//        }
//        return defaultBitmapArt;
//    }

    /**
     * Scan Android database
     * @param context
     * @param path
     */
    public static void scanFile(Context context, String path) {
        MediaScannerConnection.scanFile(context, new String[] { path }, null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri) {

                    }
                });
    }


    public static String getThumbnailUrl (EntityMediaDetail entityMediaDetail) {
        String thumbnail = null;
        if (Constants.TYPE_MUSIC.equals(entityMediaDetail.getType())) {
            thumbnail = Constants.AUDIO_IMAGE_URL + entityMediaDetail.getAudioId();
        } else if (Constants.TYPE_VIDEO.equals(entityMediaDetail.getType())) {
            thumbnail = Constants.VIDEO_IMAGE_URL + "/" + entityMediaDetail.getVideoId();
        } else if (Constants.TYPE_PHOTO.equals(entityMediaDetail.getType())) {
            thumbnail = Constants.PHOTO_IMAGE_URL + entityMediaDetail.getPath();
        }
        return thumbnail;
    }

    public static String getVideoThumbnailUrl(String paramString1, String paramString2) {
        return Constants.VIDEO_CONTENT_THUMBNAIL_URL/* + "/" + paramString1.replace(" ", "%20") */+ "/" + paramString2;
    }

    public static String getContentLogoUrl(String paramString1) {
        return Constants.VIDEO_CONTENT_LOGO_URL + "/" + paramString1.replace(" ", "%20");
    }

    public static String getVideoUrl(String paramString1, String paramString2) {
        if (StringUtils.isEmpty(paramString2)) return null;
        return Constants.VIDEO_CONTENT_URL/* + "/" + paramString1.replace(" ", "%20") */+ "/" + paramString2 + ".mp4";
    }

    public static boolean sendMessage(Switch aSwitch) {
        return !aSwitch.isChecked();
    }

    public static boolean isOrientationLandscape (Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static InputStream drawableToInputstream (Drawable drawable) {
        return bitmapToInputStream(drawableToBitmap(drawable));
    }

    public static InputStream bitmapToInputStream(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
        return bs;
//        int size = bitmap.getHeight() * bitmap.getRowBytes();
//        ByteBuffer buffer = ByteBuffer.allocate(size);
//        bitmap.copyPixelsToBuffer(buffer);
//        return new ByteArrayInputStream(buffer.array());
    }

    public static String getHotspotName (Context context) {
        String vehicleName = SharedPrefController.getVehicleName(context);
//        String startSeat = SharedPrefController.getSeatStart(context);
//        String endSeat = SharedPrefController.getSeatEnd(context);
        return vehicleName.replaceAll(" ", "").replaceAll("-", "");
    }


}
