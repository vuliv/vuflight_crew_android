package com.vucast.constants;

import android.os.Environment;

import org.java_websocket.WebSocket;

import java.util.ArrayList;

/**
 * Created by user on 01-08-2017.
 */

public interface Constants {

    int SOCKET_PORT = 8523;
    int SERVER_PORT = 9632;
    int HTTP_SERVER_PORT = 7070;

    int REQUEST_CODE_CLIENT_START = 100;
    int REQUEST_CODE_SERVER_START = 101;

    int CLIENT_SESSION_TIME = 30 * 60; // In Seconds
    int CLIENT_INACTIVE_SESSION_TIME = 1 * 60; // In Seconds
    int CLIENT_DATA_SIZE = 10; // In Seconds

    ArrayList<WebSocket> CLIENTS = new ArrayList<>();

    String STREAM_URL = "";//"http://" + DataConstants.IP_ADDRESS + ":" + SERVER_PORT + "/";
    String SERVER_PING_URL = "http://%s:" + SERVER_PORT + "/ping";
    String USER_IMAGE_URL = STREAM_URL + "user.png";
    String VIDEO_URL = STREAM_URL + "messagePlay";
    String VIDEO_IMAGE_URL = STREAM_URL + "messageVideoImage";
    String PHOTO_IMAGE_URL = STREAM_URL + "messagePhotoImage";
    String AUDIO_IMAGE_URL = STREAM_URL + "messageAudioImage";

    String VIDEO_CONTENT_URL = STREAM_URL + "contentUrl";
    String VIDEO_CONTENT_THUMBNAIL_URL = STREAM_URL + "contentThumbnail";
    String VIDEO_CONTENT_LOGO_URL = STREAM_URL + "contentLogo";

    String BASE_URL = "http://" + DataConstants.IP_ADDRESS + ":" + HTTP_SERVER_PORT;
    String DOWNLOAD_FILE_URL = BASE_URL + "/download";

    String SOCKET_MESSAGE_PLAY = "messagePlay:";
    String SOCKET_VIDEO_PLAY = "messageVideoPlay:";
    String SOCKET_AUDIO_PLAY = "messageAudioPlay:";
    String SOCKET_PHOTO = "messageAudioPlay:";
    String SOCKET_MESSAGE_CONTROLS = "controls:";
    String SOCKET_MESSAGE_PAUSE = "pause";

    String SOCKET_MESSAGE_VOLUME_UP = "volumeUp";
    String SOCKET_MESSAGE_VOLUME_DOWN = "volumeDown";

    String TYPE_PHOTO = "Photo";
    String TYPE_VIDEO = "Video";
    String TYPE_MUSIC = "Music";
    String TYPE_FILE = "File";

    String VUSHARE_FOLDER_NAME = "VuShare";
    String SAVE_FILE_PATH = Environment.getExternalStorageDirectory() + "/" + VUSHARE_FOLDER_NAME;

    String UPDATE_UI = "updateUI";
    String UPDATE_ORDERED_ITEM = "updateOrderedItem";
    String UPDATED_REQUEST_ITEM = "updatedRequestItem";

}
