package com.vucast.constants;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by user on 02-08-2017.
 */

public class DataConstants {

    public static String IP_ADDRESS = "192.168.43.1";
    public static boolean HAVE_CONTROLS = true;

    public static void runOnUiThread(Runnable runnable) {
        final Handler UIHandler = new Handler(Looper.getMainLooper());
        UIHandler.post(runnable);
    }
}
