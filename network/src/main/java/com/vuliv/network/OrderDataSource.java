package com.vuliv.network;


import com.vuliv.network.persistence.Order;

import java.util.List;

/**
 * Created by user on 23-01-2018.
 */

public interface OrderDataSource {

    List<Order> getOrders();

    Order getOrderByMsisdn(String msisdn);

    void insertorUpdateOrder(Order order);

    void deleteOrder(Order order);

}
