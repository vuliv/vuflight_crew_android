package com.vuliv.network.callback;

public interface IDownloadCallback {
	void onPreExecute();
	void onSuccess(String path, Object object);
	void onFailure(Object object);
	void cancelled(Object object);
	void alreadyProgress();

	void progress(int progress);
}
