package com.vuliv.network.viewmodel;


import android.app.Application;
import android.content.Context;

import com.vuliv.network.persistence.Order;
import com.vuliv.network.persistence.OrderDatabase;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/*public class OrderViewModel{
    OrderDatabase orderDatabase;

    public OrderViewModel(Context application){
        orderDatabase = OrderDatabase.getInstance(application);
        orderDatabase.orderDao();
    }

    public List<Order> getOrders() {
        return orderDatabase.orderDao().getOrders();
    }

    public Order getOrder(String msisdn) {
        return orderDatabase.orderDao().getOrderByMsisdn(msisdn);
    }

    public void insertorUpdateOrder(final Order order) {
        orderDatabase.orderDao().insertOrder(order);
//        return Completable.fromAction(new Action() {
//            @Override
//            public void run() throws Exception {
//            }
//        });
    }

    public void insertorUpdateOrderInThread(final Order order) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                orderDatabase.orderDao().insertOrder(order);
            }
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe();
    }

    public void deleteOrder(final Order order) {
        orderDatabase.orderDao().deleteOrder(order);
//        return Completable.fromAction(new Action() {
//            @Override
//            public void run() throws Exception {
//            }
//        });
    }
}*/
