package com.vuliv.network.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EntityDataResponse {

    @SerializedName("information")
    ArrayList<Dominos> information;

    @SerializedName("data")
    ArrayList<Data> datas;

    @SerializedName("deleted")
    ArrayList<String> deleted;

    public ArrayList<Data> getDatas() {
        return this.datas;
    }

    public ArrayList<String> getDeleted() {
        return deleted;
    }

    public ArrayList<Dominos> getInformation() {
        return information;
    }

    public void setInformation(ArrayList<Dominos> information) {
        this.information = information;
    }

    public static class Data {
        @SerializedName("id")
        int id;

        @SerializedName("folder")
        String folder;

        @SerializedName("position")
        int position;

        @SerializedName("vtype")
        String vtype;

        @SerializedName("content")
        ArrayList<Content> content;

        public int getId() {
            return this.id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFolder() {
            return this.folder;
        }

        public void setFolder(String folder) {
            this.folder = folder;
        }

        public int getPosition() {
            return this.position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getVtype() {
            return this.vtype;
        }

        public void setVtype(String vtype) {
            this.vtype = vtype;
        }

        public ArrayList<Content> getContent() {
            return this.content;
        }

        public void setContent(ArrayList<Content> content) {
            this.content = content;
        }

        public static class Content {
            @SerializedName("id")
            String id;

            @SerializedName("name")
            String name;

            @SerializedName("thumbnail")
            String thumbnail;

            @SerializedName("url")
            String url;

            @SerializedName("type")
            String type;

            @SerializedName("start_time")
            long startTime;

            @SerializedName("end_time")
            long endTime;

            @SerializedName("expiry")
            long expiry;

            @SerializedName("duration")
            long duration;

            @SerializedName("logo")
            String logo;

            @SerializedName("imageDownload")
            boolean imageDownload;

            @SerializedName("videoDownload")
            boolean videoDownload;

            @SerializedName("platform")
            String platform;

            public String getId() {
                return this.id;
            }

            public String getName() {
                return this.name;
            }

            public String getThumbnail() {
                return this.thumbnail;
            }

            public String getUrl() {
                return this.url;
            }

            public String getType() {
                return this.type;
            }

            public long getExpiry() {
                return this.expiry;
            }

            public long getDuration() {
                return this.duration;
            }

            public boolean isImageDownload() {
                return this.imageDownload;
            }

            public boolean isVideoDownload() {
                return this.videoDownload;
            }

            public long getStartTime() {
                return startTime;
            }

            public long getEndTime() {
                return endTime;
            }

            public String getLogo() {
                return logo;
            }

            public String getPlatform() {
                return platform;
            }
        }
    }

    public static class SizeAndPrice {
        @SerializedName("price")
        int price;

        @SerializedName("max_quantity")
        int max_qty;

        @SerializedName("size")
        String size;

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getMax_qty() {
            return max_qty;
        }

        public void setMax_qty(int max_qty) {
            this.max_qty = max_qty;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }


    }

    public static class Dominos {

        //        @SerializedName("gst_price")
//        int gst_price;
        String type;
        @SerializedName("name")
        String name;
        /*  @SerializedName("max_quantity")
          int max_quantity;*/
        @SerializedName("description")
        String description;
        @SerializedName("image")
        String image;
        /* @SerializedName("price")
         int price;*/
        @SerializedName("id")
        String id;
        @SerializedName("crust")
        String crust;
        @SerializedName("veg")
        boolean veg;
        @SerializedName("sizes")
        //ArrayList<String> sizes;
                ArrayList<SizeAndPrice> sizes;

//        public int getGst_price() {
//            return gst_price;
//        }
//
//        public void setGst_price(int gst_price) {
//            this.gst_price = gst_price;
//        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

     /*   public int getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(int max_quantity) {
            this.max_quantity = max_quantity;
        }*/

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

     /*   public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }*/

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCrust() {
            return crust;
        }

        public void setCrust(String crust) {
            this.crust = crust;
        }

        public boolean isVeg() {
            return veg;
        }

        public void setVeg(boolean veg) {
            this.veg = veg;
        }

        public ArrayList<SizeAndPrice> getSizes() {
            return sizes;
        }

        public void setSizes(ArrayList<SizeAndPrice> sizes) {
            this.sizes = sizes;
        }
    }

}