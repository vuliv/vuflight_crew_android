package com.vuliv.network.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 12-01-2018.
 */

public class EntityDominosRequest extends EntityBase implements Serializable {

    @SerializedName("msisdn")
    private
    String msisdn;

    @SerializedName("username")
    private
    String username;

    @SerializedName("service_request")
    private
    String serviceRequest;

    @SerializedName("otp")
    private
    String otp;

    @SerializedName("data")
    private
    ArrayList<Data> data;

    @SerializedName("price")
    private
    int price;

    @SerializedName("key")
    private
    String key;

    @SerializedName("orderId")
    private
    String orderId;

    @SerializedName("otpSent")
    private
    boolean otpSent;

    @SerializedName("verified")
    private
    boolean verified;

    @SerializedName("otpVerified")
    private
    String otpVerified;

    @SerializedName("timeStamp")
    private
    long timeStamp;

    @SerializedName("isPrebooked")
    private
    boolean isPrebooked;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ArrayList<Data> getData() {
        return data;
    }


    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public boolean isOtpSent() {
        return otpSent;
    }

    public void setOtpSent(boolean otpSent) {
        this.otpSent = otpSent;
    }

    public String getOtpVerified() {
        return otpVerified;
    }

    public void setOtpVerified(String otpVerified) {
        this.otpVerified = otpVerified;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtp() {
        return otp;
    }

    public int getPrice() {
        return price;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(String serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public boolean isPrebooked() {
        return isPrebooked;
    }

    public void setPrebooked(boolean prebooked) {
        isPrebooked = prebooked;
    }

    @Override
    public boolean equals(Object obj) {
        if (getOrderId().equals(((EntityDominosRequest) obj).getOrderId())) {
            return true;
        }
        return false;
    }

    public static class Data implements Serializable {
        private static final String TAG = Data.class.getSimpleName();

        @SerializedName("id")
        String id;

        @SerializedName("name")
        String name;

        @SerializedName("description")
        String description;

        @SerializedName("quantity")
        int quantity;

        @SerializedName("price")
        int price;

        @SerializedName("size")
        String size;

        @SerializedName("veg")
        boolean veg;

        @SerializedName("image")
        String image;

        @SerializedName("type")
        String type;

        @SerializedName("hasAccepted")
        boolean hasAccepted;

        @SerializedName("totalQuantity")
        int totalQuantity;

        public String getAllottedCartNumber() {
            return allottedCartNumber;
        }

        public void setAllottedCartNumber(String allottedCartNumber) {
            this.allottedCartNumber = allottedCartNumber;
        }

        @SerializedName("allottedCartNumber")
        String allottedCartNumber;

        public Data(String id, String name, String description, int quantity, int price, String image, String type, boolean veg) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.quantity = quantity;
            this.price = price;
            this.veg = veg;
            this.image = image;
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getTotalQuantity() {
            return totalQuantity;
        }

        public void setTotalQuantity(int totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public boolean isVeg() {
            return veg;
        }

        public void setVeg(boolean veg) {
            this.veg = veg;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isHasAccepted() {
            return hasAccepted;
        }

        public void setHasAccepted(boolean hasAccepted) {
            this.hasAccepted = hasAccepted;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Data)) {
                return false;
            }

            if (getId().equals(((Data) obj).getId())) {
                return true;
            }
            return false;
        }
    }
}
