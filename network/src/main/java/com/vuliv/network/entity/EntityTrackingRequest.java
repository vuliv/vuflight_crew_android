package com.vuliv.network.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vuliv.network.database.tables.EntityTableTracking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 24-10-2017.
 */

public class EntityTrackingRequest extends EntityBase{

    @SerializedName("tracking")
    @Expose List<EntityTableTracking> tracking;

    public List<EntityTableTracking> getTracking() {
        return tracking;
    }

    public void setTracking(List<EntityTableTracking> tracking) {
        this.tracking = tracking;
    }
}
