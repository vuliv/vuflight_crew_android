package com.vuliv.network.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 16-10-2017.
 */

public class EntityBase {

    @SerializedName("model")
    @Expose String model = new String();

    @SerializedName("_interface")
    @Expose String Interface = new String();

    @SerializedName("versionCode")
    @Expose int versionCode;

    @SerializedName("version")
    @Expose String version = new String();

    @SerializedName("deviceId")
    @Expose String deviceId;

    @SerializedName("package")
    @Expose String packageName;

    @SerializedName("partner")
    @Expose String partner;

    @SerializedName("reg_id")
    @Expose String registrationId;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getInterface() {
        return Interface;
    }

    public void setInterface(String mInterface) {
        this.Interface = mInterface;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}
