package com.vuliv.network.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 27-10-2017.
 */

public class EntityRegistrationResponse extends EntityResponse {

    @SerializedName("reg_id")
    private String registrationId;

    @SerializedName("uploaded_by")
    private String uploadedBy;

    public String getRegistrationId() {
        return registrationId;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }
}
