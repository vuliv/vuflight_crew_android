package com.vuliv.network.entity;

import com.google.gson.annotations.SerializedName;

public class EntityResponse {

	@SerializedName("model")
	String model = new String();

	@SerializedName("_interface")
	String mInterface = new String();

	@SerializedName("req_type")
	String reqType = new String();

	@SerializedName("uid")
	String uid = new String();

	@SerializedName("message")
	String message = new String();

	@SerializedName("status")
	String status = new String();

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getmInterface() {
		return mInterface;
	}

	public void setmInterface(String mInterface) {
		this.mInterface = mInterface;
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
