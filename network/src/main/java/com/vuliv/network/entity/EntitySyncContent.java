package com.vuliv.network.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by user on 16-10-2017.
 */

public class EntitySyncContent extends EntityBase{

    @SerializedName("downloaded")
    @Expose ArrayList<String> downloadedContent = new ArrayList<>();

    public void setDownloadedContent(ArrayList<String> downloadedContent) {
        this.downloadedContent = downloadedContent;
    }
}
