package com.vuliv.network.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 30-10-2017.
 */

public class EntityServerPing {

    @SerializedName("ipAddress")
    @Expose public String ipAddress;

    @SerializedName("name")
    @Expose public String name;

    @SerializedName("icon")
    @Expose public int icon;

    @SerializedName("reg_id")
    @Expose public String registrationId;

    @SerializedName("session_id")
    @Expose public String sessionId;

    @SerializedName("partner")
    @Expose public String partner;

    @SerializedName("session_time")
    @Expose public long sessionTime;

    @SerializedName("inactive_session_time")
    @Expose public long inActiveSessionTime;

    @SerializedName("data_size")
    @Expose public long dataSize;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public void setSessionTime(long sessionTime) {
        this.sessionTime = sessionTime;
    }

    public void setDataSize(long dataSize) {
        this.dataSize = dataSize;
    }

    public void setInActiveSessionTime(long inActiveSessionTime) {
        this.inActiveSessionTime = inActiveSessionTime;
    }
}
