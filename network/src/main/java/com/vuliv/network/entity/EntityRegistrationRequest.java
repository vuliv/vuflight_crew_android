package com.vuliv.network.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 27-10-2017.
 */

public class EntityRegistrationRequest extends EntityBase {

    @SerializedName("vehicle_no")
    @Expose private String vehicleNo;

    @SerializedName("source")
    @Expose private String source;

    @SerializedName("destination")
    @Expose private String destination;

    @SerializedName("seat_start")
    @Expose private int fromSeat;

    @SerializedName("seat_end")
    @Expose private int toSeat;

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setFromSeat(int fromSeat) {
        this.fromSeat = fromSeat;
    }

    public void setToSeat(int toSeat) {
        this.toSeat = toSeat;
    }
}
