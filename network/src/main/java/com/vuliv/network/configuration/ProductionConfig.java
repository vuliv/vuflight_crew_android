package com.vuliv.network.configuration;

/**
 * Created by user on 16-10-2017.
 */

public class ProductionConfig implements IUrlConfiguration {

    private static final String HOST = "https://api.vuliv.com";
    private static final String ANALYTICS_HOST = "https://analyticapi.vuliv.com";

    @Override
    public String getLatestContentUrl() {
        return HOST + "/theapp/webapi/vuscreen/feed";
    }

    @Override
    public String getRegistrationUrl() {
        return ANALYTICS_HOST + "/AnalyticsAPI/webapi/vuscreen/registration";
    }

    @Override
    public String getTrackingUrl() {
        return ANALYTICS_HOST + "/AnalyticsAPI/webapi/vuscreen/track";
    }

    @Override
    public String getTrackingEventsUrl() {
        return ANALYTICS_HOST + "/AnalyticsAPI/webapi/vuscreen/event";
    }

    @Override
    public String getPurchaseTrackingUrl() {
        return ANALYTICS_HOST + "/AnalyticsAPI/webapi/vuFlight/tracking";
    }
}