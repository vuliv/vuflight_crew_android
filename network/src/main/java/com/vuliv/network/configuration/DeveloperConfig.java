package com.vuliv.network.configuration;

/**
 * Created by user on 16-10-2017.
 */

public class DeveloperConfig implements IUrlConfiguration {

    private static final String HOST = "http://52.76.81.227:8081";

    @Override
    public String getLatestContentUrl() {
        return HOST + "/theapp/webapi/vuscreen/feed";
    }

    @Override
    public String getRegistrationUrl() {
        return HOST + "/AnalyticsAPI/webapi/vuscreen/registration";
    }

    @Override
    public String getTrackingUrl() {
        return HOST + "/AnalyticsAPI/webapi/vuFlight/video";
    }

    @Override
    public String getTrackingEventsUrl() {
        return HOST + "/AnalyticsAPI/webapi/vuFlight/event";
    }

    @Override
    public String getPurchaseTrackingUrl() {
        return HOST + "/AnalyticsAPI/webapi/vuFlight/tracking";
    }
}
