package com.vuliv.network.configuration;

/**
 * Created by user on 16-10-2017.
 */

public interface IUrlConfiguration {

    String APPLICATION_VERSION_1 = "application/json";

    String getLatestContentUrl();

    String getRegistrationUrl();

    String getTrackingUrl();

    String getTrackingEventsUrl();

    String getPurchaseTrackingUrl();

}
