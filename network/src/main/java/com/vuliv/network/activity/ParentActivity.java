package com.vuliv.network.activity;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.vuliv.network.database.DatabaseHandler;
import com.vuliv.network.receiver.AdminController;
import com.vuliv.network.receiver.DeviceAdminReceiver;

/**
 * Created by User on 11/7/2017.
 */

public class ParentActivity extends ORMLiteActivity<DatabaseHandler> {

    private DevicePolicyManager mDevicePolicyManager;
    private ComponentName mComponentName;
    private PackageManager mPackageManager;

    public static final String LOCK_ACTIVITY_KEY = "lock_activity";
    public static final int FROM_LOCK_ACTIVITY = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mPackageManager = this.getPackageManager();

        mComponentName = DeviceAdminReceiver.getComponentName(this);
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (mDevicePolicyManager.isDeviceOwnerApp(getPackageName())) {
            AdminController.setAdminPolicies(this, mDevicePolicyManager, mComponentName, true);
        } else {
            Log.i("Parent Activity", "Application not set to device owner");
            //  Toast.makeText(getApplicationContext(),"Application not set to device owner", Toast.LENGTH_SHORT).show();
        }

        Intent intent = getIntent();
        if (intent.getIntExtra(ParentActivity.LOCK_ACTIVITY_KEY, 0) == ParentActivity.FROM_LOCK_ACTIVITY) {
            mDevicePolicyManager.clearPackagePersistentPreferredActivities(
                    mComponentName, getPackageName());
            mPackageManager.setComponentEnabledSetting(
                    new ComponentName(getApplicationContext(), ParentActivity.class),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if (mDevicePolicyManager.isLockTaskPermitted(this.getPackageName())) {
                ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                if (am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_NONE) {
                    // start the lock
                    startLockTask();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
