package com.vuliv.network.server;

import android.content.Context;
import android.os.Environment;

import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.vuliv.network.database.DatabaseHandler;
import com.vuliv.network.database.sharedpref.SettingHelper;
import com.vuliv.network.database.tables.EntityTableFolder;
import com.vuliv.network.database.tables.EntityTableFolderItems;
import com.vuliv.network.database.tables.EntityTableLogo;
import com.vuliv.network.database.tables.EntityTableTracking;
import com.vuliv.network.entity.EntityBase;
import com.vuliv.network.entity.EntityDataResponse;
import com.vuliv.network.entity.EntityRegistrationRequest;
import com.vuliv.network.entity.EntityRegistrationResponse;
import com.vuliv.network.entity.EntitySyncContent;
import com.vuliv.network.utility.StringUtils;
import com.vuliv.network.utility.Utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

public class DataController {

    private String TAG = DataController.class.getCanonicalName();
    private static DataController instance = null;
    private DataController(){}
    public static LinkedHashMap<String, List<EntityTableFolderItems>> folderTreeMapMobile = new LinkedHashMap();
    public static TreeMap<Integer, String> foldersNameMobile = new TreeMap();
    public static LinkedHashMap<String, List<EntityTableFolderItems>> folderTreeMapWeb = new LinkedHashMap();
    public static TreeMap<Integer, String> foldersNameWeb = new TreeMap();
    public static TreeMap<String, EntityTableFolderItems> contentMap = new TreeMap();
    private String sessionId;
    private DatabaseHandler databaseHandler;
    private EntityDataResponse dataResponse;
    private EntityDataResponse productResponse;

    public static DataController getInstance() {
        if (instance == null) {
            instance = new DataController();
        }
        return instance;
    }


    public String getSessionId(Context context) {
        if (StringUtils.isEmpty(sessionId)) {
            sessionId = Utility.getAndroidId(context) + "_" + System.currentTimeMillis();
        }
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void insertData(Context context) {
        if (getDataResponse() == null) {
            StringBuilder buf = new StringBuilder();
            try {
                InputStream json = context.getAssets().open("data.json");
                BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
                String str;
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setDataResponse(new Gson().fromJson(buf.toString(), EntityDataResponse.class));
        }
        if (getProductResponse() == null) {
            StringBuilder buf = new StringBuilder();
            try {
                InputStream json = context.getAssets().open("product.json");
                BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
                String str;
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setProductResponse(new Gson().fromJson(buf.toString(), EntityDataResponse.class));
        }
        if (!SettingHelper.isFirstDataTransfer(context)) {
            StringBuilder buf = new StringBuilder();
            try {
                InputStream json = context.getAssets().open("preloadvideo.json");
                BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
                String str;
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            EntityDataResponse entityDataResponse = new Gson().fromJson(buf.toString(), EntityDataResponse.class);
            processAndInsertData(context, entityDataResponse, true);
            SettingHelper.setFirstDataTransfer(context, true);
        }
    }

/*
    public void fetchLiveData (final Context context) {
        Log.wtf(TAG, "fetchLiveData IN");
        String response = RestClient.getInstance().postRequest(Utility.getUrlConfig().getLatestContentUrl(), getRequest(context));
        if (!StringUtils.isEmpty(response)) {
            Log.wtf(TAG, "fetchLiveData " + response);
            SharedPrefController.setContentSync(context, System.currentTimeMillis());
            EntityDataResponse entityDataResponse = new Gson().fromJson(response, EntityDataResponse.class);
            if (entityDataResponse != null && entityDataResponse.getDeleted() != null && entityDataResponse.getDeleted().size() > 0) {
                for (String id : entityDataResponse.getDeleted()) {
                    Utility.deleteCampaignFile(id);
                }
                try {
                    getHelper(context).getFolderItemsTableOperations().deleteFolderItems(entityDataResponse.getDeleted());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    release();
                }
            }
            if (entityDataResponse != null && entityDataResponse.getDatas() != null) {
                processAndInsertData(context, entityDataResponse, false);
            }
        }
        Log.wtf(TAG, "fetchLiveData OUT");
    }
*/

    public boolean registerUser (final Context context, final String vehicleNumber, final String fromDestination, final String toDestination, final int seatStart, final int seatEnd) {
        String response = RestClient.getInstance().postRequest(Utility.getUrlConfig().getRegistrationUrl(), getRegistrationRequest(context,vehicleNumber,fromDestination, toDestination,seatStart, seatEnd));
        if (!StringUtils.isEmpty(response)) {
            EntityRegistrationResponse entityResponse = new Gson().fromJson(response, EntityRegistrationResponse.class);
            if (entityResponse != null && !StringUtils.isEmpty(entityResponse.getStatus()) && entityResponse.getStatus().equalsIgnoreCase("200")) {
                SettingHelper.setRegistrationKey(context, entityResponse.getRegistrationId());
                SettingHelper.setPartnerId(context, entityResponse.getUploadedBy());
                return true;
            }
        }
        return false;
    }

    private String getRegistrationRequest (Context context, String vehicleNumber, String fromDestination, String toDestination, int seatStart, int seatEnd) {
        EntityRegistrationRequest request = new EntityRegistrationRequest();
        getBaseRequest(context, request);
        request.setPartner(Utility.getPartner());
        request.setVehicleNo(vehicleNumber);
        request.setSource(fromDestination);
        request.setDestination(toDestination);
        request.setToSeat(seatEnd);
        request.setFromSeat(seatStart);
        Gson gson = new Gson();
        return gson.toJson(request, EntityRegistrationRequest.class);
    }

    private String getRequest (Context context) {
        EntitySyncContent entityBase = new EntitySyncContent();
        getBaseRequest(context, entityBase);
        List<EntityTableFolderItems> downloadedFolderItems = null;
        try {
            downloadedFolderItems = getHelper(context).getFolderItemsTableOperations().getDownloadedFolderItems();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            release();
        }
        if (downloadedFolderItems != null) {
            ArrayList<String> downloadedContent = new ArrayList<>();
            for (EntityTableFolderItems folderItems : downloadedFolderItems) {
                downloadedContent.add(folderItems.getItemId());
            }
            entityBase.setDownloadedContent(downloadedContent);
        }
        Gson gson = new Gson();
        return gson.toJson(entityBase, EntitySyncContent.class);
    }

    private EntityBase getBaseRequest (Context context, EntityBase entityBase) {
        entityBase.setModel(Utility.getDeviceModel());
        entityBase.setDeviceId(Utility.getAndroidId(context));
        entityBase.setVersion(Utility.getVersionName(context));
        entityBase.setVersionCode(Utility.getVersionCode(context));
        entityBase.setInterface(Utility.getInterface());
        entityBase.setPackageName(Utility.getPackageName(context));
        entityBase.setPartner(SettingHelper.getPartnerId(context));
        entityBase.setRegistrationId(SettingHelper.getRegistrationKey(context));
        return entityBase;
    }

    private void processAndInsertData(Context context, EntityDataResponse entityDataResponse, boolean preloadData) {
        DatabaseHandler databaseHandler = getHelper(context);
        try {
//            getHelper(context).getLogoTableOperations().resetData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (EntityDataResponse.Data data : entityDataResponse.getDatas()) {
            EntityTableFolder folder = new EntityTableFolder();
            folder.setFolderId(data.getId());
            folder.setPosition(data.getPosition());
            folder.setName(data.getFolder());
            folder.setVtype(data.getVtype());
            ArrayList folderArrayList = new ArrayList();
            folderArrayList.add(folder);
            try {
                databaseHandler.getFolderTableOperations().insertFolders(folderArrayList);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList listFolderItems = new ArrayList();
            for (EntityDataResponse.Data.Content content : data.getContent()) {
                EntityTableFolderItems folderItems = new EntityTableFolderItems();
                folderItems.setItemId(content.getId());
                folderItems.setName(content.getName());
                folderItems.setThumbnail(content.getThumbnail());
                folderItems.setUrl(content.getUrl());
                folderItems.setType(content.getType());
                folderItems.setExpiry(content.getExpiry());
                folderItems.setStartTime(content.getStartTime());
                folderItems.setEndTime(content.getEndTime());
                folderItems.setLogo(content.getLogo());
                folderItems.setDuration(content.getDuration());
                folderItems.setFolderId(data.getId());
                folderItems.setImageDownload(content.isImageDownload());
                folderItems.setVideoDownload(content.isVideoDownload());
                folderItems.setPlatform(content.getPlatform());
                listFolderItems.add(folderItems);
                try {
                    EntityTableLogo entityTableLogo = new EntityTableLogo();
                    entityTableLogo.setUrl(content.getLogo());
                    if (!StringUtils.isEmpty(content.getLogo())) {
                        entityTableLogo.setDownload(preloadData);
                        databaseHandler.getLogoTableOperations().insertData(entityTableLogo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                databaseHandler.getFolderItemsTableOperations().insertFolderItems(listFolderItems);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
//            getHelper(context).getLogoTableOperations().deleteDisabledData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        release();
    }

    public LinkedHashMap<String, List<EntityTableFolderItems>> getData(Context context, String type) {
        if (EntityTableFolderItems.PLATFORM_MOBILE.equalsIgnoreCase(type)) {
            if (folderTreeMapMobile.size() == 0) {
                try {
                    List<EntityTableFolder> folders = getHelper(context).getFolderTableOperations().getFolders();
                    for (EntityTableFolder folder : folders) {
                        List<EntityTableFolderItems> folderItems = getHelper(context).getFolderItemsTableOperations().getFolderItemsMobile(folder.getFolderId());
                        folderTreeMapMobile.put(folder.getName(), folderItems);
                        foldersNameMobile.put(folder.getFolderId(), folder.getName());
                        for (EntityTableFolderItems folderItem : folderItems) {
                            contentMap.put(folderItem.getItemId(), folderItem);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                release();
            }
            return folderTreeMapMobile;
        } else if (EntityTableFolderItems.PLATFORM_WEB.equalsIgnoreCase(type)) {
            if (folderTreeMapWeb.size() == 0) {
                try {
                    List<EntityTableFolder> folders = getHelper(context).getFolderTableOperations().getFolders();
                    for (EntityTableFolder folder : folders) {
                        List<EntityTableFolderItems> folderItems = getHelper(context).getFolderItemsTableOperations().getFolderItemsWeb(folder.getFolderId());
                        folderTreeMapWeb.put(folder.getName(), folderItems);
                        foldersNameWeb.put(folder.getFolderId(), folder.getName());
                        for (EntityTableFolderItems folderItem : folderItems) {
                            contentMap.put(folderItem.getItemId(), folderItem);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                release();
            }
            return folderTreeMapWeb;
        }
        return null;
    }

    public String getFolderName (int id, String type) {
        if (EntityTableFolderItems.PLATFORM_MOBILE.equalsIgnoreCase(type)) {
            return foldersNameMobile.get(id);
        } else if (EntityTableFolderItems.PLATFORM_WEB.equalsIgnoreCase(type)) {
            return foldersNameWeb.get(id);
        }
        return null;
    }

    public String getLogoDefaultPath() {
        return new StringBuilder().append(Environment.getExternalStorageDirectory().getPath()).append("/VuScreen/video/logo").toString();
    }

    public String getVideoDefaultPath(String path) {
        return new StringBuilder().append(Environment.getExternalStorageDirectory().getPath()).append("/Dominos/content").append(path).toString();
    }

    public String getThumbnailDefaultPath(String path) {
        return new StringBuilder().append(Environment.getExternalStorageDirectory().getPath()).append("/VuScreen/video/thumbnail").append(path).toString();
    }

    public String getThumbnailDefaultStreamPath(String path) {
        return new StringBuilder().append("file://").append(Environment.getExternalStorageDirectory().getPath()).append("/Dominos/thumbnail").append(path).toString();
    }

    public String getLogoDefaultStreamPath(String path) {
        return new StringBuilder().append("file://").append(Environment.getExternalStorageDirectory().getPath()).append("/VuScreen/video/logo/").append(path).toString();
    }

    public List<EntityTableFolderItems> getAdItem(Context context) {
        try {
            return  getHelper(context).getFolderItemsTableOperations().getAdItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
        release();
        return new ArrayList<>();
    }

    public String getFolderName(Context context, int id) {
        try {
            return getHelper(context).getFolderTableOperations().getFolderName(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        release();
        return "";
    }

    public void insertTracking(Context context, EntityTableTracking entityTableTracking) {
        try {
            getHelper(context).getTrackingTableOperations().insertTracking(entityTableTracking);
        } catch (Exception e) {
            e.printStackTrace();
        }
        release();
    }

    private DatabaseHandler getHelper(Context context) {
        if (databaseHandler == null) {
            databaseHandler = OpenHelperManager.getHelper(context, DatabaseHandler.class);
        }
        return databaseHandler;
    }

    private void release() {
        if (databaseHandler != null) {
            OpenHelperManager.releaseHelper();
            databaseHandler = null;
        }
    }

    public EntityDataResponse getDataResponse() {
        return dataResponse;
    }

    public void setDataResponse(EntityDataResponse dataResponse) {
        this.dataResponse = dataResponse;
    }

    public EntityDataResponse getProductResponse() {
        return productResponse;
    }

    public void setProductResponse(EntityDataResponse productResponse) {
        this.productResponse = productResponse;
    }
}