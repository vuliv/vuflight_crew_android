package com.vuliv.network.server;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.vuliv.network.activity.ParentActivity;
import com.vuliv.network.database.DatabaseHandler;
import com.vuliv.network.database.sharedpref.SettingHelper;
import com.vuliv.network.database.tables.EntityTableFolderItems;
import com.vuliv.network.database.tables.EntityTableTracking;
import com.vuliv.network.entity.EntityBase;
import com.vuliv.network.entity.EntityDataResponse;
import com.vuliv.network.entity.EntityDominosRequest;
import com.vuliv.network.entity.EntityResponse;
import com.vuliv.network.entity.EntityTrackingRequest;
import com.vuliv.network.utility.StringUtils;
import com.vuliv.network.utility.Utility;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by user on 24-10-2017.
 */

public class TrackingController {
    private static final String TAG = "TrackingController";
    private static TrackingController instance = null;
    private static boolean tracking = false;
    private DatabaseHandler databaseHandler;
    private TrackingController(){}
    public static TrackingController getInstance() {
        if (instance == null) {
            instance = new TrackingController();
        }
        return instance;
    }

    private DatabaseHandler getHelper(Context context) {
        if (databaseHandler == null) {
            databaseHandler = OpenHelperManager.getHelper(context, DatabaseHandler.class);
        }
        return databaseHandler;
    }

    private void release() {
        if (databaseHandler != null) {
            OpenHelperManager.releaseHelper();
            databaseHandler = null;
        }
    }

    public void sendTracking (final Context context) {
        if (tracking) return;
        tracking = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<EntityTableTracking> tackingList = null;
                try {
                    tackingList = getHelper(context).getTrackingTableOperations().getTrackings();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (tackingList != null && tackingList.size() > 0) {
                    String response = RestClient.getInstance().postRequest(Utility.getUrlConfig().getTrackingUrl(), getRequest(context, tackingList));
                    if (!StringUtils.isEmpty(response)) {
                        response = response.replace("@Produces(\"application/json\")", "");
                        EntityResponse entityResponse = new Gson().fromJson(response, EntityResponse.class);
                        if (entityResponse != null && !StringUtils.isEmpty(entityResponse.getStatus()) && entityResponse.getStatus().equalsIgnoreCase("200")) {
                            try {
                                getHelper(context).getTrackingTableOperations().delete(tackingList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                tackingList = null;
                try {
                    tackingList = getHelper(context).getTrackingTableOperations().getInitializeTracking();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (tackingList != null && tackingList.size() > 0) {
                    String response = RestClient.getInstance().postRequest(Utility.getUrlConfig().getTrackingEventsUrl(), getRequest(context, tackingList));
                    if (!StringUtils.isEmpty(response)) {
                        response = response.replace("@Produces(\"application/json\")", "");
                        EntityResponse entityResponse = new Gson().fromJson(response, EntityResponse.class);
                        if (entityResponse != null && !StringUtils.isEmpty(entityResponse.getStatus()) && entityResponse.getStatus().equalsIgnoreCase("200")) {
                            try {
                                getHelper(context).getTrackingTableOperations().delete(tackingList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                release();
                tracking = false;
            }
        }).start();
    }
    public void startPurchaseTracking(final Context context, final EntityDominosRequest entityBase) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String request = getPurchaseTrackingRequest(context, entityBase);
                Log.wtf(TAG, request);
                String response = RestClient.getInstance().postRequest(Utility.getUrlConfig().getPurchaseTrackingUrl(), request);
            }
        }).start();
    }

    public void updateDownloadTracking (Context context, DatabaseHandler databaseHandler, String contentType, Integer contentId) {
        EntityTableTracking entityTableTracking = new EntityTableTracking();
        entityTableTracking.setUser(EntityTableTracking.TRACKING_USER_SERVER);
        entityTableTracking.setTrackingType(EntityTableTracking.TRACKING_TYPE_INITIALIZE);
        entityTableTracking.setStatus(EntityTableTracking.TRACKING_STATUS_DOWNLOAD);
        entityTableTracking.setSessionId(DataController.getInstance().getSessionId(context));
        entityTableTracking.setContentId(contentId);
        entityTableTracking.setContentType(contentType);
        try {
            entityTableTracking.setTimestamp(System.currentTimeMillis());
            databaseHandler.getTrackingTableOperations().insertTracking(entityTableTracking);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendTracking(context);
    }

    private String getRequest (Context context, List<EntityTableTracking> tackingList) {
        EntityTrackingRequest entityBase = new EntityTrackingRequest();
        entityBase.setModel(Utility.getDeviceModel());
        entityBase.setDeviceId(Utility.getAndroidId(context));
        entityBase.setVersion(Utility.getVersionName(context));
        entityBase.setVersionCode(Utility.getVersionCode(context));
        entityBase.setInterface(Utility.getInterface());
        entityBase.setPartner(SettingHelper.getPartnerId(context));
        entityBase.setPackageName(Utility.getPackageName(context));
        entityBase.setTracking(tackingList);
        entityBase.setRegistrationId(SettingHelper.getRegistrationKey(context));
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(entityBase, EntityTrackingRequest.class);
    }

    private static String getPurchaseTrackingRequest (Context context, EntityDominosRequest entityBase) {
        entityBase.setModel(Utility.getDeviceModel());
        entityBase.setDeviceId(Utility.getAndroidId(context));
        entityBase.setVersion(Utility.getVersionName(context));
        entityBase.setVersionCode(Utility.getVersionCode(context));
        entityBase.setInterface(Utility.getInterface());
        entityBase.setPartner(SettingHelper.getPartnerId(context));
        entityBase.setPackageName(Utility.getPackageName(context));
        entityBase.setRegistrationId(SettingHelper.getRegistrationKey(context));
//        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return new Gson().toJson(entityBase, EntityDominosRequest.class);
    }

    public void startStop(Context context, boolean start){
        EntityTableTracking entityTableTracking = new EntityTableTracking();
        entityTableTracking.setUser(EntityTableTracking.TRACKING_USER_SERVER);
        entityTableTracking.setTrackingType(EntityTableTracking.TRACKING_TYPE_INITIALIZE);

        if(!start){
            entityTableTracking.setStatus(EntityTableTracking.TRACKING_STATUS_STOP);
            entityTableTracking.setSessionId(DataController.getInstance().getSessionId(context));
            DataController.getInstance().setSessionId(null);
        } else {
            DataController.getInstance().setSessionId(null);
            entityTableTracking.setStatus(EntityTableTracking.TRACKING_STATUS_START);
            entityTableTracking.setSessionId(DataController.getInstance().getSessionId(context));
        }
        try {
            entityTableTracking.setTimestamp(System.currentTimeMillis());
            getHelper(context).getTrackingTableOperations().insertTracking(entityTableTracking);
            release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        TrackingController.getInstance().sendTracking(context);
    }
}
