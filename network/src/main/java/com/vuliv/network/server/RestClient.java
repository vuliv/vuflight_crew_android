package com.vuliv.network.server;

import com.vuliv.network.utility.StringUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

public class RestClient {
    public static final String HTTPS_STRING = "https";
    private static RestClient instance = null;
    public static final int HTTP_TIMEOUT = 1000 * 30; // milliseconds

    public static RestClient getInstance() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;
    }

    public String postRequest(String url, String jsonString) {

        StringBuffer response = null;
        try {
            URL mUrl = new URL(url);
            URLConnection con;
            if (url.startsWith(HTTPS_STRING)) {
                con = (HttpsURLConnection) mUrl.openConnection();
                ((HttpsURLConnection) con).setRequestMethod("POST");
            } else {
                con = (HttpURLConnection) mUrl.openConnection();
                ((HttpURLConnection) con).setRequestMethod("POST");
            }
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setDoInput(true);
            con.setConnectTimeout(HTTP_TIMEOUT);
            con.setReadTimeout(HTTP_TIMEOUT);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
            writer.write(jsonString);
            writer.close();
//            wr.writeBytes(jsonString);
//            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            if (con instanceof HttpsURLConnection) {
                if (((HttpsURLConnection) con).getResponseCode() != HttpsURLConnection.HTTP_OK) {
                    response = null;
                }
            } else if (con instanceof HttpURLConnection) {
                if (((HttpURLConnection) con).getResponseCode() != HttpURLConnection.HTTP_OK) {
                    response = null;
                }
            }
            if (con instanceof HttpsURLConnection) {
                ((HttpsURLConnection) con).disconnect();
            } else if (con instanceof HttpURLConnection) {
                ((HttpURLConnection) con).disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null && StringUtils.isResponseIsPage(response.toString())) {
                response = null;
            }
        }
        return (response != null) ? response.toString() : null;
    }
}