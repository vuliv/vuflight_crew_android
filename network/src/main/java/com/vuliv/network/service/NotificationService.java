package com.vuliv.network.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.vuliv.network.R;

/**
 * Created by user on 18-10-2017.
 */

public class NotificationService {

    private static final int DOWNLOAD_PROGRESS = 1214;

    private static NotificationManager getNotificationManager(Context context) {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static Notification.Builder getProgressNotification(Context context, String title) {
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification);


        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(title)
                .setContentText(context.getResources().getString(R.string.notification_download_progress))
                .setLargeIcon(bm)
//                .setContentIntent(contentIntent)
//                .addAction(R.drawable.media_cancel_icon, context.getString(R.string.cancel), cancelContent)
                .setSmallIcon(R.drawable.ic_notification);
        builder.setProgress(0, 0, true);
        Notification n = builder.build();
        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        ((Service) context).startForeground(DOWNLOAD_PROGRESS, n);
        return builder;
    }

    public static void updateProgressNotification(Context context, Notification.Builder builder, int progress, String progressText) {
        NotificationManager nm = getNotificationManager(context);
        builder.setProgress(100, progress, false);
        builder.setContentText(progressText);
//		if(progress == 100){
//			builder.setContentText(context.getResources().getString(R.string.notification_download_complete));
//		}
        Notification n = builder.build();
        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        nm.notify(DOWNLOAD_PROGRESS, n);
    }

    public static void dismissProgressNotification(Context context) {
        NotificationManager nm = getNotificationManager(context);
        nm.cancel(DOWNLOAD_PROGRESS);
    }
}
