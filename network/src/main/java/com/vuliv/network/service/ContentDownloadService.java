package com.vuliv.network.service;

import com.vuliv.network.callback.IDownloadCallback;
import com.vuliv.network.server.RestClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by user on 14-10-2017.
 */

public class ContentDownloadService {
//    private String extension = ".temp";

    public ContentDownloadService(){}

    public void startDownload(final IDownloadCallback callback, String fileName, String downloadURL, String filePath, Object object) {
        callback.onPreExecute();
        int count = 0;
        boolean manuallyStopped = false;
        File outputFile = null;
        try {
            File file = new File(filePath);
            file.mkdirs();
            long range = 0;
            outputFile = new File(file, fileName);
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            } else {
                range = outputFile.length();
            }

            URL url = new URL(downloadURL);
            URLConnection con;
            if (downloadURL.startsWith(RestClient.HTTPS_STRING)) {
                con = (HttpsURLConnection) url.openConnection();
            } else {
                con = (HttpURLConnection) url.openConnection();
            }
            con.setRequestProperty("Range", "bytes=" + range + "-");

            final long lenghtOfFile = (int) con.getContentLength() + range;

            FileOutputStream fileOutputStream = new FileOutputStream(outputFile, true);
            InputStream inputStream = con.getInputStream();

            byte[] buffer = new byte[1024];

            long total = range;
            while ((count = inputStream.read(buffer)) != -1) {
                total += count;
                fileOutputStream.write(buffer, 0, count);
                int progress = (int) (total * 100 / lenghtOfFile);
                callback.progress(progress);
            }
            fileOutputStream.close();
//            httpget.abort();
            if (con instanceof HttpsURLConnection) {
                ((HttpsURLConnection) con).disconnect();
            } else if (con instanceof HttpURLConnection) {
                ((HttpURLConnection) con).disconnect();
            }
            inputStream.close();
            if (lenghtOfFile == outputFile.length()) {
                callback.onSuccess(outputFile.getAbsolutePath(), object);
            } else {
                if (outputFile != null && outputFile.exists()) //outputFile.delete();
                    if (manuallyStopped) {
                        callback.cancelled(object);
                    } else {
                        callback.onFailure(object);
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (outputFile != null && outputFile.exists()) //outputFile.delete();
                if (manuallyStopped) {
                    callback.cancelled(object);
                } else {
                    callback.onFailure(object);
                }
        }
    }
}
