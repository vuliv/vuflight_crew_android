package com.vuliv.network.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.vuliv.network.utility.SharedPrefConstants;

/**
 * Created by MB0000021 on 8/24/2017.
 */

public class SharedPrefController {

    public static String PREF_NAME = "VuSharePref";

    private static SharedPreferences getSetting(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sp;
    }

    private static void setString(Context context, String key, String value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private static String getString(Context context, String key, String defaultValue) {
        SharedPreferences sp = getSetting(context);
        String value = sp.getString(key, defaultValue);
        return value;
    }

    private static int getInt(Context context, String key, int defaultValue) {
        SharedPreferences sp = getSetting(context);
        int value = sp.getInt(key, defaultValue);
        return value;
    }

    private static void setInt(Context context, String key, int value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private static long getLong(Context context, String key, long defaultValue) {
        SharedPreferences sp = getSetting(context);
        return sp.getLong(key, defaultValue);
    }

    private static void setLong(Context context, String key, long value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    private static boolean getBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences sp = getSetting(context);
        boolean value = sp.getBoolean(key, defaultValue);
        return value;
    }

    private static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void setUserName(Context context, String value){
        setString(context, SharedPrefConstants.USER_NAME, value);
    }

    public static String getUserName(Context context){
        return "VuScreen";
//        return com.vucast.utility.Utility.getHotspotName(context);
//        return getString(context, SharedPrefConstants.USER_NAME, "VuScreen_" + new Random().nextInt(10));
    }

    public static String getUserNameDefaultEmpty(Context context){
        return getString(context, SharedPrefConstants.USER_NAME, "");
    }

    public static void setUserAvatar(Context context, int value){
        setInt(context, SharedPrefConstants.USER_AVATAR, value);
    }

    public static int getUserAvatar(Context context){
        return getInt(context, SharedPrefConstants.USER_AVATAR, 0);
    }

    public static void setProfileCreated(Context context, boolean value){
        setBoolean(context, SharedPrefConstants.USER_PROFILE_CREATED, value);
    }

    public static boolean getProfileCreated(Context context){
        return getBoolean(context, SharedPrefConstants.USER_PROFILE_CREATED, false);
    }

    public static void setVehicleName(Context context, String value) {
        setString(context, SharedPrefConstants.VEHICLE_NAME, value);
    }

    public static String getVehicleName(Context context) {
        return getString(context, SharedPrefConstants.VEHICLE_NAME, "");
    }

    public static void setTripSource(Context context, String value) {
        setString(context, SharedPrefConstants.TRIP_SOURCE, value);
    }

    public static String getTripSource(Context context) {
        return getString(context, SharedPrefConstants.TRIP_SOURCE, "");
    }

    public static void setTripDestination(Context context, String value) {
        setString(context, SharedPrefConstants.TRIP_DESTINATION, value);
    }

    public static String getTripDestination(Context context) {
        return getString(context, SharedPrefConstants.TRIP_DESTINATION, "");
    }

    public static void setSeatStart(Context context, String value) {
        setString(context, SharedPrefConstants.SEAT_START, value);
    }

    public static String getSeatStart(Context context) {
        return getString(context, SharedPrefConstants.SEAT_START, "");
    }

    public static void setSeatEnd(Context context, String value) {
        setString(context, SharedPrefConstants.SEAT_END, value);
    }

    public static String getSeatEnd(Context context) {
        return getString(context, SharedPrefConstants.SEAT_END, "");
    }

    public static void setContentSync(Context context, long value) {
        setLong(context, SharedPrefConstants.CONTENT_SYNC, value);
    }

    public static long getContentSync(Context context) {
        return getLong(context, SharedPrefConstants.CONTENT_SYNC, 0);
    }
}
