package com.vuliv.network.service;

import android.app.Notification;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vuliv.network.callback.IDownloadCallback;
import com.vuliv.network.database.DatabaseHandler;
import com.vuliv.network.database.tables.EntityTableFolderItems;
import com.vuliv.network.database.tables.EntityTableLogo;
import com.vuliv.network.database.tables.EntityTableTracking;
import com.vuliv.network.server.DataController;
import com.vuliv.network.server.TrackingController;
import com.vuliv.network.utility.StringUtils;

import java.util.Calendar;
import java.util.List;

/**
 * Created by user on 14-10-2017.
 */

public class DownloadService extends ORMLiteIntentService<DatabaseHandler>{

    private long lastTime = 0;
    Notification.Builder builder;

    public DownloadService() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        /*Calendar time = Calendar.getInstance();
        time.setTimeInMillis(System.currentTimeMillis());

        int currentHour = time.get(Calendar.HOUR_OF_DAY);

        Log.wtf("DownloadService", "Hit time: " + SharedPrefController.getContentSync(this));

        if (((currentHour >= 4 && currentHour < 6) || (currentHour >= 15 && currentHour < 17)) &&
                (SharedPrefController.getContentSync(this) == 0 ||
                ((System.currentTimeMillis() - SharedPrefController.getContentSync(this)) >= (7 * 60 * 60 * 1000)))
            ) {
        try {
                DataController.getInstance().fetchLiveData(DownloadService.this);
                List<EntityTableFolderItems> notDownloadedFolderItems = getHelper().getFolderItemsTableOperations().getNotDownloadedFolderItems();
                ContentDownloadService downloadService = new ContentDownloadService();
                for (final EntityTableFolderItems item : notDownloadedFolderItems) {
                    if (!item.isImageDownload() && !StringUtils.isEmpty(item.getThumbnail())) {
                        downloadService.startDownload(new IDownloadCallback() {
                                                          @Override
                                                          public void onPreExecute() {
                                                              lastTime = 0;
                                                              builder = NotificationService.getProgressNotification(DownloadService.this, item.getName());
                                                          }

                                                          @Override
                                                          public void onSuccess(String path, Object object) {
                                                              NotificationService.dismissProgressNotification(DownloadService.this);
                                                              lastTime = 0;
                                                              EntityTableFolderItems folderItem = (EntityTableFolderItems) object;
                                                              folderItem.setImageDownload(true);
                                                              try {
                                                                  getHelper().getFolderItemsTableOperations().updateFolderItem(folderItem);
                                                              } catch (Exception e) {
                                                                  e.printStackTrace();
                                                              }
                                                              TrackingController.getInstance().updateDownloadTracking(DownloadService.this, getHelper(), EntityTableTracking.CONTENT_TYPE_IMAGE, Integer.parseInt(folderItem.getItemId()));
                                                          }

                                                          @Override
                                                          public void onFailure(Object object) {
                                                              lastTime = 0;
                                                              NotificationService.dismissProgressNotification(DownloadService.this);
                                                          }

                                                          @Override
                                                          public void cancelled(Object object) {
                                                              lastTime = 0;
                                                              NotificationService.dismissProgressNotification(DownloadService.this);
                                                          }

                                                          @Override
                                                          public void alreadyProgress() {
                                                          }

                                                          @Override
                                                          public void progress(int progress) {
                                                              if ((System.currentTimeMillis() - lastTime) >= 500) {
                                                                  lastTime = System.currentTimeMillis();
                                                                  NotificationService.updateProgressNotification(DownloadService.this, builder, progress, "In progress");
                                                              }
                                                          }
                                                      },
                                item.getItemId(),
                                item.getThumbnail(),
                                DataController.getInstance().getThumbnailDefaultPath("/"),
                                item);
                    }
                    if (!item.isVideoDownload() && !StringUtils.isEmpty(item.getUrl())) {
                        downloadService.startDownload(new IDownloadCallback() {
                                                          @Override
                                                          public void onPreExecute() {
                                                              lastTime = 0;
                                                              builder = NotificationService.getProgressNotification(DownloadService.this, item.getName());
                                                          }

                                                          @Override
                                                          public void onSuccess(String path, Object object) {
                                                              NotificationService.dismissProgressNotification(DownloadService.this);
                                                              lastTime = 0;
                                                              EntityTableFolderItems folderItem = (EntityTableFolderItems) object;
                                                              folderItem.setVideoDownload(true);
                                                              try {
                                                                  getHelper().getFolderItemsTableOperations().updateFolderItem(folderItem);
                                                              } catch (Exception e) {
                                                                  e.printStackTrace();
                                                              }
                                                              TrackingController.getInstance().updateDownloadTracking(DownloadService.this, getHelper(), EntityTableTracking.CONTENT_TYPE_VIDEO, Integer.parseInt(folderItem.getItemId()));
                                                          }

                                                          @Override
                                                          public void onFailure(Object object) {
                                                              lastTime = 0;
                                                              NotificationService.dismissProgressNotification(DownloadService.this);
                                                          }

                                                          @Override
                                                          public void cancelled(Object object) {
                                                              lastTime = 0;
                                                              NotificationService.dismissProgressNotification(DownloadService.this);
                                                          }

                                                          @Override
                                                          public void alreadyProgress() {

                                                          }

                                                          @Override
                                                          public void progress(int progress) {
                                                              if ((System.currentTimeMillis() - lastTime) >= 500) {
                                                                  lastTime = System.currentTimeMillis();
                                                                  NotificationService.updateProgressNotification(DownloadService.this, builder, progress, "In progress");
                                                              }
                                                          }
                                                      },
                                item.getItemId(),
                                item.getUrl(),
                                DataController.getInstance().getVideoDefaultPath("/"),
                                item);
                    }
                }
                List<EntityTableLogo> notDownloadedData = getHelper().getLogoTableOperations().getNotDownloadedData();
                for (final EntityTableLogo entityTableLogo : notDownloadedData) {
                    downloadService.startDownload(new IDownloadCallback() {
                                                      @Override
                                                      public void onPreExecute() {
                                                          lastTime = 0;
                                                          builder = NotificationService.getProgressNotification(DownloadService.this, entityTableLogo.getUrl());
                                                      }

                                                      @Override
                                                      public void onSuccess(String path, Object object) {
                                                          NotificationService.dismissProgressNotification(DownloadService.this);
                                                          lastTime = 0;
                                                          EntityTableLogo item = (EntityTableLogo) object;
                                                          item.setDownload(true);
                                                          try {
                                                              getHelper().getLogoTableOperations().updateData(item);
                                                          } catch (Exception e) {
                                                              e.printStackTrace();
                                                          }
//                          TrackingController.getInstance().updateDownloadTracking(DownloadService.this, getHelper(), EntityTableTracking.CONTENT_TYPE_LOGO, Integer.parseInt(folderItem.getItemId()));
                                                      }

                                                      @Override
                                                      public void onFailure(Object object) {
                                                          lastTime = 0;
                                                          NotificationService.dismissProgressNotification(DownloadService.this);
                                                      }

                                                      @Override
                                                      public void cancelled(Object object) {
                                                          lastTime = 0;
                                                          NotificationService.dismissProgressNotification(DownloadService.this);
                                                      }

                                                      @Override
                                                      public void alreadyProgress() {

                                                      }

                                                      @Override
                                                      public void progress(int progress) {
                                                          if ((System.currentTimeMillis() - lastTime) >= 500) {
                                                              lastTime = System.currentTimeMillis();
                                                              NotificationService.updateProgressNotification(DownloadService.this, builder, progress, "In progress");
                                                          }
                                                      }
                                                  },
                            StringUtils.getLogoName(entityTableLogo.getUrl()),
                            entityTableLogo.getUrl(),
                            DataController.getInstance().getLogoDefaultPath(),
                            entityTableLogo);
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataController.folderTreeMapMobile.clear();
            DataController.foldersNameMobile.clear();
            DataController.folderTreeMapWeb.clear();
            DataController.foldersNameWeb.clear();
            DataController.getInstance().getData(this, EntityTableFolderItems.PLATFORM_MOBILE);
            DataController.getInstance().getData(this, EntityTableFolderItems.PLATFORM_WEB);
        }
        }*/
    }
}
