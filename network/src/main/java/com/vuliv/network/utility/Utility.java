package com.vuliv.network.utility;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import com.vuliv.network.configuration.ConfigEnum;
import com.vuliv.network.configuration.DeveloperConfig;
import com.vuliv.network.configuration.IUrlConfiguration;
import com.vuliv.network.configuration.ProductionConfig;
import com.vuliv.network.server.DataController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import static com.vuliv.network.utility.Constants.USERMACS;

/**
 * Created by user on 16-10-2017.
 */

public class Utility {

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private static ConfigEnum getConfiguration() {
//         return ConfigEnum.PRODUCTION;
        return ConfigEnum.DEVELOPER;
    }

    public static IUrlConfiguration getUrlConfig() {
        if (getConfiguration() == ConfigEnum.DEVELOPER) {
            return new DeveloperConfig();
        } else if (getConfiguration() == ConfigEnum.PRODUCTION) {
            return new ProductionConfig();
        }
        return null;
    }

    public static String getDeviceModel () {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String manModel = "";
        if (model.startsWith(manufacturer)) {
            manModel = model.toUpperCase(Locale.getDefault());
        } else {
            manModel = manufacturer.toUpperCase() + "" + model.toUpperCase();
        }
        if (manModel.contains(" ")) {
            manModel = manModel.replaceAll(" ", "");
        }
        return manModel;
    }

    public static String getAndroidId (Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getInterface () {
        return "AN";
    }

    public static String getPartner () {
        return "Shuttl";
//        return "Uber VuScreen";
    }

    public static int getVersionCode (Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (Exception e) {
        }
        return 0;
    }

    public static String getVersionName (Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionName;
        } catch (Exception e) {
        }
        return "";
    }

    public static String getPackageName (Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.packageName;
        } catch (Exception e) {
        }
        return "";
    }

    /**
     * @return if time is 15:30 will give 1530
     */
    public static int getTimeForMomentOfDay() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        String minute = minutes + "";
        if ((minute).length() == 1) {
            minute = "0" + minute;
        }
        String time = hour + "" + minute;
        return Integer.parseInt(time);
    }

    public static void deleteCampaignFile(String id) {
        try {
            File f = new File(DataController.getInstance().getThumbnailDefaultPath("/") + id);
            if (f.exists()) {
                f.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            File f = new File(DataController.getInstance().getVideoDefaultPath("/") + id);
            if (f.exists()) {
                f.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, String> readAddresses() {
        USERMACS.clear();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("/proc/net/arp"));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4) {
                    String ip = splitted[0];
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {
                        USERMACS.put(ip, mac);
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return USERMACS;
    }
}
