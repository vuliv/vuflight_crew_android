package com.vuliv.network.utility;

/**
 * Created by MB0000021 on 8/24/2017.
 */

public class SharedPrefConstants {

    public static final String USER_NAME = "user_name";
    public static final String USER_AVATAR = "user_avatar";
    public static final String USER_PROFILE_CREATED = "user_profile_created";
    public static final String VEHICLE_NAME = "vehicle_name";
    public static final String TRIP_SOURCE = "trip_source";
    public static final String TRIP_DESTINATION = "trip_destination";
    public static final String SEAT_START = "seat_start";
    public static final String SEAT_END = "seat_end";
    public static final String CONTENT_SYNC = "content_sync";
}
