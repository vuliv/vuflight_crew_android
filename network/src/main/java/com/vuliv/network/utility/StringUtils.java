package com.vuliv.network.utility;

/**
 * Created by user on 16-10-2017.
 */

public class StringUtils {

    /**
     * Checks whether the passed string is having some text or just a null string.
     *
     * @param text
     * @return
     */
    public static boolean isEmpty(String text) {
        boolean isEmpty = true;
        if (text != null && text.trim().length() > 0) {
            isEmpty = false;
        }
        return isEmpty;
    }

    /**
     * Check whether the response is a html page or not (Mostly in the case of Limited Network)
     *
     * @param response
     * @return
     */
    public static boolean isResponseIsPage(String response) {
        boolean page = false;
        if (response != null && response.trim().length() > 0) {
            if (response.trim().startsWith("<html>") || response.trim().startsWith("<HTML>")
                    || response.trim().startsWith("<!DOCTYPE") || response.trim().endsWith("<HTML>")
                    || response.trim().endsWith("<html>")) {
                page = true;
            }
        } else {
            page = true;
        }
        return page;
    }

    public static String getLogoName(String url) {
        if (!isEmpty(url)) {
            url = url.substring(url.lastIndexOf("/") + 1);
            String[] split = url.split("\\.");
            if (split.length > 1) {
                return split[0];
            }
        }
        return "";
    }

}
