package com.vuliv.network.receiver;

import android.annotation.TargetApi;
import android.app.admin.DevicePolicyManager;
import android.app.admin.SystemUpdatePolicy;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.UserManager;
import android.provider.Settings;

import com.vuliv.network.activity.ParentActivity;

/**
 * Created by User on 11/7/2017.
 */

public class AdminController {

    /**
     * Set admin policies
     * @param active
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static void setAdminPolicies(Context context, DevicePolicyManager devicePolicyManager,
                                        ComponentName componentName, boolean active) {
        //set user-restrictions
        setUserRestriction(devicePolicyManager, componentName, UserManager.DISALLOW_SAFE_BOOT, active);
        setUserRestriction(devicePolicyManager, componentName, UserManager.DISALLOW_FACTORY_RESET, active);
        setUserRestriction(devicePolicyManager, componentName, UserManager.DISALLOW_ADD_USER, active);
        setUserRestriction(devicePolicyManager, componentName, UserManager.DISALLOW_MOUNT_PHYSICAL_MEDIA, active);
        setUserRestriction(devicePolicyManager, componentName, UserManager.DISALLOW_ADJUST_VOLUME, active);

        //Disable keyguard and status bar
        devicePolicyManager.setKeyguardDisabled(componentName, active);
        devicePolicyManager.setKeyguardDisabled(componentName, active);

        //enable STAY_ON_WHILE_PLUGGED_IN
        enableStayOnWhilePluggedIn(devicePolicyManager, componentName, active);

        //set system update policy
        if (active) {
            devicePolicyManager.setSystemUpdatePolicy(componentName, SystemUpdatePolicy.createWindowedInstallPolicy(60,120));
        } else {
            devicePolicyManager.setSystemUpdatePolicy(componentName,null);
        }

        //set this activity as a lock task package
        devicePolicyManager.setLockTaskPackages(componentName, active ? new String[]{context.getPackageName()} : new String[]{});

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MAIN);
        intentFilter.addCategory(Intent.CATEGORY_HOME);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        if (active) {
            //set the Parent activity as home intent so that it is started on reboot
            devicePolicyManager.addPersistentPreferredActivity(componentName, intentFilter,
                    new ComponentName(context.getPackageName(), ParentActivity.class.getName()));
        } else {
            devicePolicyManager.clearPackagePersistentPreferredActivities(componentName, context.getPackageName());
        }

    }

    /**
     * Set user restrictions
     * @param restriction
     * @param disallow
     */
    private static void setUserRestriction(DevicePolicyManager devicePolicyManager, ComponentName componentName,
                                    String restriction, boolean disallow) {
        if (disallow) {
            devicePolicyManager.addUserRestriction(componentName, restriction);
        } else {
            devicePolicyManager.clearUserRestriction(componentName, restriction);
        }
    }

    /**
     * Keep the phone on when plugged in
     * @param enabled
     */
    private static void enableStayOnWhilePluggedIn(DevicePolicyManager devicePolicyManager, ComponentName componentName,
                                            boolean enabled) {
        if (enabled) {
            devicePolicyManager.setGlobalSetting(
                    componentName,
                    Settings.Global.STAY_ON_WHILE_PLUGGED_IN,
                    Integer.toString(BatteryManager.BATTERY_PLUGGED_AC
                            | BatteryManager.BATTERY_PLUGGED_USB
                            | BatteryManager.BATTERY_PLUGGED_WIRELESS));
        } else {
            devicePolicyManager.setGlobalSetting(
                    componentName,
                    Settings.Global.STAY_ON_WHILE_PLUGGED_IN,
                    "0"
            );
        }

    }
}
