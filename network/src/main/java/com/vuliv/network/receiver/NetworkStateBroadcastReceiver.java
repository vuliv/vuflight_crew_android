package com.vuliv.network.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vuliv.network.server.TrackingController;
import com.vuliv.network.service.DownloadService;

/**
 * Created by user on 31-10-2017.
 */

public class NetworkStateBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, DownloadService.class));
        TrackingController.getInstance().sendTracking(context);
    }
}
