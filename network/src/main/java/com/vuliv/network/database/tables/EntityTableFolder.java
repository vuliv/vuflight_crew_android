package com.vuliv.network.database.tables;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Folder")
public class EntityTableFolder {
    public static final String FIELD_FOLDER_ID = "folder_id";
    public static final String FIELD_POSITION = "position";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_VTYPE = "vtype";
    public static final String FIELD_ENABLE = "enable";

    @DatabaseField(generatedId = true)
    @SerializedName("_id")
    private Integer id;

    @DatabaseField(columnName = FIELD_FOLDER_ID)
    @SerializedName("id")
    private int folderId;

    @DatabaseField(columnName = FIELD_POSITION)
    @SerializedName("position")
    private int position;

    @DatabaseField(columnName = FIELD_NAME)
    @SerializedName("name")
    private String name;

    @DatabaseField(columnName = FIELD_VTYPE)
    @SerializedName("vtype")
    private String vtype;

    @DatabaseField(columnName = FIELD_ENABLE)
    @SerializedName("enable")
    private boolean enable;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFolderId() {
        return this.folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVtype() {
        return this.vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    public boolean isEnable() {
        return this.enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}