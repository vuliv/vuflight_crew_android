package com.vuliv.network.database;

public interface DatabaseConstants {
    String DATABASE_NAME = "vuscreen_ormlite.db";
    int DATABASE_VERSION = 1;
}