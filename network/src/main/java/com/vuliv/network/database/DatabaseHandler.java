package com.vuliv.network.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.vuliv.network.R;
import com.vuliv.network.database.tables.EntityTableFolder;
import com.vuliv.network.database.tables.EntityTableFolderItems;
import com.vuliv.network.database.tables.EntityTableLogo;
import com.vuliv.network.database.tables.EntityTableTracking;

public class DatabaseHandler extends OrmLiteSqliteOpenHelper
        implements DatabaseConstants {
    private FolderItemsTableOperations folderItemsTableOperations = new FolderItemsTableOperations(this);
    private FolderTableOperations folderTableOperations = new FolderTableOperations(this);
    private TrackingTableOperations trackingTableOperations = new TrackingTableOperations(this);
    private LogoTableOperations logoTableOperations = new LogoTableOperations(this);

    public DatabaseHandler(Context context) {
        super(context, "vuscreen_ormlite.db", null, 1, R.raw.vuscreen_ormlite_config);
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, EntityTableFolder.class);
            TableUtils.createTable(connectionSource, EntityTableFolderItems.class);
            TableUtils.createTable(connectionSource, EntityTableTracking.class);
            TableUtils.createTable(connectionSource, EntityTableLogo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
    }

    public FolderItemsTableOperations getFolderItemsTableOperations() {
        return this.folderItemsTableOperations;
    }

    public FolderTableOperations getFolderTableOperations() {
        return this.folderTableOperations;
    }

    public TrackingTableOperations getTrackingTableOperations() {
        return trackingTableOperations;
    }

    public LogoTableOperations getLogoTableOperations() {
        return logoTableOperations;
    }
}