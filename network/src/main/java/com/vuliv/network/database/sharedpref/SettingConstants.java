package com.vuliv.network.database.sharedpref;

public interface SettingConstants {
    String PFER_NAME = "vuscreen";
    String FIRST_DATA_TRANSFER = "vuscreen_first_data_transfer";
}