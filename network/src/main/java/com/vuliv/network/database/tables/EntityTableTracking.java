package com.vuliv.network.database.tables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Tracking")
public class EntityTableTracking {
    public static final String FIELD_ID = "_id";
    public static final String FIELD_CAMP_ID = "camp_id";
    public static final String FIELD_DURATION = "duration";
    public static final String FIELD_USER_AGENT = "user_agent";
    public static final String FIELD_MODEL = "model";
    public static final String FIELD_ANDROID_ID = "android_id";
    public static final String FIELD_TIMESTAMP = "timestamp";
    public static final String FIELD_IP = "ip";
    public static final String FIELD_SESSION_ID = "session_id";
    public static final String FIELD_STATUS = "status";
    public static final String FIELD_USER = "user";
    public static final String FIELD_MAC = "mac";
    public static final String FIELD_TRACKING_TYPE = "tracking_type";
    public static final String FIELD_TRACKING_CONTENT_TYPE = "content_type";
    public static final String FIELD_TRACKING_CONTENT_ID = "content_id";
    public static final String FIELD_NAME = "name";

    public static final String TRACKING_TYPE_EVENT = "event";
    public static final String TRACKING_TYPE_INITIALIZE = "initialize";
    public static final String TRACKING_USER_SERVER = "server";
    public static final String TRACKING_USER_CLIENT = "client";
    public static final String TRACKING_STATUS_START = "start";
    public static final String TRACKING_STATUS_STOP = "stop";
    public static final String TRACKING_STATUS_DOWNLOAD = "download";
    public static final String TRACKING_STATUS_REVIEW = "review";
    public static final String TRACKING_STATUS_CONNECTED = "connected";
    public static final String TRACKING_STATUS_DISCONNECTED = "disconnected";

    public static final String CONTENT_TYPE_IMAGE = "image";
    public static final String CONTENT_TYPE_VIDEO = "video";
    public static final String CONTENT_TYPE_LOGO = "logo";

    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    @SerializedName(FIELD_ID)
    private Integer id;

    @DatabaseField(columnName = FIELD_CAMP_ID)
    @SerializedName("id")
    @Expose private Integer campId;

    @DatabaseField(columnName = FIELD_DURATION)
    @SerializedName("duration")
    @Expose private Integer duration;

    @DatabaseField(columnName = FIELD_USER_AGENT)
    @SerializedName("user_agent")
    @Expose private String userAgent;

    @DatabaseField(columnName = FIELD_MODEL)
    @SerializedName("model")
    @Expose private String model;

    @DatabaseField(columnName = FIELD_ANDROID_ID)
    @SerializedName("android_id")
    @Expose private String androidId;

    @DatabaseField(columnName = FIELD_TIMESTAMP)
    @SerializedName("timestamp")
    @Expose private Long timestamp;

    @DatabaseField(columnName = FIELD_IP)
    @SerializedName("ip")
    @Expose private String ip;

    @DatabaseField(columnName = FIELD_SESSION_ID)
    @SerializedName("session_id")
    @Expose private String sessionId;

    @DatabaseField(columnName = FIELD_STATUS)
    @SerializedName("status")
    @Expose private String status;

    @DatabaseField(columnName = FIELD_USER)
    @SerializedName("user")
    @Expose private String user;

    @DatabaseField(columnName = FIELD_TRACKING_CONTENT_TYPE)
    @SerializedName("content_type")
    @Expose private String contentType;

    @DatabaseField(columnName = FIELD_TRACKING_CONTENT_ID)
    @SerializedName("content_id")
    @Expose private Integer contentId;

    @DatabaseField(columnName = FIELD_NAME)
    @SerializedName("name")
    @Expose private String name;

    @DatabaseField(columnName = FIELD_MAC)
    @SerializedName("mac")
    @Expose private String mac;

    @DatabaseField(columnName = FIELD_TRACKING_TYPE)
    private String trackingType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCampId() {
        return campId;
    }

    public void setCampId(Integer campId) {
        this.campId = campId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTrackingType() {
        return trackingType;
    }

    public void setTrackingType(String tracking_type) {
        this.trackingType = tracking_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}