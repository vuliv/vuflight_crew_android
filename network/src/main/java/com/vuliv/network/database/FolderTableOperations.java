package com.vuliv.network.database;

import android.support.annotation.NonNull;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.vuliv.network.database.tables.EntityTableFolder;

import java.sql.SQLException;
import java.util.List;

public class FolderTableOperations {
    private Dao<EntityTableFolder, Integer> entityTableFolders;
    private OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper;

    public FolderTableOperations(OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper) {
        this.ormLiteSqliteOpenHelper = ormLiteSqliteOpenHelper;
    }

    @NonNull
    public Dao<EntityTableFolder, Integer> getEntityTableBanner() throws SQLException {
        if (this.entityTableFolders == null) {
            this.entityTableFolders = this.ormLiteSqliteOpenHelper.getDao(EntityTableFolder.class);
        }
        return this.entityTableFolders;
    }

    public void resetFolders() throws SQLException {
        UpdateBuilder updateBuilder = getEntityTableBanner().updateBuilder();
        updateBuilder.updateColumnValue(EntityTableFolder.FIELD_ENABLE, Boolean.valueOf(false)).update();
    }

    public void deleteDisabledFolders() throws SQLException {
        Dao dao = getEntityTableBanner();
        DeleteBuilder deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq(EntityTableFolder.FIELD_ENABLE, Boolean.valueOf(false));
        deleteBuilder.delete();
    }

    public void insertFolders(List<EntityTableFolder> banners) throws SQLException {
        Dao dao = getEntityTableBanner();
        QueryBuilder builder = dao.queryBuilder();
        List list = dao.query(builder.prepare());
        if (list.size() == 0) {
            for (EntityTableFolder banner : banners) {
                banner.setEnable(true);
            }
            dao.create(banners);
        } else {
            for (EntityTableFolder banner : banners) {
                builder.where().eq(EntityTableFolder.FIELD_FOLDER_ID, Integer.valueOf(banner.getFolderId()));
                list = dao.query(builder.prepare());
                if (list.size() == 0) {
                    banner.setEnable(true);
                    dao.create(banner);
                } else {
                    EntityTableFolder banner1 = (EntityTableFolder) list.get(0);
                    banner1.setFolderId(banner.getFolderId());
                    banner1.setName(banner.getName());
                    banner1.setVtype(banner.getVtype());
                    banner1.setPosition(banner.getPosition());
                    banner1.setEnable(true);
                    dao.update(banner1);
                }
            }
        }
    }

    public List<EntityTableFolder> getFolders() throws SQLException {
        Dao dao = getEntityTableBanner();
        QueryBuilder builder = dao.queryBuilder();
        builder.orderBy(EntityTableFolder.FIELD_POSITION, true);
        List list = dao.query(builder.prepare());
        return list;
    }

    public String getFolderName(int id) throws SQLException {
        Dao dao = getEntityTableBanner();
        QueryBuilder builder = dao.queryBuilder();
        builder.where().eq(EntityTableFolder.FIELD_FOLDER_ID, id);
        List<EntityTableFolder> list = dao.query(builder.prepare());
        return list.get(0).getName();
    }
}