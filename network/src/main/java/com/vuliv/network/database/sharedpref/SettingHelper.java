package com.vuliv.network.database.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingHelper
        implements SettingConstants {
    private static SharedPreferences getSetting(Context context) {
        SharedPreferences sp = context.getSharedPreferences("vuscreen", 0);
        return sp;
    }

    public static void setFirstDataTransfer(Context context, boolean value) {
        setBoolean(context, "vuscreen_first_data_transfer", value);
    }

    public static boolean isFirstDataTransfer(Context context) {
        return getBoolean(context, "vuscreen_first_data_transfer");
    }

    public static void setRegistrationKey(Context context, String value) {
        setString(context, "vuscreen_registration", value);
    }

    public static String getRegistrationKey(Context context) {
        return getString(context, "vuscreen_registration");
    }

    public static String getPartnerId(Context context) {
        return getString(context, "vuscreen_partner_id");
    }

    public static void setPartnerId(Context context, String value) {
        setString(context, "vuscreen_partner_id", value);
    }

    private static void setString(Context context, String key, String value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private static String getString(Context context, String key) {
        SharedPreferences sp = getSetting(context);
        String value = sp.getString(key, null);
        return value;
    }

    private static void setLong(Context context, String key, Long value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(key, value.longValue());
        editor.commit();
    }

    private static Long getLong(Context context, String key) {
        SharedPreferences sp = getSetting(context);
        Long value = Long.valueOf(sp.getLong(key, 0L));
        return value;
    }

    private static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private static boolean getBoolean(Context context, String key) {
        SharedPreferences sp = getSetting(context);
        boolean value = sp.getBoolean(key, false);
        return value;
    }

    private static void setInt(Context context, String key, int value) {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private static int getInt(Context context, String key) {
        SharedPreferences sp = getSetting(context);
        int value = sp.getInt(key, 0);
        return value;
    }
}