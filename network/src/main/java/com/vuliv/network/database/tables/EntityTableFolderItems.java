package com.vuliv.network.database.tables;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "FolderItems")
public class EntityTableFolderItems {
    public static final String FIELD_ID = "_id";
    public static final String FIELD_ITEM_ID = "item_id";
    public static final String FIELD_FOLDER_ID = "folder_id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_THUMBNAIL = "thumbnail";
    public static final String FIELD_URL = "url";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_EXPIRY = "expiry";
    public static final String FIELD_START_TIME_EXPIRY = "startTime";
    public static final String FIELD_END_TIME_EXPIRY = "endTime";
    public static final String FIELD_DURATION = "duration";
    public static final String FIELD_LOGO = "logo";
    public static final String FIELD_IMAGE_DOWNLOAD = "imageDownload";
    public static final String FIELD_VIDEO_DOWNLOAD = "videoDownload";
    public static final String FIELD_PLATFORM = "platform";

    public static final String PLATFORM_MOBILE = "mobile";
    public static final String PLATFORM_WEB = "web";
    public static final String PLATFORM_BOTH = "both";

    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_AD = "ad";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_AUDIO = "audio";

    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    @SerializedName(FIELD_ID)
    private Integer id;

    @DatabaseField(columnName = FIELD_ITEM_ID)
    @SerializedName("id")
    private String itemId;

    @DatabaseField(columnName = FIELD_FOLDER_ID)
    @SerializedName("folderId")
    private int folderId;

    @DatabaseField(columnName = FIELD_NAME)
    @SerializedName("name")
    private String name;

    @DatabaseField(columnName = FIELD_THUMBNAIL)
    @SerializedName("thumbnail")
    private String thumbnail;

    @DatabaseField(columnName = FIELD_URL)
    @SerializedName("url")
    private String url;

    @DatabaseField(columnName = FIELD_TYPE)
    @SerializedName("type")
    private String type;

    @DatabaseField(columnName = FIELD_EXPIRY)
    @SerializedName("expiry")
    private long expiry;

    @DatabaseField(columnName = FIELD_START_TIME_EXPIRY)
    @SerializedName("start_time")
    private long startTime;

    @DatabaseField(columnName = FIELD_END_TIME_EXPIRY)
    @SerializedName("end_time")
    private long endTime;

    @DatabaseField(columnName = FIELD_DURATION)
    @SerializedName("duration")
    private long duration;

    @DatabaseField(columnName = FIELD_LOGO)
    @SerializedName("logo")
    private String logo;

    @DatabaseField(columnName = FIELD_IMAGE_DOWNLOAD)
    @SerializedName("imageDownload")
    private boolean imageDownload;

    @DatabaseField(columnName = FIELD_VIDEO_DOWNLOAD)
    @SerializedName("videoDownload")
    private boolean videoDownload;

    @DatabaseField(columnName = FIELD_PLATFORM)
    @SerializedName("platform")
    private String platform;

    public String getItemId() {
        return this.itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getFolderId() {
        return this.folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getExpiry() {
        return this.expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isImageDownload() {
        return this.imageDownload;
    }

    public void setImageDownload(boolean imageDownload) {
        this.imageDownload = imageDownload;
    }

    public boolean isVideoDownload() {
        return this.videoDownload;
    }

    public void setVideoDownload(boolean videoDownload) {
        this.videoDownload = videoDownload;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}