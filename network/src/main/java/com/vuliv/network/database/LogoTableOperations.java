package com.vuliv.network.database;

import android.support.annotation.NonNull;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.vuliv.network.database.tables.EntityTableLogo;

import java.sql.SQLException;
import java.util.List;

public class LogoTableOperations {
    private Dao<EntityTableLogo, Integer> entityTableFolderItems;
    private OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper;

    public LogoTableOperations(OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper) {
        this.ormLiteSqliteOpenHelper = ormLiteSqliteOpenHelper;
    }

    @NonNull
    public Dao<EntityTableLogo, Integer> getData() throws SQLException {
        if (this.entityTableFolderItems == null) {
            this.entityTableFolderItems = this.ormLiteSqliteOpenHelper.getDao(EntityTableLogo.class);
        }
        return this.entityTableFolderItems;
    }

    public void deleteDisabledData() throws SQLException {
        Dao dao = getData();
        DeleteBuilder deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq(EntityTableLogo.FIELD_ENABLE, Boolean.valueOf(false));
        deleteBuilder.delete();
    }

    public void resetData() throws SQLException {
        UpdateBuilder updateBuilder = getData().updateBuilder();
        updateBuilder.updateColumnValue(EntityTableLogo.FIELD_ENABLE, Boolean.valueOf(false)).update();
    }

    public void insertData(EntityTableLogo tableLogo) throws SQLException {
        Dao dao = getData();
        QueryBuilder builder = dao.queryBuilder();
        List list = dao.query(builder.prepare());
        if (list.size() == 0) {
            tableLogo.setEnable(true);
            dao.create(tableLogo);
        } else {
            builder.where().eq(EntityTableLogo.FIELD_URL, tableLogo.getUrl());
            list = dao.query(builder.prepare());
            if (list.size() == 0) {
                tableLogo.setEnable(true);
                dao.create(tableLogo);
            } else {
                EntityTableLogo banner1 = (EntityTableLogo) list.get(0);
                banner1.setUrl(tableLogo.getUrl());
                banner1.setEnable(true);
                dao.update(banner1);
            }
        }
    }

    public List<EntityTableLogo> getNotDownloadedData() throws SQLException {
        Dao dao = getData();
        QueryBuilder builder = dao.queryBuilder();
        builder.where().eq(EntityTableLogo.FIELD_DOWNLOAD, Boolean.valueOf(false));
        List list = dao.query(builder.prepare());
        return list;
    }

    public boolean isDownloaded(String url) throws SQLException {
        Dao dao = getData();
        QueryBuilder builder = dao.queryBuilder();
        builder.where()
                .eq(EntityTableLogo.FIELD_URL, url)
                .and().eq(EntityTableLogo.FIELD_DOWNLOAD, Boolean.valueOf(true));
        List list = dao.query(builder.prepare());
        return (list.size() > 0) ? true : false;
    }

    public void updateData(EntityTableLogo entityTableFolderItems) throws SQLException {
        Dao dao = getData();
        dao.update(entityTableFolderItems);
    }
}