package com.vuliv.network.database;

import android.support.annotation.NonNull;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.vuliv.network.database.tables.EntityTableFolder;
import com.vuliv.network.database.tables.EntityTableTracking;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TrackingTableOperations {
    private Dao<EntityTableTracking, Integer> entityTableFolders;
    private OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper;

    public TrackingTableOperations(OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper) {
        this.ormLiteSqliteOpenHelper = ormLiteSqliteOpenHelper;
    }

    @NonNull
    public Dao<EntityTableTracking, Integer> getEntityTableTracking() throws SQLException {
        if (this.entityTableFolders == null) {
            this.entityTableFolders = this.ormLiteSqliteOpenHelper.getDao(EntityTableTracking.class);
        }
        return this.entityTableFolders;
    }

    public void insertTracking(EntityTableTracking entityTableTracking) throws SQLException {
        Dao dao = getEntityTableTracking();
        dao.create(entityTableTracking);
    }

    public List<EntityTableTracking> getTrackings() throws SQLException {
        Dao dao = getEntityTableTracking();
        QueryBuilder builder = dao.queryBuilder();
        builder.where().eq(EntityTableTracking.FIELD_TRACKING_TYPE, EntityTableTracking.TRACKING_TYPE_EVENT);
        List<EntityTableTracking> list = dao.query(builder.prepare());
        return list;
    }

    public List<EntityTableTracking> getInitializeTracking() throws SQLException {
        Dao dao = getEntityTableTracking();
        QueryBuilder builder = dao.queryBuilder();
        builder.where().eq(EntityTableTracking.FIELD_TRACKING_TYPE, EntityTableTracking.TRACKING_TYPE_INITIALIZE);
        List<EntityTableTracking> list = dao.query(builder.prepare());
        return list;
    }

    public void delete(List<EntityTableTracking> trackingList) throws SQLException {
        ArrayList<Integer> idsList = new ArrayList<>();
        for (EntityTableTracking entityTableTracking : trackingList) {
            idsList.add(entityTableTracking.getId());
        }
        Dao dao = getEntityTableTracking();
        DeleteBuilder builder = dao.deleteBuilder();
        builder.where().in(EntityTableTracking.FIELD_ID, idsList);
        builder.delete();
    }
}