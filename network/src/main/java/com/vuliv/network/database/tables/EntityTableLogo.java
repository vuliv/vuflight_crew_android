package com.vuliv.network.database.tables;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Logo")
public class EntityTableLogo {
    public static final String FIELD_ID = "_id";
    public static final String FIELD_URL = "url";
    public static final String FIELD_ENABLE = "enable";
    public static final String FIELD_DOWNLOAD = "download";

    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    @SerializedName(FIELD_ID)
    private Integer id;

    @DatabaseField(columnName = FIELD_URL)
    @SerializedName("url")
    private String url;

    @DatabaseField(columnName = FIELD_ENABLE)
    @SerializedName("enable")
    private boolean enable;

    @DatabaseField(columnName = FIELD_DOWNLOAD)
    @SerializedName("download")
    private boolean download;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }
}