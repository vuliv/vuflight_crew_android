package com.vuliv.network.database;

import android.support.annotation.NonNull;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.vuliv.network.database.tables.EntityTableFolder;
import com.vuliv.network.database.tables.EntityTableFolderItems;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FolderItemsTableOperations {
    private Dao<EntityTableFolderItems, Integer> entityTableFolderItems;
    private OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper;

    public FolderItemsTableOperations(OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper) {
        this.ormLiteSqliteOpenHelper = ormLiteSqliteOpenHelper;
    }

    @NonNull
    public Dao<EntityTableFolderItems, Integer> getEntityTableBanner() throws SQLException {
        if (this.entityTableFolderItems == null) {
            this.entityTableFolderItems = this.ormLiteSqliteOpenHelper.getDao(EntityTableFolderItems.class);
        }
        return this.entityTableFolderItems;
    }

//    public void deleteDisabledFolderItems() throws SQLException {
//        Dao dao = getEntityTableBanner();
//        DeleteBuilder deleteBuilder = dao.deleteBuilder();
//        deleteBuilder.where().eq("enable", Boolean.valueOf(false));
//        deleteBuilder.delete();
//    }

    public void insertFolderItems(List<EntityTableFolderItems> banners) throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        QueryBuilder<EntityTableFolderItems, Integer> builder = dao.queryBuilder();
        List list = dao.query(builder.prepare());
        if (list.size() == 0) {
            dao.create(banners);
        } else {
            for (EntityTableFolderItems banner : banners) {
                builder.where().eq(EntityTableFolderItems.FIELD_ITEM_ID, Integer.valueOf(banner.getItemId()));
                list = dao.query(builder.prepare());
                if (list.size() == 0) {
                    dao.create(banner);
                } else {
                    EntityTableFolderItems banner1 = (EntityTableFolderItems) list.get(0);
                    banner1.setName(banner.getName());
                    banner1.setType(banner.getType());
                    banner1.setExpiry(banner.getExpiry());
                    banner1.setFolderId(banner.getFolderId());
                    banner1.setThumbnail(banner.getThumbnail());
                    banner1.setUrl(banner.getUrl());
                    banner1.setStartTime(banner.getStartTime());
                    banner1.setEndTime(banner.getEndTime());
                    banner1.setLogo(banner.getLogo());
                    dao.update(banner1);
                }
            }
        }
    }

    public List<EntityTableFolderItems> getFolderItemsMobile(int folderId) throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        QueryBuilder<EntityTableFolderItems, Integer> builder = dao.queryBuilder();
        Where<EntityTableFolderItems, Integer> eq = builder.where()
                .eq(EntityTableFolderItems.FIELD_PLATFORM, EntityTableFolderItems.PLATFORM_MOBILE)
                .or().eq(EntityTableFolderItems.FIELD_PLATFORM, EntityTableFolderItems.PLATFORM_BOTH)
                .and().eq(EntityTableFolderItems.FIELD_FOLDER_ID, Integer.valueOf(folderId))
                .and().eq(EntityTableFolderItems.FIELD_TYPE, EntityTableFolderItems.TYPE_VIDEO)
                .and().le(EntityTableFolderItems.FIELD_START_TIME_EXPIRY, System.currentTimeMillis())
                .and().gt(EntityTableFolderItems.FIELD_END_TIME_EXPIRY, System.currentTimeMillis())
                .and().eq(EntityTableFolderItems.FIELD_IMAGE_DOWNLOAD, Boolean.valueOf(true))
                .and().eq(EntityTableFolderItems.FIELD_VIDEO_DOWNLOAD, Boolean.valueOf(true));
        List list = dao.query(builder.prepare());
        return list;
    }

    public List<EntityTableFolderItems> getFolderItemsWeb(int folderId) throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        QueryBuilder<EntityTableFolderItems, Integer> builder = dao.queryBuilder();
        Where<EntityTableFolderItems, Integer> eq = builder.where()
                .eq(EntityTableFolderItems.FIELD_PLATFORM, EntityTableFolderItems.PLATFORM_WEB)
                .or().eq(EntityTableFolderItems.FIELD_PLATFORM, EntityTableFolderItems.PLATFORM_BOTH)
                .and().eq(EntityTableFolderItems.FIELD_FOLDER_ID, Integer.valueOf(folderId))
                .and().eq(EntityTableFolderItems.FIELD_TYPE, EntityTableFolderItems.TYPE_VIDEO)
                .and().le(EntityTableFolderItems.FIELD_START_TIME_EXPIRY, System.currentTimeMillis())
                .and().gt(EntityTableFolderItems.FIELD_END_TIME_EXPIRY, System.currentTimeMillis())
                .and().eq(EntityTableFolderItems.FIELD_IMAGE_DOWNLOAD, Boolean.valueOf(true))
                .and().eq(EntityTableFolderItems.FIELD_VIDEO_DOWNLOAD, Boolean.valueOf(true));
        List list = dao.query(builder.prepare());
        return list;
    }

    public List<EntityTableFolderItems> getAdItems() throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        QueryBuilder<EntityTableFolderItems, Integer> builder = dao.queryBuilder();
        Where eq = builder.where()
                .eq(EntityTableFolderItems.FIELD_TYPE, EntityTableFolderItems.TYPE_AD)
                .and().le(EntityTableFolderItems.FIELD_START_TIME_EXPIRY, System.currentTimeMillis())
                .and().gt(EntityTableFolderItems.FIELD_END_TIME_EXPIRY, System.currentTimeMillis())
                .and().eq(EntityTableFolderItems.FIELD_IMAGE_DOWNLOAD, Boolean.valueOf(true))
                .and().eq(EntityTableFolderItems.FIELD_VIDEO_DOWNLOAD, Boolean.valueOf(true));
        List list = dao.query(builder.prepare());
        return list;
    }

    public List<EntityTableFolderItems> getNotDownloadedFolderItems() throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        QueryBuilder<EntityTableFolderItems, Integer> builder = dao.queryBuilder();
        Where<EntityTableFolderItems, Integer> eq = builder.where()
                .gt(EntityTableFolderItems.FIELD_START_TIME_EXPIRY, System.currentTimeMillis())
//                .and().gt(EntityTableFolderItems.FIELD_END_TIME_EXPIRY, System.currentTimeMillis())
                .and().eq(EntityTableFolderItems.FIELD_IMAGE_DOWNLOAD, Boolean.valueOf(false))
                .or().eq(EntityTableFolderItems.FIELD_VIDEO_DOWNLOAD, Boolean.valueOf(false));
        List list = dao.query(builder.prepare());
        return list;
    }

    public List<EntityTableFolderItems> getDownloadedFolderItems() throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        QueryBuilder<EntityTableFolderItems, Integer> builder = dao.queryBuilder();
        Where<EntityTableFolderItems, Integer> eq = builder.where()
                .eq(EntityTableFolderItems.FIELD_IMAGE_DOWNLOAD, Boolean.valueOf(true))
                .or().eq(EntityTableFolderItems.FIELD_VIDEO_DOWNLOAD, Boolean.valueOf(true));
        List list = dao.query(builder.prepare());
        return list;
    }

    public void deleteFolderItems(ArrayList<String> deleteIds) throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        DeleteBuilder<EntityTableFolderItems, Integer> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().in(EntityTableFolderItems.FIELD_ITEM_ID, deleteIds);
        deleteBuilder.delete();
    }

    public void updateFolderItem(EntityTableFolderItems entityTableFolderItems) throws SQLException {
        Dao<EntityTableFolderItems, Integer> dao = getEntityTableBanner();
        dao.update(entityTableFolderItems);
    }
}