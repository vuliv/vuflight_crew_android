package com.vuliv.network.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by MB0000004 on 07-Mar-18.
 */

@Database(entities = {Order.class}, version = 1)
public abstract class OrderDatabase extends RoomDatabase {

    private static volatile OrderDatabase mInstance;

    public abstract OrderDao orderDao();

    public static OrderDatabase getInstance (Context context) {
        if (mInstance == null) {
            synchronized (OrderDatabase.class) {
                if (mInstance == null) {
                    mInstance = Room.databaseBuilder(context.getApplicationContext(),
                            OrderDatabase.class, "order.db").build();
                }
            }
        }
        return mInstance;
    }
}