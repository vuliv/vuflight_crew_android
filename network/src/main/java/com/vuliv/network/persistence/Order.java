package com.vuliv.network.persistence;

/**
 * Created by MB0000004 on 07-Mar-18.
 */

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Table of Orders
 */
@Entity(tableName = "orders")
public class Order {
    /**
     * Auto incremented id of order
     */
//    @PrimaryKey(autoGenerate = true)
//    int id;

    @ColumnInfo(name = "order")
    String orderJson;

    @ColumnInfo(name = "orderPlaced")
    Boolean orderPlaced;

    @ColumnInfo(name = "time")
    long time;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "phoneNumber")
    String phoneNumber;

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public String getOrderJson() {
        return orderJson;
    }

    public void setOrderJson(String orderJson) {
        this.orderJson = orderJson;
    }

    public Boolean getOrderPlaced() {
        return orderPlaced;
    }

    public void setOrderPlaced(Boolean orderPlaced) {
        this.orderPlaced = orderPlaced;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
