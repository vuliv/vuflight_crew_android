package com.vuliv.network.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by user on 23-01-2018.
 */
@Dao
public interface OrderDao {

    @Query("SELECT * from orders order by time DESC")
    List<Order> getOrders();

    @Query("SELECT * from orders where phoneNumber = :msisdn")
    Order getOrderByMsisdn(String msisdn);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrder(Order order);

    @Delete
    void deleteOrder(Order order);

}
