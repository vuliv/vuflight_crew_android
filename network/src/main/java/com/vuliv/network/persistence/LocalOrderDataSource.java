package com.vuliv.network.persistence;

import com.vuliv.network.OrderDataSource;

import java.util.List;

/**
 * Created by user on 23-01-2018.
 */

public class LocalOrderDataSource implements OrderDataSource{

    private final OrderDao orderDao;

    public LocalOrderDataSource(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public List<Order> getOrders() {
        return orderDao.getOrders();
    }

    @Override
    public Order getOrderByMsisdn(String msisdn) {
        return orderDao.getOrderByMsisdn(msisdn);
    }

    @Override
    public void insertorUpdateOrder(Order order) {
        orderDao.insertOrder(order);
    }

    @Override
    public void deleteOrder(Order order) {
        orderDao.deleteOrder(order);
    }
}
