package com.vuliv.network;

import android.content.Context;

import com.vuliv.network.persistence.LocalOrderDataSource;
import com.vuliv.network.persistence.OrderDatabase;

/**
 * Created by user on 23-01-2018.
 */

public class Injection {
    /**
     * returns the Object of OrderDataSource which have the methods of Database
     * @param context
     * @return
     */
    public static OrderDataSource provideOrderDataSource (Context context) {
        OrderDatabase orderDatabase = OrderDatabase.getInstance(context);
        return new LocalOrderDataSource(orderDatabase.orderDao());
    }
}
