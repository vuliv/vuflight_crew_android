package com.vushare.customdialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.vucast.constants.Constants;
import com.vucast.constants.DataConstants;
import com.vuliv.network.service.SharedPrefController;
import com.vushare.R;

/**
 * Created by MB0000005 on 03-09-2016.
 */
public class DialogInvite extends Dialog {

    private Context context;
    private TextView tvDescHotspotName, tvDescInviteUrl, tvMore;

    public DialogInvite(Context context) {
        super(context);
        this.context = context;
    }

    public DialogInvite(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogInvite(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_invite);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (getDeviceMetrics(context).widthPixels * 0.9);
        getWindow().setAttributes(lp);
//        getWindow().getAttributes().windowAnimations = R.style.DialogInAppCardAnimation;
        init();
    }

    private void init() {
        tvDescHotspotName = (TextView) findViewById(R.id.tv_desc1);
        final String hotspotName = SharedPrefController.getUserName(context) + "-" +
                SharedPrefController.getUserAvatar(context) + "-" + "VuShareHT";
        tvDescHotspotName.setText(Html.fromHtml(context.getString(R.string.ask_hotspotname, hotspotName)));

        String inviteUrl = "http://" + DataConstants.IP_ADDRESS + ":" +
                com.vucast.constants.Constants.SERVER_PORT + "/invite.html";
        tvDescInviteUrl = (TextView) findViewById(R.id.tv_desc2);
        tvDescInviteUrl.setText(Html.fromHtml(context.getString(R.string.ask_inviteurl, inviteUrl)));

        tvMore = (TextView) findViewById(R.id.tv_more);
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inviteMore(hotspotName);
            }
        });
    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }

    public void inviteMore(String ssid) {
        StringBuilder message = new StringBuilder();
        message.append("http://");
        message.append(DataConstants.IP_ADDRESS);
        message.append(":" + Constants.SERVER_PORT);

        String shareText = context.getString(R.string.share_text, ssid, message.toString());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

}
