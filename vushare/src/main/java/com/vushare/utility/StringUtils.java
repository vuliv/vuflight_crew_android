package com.vushare.utility;


public class StringUtils {

    /**
     * Checks whether the passed string is having some text or just a null string.
     *
     * @param text
     * @return
     */
    public static boolean isEmpty(String text) {
        boolean isEmpty = true;
        if (text != null && text.trim().length() > 0 && !text.trim().equalsIgnoreCase("null")) {
            isEmpty = false;
        }
        return isEmpty;
    }

    public static String capitalizeWord(String str){
        String words[]=str.split(" ");
        String capitalizeWord="";
        for(String w:words){
            if(!w.isEmpty())
            {
                String first= w.substring(0,1);
                String afterfirst= w.substring(1);
                capitalizeWord+=first.toUpperCase()+afterfirst+" ";
            }
        }
        return capitalizeWord.trim();
    }

}
