package com.vushare.utility;

import android.os.Environment;

import com.vushare.server.HttpServer;

import java.util.HashMap;

/**
 * Created by MB0000021 on 8/22/2017.
 */

public class Constants {

  //  public static final String WIFI_NAME = "-vu-joy";
    public static final String WIFI_NAME = "-aero-joy";
    public static final boolean IS_SHUTTLE = true;
    public static final int READ_TIMEOUT = 10000;
    public static final int CONNECTION_TIMEOUT = 15000;

    public static final boolean SELF_SERVER = true;
    public static final String ROUTER_SSID = "VuBox";

    public static final String VUSHARE_HOTSPOT_SEPARATOR = "-";
    public static final String VUSHARE_HOTSPOT_SUFFIX = "VuShareHT";
    public static final String VUSHARE_HOTSPOT_PASSWORD = "123456789";
    public static final String VUSHARE_UNLOCK_PASSWORD = "nag!05@30#11";

    public static final String APP_NAME = "VuShare";
    public static final String VUSHARE_FOLDER_NAME = "VuShare";
    public static final String SAVE_FILE_PATH = Environment.getExternalStorageDirectory() + "/" + VUSHARE_FOLDER_NAME;

    public static final String HTTPS_STRING = "https";
    public static final String SERVER_IP = "192.168.43.1";
    public static final int SERVER_PORT = 7070;
    public static final String BASE_URL = "http://" + SERVER_IP + ":" + HttpServer.SERVER_PORT;
    public static final String FILES_URL = BASE_URL + "/api";
    public static final String DOWNLOAD_FILE_URL = BASE_URL + "/download";
    public static final String API_BASE_URL = "http://52.76.81.227:8081/VuFlight/webapi/";

    public static final int PERMISSION_REQ_CODE_READ_PHONE_STATE = 101;

    public static final String SELECTED_SEAT_KEY = "selectedSeatKey";
    public static final String ORDERS_SCREEN_KEY = "ordersScreen";
    public static final String ORDER_ITEMS_KEY = "orderItems";
    public static final String CART_NO_KEY = "cartNoKey";
    public static final String EMP_ID_KEY = "empIdKey";
    public static final String REQUEST_KEY = "requestKey";

    public static final String MIMETYPE_IMG = "image/*";
    public static final String SELECT_FILES = "select file";

    public static final int PERMISSION_REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST_LAUNCH_CAMERA = 101;
    public static final int SELECT_FILE = 102;
    public static final int REQUEST_CAMERA = 103;
    public static final int REQUIRED_SIZE = 512;

    public static final String[] SEAT_CART = {"Cart One", "Cart Two", "Cart Three", "Cart Four"};
}
