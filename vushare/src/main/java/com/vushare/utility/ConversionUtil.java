package com.vushare.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.vuliv.network.entity.EntityDataResponse;
import com.vushare.controller.InventoryManager;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Ankur on 7/6/2018.
 */

public class ConversionUtil {

    public static EntityDataResponse toEntityDataResponse(InventoryManager inventoryManager, boolean isEcom) {
        EntityDataResponse entityDataResponse = new EntityDataResponse();
        ArrayList<EntityDataResponse.Dominos> dominosList = new ArrayList<>();
        entityDataResponse.setInformation(dominosList);
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        Map<String, List<CartInventory.Inventory>> map = inventoryManager.getInventoryMap(crewProfiles.getLogdInCartNo());
        if (map != null) {

            for (String type : map.keySet()) {
                if ((!isEcom && type.equalsIgnoreCase(CartInventory.ITEM_TYPE_ECOM)) || (isEcom && !type.equalsIgnoreCase(CartInventory.ITEM_TYPE_ECOM))) {
                    continue;
                }

                for (CartInventory.Inventory inventory : map.get(type)) {
                    EntityDataResponse.Dominos dominos = new EntityDataResponse.Dominos();
                    dominos.setType(inventory.getType());
                    dominos.setId("" + inventory.getItemId());
                    dominos.setName(inventory.getItemName());
                    dominos.setDescription(inventory.getItemDescription());
                    dominos.setImage(inventory.getImageUrl());
                    dominos.setVeg(inventory.isVeg());
                    ArrayList<EntityDataResponse.SizeAndPrice> sizeAndPrices = new ArrayList<>();
                    dominos.setSizes(sizeAndPrices);
                    EntityDataResponse.SizeAndPrice sizeAndPrice = new EntityDataResponse.SizeAndPrice();
                    sizeAndPrice.setSize("S");
                    sizeAndPrice.setMax_qty(inventory.getQuantity());
                    sizeAndPrice.setPrice(inventory.getPrice());
                    sizeAndPrices.add(sizeAndPrice);

                    dominosList.add(dominos);
                }
            }
        }
        return entityDataResponse;
    }

    public static Bitmap decodeFile(String filePath, int reqSize) {
        if (filePath != null) {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, o);

            // The new size we want to scale to

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < reqSize && height_tmp < reqSize)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(filePath, o2);
        } else {
            return null;
        }
    }

}
