/**/
package com.vushare.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.vushare.ui.activity.HomeActivity;
import com.vushare.ui.activity.LauncherActivity;
import com.vushare.ui.activity.ReceiverActivity;

/**
 * RestartReceiver.java
 *
 *  Created on: 30-Jul-2015
 *  Author: Vibhor Chopra
 */
public class RestartReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
        Log.i("RestartReceiver", "onRecieve");
        Toast.makeText(context, "onReceive", Toast.LENGTH_SHORT).show();
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
			Intent launchIntent = new Intent(context, LauncherActivity.class);
			context.startActivity(launchIntent);
            Log.i("RestartReceiver", "launch");
            Toast.makeText(context, "launch", Toast.LENGTH_SHORT).show();
		}
	}
}