package com.vushare.utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.WindowManager;

import com.vuliv.network.receiver.AdminController;
import com.vushare.R;

import java.text.DecimalFormat;

/**
 * Created by MB0000021 on 8/3/2017.
 */

public class Utility {

    /**
     * Check whether permission is granted or not
     *
     * @param context
     * @param permission
     * @return
     */
    public static boolean isPermissionGranted(Context context, String permission) {
        if (!isMarshmallow()) {
            return true;
        }
        if (ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether its Marshmallow or not
     *
     * @return
     */
    public static boolean isMarshmallow() {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {
            return true;
        }
        return false;
    }

    public static String getFileSize(long length) {
        DecimalFormat df = new DecimalFormat("#,##0.#");
        if (length <= 0L)
            return "0";
        String[] arrayOfString = {"B", "KB", "MB", "GB", "TB"};
        int i = (int) (Math.log10(length) / Math.log10(1024.0D));
        return df.format(length / Math.pow(1024.0D, i)) + " " + arrayOfString[i];
    }

    /**
     * Show alert dialog
     *
     * @param context
     * @param message
     * @param positiveText
     * @param negativeText
     * @param positiveListener
     * @param negativeListener
     */
    public static void showAlertDialog(Context context, String message, String positiveText, String negativeText,
                                       DialogInterface.OnClickListener positiveListener,
                                       DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, positiveListener);
        if (!StringUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText,
                    negativeListener == null ? new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    } : negativeListener);
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#ed008c"));
    }

    /**
     * Show alert dialog
     *
     * @param context
     * @param message
     * @param positiveText
     * @param negativeText
     * @param positiveListener
     * @param negativeListener
     */
    public static void showServiceAlertDialog(Context context, String message, String positiveText, String negativeText,
                                              DialogInterface.OnClickListener positiveListener,
                                              DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, positiveListener);
        if (!StringUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText,
                    negativeListener == null ? new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    } : negativeListener);
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#ed008c"));
    }

    /**
     * Scan Android database
     *
     * @param context
     * @param path
     */
    public static void scanFile(Context context, String path) {
        MediaScannerConnection.scanFile(context, new String[]{path}, null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri) {

                    }
                });
    }

    /**
     * Unlock the device
     *
     * @param context
     * @param devicePolicyManager
     * @param componentName
     */

    public static void unlockDevice(Context context, DevicePolicyManager devicePolicyManager, ComponentName componentName) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                if (am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_LOCKED) {
                    ((Activity) context).stopLockTask();
                }
                // set the policies to false and enable everything back.
                AdminController.setAdminPolicies(context, devicePolicyManager, componentName, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove app as device owner
     * @param context
     * @param devicePolicyManager
     */
    public static void removeOwner(Context context, DevicePolicyManager devicePolicyManager) {
        devicePolicyManager.clearDeviceOwnerApp(context.getPackageName());
    }

    public static boolean checkDrawOverAppsPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(context)) {
                return false;
            }
            return true;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean checkDrawOverlayPermission(Context context) {
        if (!Settings.canDrawOverlays(context)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + context.getPackageName()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK /*| Intent.FLAG_ACTIVITY_CLEAR_TASK*/);
            context.startActivity(intent);
            return true;
        }
        return false;
    }

    public static boolean isOverlayServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
