package com.vushare.utility;

/**
 * Created by Harsh on 07-Aug-18.
 */
public class SharedPrefsKeys {

    public static final String SHARED_PREFERENCES_NAME = "VU_LIVE";
    public static final String PROFILE_IMAGE_URI = "profile_image_uri";
    public static final String LOGIN_CREW_NAME = "loginCrewName";
    public static final String LOGIN_CREW_ID = "loginCrewId";
    public static final String LOGIN_CREW_CART_NO = "loginCrewCartNo";
    public static final String LOGIN_CREW_FLIGHT_NO = "loginCrewFlightNo";
    public static final String SOLD_ITEM = "soldItem";
    public static final String POINTS_EARNED = "pointsEarned";
    public static final String LOGIN_CREW_SEAT_RANGE = "loginCrewSeatRange";
    public static final String POINTS_TOTAL = "pointsTotal";
    public static final String REQUEST_COUNT = "requestCount";
    public static final String IS_PA_MODE_ENABLE = "ispamodeenable";
}
