package com.vushare.device;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.text.TextUtils;

import com.vushare.entity.EntityDirectoryDetail;
import com.vushare.utility.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Gursewak on 11/20/2016.
 */

public class ContentRetriever {

    private Context context;
    private String currentPath;

    public String[] getCardsPath() {
        return cardsPath;
    }

    public void setCardsPath(String[] cardsPath) {
        this.cardsPath = cardsPath;
    }

    private String[] cardsPath;

    public ContentRetriever(Context context) {
        this.context = context;
    }

    /**
     * Returns all available SD-Cards in the system (include emulated)
     * <p>
     * Warning: Hack! Based on Android source code of version 4.3 (API 18)
     * Because there is no standard way to get it.
     *
     * @return paths to all available SD-Cards in the system (include emulated)
     */
    public static String[] getStorageDirectories(Context pContext) {
        // Final set of paths
        final Set<String> rv = new HashSet<>();

        //Get primary & secondary external device storage (internal storage & micro SDCARD slot...)
        File[] listExternalDirs = ContextCompat.getExternalFilesDirs(pContext, null);
        for (int i = 0; i < listExternalDirs.length; i++) {
            if (listExternalDirs[i] != null) {
                String path = listExternalDirs[i].getAbsolutePath();
                int indexMountRoot = path.indexOf("/Android/data/");
                if (indexMountRoot >= 0 && indexMountRoot <= path.length()) {
                    //Get the root path for the external directory
                    rv.add(path.substring(0, indexMountRoot));
                }
            }
        }
        return rv.toArray(new String[rv.size()]);
    }


    public CursorLoader getData() {
        Uri uri = MediaStore.Files.getContentUri("external");
        String[] projection = {
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
        };
        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "=" + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
        CursorLoader cursorLoader = new CursorLoader(context, uri, projection, selection, null, null);
        return cursorLoader;

    }

    public String getCurrentPath() {
        return currentPath;
    }

    public ArrayList<EntityDirectoryDetail> getStorageDirectories() {
        ArrayList<EntityDirectoryDetail> directoryArrayList = new ArrayList<>();
        cardsPath = getStorageDirectories(context);
        setCardsPath(cardsPath);
        String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
        currentPath = "Root";
        for (String path : cardsPath) {
            File fileData = new File(path);
            EntityDirectoryDetail entityDirectory = new EntityDirectoryDetail();
            entityDirectory.setFileName(fileData.getName());
            entityDirectory.setFolderName(fileData.getParent());
            entityDirectory.setSize(fileData.length());
//            entityDirectory.setMusicCount(externalStorage.equals(path) ? context.getResources().getString(R.string.internal)
//                    : context.getResources().getString(R.string.external));
            entityDirectory.setPath(fileData.getAbsolutePath());
            directoryArrayList.add(entityDirectory);
        }
        return directoryArrayList;
    }

    public ArrayList<EntityDirectoryDetail> getOnlyDirectories(String currentPath) {
        return getAllDirectories(currentPath, true);
    }

    public ArrayList<EntityDirectoryDetail> getAllDirectories(String currentPath) {
        return getAllDirectories(currentPath, false);
    }

    public ArrayList<EntityDirectoryDetail> getAllDirectories(String currentPath, boolean ignoreNonDirectories) {
        ArrayList<EntityDirectoryDetail> directoryArrayList = new ArrayList<>();
        if (StringUtils.isEmpty(currentPath)) {
            currentPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            this.currentPath = currentPath;
        } else {
            this.currentPath = currentPath;
        }
        File file = new File(currentPath);
        File[] files = file.listFiles();
        if (files != null) {
            for (File fileData : files) {
                // don't show hidden files
                if (fileData.isHidden()) continue;
                if (ignoreNonDirectories && !fileData.isDirectory()) continue;
                EntityDirectoryDetail entityDirectory = new EntityDirectoryDetail();
                entityDirectory.setFileName(fileData.getName());
                entityDirectory.setFolderName(file.getParent());
                entityDirectory.setPath(fileData.getAbsolutePath());
                entityDirectory.setSize(fileData.length());
                directoryArrayList.add(entityDirectory);
            }
        }
        return directoryArrayList;
    }

    public ArrayList<File> findSong(File root) {
        ArrayList<File> al = new ArrayList<File>();
        File[] files = root.listFiles();    // All file and folder automatic collect
        if (files != null) {
            for (File singleFile : files) {
                if (singleFile.isDirectory() && !singleFile.isHidden()) {
                    al.addAll(findSong(singleFile)); //Recursively call
                } else {
                    if (singleFile.getName().endsWith(".mp3")) {
                        al.add(singleFile);
                    }
                }
            }
        }
        return al;
    }

    private int getMusicCount(String path) {
        int musicCount = 0;
        File file = new File(path);
        File[] files = file.listFiles();
        if (files != null) {
            for (File fileData : files) {
                if (isMusicFile(fileData.getPath())) {
                    musicCount = musicCount + 1;
                }
            }
        }
        return musicCount;
    }

    public boolean isDirectory(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            return true;
        }
        return false;
    }

    /*
    * Get the extension of a file.
    */
    private static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public boolean isMusicFile(String path) {
        File file = new File(path);
        String extension = getExtension(file);
        if (!TextUtils.isEmpty(extension) && extension.equalsIgnoreCase("mp3")) {
            return true;
        }
        return false;
    }
}