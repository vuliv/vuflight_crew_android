package com.vushare.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.TheAplication;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfile;
import com.vushare.model.Prizes;
import com.vushare.ui.activity.ProfileAndPrizeEligiblityActivity;
import com.vushare.utility.SharedPrefsKeys;
import com.vushare.utility.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import static android.graphics.Typeface.BOLD;

/**
 * Created by Harsh on 17-Aug-18.
 */
public class RecyclerAdapterPrizes extends RecyclerView.Adapter<RecyclerAdapterPrizes.ViewHolder> {
    private static final String TAG = RecyclerAdapterPrizes.class.getSimpleName();
    private Context context;
    private ArrayList<Prizes> prizesArrayList;
    private CrewProfile crewProfile;
    RequestOptions requestOptions;
    private SharedPreferences pref;

    public RecyclerAdapterPrizes(Context context, CrewProfile crewProfile)
    {
        this.context = context;
        this.crewProfile = crewProfile;
        requestOptions = new RequestOptions();
        pref = TheAplication.getInstance().getSharedPreferences(SharedPrefsKeys.SHARED_PREFERENCES_NAME, TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prize, parent, false);
        RecyclerAdapterPrizes.ViewHolder viewHolder = new RecyclerAdapterPrizes.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.setData(prizesArrayList,position);

    }

    @Override
    public int getItemCount() {
        if (prizesArrayList == null) {
            return 0;
        }
        return prizesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgPrizeImage;
        private TextView txtPrizeDescription,txtPrizePoints,txtRedeemBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            imgPrizeImage = (ImageView)itemView.findViewById(R.id.imgPrizeImage);
            txtPrizeDescription = (TextView)itemView.findViewById(R.id.txtPrizeText);
            txtPrizePoints = (TextView)itemView.findViewById(R.id.txtPrizePoints);
            txtRedeemBtn = (TextView)itemView.findViewById(R.id.txtRedeemBtn);
        }

        private void setData(ArrayList<Prizes> prizesArrayList, int position) {
            final Prizes prizes = prizesArrayList.get(position);
            txtPrizeDescription.setText(prizes.getItemDescription());
            txtPrizePoints.setText(prizes.getPoints()+"Pts");
            if(prizes.getPoints() <= crewProfile.getPoints())
            {
                txtRedeemBtn.setText(context.getResources().getString(R.string.redeem));
                txtRedeemBtn.setBackground(context.getDrawable(R.drawable.redeem_btn_background));
            } else {

                txtRedeemBtn.setText("Need "+(prizes.getPoints() - crewProfile.getPoints())+"Pts");
                txtRedeemBtn.setBackground(context.getDrawable(R.drawable.non_redeem_btn_background));
            }
            txtRedeemBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prizes.getPoints() <= crewProfile.getPoints()) {
                        loadRedeemProduct(prizes);
                    } else {
                        displayAlert(prizes);
                    }
                }
            });
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions.placeholder(R.mipmap.ic_launcher))
                    .load(prizes.getImageUrl())
                    .into(imgPrizeImage);
        }
    }

    private void loadRedeemProduct(Prizes prizes) {
        String url = "http://192.168.43.1:8080/api/redeemProduct";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("crewId", crewProfile.getEmpId());
            jsonRequest.put("redeemItemId", prizes.getItemId());
            jsonRequest.put("currentTripPoints", pref.getInt(SharedPrefsKeys.POINTS_EARNED, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String status = null;
                        try {
                            status = response.getString("StatusCode");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (Integer.parseInt(status) == HttpURLConnection.HTTP_OK) {
                            displayAlert(prizes);
                        } else {
                            Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "VolleyError" + error, Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(context, jsonObjectRequest, "redeemProduct");
    }

    private void displayAlert(Prizes prizes)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.CustomDialog);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_prize_alert,null);
        builder.setView(view);
        TextView txtProductToBeSold = (TextView) view.findViewById(R.id.txtProductToBeSold);
        TextView txtAlertCrewName = (TextView) view.findViewById(R.id.txtAlertCrewName);
        TextView txtPrizePoint = (TextView) view.findViewById(R.id.txtPrizePoint);
        ImageView ivPrizeImage = (ImageView) view.findViewById(R.id.ivPrizeImage);

        txtAlertCrewName.setText("Hi "+crewProfile.getEmpName()+"!");
        txtPrizePoint.setText(prizes.getPoints()+"Pts");
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions.placeholder(R.mipmap.ic_launcher))
                .load(prizes.getImageUrl())
                .into(ivPrizeImage);

        if (prizes.getPoints() <= crewProfile.getPoints()) {
            txtProductToBeSold.setText(context.getString(R.string.product_delivery_message), TextView.BufferType.SPANNABLE);
            updateData(prizes.getPoints());
        } else {
            String text = String.format(context.getString(R.string.earn_points_and_win), (prizes.getPoints() - crewProfile.getPoints()), prizes.getItenName());
            String txt = StringUtils.capitalizeWord(prizes.getItenName());
            Spannable spannable = new SpannableString(text + txt);
            spannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.dark_grey)),
                    text.length(), (text + txt).length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(
                    new StyleSpan(BOLD),
                    (text).length(), (text + txt).length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(
                    new StyleSpan(BOLD),
                    (text).length(), (text + txt).length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtProductToBeSold.setText(spannable, TextView.BufferType.SPANNABLE);
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateData(int points) {
        crewProfile.setPoints(crewProfile.getPoints()-points);
        ProfileAndPrizeEligiblityActivity profileAndPrizeEligiblityActivity = (ProfileAndPrizeEligiblityActivity) context;
        profileAndPrizeEligiblityActivity.setTotalPoints(crewProfile.getPoints());
        notifyDataSetChanged();
    }

    public void setList(ArrayList<Prizes> prizesArrayList)
    {
        this.prizesArrayList = prizesArrayList;
        notifyDataSetChanged();
    }
}
