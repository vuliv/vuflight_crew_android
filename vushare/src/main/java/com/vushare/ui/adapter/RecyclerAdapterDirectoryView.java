package com.vushare.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.entity.EntityDirectoryDetail;
import com.vushare.device.ContentRetriever;

import java.util.ArrayList;

import static android.view.View.OnClickListener;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterDirectoryView extends RecyclerView.Adapter {

    private Context context;
    private final ICallback mCallback;
    private ArrayList<EntityDirectoryDetail> directoryList;
    private ContentRetriever contentRetriever;

    public RecyclerAdapterDirectoryView(Context context, ICallback callback,
                                        ArrayList<EntityDirectoryDetail> directoryList,
                                        ContentRetriever contentRetriever) {
        this.context = context;
        this.mCallback = callback;
        this.directoryList = directoryList;
        this.contentRetriever = contentRetriever;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            setDetails(holder, position);
            setListeners(holder, position);
        }
    }

    private void setListeners(final RecyclerView.ViewHolder holder, final int position) {
        ((ViewHolder) holder).rlRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentRetriever.isDirectory(directoryList.get(position).getPath())) {
                    // Its a folder
                    mCallback.onSuccess(directoryList.get(position).getPath());
                } else {
                    // Its a file
                }
            }
        });
    }

    private void setDetails(final RecyclerView.ViewHolder holder, final int position) {
        if (contentRetriever.isDirectory(directoryList.get(position).getPath())) {
            ((ViewHolder) holder).mAlbumArt.setImageResource(R.drawable.ic_folder);
            ((ViewHolder) holder).mAlbumArt.setColorFilter(Color.BLACK);
            ((ViewHolder) holder).mAlbumArt.setAlpha(0.60F);
        } else {
            ((ViewHolder) holder).mAlbumArt.setImageResource(R.drawable.ic_file);
        }
        ((ViewHolder) holder).tvTitle.setText(directoryList.get(position).getFileName());
    }

    @Override
    public int getItemCount() {
        return directoryList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private RelativeLayout rlRoot;
        public ImageView mAlbumArt;

        public ViewHolder(View itemView) {
            super(itemView);
            mAlbumArt = (ImageView) itemView.findViewById(R.id.list_item_imageview);
            tvTitle = (TextView) itemView.findViewById(R.id.list_item_title);
            rlRoot = (RelativeLayout) itemView.findViewById(R.id.list_root);
        }
    }
}