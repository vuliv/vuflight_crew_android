package com.vushare.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vushare.R;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfiles;
import com.vushare.ui.fragment.CartViewFragment;
import com.vushare.ui.view.CircleImageView;

import java.util.List;

public class CartViewRecyclerViewAdapter extends RVAdapterBase<CartViewRecyclerViewAdapter.ViewHolder> {
    private CartViewFragment cartViewFragment;
    private List<CartInventory.Inventory> list;
    private int empId;

    public CartViewRecyclerViewAdapter(CartViewFragment cartViewFragment, List<CartInventory.Inventory> list, int empId) {
        this.cartViewFragment = cartViewFragment;
        this.list = list;
        this.empId = empId;
    }

    public void setList(List<CartInventory.Inventory> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CartViewRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_cart_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.setData(position, list, cartViewFragment, empId);
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtProductTitle, txtProductDes, txtPrice, txtProductPrice, txtProductPoints;
        private CircleImageView imgVwProducts;
        private RelativeLayout rltLytRequest;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductTitle = itemView.findViewById(R.id.txtProductTitle);
            txtProductDes = itemView.findViewById(R.id.txtProductDes);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            imgVwProducts = itemView.findViewById(R.id.imgVwProducts);
            txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
            rltLytRequest = itemView.findViewById(R.id.rltLytRequest);
            txtProductPoints = itemView.findViewById(R.id.txtProductPoints);
        }

        public void setData(int position, List<CartInventory.Inventory> list, CartViewFragment cartViewFragment, int empId) {
            int logdInCrewMemId = CrewProfiles.getInstance().getLogdInCrewMemId();
            if (logdInCrewMemId != empId) {
                rltLytRequest.setVisibility(View.VISIBLE);
            }
            CartInventory.Inventory inventory = list.get(position);
            txtProductTitle.setText(inventory.getItemName());
            txtProductDes.setText(inventory.getItemDescription());
            txtPrice.setText(cartViewFragment.getResources().getString(R.string.rupee) + inventory.getPrice());
            if (inventory.getImageUrl() != null && !inventory.getImageUrl().isEmpty()) {
                Glide.with(cartViewFragment)
                        .load(inventory.getImageUrl())
                        .into(imgVwProducts);
            }
            txtProductPrice.setText(cartViewFragment.getResources().getString(R.string.left) + " " + inventory.getQuantity());
            txtProductPoints.setText(cartViewFragment.getResources().getString(R.string.label_product_points, inventory.getPoints()));      // Added by Sumit Agrawal
        }
    }
}
