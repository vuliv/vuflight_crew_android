package com.vushare.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vushare.R;
import com.vushare.model.CartInventory;
import com.vushare.model.salesreport.SalesItemDetail;
import com.vushare.ui.fragment.SalesReportFragment;
import com.vushare.ui.view.CircleImageView;

import java.util.List;

public class SalesReprtRecyclerViewAdapter extends RVAdapterBase<SalesReprtRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = SalesReprtRecyclerViewAdapter.class.getSimpleName();
    private SalesReportFragment salesReportFragment;
    private List<SalesItemDetail> list;

    public SalesReprtRecyclerViewAdapter(SalesReportFragment salesReportFragment) {
        this.salesReportFragment = salesReportFragment;
    }

    @NonNull
    @Override
    public SalesReprtRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sales_report_item, parent, false);
        SalesReprtRecyclerViewAdapter.ViewHolder viewHolder = new SalesReprtRecyclerViewAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SalesReprtRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.setData(position, list, salesReportFragment);
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public void setList(List<SalesItemDetail> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtProductTitle, txtProductDes, txtPrice, txtSale, txtTotal, txtUpLift;
        private CircleImageView imgVwProducts;
        private ImageView imgViewVeg;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductTitle = itemView.findViewById(R.id.txtProductTitle);
            txtProductDes = itemView.findViewById(R.id.txtProductDes);
            imgVwProducts = itemView.findViewById(R.id.imgVwProducts);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtSale = itemView.findViewById(R.id.txtSale);
            txtTotal = itemView.findViewById(R.id.txtTotal);
            txtUpLift = itemView.findViewById(R.id.txtUpLift);
            imgViewVeg = itemView.findViewById(R.id.imgViewVeg);
        }

        public void setData(int position, List<SalesItemDetail> list, SalesReportFragment salesReportFragment) {
            SalesItemDetail inventory = list.get(position);
            txtProductTitle.setText(inventory.getItemName());
            txtProductDes.setText(inventory.getItemDescription());
            txtPrice.setText("" + inventory.getPrice());
            txtSale.setText("" + inventory.getSaleCount());
            txtTotal.setText("" + (inventory.getQuantity() + inventory.getQuantity()));
            txtUpLift.setText("" + (inventory.getQuantity() + inventory.getQuantity()));
            if (inventory.getImageUrl() != null && !inventory.getImageUrl().isEmpty()) {
                Glide.with(salesReportFragment)
                        .load(inventory.getImageUrl())
                        .into(imgVwProducts);
            }
            if (list.get(position).getType().equals(CartInventory.ITEM_TYPE_MEAL)) {
                imgViewVeg.setVisibility(View.VISIBLE);
                if (list.get(position).isVeg()) {
                    imgViewVeg.setImageResource(R.drawable.icon_veg);
                } else {
                    imgViewVeg.setImageResource(R.drawable.icon_non_veg);
                }
            } else {
                imgViewVeg.setVisibility(View.GONE);
            }
        }
    }
}

