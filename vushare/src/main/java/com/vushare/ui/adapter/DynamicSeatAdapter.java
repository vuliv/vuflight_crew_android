package com.vushare.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vushare.R;
import com.vushare.ui.fragment.OrdersFragment;

import java.util.ArrayList;

public class DynamicSeatAdapter extends RecyclerView.Adapter<DynamicSeatAdapter.MyViewHolder>{
    ArrayList<ArrayList<String>> seats;
    Context mContext;
    DynamicSeatViewInnerAdapter dynamicSeatViewInnerAdapter;
    private OrdersFragment ordersFragment;

    public DynamicSeatAdapter(Context context, ArrayList<ArrayList<String>> seats, OrdersFragment ordersFragment) {
        this.mContext = context;
        this.seats = seats;
        this.ordersFragment = ordersFragment;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dynamic_seat_adapter, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rv_seats_horizontal.setLayoutManager(linearLayoutManager);
        holder.rv_seats_horizontal.setNestedScrollingEnabled(false);
        dynamicSeatViewInnerAdapter = new DynamicSeatViewInnerAdapter(mContext, seats.get(position),ordersFragment);
        holder.rv_seats_horizontal.setAdapter(dynamicSeatViewInnerAdapter);
    }

    @Override
    public int getItemCount() {
        return seats.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RecyclerView rv_seats_horizontal;

        public MyViewHolder(View itemView) {
            super(itemView);
            rv_seats_horizontal = itemView.findViewById(R.id.rv_seats_horizontal);
        }
    }
}
