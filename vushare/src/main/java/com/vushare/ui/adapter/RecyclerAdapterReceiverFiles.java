package com.vushare.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.FileSelectController;
import com.vushare.entity.EntityFileHost;
import com.vushare.entity.EntityPhoto;
import com.vushare.utility.Utility;

import java.util.ArrayList;

import static android.view.View.OnClickListener;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterReceiverFiles extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<EntityFileHost> mFiles;

    public RecyclerAdapterReceiverFiles(Context context, ArrayList<EntityFileHost> files) {
        this.context = context;
        this.mFiles = files;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getFileViewHolder(parent);
    }

    private FileViewHolder getFileViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list2, parent, false);
        final FileViewHolder holder = new FileViewHolder(view);
        holder.mImageView.setVisibility(View.GONE);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof FileViewHolder) {
            EntityFileHost entityFileHost = mFiles.get(position);
            ((FileViewHolder) holder).mTitle.setText(entityFileHost.getName());
            ((FileViewHolder) holder).mSubtitle.setText(
                    Utility.getFileSize(entityFileHost.getDownloadedSize()) + " / " +
                    Utility.getFileSize(entityFileHost.getActualSize()));
        }
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }

    private class FileViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTitle;
        public TextView mSubtitle;

        public FileViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.list_item_imageview);
            mTitle = (TextView) itemView.findViewById(R.id.list_item_title);
            mSubtitle = (TextView) itemView.findViewById(R.id.list_item_subtitle);
        }
    }
}