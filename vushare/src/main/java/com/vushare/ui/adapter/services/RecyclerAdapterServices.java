package com.vushare.ui.adapter.services;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.vucast.callback.IClickCallback;
import com.vushare.R;
import com.vushare.model.services.ServiceRequestModel;

import java.util.ArrayList;

import static android.view.View.OnClickListener;

/**
 * Created by Sumit Agrawal on 13/09/2018.
 */

public class RecyclerAdapterServices extends RecyclerView.Adapter<RecyclerAdapterServices.ServiceViewHolder> {

    private final IClickCallback mCallback;
    private ArrayList<ServiceRequestModel> serviceRequestModels;

    public RecyclerAdapterServices(IClickCallback callback, ArrayList<ServiceRequestModel> serviceRequestModels) {
        this.mCallback = callback;
        this.serviceRequestModels = serviceRequestModels;
    }

    public void addList(ArrayList<ServiceRequestModel> serviceRequestModels) {
        this.serviceRequestModels = serviceRequestModels;
        notifyDataSetChanged();
    }

    public void addServiceRequest(ServiceRequestModel serviceRequestModel) {
        serviceRequestModels.add(serviceRequestModel);
        notifyDataSetChanged();
    }

    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false);
        return new ServiceViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ServiceViewHolder holder, final int position) {
        ServiceRequestModel serviceRequestModel = serviceRequestModels.get(position);
        holder.tv_SeatNumber.setText(serviceRequestModel.getMsisdn());
        holder.tv_service_name.setText(serviceRequestModel.getServiceRequest());
        holder.tv_done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceRequestModel.setHasAccepted(true);
                mCallback.click(serviceRequestModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serviceRequestModels.size();
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_SeatNumber;
        TextView tv_service_name;
        Button tv_done;

        ServiceViewHolder(View itemView) {
            super(itemView);
            tv_SeatNumber = itemView.findViewById(R.id.tv_SeatNumber);
            tv_service_name = itemView.findViewById(R.id.tv_service_name);
            tv_done = itemView.findViewById(R.id.tv_done);

        }
    }
}