package com.vushare.ui.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vushare.R;
import com.vucast.entity.EntityWiFiDetail;
import com.vushare.ui.activity.ReceiverActivity;
import com.vushare.utility.Constants;

import java.util.ArrayList;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterSendersList extends RecyclerView.Adapter {

    private final TypedArray mAvatars;
    private Context context;
    private ArrayList<EntityWiFiDetail> mSSIDList;

    public RecyclerAdapterSendersList(Context context, ArrayList<EntityWiFiDetail> ssidList, TypedArray avatars) {
        this.context = context;
        this.mSSIDList = ssidList;
        this.mAvatars = avatars;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getSenderViewHolder(parent);
    }

    private SenderViewHolder getSenderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list2, parent, false);
        final SenderViewHolder holder = new SenderViewHolder(view);
        holder.mImageView.setVisibility(View.VISIBLE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ReceiverActivity) context).connectToWifi(mSSIDList.get(holder.getAdapterPosition()));
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SenderViewHolder) {
            EntityWiFiDetail ssid = mSSIDList.get(position);
            ((SenderViewHolder) holder).mTitle.setText(ssid.getName());
            ((SenderViewHolder) holder).mSubtitle.setText(ssid.getName() + "-" + ssid.getAvatarId() + "-" + Constants.VUSHARE_HOTSPOT_SUFFIX);
            ((SenderViewHolder) holder).mImageView.setImageResource(mAvatars.getResourceId(ssid.getAvatarId(), 0));
        }
    }

    @Override
    public int getItemCount() {
        return mSSIDList.size();
    }

    private class SenderViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTitle;
        public TextView mSubtitle;

        public SenderViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.list_item_imageview);
            mTitle = (TextView) itemView.findViewById(R.id.list_item_title);
            mSubtitle = (TextView) itemView.findViewById(R.id.list_item_subtitle);
        }
    }
}