package com.vushare.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vushare.R;
import com.vushare.model.CartInventory;
import com.vushare.ui.fragment.ReconciliationFragment;
import com.vushare.ui.view.CircleImageView;

import java.util.List;

public class ReconciliationRecyclerViewAdapter extends RVAdapterBase<ReconciliationRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = ReconciliationRecyclerViewAdapter.class.getSimpleName();
    private List<CartInventory.Inventory> list;
    private ReconciliationFragment reconciliationFragment;

    public ReconciliationRecyclerViewAdapter(ReconciliationFragment reconciliationFragment) {
        this.reconciliationFragment = reconciliationFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reconciliation_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(position, list, reconciliationFragment);
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public void setList(List<CartInventory.Inventory> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtProductTitle, txtProductDes, txtPrice, txtTotal, txtRemaining, txtRequested, txtShared, txtDamaged, txtAmount;
        private CircleImageView imgVwProducts;
        private ImageView imgViewVeg;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductTitle = itemView.findViewById(R.id.txtProductTitle);
            txtProductDes = itemView.findViewById(R.id.txtProductDes);
            imgVwProducts = itemView.findViewById(R.id.imgVwProducts);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            imgViewVeg = itemView.findViewById(R.id.imgViewVeg);
            txtTotal = itemView.findViewById(R.id.txtTotal);
            txtRemaining = itemView.findViewById(R.id.txtRemaining);
            txtAmount = itemView.findViewById(R.id.txtAmount);
        }

        public void setData(int position, List<CartInventory.Inventory> list, ReconciliationFragment reconciliationFragment) {
            CartInventory.Inventory inventory = list.get(position);
            txtProductTitle.setText(inventory.getItemName());
            txtProductDes.setText(inventory.getItemDescription());
            txtPrice.setText("" + inventory.getPrice());
            txtRemaining.setText("" + inventory.getQuantity());
            txtTotal.setText("" + (inventory.getQuantity() + inventory.getQuantitySold()));
            txtAmount.setText("" + (inventory.getQuantity() * inventory.getPrice()));
            if (inventory.getImageUrl() != null && !inventory.getImageUrl().isEmpty()) {
                Glide.with(reconciliationFragment)
                        .load(inventory.getImageUrl())
                        .into(imgVwProducts);
            }
            if (list.get(position).getType().equals(CartInventory.ITEM_TYPE_MEAL)) {
                imgViewVeg.setVisibility(View.VISIBLE);
                if (list.get(position).isVeg()) {
                    imgViewVeg.setImageResource(R.drawable.icon_veg);
                } else {
                    imgViewVeg.setImageResource(R.drawable.icon_non_veg);
                }
            } else {
                imgViewVeg.setVisibility(View.GONE);
            }
        }
    }
}
