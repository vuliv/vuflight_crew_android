package com.vushare.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.entity.EntityAvatar;

import java.util.ArrayList;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterProfileAvatar extends RecyclerView.Adapter {

    private Context context;
    private ICallback mCallback;
    private ArrayList<EntityAvatar> mAvatars;

    public RecyclerAdapterProfileAvatar(Context context, ICallback callback, ArrayList<EntityAvatar> avatars) {
        this.context = context;
        this.mCallback = callback;
        this.mAvatars = avatars;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getPhotoViewHolder(parent);
    }

    private AvatarViewHolder getPhotoViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_avatar, parent, false);
        final AvatarViewHolder holder = new AvatarViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onSuccess(holder.getAdapterPosition());
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AvatarViewHolder) {
            EntityAvatar entityAvatar = mAvatars.get(position);
            ((AvatarViewHolder) holder).mImageView.setImageResource(entityAvatar.getDrawable());
        }
    }

    @Override
    public int getItemCount() {
        return mAvatars.size();
    }

    private class AvatarViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;

        public AvatarViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.item_avatar_image);
        }
    }
}