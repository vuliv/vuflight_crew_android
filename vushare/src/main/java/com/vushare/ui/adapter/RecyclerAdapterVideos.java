package com.vushare.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.FileSelectController;
import com.vushare.entity.EntityVideo;

import java.util.ArrayList;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterVideos extends RecyclerView.Adapter {

    private Context context;
    private final ICallback<String, String> mCallback;
    private ArrayList<EntityVideo> mVideos;

    public RecyclerAdapterVideos(Context context, ICallback callback, ArrayList<EntityVideo> videos) {
        this.context = context;
        this.mCallback = callback;
        this.mVideos = videos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getPhotoViewHolder(parent);
    }

    private VideoViewHolder getPhotoViewHolder(ViewGroup parent) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        final VideoViewHolder holder = new VideoViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EntityVideo video = mVideos.get(holder.getAdapterPosition());
                if (video.isSelected()) {
                    video.setSelected(false);
                    FileSelectController.getInstance(context).removeFile(video);
                } else {
                    video.setSelected(true);
                    FileSelectController.getInstance(context).addFile(video);
                }
                notifyDataSetChanged();
                mCallback.onSuccess("");
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof VideoViewHolder) {
            EntityVideo entityVideo = mVideos.get(position);
            Glide.with(context)
                    .setDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.grey_placeholder))
                    .load("file://" + entityVideo.getPath())
                    .into(((VideoViewHolder) holder).mImageView);

            if (entityVideo.isSelected()) {
                ((VideoViewHolder) holder).mSelectedLayout.setVisibility(View.VISIBLE);
            } else {
                ((VideoViewHolder) holder).mSelectedLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mVideos.size();
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public RelativeLayout mSelectedLayout;

        public VideoViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.video_imageview);
            mSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.video_selected_layout);
        }
    }
}