package com.vushare.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vuliv.network.entity.EntityDominosRequest;
import com.vushare.R;
import com.vushare.ui.fragment.OrdersFragment;

import java.util.ArrayList;

public class DynamicSeatViewInnerAdapter extends RecyclerView.Adapter<DynamicSeatViewInnerAdapter.MyViewHolder> {
    private static final String TAG = DynamicSeatViewInnerAdapter.class.getSimpleName();
    ArrayList<String> seats;
    Context mContext;
    private ArrayList<EntityDominosRequest> orderedSeatsList;
    private OrdersFragment ordersFragment;
    OnClickInAdapter onClickInAdapter;

    public DynamicSeatViewInnerAdapter(Context context, ArrayList<String> seats, OrdersFragment ordersFragment) {
        this.mContext = context;
        this.seats = seats;
        this.ordersFragment = ordersFragment;
        onClickInAdapter = ordersFragment;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dynamic_seat_view_inner_adapter, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position == 2) {
            holder.v_space.setVisibility(View.VISIBLE);
        } else {
            holder.v_space.setVisibility(View.GONE);
        }
        holder.tv_seat.setText(seats.get(position).toString());
        try {
            if (holder.tv_seat instanceof TextView) {
                if (OrdersFragment.requestMap.containsKey(((TextView) holder.tv_seat).getText())) {
                    if (OrdersFragment.OrderFragment1bundle != null) {
                        if (OrdersFragment.selectedSeatNo.equals(((TextView) holder.tv_seat).getText())) {
                            holder.tv_seat.setBackgroundResource(R.drawable.icon_seat_highlight);
                            onClickInAdapter.onClickInAdapterM(holder.tv_seat);
                        } else {
                            holder.tv_seat.setBackgroundResource(R.drawable.icon_seat_selected);
                        }
                    } else if (OrdersFragment.lastSelectedClickedSeat == null) {
                        holder.tv_seat.setBackgroundResource(R.drawable.icon_seat_highlight);
                        onClickInAdapter.onClickInAdapterM(holder.tv_seat);

                    } else {
                        OrdersFragment.listView.add(holder.tv_seat);
                        holder.tv_seat.setBackgroundResource(R.drawable.icon_seat_selected);
                    }
                    holder.tv_seat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view == OrdersFragment.lastSelectedClickedSeat) {
                                return;
                            }

                            if (OrdersFragment.orderCompleteSeat.equals(((TextView) view).getText())) {
                                return;
                            }

                            view.setBackgroundResource(R.drawable.icon_seat_highlight);
                            if (OrdersFragment.lastSelectedClickedSeat != null) {
                                OrdersFragment.lastSelectedClickedSeat.setBackgroundResource(R.drawable.icon_seat_selected);
                            }
                            onClickInAdapter.onClickInAdapterM(view);
                        }
                    });

                } else {
                    holder.tv_seat.setBackgroundResource(R.drawable.icon_seat);
                }
                if (OrdersFragment.updateOrderStatus) {
                    if (OrdersFragment.orderCompleteSeat.contains(((TextView) holder.tv_seat).getText())) {
                        holder.tv_seat.setBackgroundResource(R.drawable.ic_order_placed);
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return seats.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_seat;
        View v_space;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_seat = itemView.findViewById(R.id.tv_seat);
            v_space = itemView.findViewById(R.id.v_space);
        }
    }

    public interface OnClickInAdapter {
        void onClickInAdapterM(View view);
    }
}
