package com.vushare.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vuliv.network.entity.EntityDominosRequest;
import com.vushare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 15-01-2018.
 */

public class RecyclerAdapterOrderDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<EntityDominosRequest.Data> mOrdersList = null;

    public RecyclerAdapterOrderDetail(Context context, @NonNull ArrayList<EntityDominosRequest.Data> orderListList) {
        mContext = context;
        this.mOrdersList = new ArrayList<>(orderListList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_order_detail, parent, false);
        final OrderHolder orderHolder = new OrderHolder(view);
        return orderHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof OrderHolder) {
            final EntityDominosRequest.Data data = mOrdersList.get(position);

            ((OrderHolder)holder).tv_item.setText(data.getName());
            ((OrderHolder)holder).tv_item_price.setText(data.getQuantity() + " X Rs. " + (data.getPrice() / data.getQuantity()));
        }
    }

    @Override
    public int getItemCount() {
        return mOrdersList.size();
    }

    private static class OrderHolder extends RecyclerView.ViewHolder {
        private TextView tv_item, tv_item_price;

        public OrderHolder(View itemView) {
            super(itemView);
            tv_item = (TextView) itemView.findViewById(R.id.tv_item);
            tv_item_price = (TextView) itemView.findViewById(R.id.tv_item_price);
        }
    }

}
