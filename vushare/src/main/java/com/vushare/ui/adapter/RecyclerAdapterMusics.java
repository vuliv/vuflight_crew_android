package com.vushare.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.FileSelectController;
import com.vushare.entity.EntityMusic;

import java.util.ArrayList;

import static android.view.View.OnClickListener;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterMusics extends RecyclerView.Adapter {

    private Context context;
    private final ICallback<String, String> mCallback;
    private ArrayList<EntityMusic> mMusics;

    public RecyclerAdapterMusics(Context context, ICallback callback, ArrayList<EntityMusic> musics) {
        this.context = context;
        this.mCallback = callback;
        this.mMusics = musics;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getMusicViewHolder(parent);
    }

    private MusicViewHolder getMusicViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list2, parent, false);
        final MusicViewHolder holder = new MusicViewHolder(view);
        holder.mImageView.setVisibility(View.VISIBLE);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EntityMusic music = mMusics.get(holder.getAdapterPosition());
                if (music.isSelected()) {
                    music.setSelected(false);
                    FileSelectController.getInstance(context).removeFile(music);
                } else {
                    music.setSelected(true);
                    FileSelectController.getInstance(context).addFile(music);
                }
                notifyDataSetChanged();
                mCallback.onSuccess("");
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MusicViewHolder) {
            EntityMusic entityMusic = mMusics.get(position);

            Glide.with(context)
                    .load(entityMusic.getThumbnail())
                    .apply(new RequestOptions().placeholder(R.drawable.thumb_music))
                    .into(((MusicViewHolder) holder).mImageView);

            ((MusicViewHolder) holder).mTitle.setText(entityMusic.getSongName());
            ((MusicViewHolder) holder).mSubTitle.setText(entityMusic.getAlbumName());

            if (entityMusic.isSelected()) {
                ((MusicViewHolder) holder).mSelectedLayout.setVisibility(View.VISIBLE);
            } else {
                ((MusicViewHolder) holder).mSelectedLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mMusics.size();
    }

    private class MusicViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTitle;
        public TextView mSubTitle;
        public RelativeLayout mSelectedLayout;
        public RelativeLayout mRootLayout;

        public MusicViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.list_item_imageview);
            mTitle = (TextView) itemView.findViewById(R.id.list_item_title);
            mSubTitle = (TextView) itemView.findViewById(R.id.list_item_subtitle);
            mSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.list_item_selected_layout);
            mRootLayout = (RelativeLayout) itemView.findViewById(R.id.list_root);
        }
    }
}