package com.vushare.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.vushare.R;
import com.vushare.ui.fragment.OrdersFragment;
import com.vushare.utility.FragmentUtil;

public class CustomArrayAdapter extends ArrayAdapter<String> {

    private static final String TAG = CustomArrayAdapter.class.getSimpleName();
    private String[] paths;
    private OrdersFragment ordersFragment;
    private TextView textView;
    private int selectedPos;

    public CustomArrayAdapter(@NonNull OrdersFragment ordersFragment, int resource, String[] paths) {
        super(FragmentUtil.getActivity(ordersFragment), resource);
        this.paths = paths;
        this.ordersFragment = ordersFragment;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_currency_drop_down_item, parent, false);
        textView = view.findViewById(R.id.spinnerItem);
        textView.setTextColor(FragmentUtil.getResources(ordersFragment).getColor(R.color.black));
        if (position == selectedPos) {
            textView.setTextColor(FragmentUtil.getResources(ordersFragment).getColor(android.R.color.darker_gray));
        } else {
            textView.setTextColor(FragmentUtil.getResources(ordersFragment).getColor(R.color.black));
        }
        setTextCurrency(position);
        return view;
    }

    private void setTextCurrency(int position) {
        switch (position){
            case 0:
                textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.dollar)+ " " + paths[position]);
                break;
            case 1:
                textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.euro) + " " + paths[position]);
                break;
            case 2:
                textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.gbp) + " " + paths[position]);
                break;
            case 3:
                textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.yen) + " " + paths[position]);
                break;
            case 4:
                textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.rupee) + " " + paths[position]);
                break;
            case 5:
                textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.chf)+ " " + paths[position]);
                break;
        }
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_currency_item, parent, false);
        textView = view.findViewById(R.id.spinnerItem);
        textView.setTextColor(FragmentUtil.getResources(ordersFragment).getColor(R.color.white));
        textView.setText(FragmentUtil.getResources(ordersFragment).getString(R.string.currency) + " " + paths[position]);
        return view;
    }

    @Override
    public int getCount() {
        return paths.length;
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }
}
