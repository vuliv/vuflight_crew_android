package com.vushare.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vucast.utility.DominosAPI;
import com.vushare.R;

import java.util.ArrayList;

import vuflight.com.vuliv.chatlib.entity.EntityCrewChatMessage;


public class RecyclerAdapterChat extends RecyclerView.Adapter {

    private RecyclerView mRecyclerView;
    private int TYPE_USER_ME = 0;
    private int TYPE_USER_OTHER = 1;
    private ArrayList<EntityCrewChatMessage> chatTextList;

    public RecyclerAdapterChat(Context context, String crewName) {
        chatTextList = DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap().get(crewName);
    }

    @Override
    public int getItemCount() {
        return chatTextList.size();
    }

    @Override
    public int getItemViewType(int position) {
        EntityCrewChatMessage message = chatTextList.get(position);
        if (!message.isOtherMsg()) {
            return TYPE_USER_OTHER;
        } else {
            return TYPE_USER_ME;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == TYPE_USER_ME) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_me, parent, false);
            return new UserMe(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_other, parent, false);
            return new UserOther(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EntityCrewChatMessage entityDetail = chatTextList.get(position);
        if (holder instanceof UserMe) {
            ((UserMe) holder).bind(entityDetail);
        } else if (holder instanceof UserOther) {
            ((UserOther) holder).bind(entityDetail);
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }


    public void addUserMessage(ArrayList<EntityCrewChatMessage> entityCrewChatMessages) {
        this.chatTextList = entityCrewChatMessages;
        notifyDataSetChanged();
        if (chatTextList.size() > 2) {
            mRecyclerView.smoothScrollToPosition(chatTextList.size() - 1);
        }
    }

    public class UserMe extends RecyclerView.ViewHolder {
        TextView userMeMessage, tv_crewName, tv_timeStamp;

        UserMe(View itemView) {
            super(itemView);
            userMeMessage = itemView.findViewById(R.id.tv_user_me_text);
            tv_crewName = itemView.findViewById(R.id.tv_crewName);
            tv_timeStamp = itemView.findViewById(R.id.tv_timeStamp);
        }

        void bind(EntityCrewChatMessage entityCrewChatMessage) {
            tv_crewName.setText(entityCrewChatMessage.getCrewName());
            tv_timeStamp.setText(entityCrewChatMessage.getCreatedAt());
            userMeMessage.setText(entityCrewChatMessage.getMessage());
        }
    }

    public class UserOther extends RecyclerView.ViewHolder {
        TextView userOtherMessage, tvUserName, tv_created_at;

        UserOther(View itemView) {
            super(itemView);
            userOtherMessage = itemView.findViewById(R.id.tv_user_other_text);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tv_created_at = itemView.findViewById(R.id.tv_created_at);
        }

        void bind(EntityCrewChatMessage entityCrewChatMessage) {
            tvUserName.setText(entityCrewChatMessage.getCrewName());
            tv_created_at.setText(entityCrewChatMessage.getCreatedAt());
            userOtherMessage.setText(entityCrewChatMessage.getMessage());
        }
    }
}
