package com.vushare.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.vucast.utility.DominosAPI;
import com.vucast.utility.StringUtils;
import com.vuliv.network.entity.EntityDominosRequest;
import com.vushare.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by user on 15-01-2018.
 */

public class RecyclerAdapterOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<String> mOrdersList = new ArrayList<>();

    public RecyclerAdapterOrder(Context context,@NonNull Set<String> orderListList) {
        mContext = context;
        ArrayList<String> mOrdersList = new ArrayList<String>(orderListList);
        for (int i = mOrdersList.size() - 1; i >= 0; i--) {
            this.mOrdersList.add(mOrdersList.get(i));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_order, parent, false);
        final OrderHolder orderHolder = new OrderHolder(view);
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                DominosAPI.getInstance().clickedData = DominosAPI.getInstance().orderMap.get(DominosAPI.getInstance().orderList.get(orderHolder.getAdapterPosition())).getData();
////                mContext.startActivity(new Intent(mContext, OrderDetailActivity.class));
//            }
//        });
        return orderHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof OrderHolder) {
            final String order = mOrdersList.get(position);

            final EntityDominosRequest entityDominosRequest = DominosAPI.getInstance().getRequest(order);

            ((OrderHolder)holder).tvMsisdn.setText(entityDominosRequest.getMsisdn().toUpperCase());

            if (!StringUtils.isEmpty(entityDominosRequest.getServiceRequest())) {
                ((OrderHolder) holder).rv_data.setVisibility(View.GONE);
                ((OrderHolder) holder).tv_total.setVisibility(View.GONE);
                ((OrderHolder) holder).tv_service.setText(entityDominosRequest.getServiceRequest());
                ((OrderHolder) holder).tv_service.setVisibility(View.VISIBLE);
                ((OrderHolder) holder).btn_verfied.setVisibility(View.GONE);
            } else {
                int totalAmount = 0;
                for (EntityDominosRequest.Data data : entityDominosRequest.getData()) {
//                stringBuilder.append(data.getName());
//                stringBuilder.append("\n");
                    totalAmount += data.getPrice();
                }
                ((OrderHolder) holder).rv_data.setVisibility(View.VISIBLE);
                ((OrderHolder) holder).tv_total.setVisibility(View.VISIBLE);
                ((OrderHolder) holder).tv_service.setVisibility(View.GONE);
                ((OrderHolder) holder).rv_data.setLayoutManager(new LinearLayoutManager(mContext));
                ((OrderHolder) holder).rv_data.setAdapter(new RecyclerAdapterOrderDetail(mContext, entityDominosRequest.getData()));
                ((OrderHolder) holder).tv_total.setText("Grand Total : Rs. " + totalAmount);


                if (entityDominosRequest.isVerified()) {
                    ((OrderHolder) holder).btn_verfied.setVisibility(View.GONE);
                } else {
                    ((OrderHolder) holder).btn_verfied.setVisibility(View.VISIBLE);
                    ((OrderHolder) holder).btn_verfied.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DominosAPI.getInstance().getRequest(entityDominosRequest.getKey()).setVerified(true);
                            DominosAPI.getInstance().getRequest(entityDominosRequest.getKey()).setOtpVerified("YES");
                            notifyDataSetChanged();
                        }
                    });
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mOrdersList.size();
    }

    private static class OrderHolder extends RecyclerView.ViewHolder {
        private TextView tvMsisdn, tv_total, tv_service;
        private Button btn_verfied;
        private RecyclerView rv_data;


        public OrderHolder(View itemView) {
            super(itemView);
            tvMsisdn = (TextView) itemView.findViewById(R.id.tv_msisdn);
            tv_service = (TextView) itemView.findViewById(R.id.tv_service);
            rv_data = (RecyclerView) itemView.findViewById(R.id.rv_data);
            tv_total = (TextView) itemView.findViewById(R.id.tv_total);
            btn_verfied = (Button) itemView.findViewById(R.id.btn_verfied);
        }
    }

}
