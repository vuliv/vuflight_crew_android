package com.vushare.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.FileSelectController;
import com.vushare.entity.EntityPhoto;

import java.util.ArrayList;

import static android.view.View.OnClickListener;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class RecyclerAdapterPhotos extends RecyclerView.Adapter {

    private Context context;
    private final ICallback<String, String> mCallback;
    private ArrayList<EntityPhoto> mPhotos;

    public RecyclerAdapterPhotos(Context context, ICallback callback, ArrayList<EntityPhoto> photos) {
        this.context = context;
        this.mCallback = callback;
        this.mPhotos = photos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getPhotoViewHolder(parent);
    }

    private PhotoViewHolder getPhotoViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        final PhotoViewHolder holder = new PhotoViewHolder(view);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EntityPhoto photo = mPhotos.get(holder.getAdapterPosition());
                if (photo.isSelected()) {
                    photo.setSelected(false);
                    FileSelectController.getInstance(context).removeFile(photo);
                } else {
                    photo.setSelected(true);
                    FileSelectController.getInstance(context).addFile(photo);
                }
                notifyDataSetChanged();
                mCallback.onSuccess("");
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PhotoViewHolder) {
            EntityPhoto entityPhoto = mPhotos.get(position);
            Glide.with(context)
                    .setDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.grey_placeholder))
                    .load("file://" + entityPhoto.getPath())
                    .into(((PhotoViewHolder) holder).mImageView);

            if (entityPhoto.isSelected()) {
                ((PhotoViewHolder) holder).mSelectedLayout.setVisibility(View.VISIBLE);

            } else {
                ((PhotoViewHolder) holder).mSelectedLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    private class PhotoViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public RelativeLayout mSelectedLayout;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.photo_imageview);
            mSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.photo_selected_layout);
        }
    }
}