package com.vushare.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vuliv.network.entity.EntityDominosRequest;
import com.vushare.R;
import com.vushare.ui.adapter.RecyclerAdapterMusics;

import java.util.ArrayList;


public class OrderDetail extends ArrayAdapter<EntityDominosRequest.Data> {

    private ArrayList<EntityDominosRequest.Data> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        ImageView vegIcon;
        TextView txtPizzaName;
        TextView txtPrice, tvSize, tvQuantity;
        ImageView pizzaImage;

    }

    public OrderDetail(ArrayList<EntityDominosRequest.Data> data, Context context) {
        super(context, R.layout.order_detail_item, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        EntityDominosRequest.Data model = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.order_detail_item, parent, false);
            viewHolder.vegIcon = (ImageView) convertView.findViewById(R.id.veg_icon);
            viewHolder.txtPizzaName = (TextView) convertView.findViewById(R.id.pizza_text);
            viewHolder.tvSize = (TextView) convertView.findViewById(R.id.tv_size);
            viewHolder.tvQuantity = (TextView) convertView.findViewById(R.id.tv_quantity);
            viewHolder.txtPrice = (TextView) convertView.findViewById(R.id.price_text);
            viewHolder.pizzaImage = (ImageView) convertView.findViewById(R.id.pizza_icon);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (model.isVeg()) {
            viewHolder.vegIcon.setImageResource(R.drawable.mark_veg);
        } else {
            viewHolder.vegIcon.setImageResource(R.drawable.mark_non_veg);
        }

        Glide.with(mContext)
                .load(model.getImage())
                .apply(new RequestOptions().placeholder(R.drawable.thumb_music))
                .into(viewHolder.pizzaImage);

        viewHolder.txtPizzaName.setText(model.getName());
        viewHolder.txtPrice.setText(String.valueOf(model.getPrice()));
        viewHolder.tvSize.setText(    "Size:     " + String.valueOf(model.getSize()));
        viewHolder.tvQuantity.setText("Quantity: " + String.valueOf(model.getQuantity()));

        // Return the completed view to render on screen
        return convertView;
    }


}
