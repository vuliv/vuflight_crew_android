package com.vushare.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vushare.R;
import com.vushare.controller.InventoryManager;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.ui.activity.BaseActivity;
import com.vushare.ui.activity.CrewChatActivity;
import com.vushare.ui.adapter.CartViewRecyclerViewAdapter;
import com.vushare.utility.Constants;
import com.vushare.utility.FragmentUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartViewFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = CartViewFragment.class.getSimpleName();

    private RecyclerView rvCartView;
    private CartViewRecyclerViewAdapter cartViewRvAdapter;
    private RelativeLayout lnrLytPerishable, lnrLytNonPerishable, lnrLytMerchandise, lnrLytDutyFree, lnrLytDryStore, lnrLytCrewFood;
    private ImageView imgPerishable, imgNonPerishable, imgMerchandise, imgDutyFree, imgDryStore, imgCrewFood, imgCrewImage;
    private TextView txtPerishable, txtNonPerishable, txtMerchandise, txtDutyFree, txtDryStore, txtCrewFood;
    private TextView txtPerishableCount, txtNonPerishableCount, txtMerchandiseCount, txtDutyFreeCount, txtDryStoreCount, txtCrewFoodCount, txtCrewName;
    private FoodItemType selectedFoodItemType = FoodItemType.Perishable;
    private Map<FoodItemType, List<CartInventory.Inventory>> cartInventoryMap = new HashMap<>();
    private String cartNo;
    private int empId;
    // Image View Chat
    ImageView iv_open_chat;

    public enum FoodItemType {
        Perishable, NonPerishable, Merchandise, DutyFree, DryStore, CrewFood;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart_view, container, false);
        if (getArguments() != null) {
            cartNo = getArguments().getString(Constants.CART_NO_KEY);
            empId = getArguments().getInt(Constants.EMP_ID_KEY);
        }

        initComponents(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).setSelectedCrewOnTB(cartNo, empId);
    }

    private void initComponents(View view) {
        txtCrewName = view.findViewById(R.id.txtCrewName);

        rvCartView = (RecyclerView) view.findViewById(R.id.rvCartView);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(FragmentUtil.getActivity(CartViewFragment.this), 2);
        rvCartView.setLayoutManager(gridLayoutManager);

        lnrLytPerishable = (RelativeLayout) view.findViewById(R.id.lnrLytPerishable);
        lnrLytPerishable.setOnClickListener(this);
        imgPerishable = (ImageView) view.findViewById(R.id.imgPerishable);
        txtPerishable = (TextView) view.findViewById(R.id.txtPerishable);
        txtPerishableCount = (TextView) view.findViewById(R.id.txtPerishableCount);

    /*    lnrLytNonPerishable = (RelativeLayout) view.findViewById(R.id.lnrLytNonPerishable);
        lnrLytNonPerishable.setOnClickListener(this);
        imgNonPerishable = (ImageView) view.findViewById(R.id.imgNonPerishable);
        txtNonPerishable = (TextView) view.findViewById(R.id.txtNonPerishable);
        txtNonPerishableCount = (TextView) view.findViewById(R.id.txtNonPerishableCount);
*/
        lnrLytMerchandise = (RelativeLayout) view.findViewById(R.id.lnrLytMerchandise);
        lnrLytMerchandise.setOnClickListener(this);
        imgMerchandise = (ImageView) view.findViewById(R.id.imgMerchandise);
        txtMerchandise = (TextView) view.findViewById(R.id.txtMerchandise);
        txtMerchandiseCount = (TextView) view.findViewById(R.id.txtMerchandiseCount);

     /*   lnrLytDutyFree = (RelativeLayout) view.findViewById(R.id.lnrLytDutyFree);
        lnrLytDutyFree.setOnClickListener(this);
        imgDutyFree = (ImageView) view.findViewById(R.id.imgDutyFree);
        txtDutyFree = (TextView) view.findViewById(R.id.txtDutyFree);
        txtDutyFreeCount = (TextView) view.findViewById(R.id.txtDutyFreeCount);
*/
       /* lnrLytDryStore = (RelativeLayout) view.findViewById(R.id.lnrLytDryStore);
        lnrLytDryStore.setOnClickListener(this);
        imgDryStore = (ImageView) view.findViewById(R.id.imgDryStore);
        txtDryStore = (TextView) view.findViewById(R.id.txtDryStore);
        txtDryStoreCount = (TextView) view.findViewById(R.id.txtDryStoreCount);
*/
      /*  lnrLytCrewFood = (RelativeLayout) view.findViewById(R.id.lnrLytCrewFood);
        lnrLytCrewFood.setOnClickListener(this);
        imgCrewFood = (ImageView) view.findViewById(R.id.imgCrewFood);
        txtCrewFood = (TextView) view.findViewById(R.id.txtCrewFood);
        txtCrewFoodCount = (TextView) view.findViewById(R.id.txtCrewFoodCount);
*/
        imgCrewImage = view.findViewById(R.id.imgCrewImage);

        iv_open_chat = view.findViewById(R.id.iv_open_chat);
        iv_open_chat.setOnClickListener(this);
        setCrewMemberData();
    }

    private void setCrewMemberData() {
        List<CrewProfile> crewProfileList = CrewProfiles.getInstance().getCrewProfileList();
        for (CrewProfile crewProfile : crewProfileList) {
            if (crewProfile.getEmpId() == empId) {
                Glide.with(this)
                        .load(crewProfile.getImageUrl())
                        .into(imgCrewImage);
                txtCrewName.setText(crewProfile.getEmpName());
            }
        }
        hideChatForMe();
    }

    private void hideChatForMe() {
        int loggedInId = CrewProfiles.getInstance().getLogdInCrewMemId();
        if (loggedInId == empId) {
            iv_open_chat.setVisibility(View.GONE);
        } else {
            iv_open_chat.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (cartViewRvAdapter == null) {
            fillCartInventoryMap();
            cartViewRvAdapter = new CartViewRecyclerViewAdapter(this, null, empId);
            selectFoodItemType(FoodItemType.Perishable);
        }
        setCounts();
        rvCartView.setAdapter(cartViewRvAdapter);
    }

    private void fillCartInventoryMap() {
        Map<String, List<CartInventory.Inventory>> inventoryMap = InventoryManager.getInstance().getInventoryMap(cartNo);
        if (inventoryMap != null) {
            cartInventoryMap.put(FoodItemType.Perishable, inventoryMap.get(CartInventory.ITEM_TYPE_MEAL));
            cartInventoryMap.put(FoodItemType.Merchandise, inventoryMap.get(CartInventory.ITEM_TYPE_ECOM));
        }
    }

    private void setCounts() {
        for (FoodItemType foodItemType : cartInventoryMap.keySet()) {
            List<CartInventory.Inventory> inventories = cartInventoryMap.get(foodItemType);
            if (inventories != null) {
                switch (foodItemType) {

                    case Perishable:
                        txtPerishableCount.setText("" + inventories.size());
                        break;

                   /* case NonPerishable:
                        txtNonPerishableCount.setText("" + inventories.size());
                        break;
*/
                    case Merchandise:
                        txtMerchandiseCount.setText("" + inventories.size());
                        break;

                  /*  case DutyFree:
                        txtDutyFreeCount.setText("" + inventories.size());
                        break;

                    case DryStore:
                        txtDryStoreCount.setText("" + inventories.size());
                        break;

                    case CrewFood:
                        txtCrewFoodCount.setText("" + inventories.size());
                        break;*/
                }
            }
        }
    }

    private void selectFoodItemType(FoodItemType foodItemType) {
        selectedFoodItemType = foodItemType;
        cartViewRvAdapter.setList(cartInventoryMap.get(foodItemType));
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.lnrLytPerishable) {
            if (selectedFoodItemType != FoodItemType.Perishable) {
                changeBackGroundOnclick(FoodItemType.Perishable, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.Perishable);
            }

        }/* else if (i == R.id.lnrLytNonPerishable) {
            if (selectedFoodItemType != FoodItemType.NonPerishable) {
                changeBackGroundOnclick(FoodItemType.NonPerishable, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.NonPerishable);
            }

        }*/ else if (i == R.id.lnrLytMerchandise) {
            if (selectedFoodItemType != FoodItemType.Merchandise) {
                changeBackGroundOnclick(FoodItemType.Merchandise, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.Merchandise);
            }

        } /*else if (i == R.id.lnrLytDutyFree) {
            if (selectedFoodItemType != FoodItemType.DutyFree) {
                changeBackGroundOnclick(FoodItemType.DutyFree, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.DutyFree);
            }

        } else if (i == R.id.lnrLytDryStore) {
            if (selectedFoodItemType != FoodItemType.DryStore) {
                changeBackGroundOnclick(FoodItemType.DryStore, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.DryStore);
            }

        } else if (i == R.id.lnrLytCrewFood) {
            if (selectedFoodItemType != FoodItemType.CrewFood) {
                changeBackGroundOnclick(FoodItemType.CrewFood, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.CrewFood);
            }
        } */ else if (i == R.id.iv_open_chat) {    // Added By Sumit Agrawal
            Intent chatIntent = new Intent(getActivityRef(), CrewChatActivity.class);
            chatIntent.putExtra(Constants.EMP_ID_KEY, empId);
            startActivity(chatIntent);
        }
    }

    private void changeBackGroundOnclick(FoodItemType foodItemType, boolean isSelected) {
        switch (foodItemType) {
            case Perishable:
                if (isSelected) {
                    lnrLytPerishable.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgPerishable.setImageResource(R.drawable.inventory_perishable_selected);
                    txtPerishable.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                    txtPerishableCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytPerishable.setBackgroundResource(R.drawable.rounded_edittext);
                    imgPerishable.setImageResource(R.drawable.inventory_perishable);
                    txtPerishable.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                    txtPerishableCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                }
                break;
           /* case NonPerishable:
                if (isSelected) {
                    lnrLytNonPerishable.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgNonPerishable.setImageResource(R.drawable.inventory_non_perishable_selected);
                    txtNonPerishable.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                    txtNonPerishableCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytNonPerishable.setBackgroundResource(R.drawable.rounded_edittext);
                    imgNonPerishable.setImageResource(R.drawable.inventory_non_perishable);
                    txtNonPerishable.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                    txtNonPerishableCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                }
                break;*/
            case Merchandise:
                if (isSelected) {
                    lnrLytMerchandise.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgMerchandise.setImageResource(R.drawable.inventory_merchandise_selected);
                    txtMerchandise.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                    txtMerchandiseCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytMerchandise.setBackgroundResource(R.drawable.rounded_edittext);
                    imgMerchandise.setImageResource(R.drawable.inventory_merchandise);
                    txtMerchandise.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                    txtMerchandiseCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                }
                break;
          /*  case DutyFree:
                if (isSelected) {
                    lnrLytDutyFree.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgDutyFree.setImageResource(R.drawable.inventory_duty_free_selected);
                    txtDutyFree.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                    txtDutyFreeCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytDutyFree.setBackgroundResource(R.drawable.rounded_edittext);
                    imgDutyFree.setImageResource(R.drawable.inventory_duty_free);
                    txtDutyFree.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                    txtDutyFreeCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                }
                break;
            case DryStore:
                if (isSelected) {
                    lnrLytDryStore.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgDryStore.setImageResource(R.drawable.inventory_dish_selected);
                    txtDryStore.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                    txtDryStoreCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytDryStore.setBackgroundResource(R.drawable.rounded_edittext);
                    imgDryStore.setImageResource(R.drawable.inventory_dish);
                    txtDryStore.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                    txtDryStoreCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                }
                break;
            case CrewFood:
                if (isSelected) {
                    lnrLytCrewFood.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgCrewFood.setImageResource(R.drawable.inventory_tray_selected);
                    txtCrewFood.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                    txtCrewFoodCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytCrewFood.setBackgroundResource(R.drawable.rounded_edittext);
                    imgCrewFood.setImageResource(R.drawable.inventory_tray);
                    txtCrewFood.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                    txtCrewFoodCount.setTextColor(FragmentUtil.getResources(CartViewFragment.this).getColor(R.color.colorBlue));
                }
                break;*/
        }
    }
}

