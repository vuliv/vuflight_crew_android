package com.vushare.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.thanosfisherman.wifiutils.WifiUtils;
import com.thanosfisherman.wifiutils.wifiConnect.ConnectionSuccessListener;
import com.thanosfisherman.wifiutils.wifiScan.ScanResultsListener;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.ui.activity.OnBoardSalesActivity;
import com.vushare.ui.view.CircleImageView;
import com.vushare.utility.Constants;
import com.vushare.utility.ConversionUtil;
import com.vushare.utility.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;


public class WelcomeFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final String TAG = WelcomeFragment.class.getSimpleName();
    private TextView tvLetsFly, txtViewSubmit, txtContinueAsGuest;
    private Spinner spinnerSeatRange, spinnerCart;
    // private String[] seatRange = {"1A - 2F", "3A - 4F", "5A - 6F", "7A - 8F", "9A- 9F"};
    private ArrayAdapter arrayAdapterSeatRange, arrayAdapterCart;
    private EditText etCrewName, etEmpId, etFlightNo;
    private CircleImageView ivCrewProfileImage;
    public static final int PERMISSION_REQUEST_CODE_EXTERNAL_STORAGE = 100;
    private Resources res;
    public static final int REQUIRED_SIZE = 512;
    private String profileImageURI;
    private boolean connectedToVuFlight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WifiUtils.withContext(FragmentUtil.getActivity(WelcomeFragment.this)).scanWifi(new ScanResultsListener() {
            @Override
            public void onScanResults(@NonNull List<ScanResult> scanResults) {
                ScanResult result = null;
                for (ScanResult scanResult : scanResults) {
                    //if (scanResult.SSID.startsWith("VuFlightServer")) {
                    if (scanResult.SSID.contains(Constants.WIFI_NAME)) {
                        scanResult.level = WifiManager.calculateSignalLevel(scanResult.level, 5);
                        result = scanResult;
                        break;
                    }
                }
                if (result != null) {
                    WifiUtils.withContext(FragmentUtil.getActivity(WelcomeFragment.this))
                            .connectWith(result.SSID, result.BSSID)
                            .setTimeout(30000)
                            .onConnectionResult(new ConnectionSuccessListener() {
                                @Override
                                public void isSuccessful(boolean isSuccess) {
                                    connectedToVuFlight = true;
                                }
                            })
                            .start();
                }
            }
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        initComponents(view);
        res = FragmentUtil.getResources(WelcomeFragment.this);
        return view;
    }

    private void initComponents(View view) {
        //  spinnerSeatRange = (Spinner) view.findViewById(R.id.spinnerSeatRange);
        // spinnerSeatRange.setOnItemSelectedListener(this);
        spinnerCart = (Spinner) view.findViewById(R.id.spinnerCart);
        spinnerCart.setOnItemSelectedListener(this);
        txtViewSubmit = (TextView) view.findViewById(R.id.txtViewSubmit);
        txtViewSubmit.setOnClickListener(this);
        txtContinueAsGuest = (TextView) view.findViewById(R.id.txtContinueAsGuest);
        txtContinueAsGuest.setOnClickListener(this);
        //etCrewName = (EditText) view.findViewById(R.id.etCrewName);
        etEmpId = (EditText) view.findViewById(R.id.etEmpId);
        //  etFlightNo = (EditText) view.findViewById(R.id.etFlightNo);
        ivCrewProfileImage = (CircleImageView) view.findViewById(R.id.imgVwProfile);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
      /*  arrayAdapterSeatRange = new ArrayAdapter(FragmentUtil.getActivity(this), R.layout.spinner_item, seatRange);
        arrayAdapterSeatRange.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSeatRange.setAdapter(arrayAdapterSeatRange);
*/
        arrayAdapterCart = new ArrayAdapter(FragmentUtil.getActivity(this), R.layout.spinner_item, Constants.SEAT_CART);
        arrayAdapterCart.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCart.setAdapter(arrayAdapterCart);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.txtViewSubmit) {
            if (validateCrewDetails()) {
                if (connectedToVuFlight) {
                    authenticateCrew();
                } else {
                    Toast.makeText(FragmentUtil.getActivity(WelcomeFragment.this), getString(R.string.connect_to_hotspot), Toast.LENGTH_LONG).show();
                }
            }
        } else if (i == R.id.imgVwProfile) {
            int permissionCheck = ContextCompat.checkSelfPermission(FragmentUtil.getActivity(this),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(FragmentUtil.getActivity(this),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        Constants.PERMISSION_REQUEST_CODE);
            } else {
                selectImage();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.REQUEST_CAMERA) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");

                storeImage(photo);
                decodeFile(getOutputMediaFile().getAbsolutePath());

            } else if (requestCode == Constants.SELECT_FILE) {
                Uri selectedImage = data.getData();
                Bitmap photo = null;
                try {
                    photo = MediaStore.Images.Media.getBitmap(FragmentUtil.getActivity(this).getContentResolver(), selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (photo != null) {
                    storeImage(photo);
                    decodeFile(getOutputMediaFile().getAbsolutePath());
                } else {
                    decodeFile(getOutputMediaFile().getAbsolutePath());
                }
            }
        }
    }

    private void setUserData() {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        CrewProfile crewProfile = new CrewProfile();
        crewProfile.setCrewProfile("", Integer.parseInt(etEmpId.getText().toString()), "", spinnerCart.getSelectedItem().toString(), "");
        //  crewProfile.setCrewProfile(etCrewName.getText().toString(), Integer.parseInt(etEmpId.getText().toString()), etFlightNo.getText().toString(), spinnerCart.getSelectedItem().toString(), spinnerSeatRange.getSelectedItem().toString());
        crewProfiles.addCrewProfile(crewProfile);

        crewProfiles.setLogdInCrewMemId(crewProfile.getEmpId());
        crewProfiles.setLogdInMemCartNo(crewProfile.getCartNo());
        crewProfiles.setLoginMemName(crewProfile.getCrewName());
        crewProfiles.setFlightMemName(crewProfile.getFlightNo());
        crewProfiles.updateSelectedProfileImgUri(FragmentUtil.getActivity(WelcomeFragment.this), profileImageURI);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private boolean validateCrewDetails() {
        boolean isFieldsValid = false;
        // String crewName = etCrewName.getText().toString();
        String crewEmpId = etEmpId.getText().toString();
        //String flightNo = etFlightNo.getText().toString();

        /*if (crewName == null || crewName.isEmpty()) {
            Toast.makeText(FragmentUtil.getActivity(WelcomeFragment.this), FragmentUtil.getResources(this).getString(R.string.empty_emp_name), Toast.LENGTH_LONG).show();
        } else*/
        if (crewEmpId == null || crewEmpId.isEmpty()) {
            Toast.makeText(FragmentUtil.getActivity(WelcomeFragment.this), FragmentUtil.getResources(this).getString(R.string.empty_emp_id), Toast.LENGTH_LONG).show();
        }/* else if (flightNo == null || flightNo.isEmpty()) {
            Toast.makeText(FragmentUtil.getActivity(WelcomeFragment.this), FragmentUtil.getResources(this).getString(R.string.empty_flight_no), Toast.LENGTH_LONG).show();
        }*/ else {
            isFieldsValid = true;
        }
        return isFieldsValid;
    }

    private void authenticateCrew() {
        final Activity activity = FragmentUtil.getActivity(WelcomeFragment.this);
        String url = "http://192.168.43.1:8080/api/authenticateCrew";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("empId", Integer.parseInt(etEmpId.getText().toString()));
            //  jsonObject.put("flightNumber",etFlightNo.getText().toString());
            jsonObject.put("cartNumber", spinnerCart.getSelectedItem().toString());
            //jsonObject.put("seatRange",spinnerSeatRange.getSelectedItem().toString());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        JsonRequest<Boolean> authenticateCrewDetailsResponse = new JsonRequest<Boolean>(Request.Method.POST, url, jsonObject.toString(), new Response.Listener<Boolean>() {
            @Override
            public void onResponse(Boolean response) {
                if (response) {
                    setUserData();
                    getActivity().startActivity(new Intent(FragmentUtil.getActivity(WelcomeFragment.this), OnBoardSalesActivity.class));
                    FragmentUtil.getActivity(WelcomeFragment.this).finish();
                } else {
                    Toast.makeText(activity, FragmentUtil.getResources(WelcomeFragment.this).getString(R.string.invalid_crew), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, "Volley Error " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Response<Boolean> parseNetworkResponse(NetworkResponse response) {
                Boolean parsed;
                try {
                    parsed = Boolean.valueOf(new String(response.data, HttpHeaderParser.parseCharset(response.headers)));
                } catch (UnsupportedEncodingException e) {
                    parsed = Boolean.valueOf(new String(response.data));
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        };


        authenticateCrewDetailsResponse.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(activity, authenticateCrewDetailsResponse, "authenticateCrew");
    }

    public void selectImage() {
        final CharSequence[] items = {res.getString(R.string.take_photo), res.getString(R.string.choose_from_gallery)};

        AlertDialog.Builder builder = new AlertDialog.Builder(FragmentUtil.getActivity(WelcomeFragment.this));
        builder.setTitle(res.getString(R.string.add_photo));
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(res.getString(R.string.take_photo))) {
                    int permissionCheck = ContextCompat.checkSelfPermission(FragmentUtil.getActivity(WelcomeFragment.this),
                            Manifest.permission.CAMERA);
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        //Log.d(TAG, "PermissionTask Request Exception getDeviceMobileNumber CAMERA NO PERMISSION");
                        ActivityCompat.requestPermissions(FragmentUtil.getActivity(WelcomeFragment.this),
                                new String[]{Manifest.permission.CAMERA},
                                Constants.PERMISSION_REQUEST_LAUNCH_CAMERA);
                    } else {
                        launchCameraIntent();
                    }
                } else if (items[item].equals(res.getString(R.string.choose_from_gallery))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType(Constants.MIMETYPE_IMG);
                    startActivityForResult(
                            Intent.createChooser(intent, Constants.SELECT_FILES),
                            Constants.SELECT_FILE);
                }
            }
        });
        builder.show();
    }

    public void launchCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, Constants.REQUEST_CAMERA);
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            //Log.d(TAG, "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);

            fos.close();
        } catch (FileNotFoundException e) {
            //Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            //Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        File mediaStorageDir = FragmentUtil.getApplication(this).getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String mImageName = "profile_image.jpg";
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void decodeFile(String filePath) {
        profileImageURI = filePath;
        Bitmap bitmap = ConversionUtil.decodeFile(filePath, REQUIRED_SIZE);
        if (bitmap != null) {
            ivCrewProfileImage.setImageBitmap(bitmap);
        } else {
            ivCrewProfileImage.setImageResource(R.mipmap.ic_launcher);
        }
    }


}
