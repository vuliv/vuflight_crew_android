package com.vushare.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.entity.EntityDirectoryDetail;
import com.vushare.device.ContentRetriever;
import com.vushare.ui.activity.FileChooseActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by MB0000021 on 8/3/2017.
 */

public class FolderFragment extends Fragment {

    private Context mContext;
    private ContentRetriever contentRetriever;
    private View mRootView;
//    private BreadcrumbToolbar toolbar;

    private ICallback mCallback = new ICallback() {
        @Override
        public void onSuccess(Object object) {
            loadDirectoryPage(object.toString(), true);
        }

        @Override
        public void onFailure(Object msg) {}
    };

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FolderFragment newInstance() {
        FolderFragment fragment = new FolderFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_folder, container, false);
        contentRetriever = new ContentRetriever(mContext);
        // initialize toolbar
//        initToolbar();
        // Load root page
        loadDirectoryPage("", false);
        return mRootView;
    }

    /*@Override
    public void onBackStackChanged() {
        Fragment currentFragment = ((FileChooseActivity) mContext).getCurrentFragment();
        if (currentFragment instanceof FolderFragment) {
            if (toolbar != null) {
                int stackCount = ((FileChooseActivity) mContext).getSupportFragmentManager().getBackStackEntryCount();
                // Handle breadcrumb items add/remove anywhere, as long as you track their size
                toolbar.onBreadcrumbAction(stackCount, Color.BLACK);
            }
        }
    }*/

    /*@Override
    public void onBreadcrumbToolbarItemPop(int stackSize) {

    }

    @Override
    public void onBreadcrumbToolbarEmpty() {

    }

    @Override
    public String getFragmentName() {
        Fragment fragment = ((FileChooseActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.folder_container);
        if (fragment instanceof FragmentFolderView) {
            return ((FragmentFolderView) fragment).getFragmentName();
        }
        return null;
    }*/

    /*private void initToolbar() {
        toolbar = (BreadcrumbToolbar) mRootView.findViewById(R.id.folder_toolbar);
        toolbar.setBreadcrumbToolbarListener(this);
//        toolbar.setTitle(getString(R.string.navigation_drawer_folder));
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.BLACK);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//        ATH.setActivityToolbarColorAuto(this, getATHToolbar());
//        ATH.setStatusbarColor(this, ColorUtils.shiftColor(primaryColor, 0.6F));
//        ((FileChooseActivity) mContext).getSupportFragmentManager().addOnBackStackChangedListener(this);
    }*/

    public void loadDirectoryPage(String path, boolean addToBackStack) {
        ArrayList<EntityDirectoryDetail> directoryArrayList;
        if (addToBackStack) {
            directoryArrayList = contentRetriever.getAllDirectories(path);
            // sort list
            Collections.sort(directoryArrayList, new Comparator<EntityDirectoryDetail>() {
                @Override
                public int compare(EntityDirectoryDetail s1, EntityDirectoryDetail s2) {
                    return s1.getFileName().compareToIgnoreCase(s2.getFileName());
                }
            });
        } else {
            directoryArrayList = contentRetriever.getStorageDirectories();
        }
        FragmentTransaction ft = ((FileChooseActivity) mContext).getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        String fragmentName = "";
        File file = new File(path);
        if (file != null) {
            fragmentName = file.getName();
        }
        ft.replace(R.id.folder_container,
                FragmentFolderView.newInstance(fragmentName,
                        directoryArrayList, contentRetriever,
                        mCallback)).commitAllowingStateLoss();
    }
}
