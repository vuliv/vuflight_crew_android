package com.vushare.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vushare.R;
import com.vushare.controller.InventoryManager;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfiles;
import com.vushare.ui.adapter.ReconciliationRecyclerViewAdapter;
import com.vushare.utility.FragmentUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReconciliationFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = ReconciliationFragment.class.getSimpleName();
    private RelativeLayout rltLytPerishable, rltLytMerchandise, rltLytDryStore, rltLytNonPerishable, rltLytDutyFree, rltLytCrewFood;
    private RecyclerView rvReconciliation;
    private ReconciliationRecyclerViewAdapter reconciliationRecyclerViewAdapter;
    private Map<FoodItemType, List<CartInventory.Inventory>> cartInventoryMap = new HashMap<>();
    private FoodItemType selectedFoodItemType = FoodItemType.Perishable;
    private ImageView imgPerishable, imgMerchandise, imgDryStore, imgNonPerishable, imgDutyFree, imgCrewFood;
    private TextView txtPerishable, txtMerchandise, txtDryStore, txtNonPerishable, txtDutyFree, txtCrewFood;

    public enum FoodItemType {
        Perishable, NonPerishable, Merchandise, DutyFree, DryStore, CrewFood;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reconciliation, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(View view) {
        rltLytPerishable = view.findViewById(R.id.rltLytPerishable);
        rltLytMerchandise = view.findViewById(R.id.rltLytMerchandise);
        //rltLytDryStore = view.findViewById(R.id.rltLytDryStore);   // Commented By Sumit Agrawal,as of now we are not showing this Data
        //rltLytNonPerishable = view.findViewById(R.id.rltLytNonPerishable);
        //rltLytDutyFree = view.findViewById(R.id.rltLytDutyFree);
        //rltLytCrewFood = view.findViewById(R.id.rltLytCrewFood);
        imgPerishable = view.findViewById(R.id.imgPerishable);
        imgMerchandise = view.findViewById(R.id.imgMerchandise);
        //imgDryStore = view.findViewById(R.id.imgDryStore);             // Commented By Sumit Agrawal,as of now we are not showing this Data
        //imgNonPerishable = view.findViewById(R.id.imgNonPerishable);
        //imgDutyFree = view.findViewById(R.id.imgDutyFree);
        //imgCrewFood = view.findViewById(R.id.imgCrewFood);
        txtPerishable = view.findViewById(R.id.txtPerishable);
        txtMerchandise = view.findViewById(R.id.txtMerchandise);
        // txtDryStore = view.findViewById(R.id.txtDryStore);            // Commented By Sumit Agrawal,as of now we are not showing this Data
        //txtNonPerishable = view.findViewById(R.id.txtNonPerishable);
        //txtDutyFree = view.findViewById(R.id.txtDutyFree);
        //  txtCrewFood = view.findViewById(R.id.txtCrewFood);

        //rltLytPerishable.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue,null)); // Changed by sumit agrawal,because it was not working below OS version 21
        rltLytPerishable.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));

        rltLytPerishable.setOnClickListener(this);
        rltLytMerchandise.setOnClickListener(this);
        //rltLytDryStore.setOnClickListener(this);                       // Commented By Sumit Agrawal,as of now we are not showing this Data
        //rltLytNonPerishable.setOnClickListener(this);
        // rltLytDutyFree.setOnClickListener(this);
        // rltLytCrewFood.setOnClickListener(this);

        rvReconciliation = view.findViewById(R.id.rvReconciliation);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FragmentUtil.getActivity(this));
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvReconciliation.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.rltLytPerishable) {
            if (selectedFoodItemType != FoodItemType.Perishable) {
                changeBackGroundOnclick(FoodItemType.Perishable, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.Perishable);
            }
        } else if (i == R.id.rltLytMerchandise) {
            if (selectedFoodItemType != FoodItemType.Merchandise) {
                changeBackGroundOnclick(FoodItemType.Merchandise, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.Merchandise);
            }

        } /*else if (i == R.id.rltLytDryStore) {                        // Commented By Sumit Agrawal,as of now we are not showing this Data
            if (selectedFoodItemType != FoodItemType.DryStore) {
                changeBackGroundOnclick(FoodItemType.DryStore, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.DryStore);
            }
        } else if (i == R.id.rltLytNonPerishable) {
            if (selectedFoodItemType != FoodItemType.NonPerishable) {
                changeBackGroundOnclick(FoodItemType.NonPerishable, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.NonPerishable);
            }
        } else if (i == R.id.rltLytDutyFree) {
            if (selectedFoodItemType != FoodItemType.DutyFree) {
                changeBackGroundOnclick(FoodItemType.DutyFree, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.DutyFree);
            }
        } else if (i == R.id.rltLytCrewFood) {
            if (selectedFoodItemType != FoodItemType.CrewFood) {
                changeBackGroundOnclick(FoodItemType.CrewFood, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.CrewFood);
            }
        }*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (reconciliationRecyclerViewAdapter == null) {
            fillCartInventoryMap();
            reconciliationRecyclerViewAdapter = new ReconciliationRecyclerViewAdapter(this);
            selectFoodItemType(FoodItemType.Perishable);
        }
        rvReconciliation.setAdapter(reconciliationRecyclerViewAdapter);
    }

    private void fillCartInventoryMap() {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        Map<String, List<CartInventory.Inventory>> inventoryMap = InventoryManager.getInstance().getInventoryMap(crewProfiles.getLogdInCartNo());
        cartInventoryMap.put(FoodItemType.Perishable, inventoryMap.get(CartInventory.ITEM_TYPE_MEAL));
        cartInventoryMap.put(FoodItemType.Merchandise, inventoryMap.get(CartInventory.ITEM_TYPE_ECOM));
    }

    private void selectFoodItemType(FoodItemType foodItemType) {
        selectedFoodItemType = foodItemType;
        reconciliationRecyclerViewAdapter.setList(cartInventoryMap.get(foodItemType));
    }

    private void changeBackGroundOnclick(FoodItemType foodItemType, boolean isSelected) {
        switch (foodItemType) {
            case Perishable:
                if (isSelected) {
                    rltLytPerishable.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                    imgPerishable.setImageResource(R.drawable.inventory_perishable_selected);
                    txtPerishable.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorWhite));
                } else {
                    rltLytPerishable.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.reconciliation_background_tab_color));
                    imgPerishable.setImageResource(R.drawable.inventory_perishable);
                    txtPerishable.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                }
                break;
            case NonPerishable:
                if (isSelected) {
                    rltLytNonPerishable.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                    imgNonPerishable.setImageResource(R.drawable.inventory_non_perishable_selected);
                    txtNonPerishable.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorWhite));
                } else {
                    rltLytNonPerishable.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.reconciliation_background_tab_color));
                    imgNonPerishable.setImageResource(R.drawable.inventory_non_perishable);
                    txtNonPerishable.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                }
                break;
            case Merchandise:
                if (isSelected) {
                    rltLytMerchandise.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                    imgMerchandise.setImageResource(R.drawable.inventory_merchandise_selected);
                    txtMerchandise.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorWhite));
                } else {
                    rltLytMerchandise.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.reconciliation_background_tab_color));
                    imgMerchandise.setImageResource(R.drawable.inventory_merchandise);
                    txtMerchandise.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                }
                break;
            case DutyFree:
                if (isSelected) {
                    rltLytDutyFree.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                    imgDutyFree.setImageResource(R.drawable.inventory_duty_free_selected);
                    txtDutyFree.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorWhite));
                } else {
                    rltLytDutyFree.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.reconciliation_background_tab_color));
                    imgDutyFree.setImageResource(R.drawable.inventory_duty_free);
                    txtDutyFree.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                }
                break;
            case DryStore:
                if (isSelected) {
                    rltLytDryStore.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                    imgDryStore.setImageResource(R.drawable.inventory_dish_selected);
                    txtDryStore.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorWhite));
                } else {
                    rltLytDryStore.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.reconciliation_background_tab_color));
                    imgDryStore.setImageResource(R.drawable.inventory_dish);
                    txtDryStore.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                }
                break;
            case CrewFood:
                if (isSelected) {
                    rltLytCrewFood.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                    imgCrewFood.setImageResource(R.drawable.inventory_tray_selected);
                    txtCrewFood.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorWhite));
                } else {
                    rltLytCrewFood.setBackgroundColor(FragmentUtil.getResources(this).getColor(R.color.reconciliation_background_tab_color));
                    imgCrewFood.setImageResource(R.drawable.inventory_tray);
                    txtCrewFood.setTextColor(FragmentUtil.getResources(this).getColor(R.color.colorBlue));
                }
                break;
        }

    }
}
