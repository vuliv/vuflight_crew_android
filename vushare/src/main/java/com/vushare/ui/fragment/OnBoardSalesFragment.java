package com.vushare.ui.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.TheAplication;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.vucast.constants.Constants;
import com.vuliv.network.entity.EntityDominosRequest;
import com.vushare.R;
import com.vushare.controller.InventoryManager;
import com.vushare.controller.NetworkController;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.model.InventoryCountDetail;
import com.vushare.model.PassengerCount;
import com.vushare.ui.activity.BaseActivity;
import com.vushare.ui.activity.OrdersActivity;
import com.vushare.utility.DeviceUtil;
import com.vushare.utility.FragmentUtil;
import com.vushare.utility.WifiUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OnBoardSalesFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = OnBoardSalesFragment.class.getSimpleName();

    private TextView txtViewSeatMap, txtViewInventoryTotal;
    TextView txtViewBeverageCount, txtViewBeverage, txtViewSandwich, txtViewSandwichCount, txtViewHotSnacks, txtViewHotSnacksCount, txtViewMunch, txtViewMunchCount, txtViewMealOther, txtViewMealOtherCount;
    TextView txtViewNonVegBeveragesCount, txtViewNonVegSandwichCount, txtViewNonVegHotSnacksCount, txtViewNonVegMunchCount, txtViewNonVegMealOtherCount;
    private CrewProfiles crewProfiles;
    private TextView txtViewMaleCount, txtViewFemaleCount, txtViewSeniorCount, txtViewChildrenCount, txtViewSpecialCareCount, txtViewTotalCount, txtViewPreBookedTotal;
    //PreOrder Veg Views
    TextView txtViewVegBevPre, txtViewVegBevPreCount, txtViewVegMunPre, txtViewVegMunPreCount, txtViewVegSandPre, txtViewVegSandPreCount, txtViewVegSnacksPre, txtViewVegSnacksPreCount, txtViewVegOtherLunch, txtViewVegOtherLunchCount;
    //PreOrder NonVeg Views
    TextView txtViewNonVegBrevPreCount, txtViewNonVegMunPreCount, txtViewNonVegSandPreCount, txtViewNonVegSnacksPreCount, txtViewNonVegLunchPreCount;
    private EntityDominosRequest request;
    private Map<String, List<EntityDominosRequest>> requestMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.UPDATE_ORDERED_ITEM);
        LocalBroadcastManager.getInstance(TheAplication.instance).registerReceiver(broadcastReceiver, intentFilter);
        crewProfiles = CrewProfiles.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboard_sales, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //loadInventory(crewProfiles.getLogdInCartNo(), crewProfiles.getLogdInCrewMemId());
        loadCrewDetail();
    }

    private void setPassengerData(PassengerCount passengerCount) {
        txtViewMaleCount.setText("" + passengerCount.getMale());
        txtViewFemaleCount.setText("" + passengerCount.getFemales());
        txtViewSeniorCount.setText("" + passengerCount.getSeniors());
        txtViewChildrenCount.setText("" + passengerCount.getChildren());
        txtViewSpecialCareCount.setText("" + passengerCount.getSpecialCare());
        txtViewTotalCount.setText("" + passengerCount.getTotal());
    }

    private void initViews(View view) {
        txtViewSeatMap = (TextView) view.findViewById(R.id.txtViewSeatMap);
        txtViewSeatMap.setOnClickListener(this);

        txtViewInventoryTotal = view.findViewById(R.id.txtViewInventoryTotal);
        txtViewPreBookedTotal = view.findViewById(R.id.txtViewPreBookedTotal);

        txtViewMaleCount = view.findViewById(R.id.txtViewMaleCount);
        txtViewFemaleCount = view.findViewById(R.id.txtViewFemaleCount);
        txtViewSeniorCount = view.findViewById(R.id.txtViewSeniorCount);
        txtViewChildrenCount = view.findViewById(R.id.txtViewChildrenCount);
        txtViewSpecialCareCount = view.findViewById(R.id.txtViewSpecialCareCount);
        txtViewTotalCount = view.findViewById(R.id.txtViewTotalCount);


        txtViewBeverageCount = view.findViewById(R.id.txtViewBeverageCount);
        txtViewBeverage = view.findViewById(R.id.txtViewBeverage);
        txtViewSandwich = view.findViewById(R.id.txtViewSandwich);
        txtViewSandwichCount = view.findViewById(R.id.txtViewSandwichCount);
        txtViewHotSnacks = view.findViewById(R.id.txtViewHotSnacks);
        txtViewHotSnacksCount = view.findViewById(R.id.txtViewHotSnacksCount);
        txtViewNonVegHotSnacksCount = view.findViewById(R.id.txtViewNonVegHotSnacksCount);
        txtViewMunch = view.findViewById(R.id.txtViewMunch);
        txtViewMunchCount = view.findViewById(R.id.txtViewMunchCount);
        txtViewMealOther = view.findViewById(R.id.txtViewMealOther);
        txtViewMealOtherCount = view.findViewById(R.id.txtViewMealOtherCount);
        txtViewNonVegBeveragesCount = view.findViewById(R.id.txtViewNonVegBeveragesCount);
        txtViewNonVegSandwichCount = view.findViewById(R.id.txtViewNonVegSandwichCount);
        txtViewNonVegMunchCount = view.findViewById(R.id.txtViewNonVegMunchCount);
        txtViewNonVegMealOtherCount = view.findViewById(R.id.txtViewNonVegMealOtherCount);


        //PreOrder Veg id..

        txtViewVegBevPre = view.findViewById(R.id.txtViewVegBevPre);
        txtViewVegBevPreCount = view.findViewById(R.id.txtViewVegBevPreCount);
        txtViewVegMunPre = view.findViewById(R.id.txtViewVegMunPre);
        txtViewVegMunPreCount = view.findViewById(R.id.txtViewVegMunPreCount);

        txtViewVegSandPre = view.findViewById(R.id.txtViewVegSandPre);
        txtViewVegSandPreCount = view.findViewById(R.id.txtViewVegSandPreCount);
        txtViewVegSnacksPre = view.findViewById(R.id.txtViewVegSnacksPre);
        txtViewVegSnacksPreCount = view.findViewById(R.id.txtViewVegSnacksPreCount);
        txtViewVegOtherLunch = view.findViewById(R.id.txtViewVegOtherLunch);
        txtViewVegOtherLunchCount = view.findViewById(R.id.txtViewVegOtherLunchCount);
        txtViewVegSandPreCount = view.findViewById(R.id.txtViewVegSandPreCount);

        //PreOrder Nonveg id..

        txtViewNonVegBrevPreCount = view.findViewById(R.id.txtViewNonVegBrevPreCount);
        txtViewNonVegMunPreCount = view.findViewById(R.id.txtViewNonVegMunPreCount);
        txtViewNonVegSandPreCount = view.findViewById(R.id.txtViewNonVegSandPreCount);
        txtViewNonVegSnacksPreCount = view.findViewById(R.id.txtViewNonVegSnacksPreCount);
        txtViewNonVegLunchPreCount = view.findViewById(R.id.txtViewNonVegLunchPreCount);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.txtViewSeatMap) {
            startActivity(new Intent(FragmentUtil.getActivity(this), OrdersActivity.class));

        }
    }

    private void loadInventory(final String cartNo, int crewMemId) {
        final Activity activity = FragmentUtil.getActivity(this);
        final CrewProfile crewProfile = crewProfiles.getCrewProfileByEmpID(crewMemId);
        if (crewProfile == null) {
            return;
        }
        String url = "http://192.168.43.1:8080/api/getProductByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("cartNumber", cartNo);
            jsonRequest.put("flightNumber", crewProfiles.getFlightNo());
            jsonRequest.put("androidId", DeviceUtil.getAndroidId(activity.getContentResolver()));
            jsonRequest.put("imei", "987456321");
            jsonRequest.put("networkType", "" + WifiUtils.getNetworkType(activity));
            jsonRequest.put("modelName", DeviceUtil.getModel());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        CartInventory cartInventory = gson.fromJson(response.toString(), CartInventory.class);
                        crewProfile.setCartInventory(cartInventory);
                        InventoryManager.getInstance().addInventory(cartInventory, cartNo, crewProfile.getEmpId());

                        //todo Sumit has commented this code ,Now we are getting this calculated data from Server
                       /* if (crewProfile.getEmpId() == crewProfiles.getLogdInCrewMemId()) {
                            if (response.length() > 0) {
                                setData(crewProfiles.getLogdInCartNo());
                            }
                        }*/
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(activity, jsonObjectRequest, "getProductByCart");
    }

    private void loadCrewDetail() {
        final Activity activity = FragmentUtil.getActivity(this);
        final CrewProfiles crewProfiles = CrewProfiles.getInstance();
        String url = "http://192.168.43.1:8080/api/getPassengerCrewDetailByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("cartNumber", crewProfiles.getLogdInCrewMemId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                CrewProfile profile = gson.fromJson(response.toString(), CrewProfile.class);
                List<CrewProfile> listofCrewMember = profile.getCrewDetailList();
                for (CrewProfile crewProfile : listofCrewMember) {
                    CrewProfiles.getInstance().addCrewProfile(crewProfile);
                    if (crewProfile.getEmpId() == crewProfiles.getLogdInCrewMemId()) {
                        crewProfile.setFlightNo(crewProfiles.getFlightNo());
                        setUserData(crewProfile);
                    } else {
                        CrewProfiles.getInstance().addOtherCrewProfileList(crewProfile);
                    }
                }
                /* setting inventory Count Category Wise*/
                setData(profile.getInventoryCount()); // Sumit Agrawal has added

                setCrewMemberData(listofCrewMember);

                if (listofCrewMember.size() == 4) {
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[0], listofCrewMember.get(0).getEmpId());
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[1], listofCrewMember.get(1).getEmpId());
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[2], listofCrewMember.get(2).getEmpId());
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[3], listofCrewMember.get(3).getEmpId());
                } else if (listofCrewMember.size() == 3) {
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[0], listofCrewMember.get(0).getEmpId());
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[1], listofCrewMember.get(1).getEmpId());
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[2], listofCrewMember.get(2).getEmpId());
                } else if (listofCrewMember.size() == 2) {
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[0], listofCrewMember.get(0).getEmpId());
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[1], listofCrewMember.get(1).getEmpId());
                } else if (listofCrewMember.size() == 1) {
                    loadInventory(com.vushare.utility.Constants.SEAT_CART[0], listofCrewMember.get(0).getEmpId());
                }
                setPassengerData(profile.getPassengerCount());

                /* Setting Pre Order Count Category Wise */
                setPreOrderData(profile.getPreOrderCount());        // Added by Sumit Agrawal
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(activity, jsonObjectRequest, "getPassengerCrewDetailByCart");
    }

    private void setCrewMemberData(List<CrewProfile> listofCrewMember) {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        crewProfiles.setCrewProfileList(listofCrewMember);

        ((BaseActivity) FragmentUtil.getActivity(this)).displayCrewMember();
    }

    private void setUserData(CrewProfile crewProfile) {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        CrewProfile loggedInCrewProfile = new CrewProfile();
        loggedInCrewProfile.setCrewProfile(crewProfile.getCrewName(), crewProfile.getEmpId(), crewProfile.getFlightNo(), crewProfiles.getLogdInCartNo(), crewProfile.getSeatRange());
        crewProfiles.addCrewProfile(crewProfile);
        crewProfiles.setLogdInCrewMemId(crewProfile.getEmpId());
        crewProfiles.setLogdInMemCartNo(crewProfiles.getLogdInCartNo());
        crewProfiles.setLoginMemName(crewProfile.getEmpName());
        crewProfiles.setLoginCrewSeatRange(crewProfile.getSeatRange());             // Added By Sumit Agrawal
        crewProfiles.setLoginCrewTotalPoints(crewProfile.getPoints());

    }


    /**
     * Used to display Inventory Total Data Category wise(MEAL,NEXT CAN BE ANYTHING)
     *
     * @param inventoryCountDetailList
     * @author Sumit Agrawal
     */
    private void setData(List<InventoryCountDetail> inventoryCountDetailList) {
        LinkedHashMap<Integer, TextView> viewMap = new LinkedHashMap<>();
        viewMap.put(0, txtViewBeverage);
        viewMap.put(1, txtViewSandwich);
        viewMap.put(2, txtViewHotSnacks);
        viewMap.put(3, txtViewMunch);
        viewMap.put(4, txtViewMealOther);

        LinkedHashMap<Integer, TextView> vegMap = new LinkedHashMap<>();
        vegMap.put(0, txtViewBeverageCount);
        vegMap.put(1, txtViewSandwichCount);
        vegMap.put(2, txtViewHotSnacksCount);
        vegMap.put(3, txtViewMunchCount);
        vegMap.put(4, txtViewMealOtherCount);

        LinkedHashMap<Integer, TextView> nonVegMap = new LinkedHashMap<>();
        nonVegMap.put(0, txtViewNonVegBeveragesCount);
        nonVegMap.put(1, txtViewNonVegSandwichCount);
        nonVegMap.put(2, txtViewNonVegHotSnacksCount);
        nonVegMap.put(3, txtViewNonVegMunchCount);
        nonVegMap.put(4, txtViewNonVegMealOtherCount);

        int total = 0;
        if (inventoryCountDetailList.size() > 0) {
            for (int index = 0; index < inventoryCountDetailList.size(); index++) {

                InventoryCountDetail inventoryCountDetail = inventoryCountDetailList.get(index);
                TextView nameOfCategory = viewMap.get(index);
                nameOfCategory.setText(inventoryCountDetail.getType());

                TextView vegCount = vegMap.get(index);
                vegCount.setText(inventoryCountDetail.getVeg());


                TextView nonVegCount = nonVegMap.get(index);
                nonVegCount.setText(inventoryCountDetail.getNonVeg());

                total = total + Integer.valueOf(inventoryCountDetail.getVeg()) + Integer.valueOf(inventoryCountDetail.getNonVeg());
            }
            txtViewInventoryTotal.setText("" + total);

        }
    }

    /**
     * Used to display PreOrder Count Category Wise
     *
     * @param inventoryCountDetailList
     * @author Sumit Agrawal
     */
    private void setPreOrderData(List<InventoryCountDetail> inventoryCountDetailList) {

        LinkedHashMap<Integer, TextView> viewMap = new LinkedHashMap<>();
        viewMap.put(0, txtViewVegBevPre);
        viewMap.put(1, txtViewVegSandPre);
        viewMap.put(2, txtViewVegSnacksPre);
        viewMap.put(3, txtViewVegMunPre);
        viewMap.put(4, txtViewVegOtherLunch);

        LinkedHashMap<Integer, TextView> vegMap = new LinkedHashMap<>();
        vegMap.put(0, txtViewVegBevPreCount);
        vegMap.put(1, txtViewVegSandPreCount);
        vegMap.put(2, txtViewVegSnacksPreCount);
        vegMap.put(3, txtViewVegMunPreCount);
        vegMap.put(4, txtViewVegOtherLunchCount);

        LinkedHashMap<Integer, TextView> nonVegMap = new LinkedHashMap<>();
        nonVegMap.put(0, txtViewNonVegBrevPreCount);
        nonVegMap.put(1, txtViewNonVegSandPreCount);
        nonVegMap.put(2, txtViewNonVegSnacksPreCount);
        nonVegMap.put(3, txtViewNonVegMunPreCount);
        nonVegMap.put(4, txtViewNonVegLunchPreCount);

        int total = 0;
        if (inventoryCountDetailList.size() > 0) {
            for (int index = 0; index < inventoryCountDetailList.size(); index++) {

                InventoryCountDetail inventoryCountDetail = inventoryCountDetailList.get(index);
                TextView nameOfCategory = viewMap.get(index);
                nameOfCategory.setText(inventoryCountDetail.getType());

                TextView vegCount = vegMap.get(index);
                vegCount.setText(inventoryCountDetail.getVeg());


                TextView nonVegCount = nonVegMap.get(index);
                nonVegCount.setText(inventoryCountDetail.getNonVeg());


                total = total + Integer.valueOf(inventoryCountDetail.getVeg()) + Integer.valueOf(inventoryCountDetail.getNonVeg());

            }
            txtViewPreBookedTotal.setText("" + total);

        }

    }

    /*private void setData(String cartNo) {
        InventoryManager inventoryManager = InventoryManager.getInstance();
        List<String> list = new ArrayList<>(inventoryManager.getInventoryMap(cartNo).keySet());
        for (String type : list) {
            if (!type.equalsIgnoreCase(CartInventory.ITEM_TYPE_ECOM)) {
                int veg = inventoryManager.getQuantity(cartNo, type, true);
                int nonVeg = inventoryManager.getQuantity(cartNo, type, false);
                txtLunchVeg.setText("" + veg);
                txtViewNonVeg.setText("" + nonVeg);
                txtViewLunchVeg.setText(type);
                txtViewInventoryTotal.setText("" + (veg + nonVeg));
                break;
            }
        }
    }*/

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            /*EntityDominosRequest request = (EntityDominosRequest) intent.getExtras().getSerializable(Constants.UPDATED_REQUEST_ITEM);
            for (EntityDominosRequest.Data data: request.getData()) {
                InventoryManager.getInstance().orderedItems(crewProfiles.getLogdInCartNo(),Integer.parseInt(data.getId()), data.getQuantity());
            }*/
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(TheAplication.instance).unregisterReceiver(broadcastReceiver);
    }
}
