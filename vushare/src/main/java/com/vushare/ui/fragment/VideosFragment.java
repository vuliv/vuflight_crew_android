package com.vushare.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.MediaController;
import com.vushare.ui.adapter.RecyclerAdapterVideos;

/**
 * Created by MB0000021 on 8/9/2017.
 */

public class VideosFragment extends Fragment {

    private View mRootView;
    private Context mContext;
    private ICallback mCallback = new ICallback() {
        @Override
        public void onSuccess(Object object) {

        }

        @Override
        public void onFailure(Object msg) {

        }
    };

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static VideosFragment newInstance() {
        VideosFragment fragment = new VideosFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_photos, container, false);
        init();
        return mRootView;
    }

    private void init() {
        RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.photos_recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));

        RecyclerAdapterVideos adapterVideos = new RecyclerAdapterVideos(mContext, mCallback,
                MediaController.getInstance(mContext).getVideos());
        recyclerView.setAdapter(adapterVideos);
    }
}
