package com.vushare.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.device.ContentRetriever;
import com.vushare.entity.EntityDirectoryDetail;
import com.vushare.ui.adapter.RecyclerAdapterDirectoryView;
import com.vushare.utility.StringUtils;

import java.util.ArrayList;

/**
 * Created by Gursewak on 12/3/2016.
 */

public class FragmentFolderView extends Fragment {

    private Context context;
    ArrayList<EntityDirectoryDetail> dirList;
    private RecyclerView recyclerView;
    private View view;
    private TextView tvNoFiles;
    private ContentRetriever contentRetriever;
    private String fragmentName;
    private GridLayoutManager mGridLayoutManager;
    private ICallback mCallback;

    public static FragmentFolderView newInstance(String fragmentName, ArrayList<EntityDirectoryDetail> dirList,
                                                 ContentRetriever contentRetriever, ICallback callback) {
        FragmentFolderView fragmentDirectoryView = new FragmentFolderView();
        fragmentDirectoryView.dirList = dirList;
        fragmentDirectoryView.contentRetriever = contentRetriever;
        fragmentDirectoryView.fragmentName = fragmentName;
        fragmentDirectoryView.mCallback = callback;
        return fragmentDirectoryView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_directory_view, container, false);
        if (StringUtils.isEmpty(fragmentName)) {
            fragmentName = "Storage";
        }
        init();
        return view;
    }

    private void init() {
        initViews();
        if (dirList != null && dirList.size() == 0) {
            tvNoFiles.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            setAdapter();
        }
    }

    private void setAdapter() {
        RecyclerAdapterDirectoryView recyclerAdapterDirectoryView = new RecyclerAdapterDirectoryView(context,
                mCallback, dirList, contentRetriever);
        recyclerView.setAdapter(recyclerAdapterDirectoryView);
    }


    private void initViews() {
        recyclerView = (RecyclerView) view.findViewById(R.id.folder_recyclerview);
        tvNoFiles = (TextView) view.findViewById(R.id.folder_no_files);
        mGridLayoutManager = new GridLayoutManager(context, 1);
        recyclerView.setLayoutManager(mGridLayoutManager);
    }

    public String getFragmentName() {
        return fragmentName;
    }
}