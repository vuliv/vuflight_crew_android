package com.vushare.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.TheAplication;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.vucast.utility.DominosAPI;
import com.vucast.utility.OrderReceiver;
import com.vuliv.network.entity.EntityDominosRequest;

import com.vushare.R;
import com.vushare.controller.InventoryManager;
import com.vushare.controller.NetworkController;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.model.PendingOrders;
import com.vushare.model.services.ServiceRequestModel;
import com.vushare.model.services.ServiceResponse;
import com.vushare.ui.activity.BaseActivity;
import com.vushare.ui.activity.CartViewActivity;
import com.vushare.ui.activity.ZxingScannerActivity;
import com.vushare.ui.adapter.CustomArrayAdapter;
import com.vushare.ui.adapter.DynamicSeatAdapter;
import com.vushare.ui.adapter.DynamicSeatViewInnerAdapter;
import com.vushare.ui.view.CircleImageView;
import com.vushare.utility.Constants;
import com.vushare.utility.FragmentUtil;
import com.vushare.utility.SharedPrefsKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OrdersFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, DynamicSeatViewInnerAdapter.OnClickInAdapter, OrderReceiver {

    private static final String TAG = OrdersFragment.class.getSimpleName();
    private static final String[] paths = {"USD", "EURO", "GBP", "YEN", "INR", "CHF"};

    public static Spinner spinner;
    public static LinearLayout linearOrder, linearReconc;
    public static LinearLayout linsnxt, linnextt, linsbackk, relscan, lnrLytBillItems;
    public static LinearLayout relMain;
    public static View lastSelectedClickedSeat;
    public static TextView tvName, tvSeat;
    public static int selectedItemPosition;
    public static Context mContext;

    public static Map<String, OrderItems> requestMap;

    public static String selectedSeat;
    public static String selectedSeatNo;
    private String seatNo;
    private boolean isOrderedItem;
    private CustomArrayAdapter adapter;
    public static boolean updateOrderStatus;
    private EntityDominosRequest request;
    public static OrderItems orderItems;
    private SharedPreferences pref;
    private TextView txtOrderId;
    private boolean ordersScreen;
    public static ArrayList<String> orderCompleteSeat;
    public static Set<View> listView;

    public static Bundle OrderFragment1bundle;

    public static RecyclerView recyclerViewSeats;
    public static DynamicSeatAdapter dynamicSeatAdapter;
    public static String seatRange;
    public static Button btn_refresh;


    public OrdersFragment() {
        pref = TheAplication.getInstance().getSharedPreferences(SharedPrefsKeys.SHARED_PREFERENCES_NAME, TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if user comes through scannin0g screen then the scanned item for particular seat will be highlighted
        mContext = getActivity();
        OrderFragment1bundle = getArguments();
        if (getArguments() != null) {
            selectedSeatNo = getArguments().getString(Constants.SELECTED_SEAT_KEY);
            request = (EntityDominosRequest) getArguments().getSerializable(Constants.REQUEST_KEY);
            ordersScreen = getArguments().getBoolean(Constants.ORDERS_SCREEN_KEY);
            orderItems = (OrderItems) getArguments().getSerializable(Constants.ORDER_ITEMS_KEY);
        }
        listView = new LinkedHashSet<>();
        requestMap = new HashMap<>();
        orderCompleteSeat = new ArrayList<>();
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        CrewProfile crewProfile = crewProfiles.getCrewProfileMemById(crewProfiles.getLogdInCrewMemId());
        seatRange = crewProfile.getSeatRange();
    }

    private void fetchRequests() {
        for (EntityDominosRequest request : DominosAPI.getInstance().getOrderMapValues()) {
            seatNo = request.getMsisdn();
            OrderItems orderItems = requestMap.get(seatNo);
            if (orderItems == null) {
                orderItems = new OrderItems();
                requestMap.put(seatNo, orderItems);
            }
            orderItems.addItemInList(request);
        }
    }

    public static LayoutInflater inflater1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        inflater1 = inflater;
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        initComponents(view);
        setSpinner();
        return view;
    }

    private void setSpinner() {
        adapter = new CustomArrayAdapter(this,
                android.R.layout.simple_spinner_item, paths);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner.setSelection(4, false);
    }

    private void initComponents(View view) {
        btn_refresh = view.findViewById(R.id.btn_refresh);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        relscan = (LinearLayout) view.findViewById(R.id.relscan);
        linearOrder = (LinearLayout) view.findViewById(R.id.linearOrder);
        linearReconc = (LinearLayout) view.findViewById(R.id.linearReconc);
        lnrLytBillItems = view.findViewById(R.id.lnrLytBillItems);
        relMain = view.findViewById(R.id.relMain);
        linsnxt = (LinearLayout) view.findViewById(R.id.linsnxt);
        linnextt = (LinearLayout) view.findViewById(R.id.linnextt);
        linsbackk = (LinearLayout) view.findViewById(R.id.linsbackk);
        linsnxt.setOnClickListener(this);
        linnextt.setOnClickListener(this);
        linsbackk.setOnClickListener(this);
        relscan.setOnClickListener(this);

        txtOrderId = view.findViewById(R.id.txtOrderId);
        tvName = view.findViewById(R.id.tvname);
        tvSeat = view.findViewById(R.id.tvseat);

        recyclerViewSeats = view.findViewById(R.id.recyclerViewSeats);
        updateSeats(recyclerViewSeats);
    }

    private void updateSeats(RecyclerView recyclerViewSeats) {
        updateSeatSelection(recyclerViewSeats);
    }

    private ArrayList<ArrayList<String>> dataFillingInArralist(String[] seatsArr) {
        ArrayList<ArrayList<String>> seatsList = new ArrayList<>();
        int indexCount = 0;
        for (int i = 0; i < seatsArr.length / 6; i++) {
            ArrayList<String> subList = new ArrayList<>();
            for (int j = 0; j < 6; j++) {
                subList.add(seatsArr[indexCount]);
                indexCount++;
            }
            seatsList.add(subList);
        }
        ArrayList<String> subList = new ArrayList<>();
        for (int k = 0; k < seatsArr.length % 6; k++) {
            subList.add(seatsArr[indexCount]);
            indexCount++;
        }
        seatsList.add(subList);
        return seatsList;
    }

    private void updateSeatSelection(RecyclerView recyclerViewSeats) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewSeats.setLayoutManager(linearLayoutManager);
        dynamicSeatAdapter = new DynamicSeatAdapter(getActivity(), dataFillingInArralist(seatRange.split("~")), this);
        recyclerViewSeats.setAdapter(dynamicSeatAdapter);
        dynamicSeatAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        DominosAPI.IS_ORDER_FRAGMENT_VISIBLE = true;
        DominosAPI.orderReceiver = this;

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Refreshing....", Toast.LENGTH_SHORT).show();
                listView = new LinkedHashSet<>();
                requestMap = new HashMap<>();
                orderCompleteSeat = new ArrayList<>();
                ordersScreen = false;
                CrewProfiles crewProfiles = CrewProfiles.getInstance();
                CrewProfile crewProfile = crewProfiles.getCrewProfileMemById(crewProfiles.getLogdInCrewMemId());
                seatRange = crewProfile.getSeatRange();
                if (!ordersScreen) {
                    loadPendingOrder();
                } else {
                    fetchReqDisSeats();
                }


            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        DominosAPI.IS_ORDER_FRAGMENT_VISIBLE = false;
    }

    @Override
    public void onClickInAdapterM(View view) {
        selectSeat(view);
    }

    private void selectSeat(View v) {
        orderItems = null;
        lastSelectedClickedSeat = v;
        selectedSeat = (String) ((TextView) v).getText();
        refreshPassengerDetails();
    }

    private void refreshPassengerDetails() {
        if (getActivity().isDestroyed()) {
            return;
        }
        // orderItems = null;
        if (selectedSeat == null) {
            return;
        }
        if (orderItems == null) {
            orderItems = requestMap.get(selectedSeat);
        }
        int childCount = relMain.getChildCount();
        for (int i = 1; i < childCount - 2; i++) {
            relMain.removeViewAt(1);
        }

        if (orderItems == null) {
            return;
        }


        tvName.setText(orderItems.getRequestList().get(0).getUsername());
        tvSeat.setText("SEAT NO: " + selectedSeat);

        relMain.setVisibility(View.VISIBLE);
        linearReconc.setVisibility(View.GONE);
        linearOrder.setVisibility(View.GONE);

        for (String key : orderItems.getMapOfDataList().keySet()) {
            boolean singleRow = false;
            int quantity = 0;
            TextView tvq1 = null;
            TextView txtViewQuantityLeft = null;
            for (final OrderItems.DataItem dataItem : orderItems.getMapOfDataList().get(key)) {
                EntityDominosRequest.Data data = dataItem.getData();
                if (!singleRow) {
                    View child = inflater1.inflate(R.layout.horizontal_row, relMain, false);
                    relMain.addView(child, relMain.getChildCount() - 2);
                    ((TextView) child.findViewById(R.id.tv1)).setText(data.getName());
                    ((TextView) child.findViewById(R.id.tvsub1)).setText(data.getDescription());
                    data.setHasAccepted(true);
                    CircleImageView circleImageViewCancel = (CircleImageView) child.findViewById(R.id.imgCancel);


                    circleImageViewCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            data.setHasAccepted(false);
                            for (EntityDominosRequest request : orderItems.getRequestList()) {
                                if (request.getData().contains(data)) {
                                    loadCancelDataApi(data, request, orderItems.getRequestList(), key, orderItems);
                                }
                            }
                        }
                    });

                    CircleImageView circleImgViewConnect = child.findViewById(R.id.circleImgViewConnect);

                    // For hiding crew to Crew Communication if same cart with logged user cart
                    CrewProfiles crewProfiles = CrewProfiles.getInstance();
                    CrewProfile crewProfile = crewProfiles.getCrewProfileMemById(crewProfiles.getLogdInCrewMemId());
                    // circleImgViewConnect.setVisibility(data.getAllottedCartNumber().equals(crewProfile.getCartNumber()) ? View.GONE : View.VISIBLE);


                    circleImgViewConnect.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ArrayList<CrewProfile> crewProfileList = (ArrayList<CrewProfile>) CrewProfiles.getInstance().getCrewProfileList();
                            int empId = 0;
                            for (CrewProfile crewProfile : crewProfileList) {
                                if (crewProfile.getCartNumber().equals(data.getAllottedCartNumber())) {
                                    empId = crewProfile.getEmpId();
                                    break;
                                }
                            }

                            Intent intent = new Intent(mContext, CartViewActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.CART_NO_KEY, data.getAllottedCartNumber());
                            // bundle.putString(Constants.CART_NO_KEY, "36CA");
                            bundle.putInt(Constants.EMP_ID_KEY, empId);
                            //bundle.putInt(Constants.EMP_ID_KEY, crewProfile.getEmpId());
                            intent.putExtras(bundle);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mContext.startActivity(intent);
                        }
                    });

                    Glide.with(mContext)
                            .load(data.getImage())
                            .into((ImageView) child.findViewById(R.id.img1));
                    ImageView imgVeg = child.findViewById(R.id.imgVeg);
                    if (data.getType().equalsIgnoreCase(CartInventory.ITEM_TYPE_MEAL)) {
                        imgVeg.setVisibility(View.VISIBLE);
                        if (data.isVeg()) {
                            imgVeg.setImageResource(R.drawable.icon_veg);
                        } else {
                            imgVeg.setImageResource(R.drawable.icon_non_veg);
                        }

                    } else {
                        imgVeg.setVisibility(View.GONE);
                    }

                    tvq1 = ((TextView) child.findViewById(R.id.tvq1));
                    txtViewQuantityLeft = child.findViewById(R.id.txtViewQuantityLeft);

                    TextView tvconf1 = ((TextView) child.findViewById(R.id.tvconf1));

                    if (key.contains("true")) {
                        circleImageViewCancel.setVisibility(View.INVISIBLE);
                        circleImgViewConnect.setVisibility(View.INVISIBLE);
                        tvconf1.setText(mContext.getResources().getString(R.string.pre_booked));
                    } else {
                        //   circleImageViewCancel.setVisibility(View.VISIBLE);
                        circleImgViewConnect.setVisibility(View.VISIBLE);
                        displayPriceInSelectedCurrency(tvconf1, data);
                    }
                }
                singleRow = true;
                if (!ordersScreen) {
                    quantity += data.getQuantity();
                    data.setTotalQuantity(quantity);
                    dataItem.setLeftQuantity(data.getTotalQuantity());
                } else {
                    quantity = data.getTotalQuantity();
                }
                tvq1.setText("" + quantity);
                txtViewQuantityLeft.setText("" + dataItem.getLeftQuantity());
            }
        }
    }

    private void loadCancelDataApi(final EntityDominosRequest.Data data, final EntityDominosRequest request, final List<EntityDominosRequest> requestList, String key, OrderItems orderItems) {
        String url = "http://192.168.43.1:8080/api/updateOrderStatus";

        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();

            JSONObject orderJsonObject = new JSONObject();
            orderJsonObject.put("id", data.getId());
            orderJsonObject.put("hasAccepted", data.isHasAccepted());
            jsonArray.put(orderJsonObject);

            jsonObject.put("data", jsonArray);
            jsonObject.put("msisdn", request.getMsisdn());
            jsonObject.put("hasAccepted", true);
            jsonObject.put("orderId", request.getOrderId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest updateStatusRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                removeData(data, request, requestList, key, orderItems);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(FragmentUtil.getActivity(OrdersFragment.this), "Volley " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        updateStatusRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(FragmentUtil.getActivity(OrdersFragment.this), updateStatusRequest, "updateOrderStatus");
    }

    private void removeData(EntityDominosRequest.Data data, EntityDominosRequest request, List<EntityDominosRequest> requestList, String key, OrderItems orderItems) {
        request.getData().remove(data);
        //InventoryManager.getInstance().orderCancelItems(CrewProfiles.getInstance().getLogdInCartNo(), Integer.parseInt(data.getId()), data.getQuantity());
        orderItems.getMapOfDataList().remove(key);
        if (request.getData().isEmpty()) {
            requestList.remove(request);
            DominosAPI.getInstance().removeDataFromMap(request.getKey());
            if (requestList.isEmpty()) {
                requestMap.remove(selectedSeat);
            }
        }
        refreshPassengerDetails();
        updateSeatSelection(recyclerViewSeats);
    }

    private void displayPriceInSelectedCurrency(TextView tvconf1, EntityDominosRequest.Data data) {
        switch (selectedItemPosition) {
            case 0:
                tvconf1.setText(mContext.getResources().getString(R.string.dollar) + " " + data.getQuantity() * (Math.round((data.getPrice() * 0.015) * 100.0) / 100.0));
                break;
            case 1:
                tvconf1.setText(mContext.getResources().getString(R.string.euro) + " " + data.getQuantity() * (Math.round((data.getPrice() * 0.013) * 100.0) / 100.0));
                break;
            case 2:
                tvconf1.setText(mContext.getResources().getString(R.string.gbp) + " " + data.getQuantity() * (Math.round((data.getPrice() * 0.011) * 100.0) / 100.0));
                break;
            case 3:
                tvconf1.setText(mContext.getResources().getString(R.string.yen) + " " + data.getQuantity() * (Math.round((data.getPrice() * 1.61) * 100.0) / 100.0));
                break;
            case 4:
                tvconf1.setText(mContext.getResources().getString(R.string.rupee) + " " + data.getQuantity() * data.getPrice());
                break;
            case 5:
                tvconf1.setText(mContext.getResources().getString(R.string.chf) + " " + data.getQuantity() * (Math.round((data.getPrice() * 0.014) * 100.0) / 100.0));
                break;
        }
    }

    private void refreshBill() {
        lnrLytBillItems.removeAllViews();

        int totalBill = 0;
        OrderItems orderItems = requestMap.get(selectedSeat);
        if (orderItems == null)
            return;
        for (EntityDominosRequest request : orderItems.getRequestList()) {
            for (EntityDominosRequest.Data data : request.getData()) {
                View child = inflater1.inflate(R.layout.row_item_bill, null);
                lnrLytBillItems.addView(child);

                ((TextView) child.findViewById(R.id.tv1)).setText(data.getName());
                displayPriceInSelectedCurrency(((TextView) child.findViewById(R.id.tv3)), data);
                totalBill += data.getQuantity() * data.getPrice();
            }
        }
        RelativeLayout btnCash = linearReconc.findViewById(R.id.btn_cash);
        btnCash.setBackgroundResource(R.drawable.rounded_edittext_green);
        btnCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnCash.setBackgroundResource(R.drawable.rounded_edittext_red);
            }
        });
        TextView tv7 = linearReconc.findViewById(R.id.tv7);
        TextView tv8 = linearReconc.findViewById(R.id.tv8);
        TextView tv10 = linearReconc.findViewById(R.id.tv10);
        displayPriceInSelectedCurrency(totalBill, tv7, tv8, tv10);  // Sumit Agrawal has added (it was hardcoded value 10)
        //  displayPriceInSelectedCurrency(10, tv7, tv8, tv10);
    }

    private void displayPriceInSelectedCurrency(int totalBill, TextView tv7, TextView tv8, TextView tv10) {
        switch (selectedItemPosition) {
            case 0:
                tv7.setText(mContext.getResources().getString(R.string.dollar) + " " + (Math.round((totalBill * 0.015) * 100.0) / 100.0));
                tv8.setText(mContext.getResources().getString(R.string.dollar) + " " + (Math.round((0 * 0.015) * 100.0) / 100.0));
                tv10.setText(mContext.getResources().getString(R.string.dollar) + " " + (Math.round((totalBill * 0.015) * 100.0) / 100.0));
                break;
            case 1:
                tv7.setText(mContext.getResources().getString(R.string.euro) + " " + (Math.round((totalBill * 0.013) * 100.0) / 100.0));
                tv8.setText(mContext.getResources().getString(R.string.euro) + " " + (Math.round((0 * 0.013) * 100.0) / 100.0));
                tv10.setText(mContext.getResources().getString(R.string.euro) + " " + (Math.round((totalBill * 0.013) * 100.0) / 100.0));
                break;
            case 2:
                tv7.setText(mContext.getResources().getString(R.string.gbp) + " " + (Math.round((totalBill * 0.011) * 100.0) / 100.0));
                tv8.setText(mContext.getResources().getString(R.string.gbp) + " " + (Math.round((0 * 0.011) * 100.0) / 100.0));
                tv10.setText(mContext.getResources().getString(R.string.gbp) + " " + (Math.round((totalBill * 0.011) * 100.0) / 100.0));
                break;
            case 3:
                tv7.setText(mContext.getResources().getString(R.string.yen) + " " + (Math.round((totalBill * 1.61) * 100.0) / 100.0));
                tv8.setText(mContext.getResources().getString(R.string.yen) + " " + (Math.round((0 * 1.61) * 100.0) / 100.0));
                tv10.setText(mContext.getResources().getString(R.string.yen) + " " + (Math.round((totalBill * 1.61) * 100.0) / 100.0));
                break;
            case 4:
                tv7.setText(mContext.getResources().getString(R.string.rupee) + " " + totalBill);
                tv8.setText(mContext.getResources().getString(R.string.rupee) + " " + 0);
                tv10.setText(mContext.getResources().getString(R.string.rupee) + " " + totalBill);
                break;
            case 5:
                tv7.setText(mContext.getResources().getString(R.string.chf) + " " + (Math.round((totalBill * 0.014) * 100.0) / 100.0));
                tv8.setText(mContext.getResources().getString(R.string.chf) + " " + (Math.round((0 * 0.014) * 100.0) / 100.0));
                tv10.setText(mContext.getResources().getString(R.string.chf) + " " + (Math.round((totalBill * 0.014) * 100.0) / 100.0));
                break;
        }
    }

    private void onOrderComplete() {
        StringBuilder text = new StringBuilder();
        JSONObject jsonObject = new JSONObject();
        try {
            for (EntityDominosRequest request : orderItems.getRequestList()) {
                text.append(this.getString(R.string.order_hash_tag) + request.getOrderId() + "\n");
                txtOrderId.setText(text);
                JSONArray jsonArray = new JSONArray();
                for (EntityDominosRequest.Data data : request.getData()) {
                    JSONObject orderJsonObject = new JSONObject();
                    orderJsonObject.put("id", data.getId());
                    orderJsonObject.put("hasAccepted", true);
                    jsonArray.put(orderJsonObject);
                }
                jsonObject.put("data", jsonArray);
                jsonObject.put("msisdn", request.getMsisdn());
                jsonObject.put("hasAccepted", true);
                jsonObject.put("orderId", request.getOrderId());
                updateOrderStatus(jsonObject, request);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateOrderStatus(JSONObject jsonObject, EntityDominosRequest request) {

        final Activity activity = FragmentUtil.getActivity(OrdersFragment.this);
        String url = "http://192.168.43.1:8080/api/updateOrderStatus";
        JsonObjectRequest updateStatusRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int success = response.getInt("StatusCode");
                    if (success == 200) {
                        updateOrder(request);
                    } else {
                        Toast.makeText(FragmentUtil.getActivity(OrdersFragment.this),
                                FragmentUtil.getResources(OrdersFragment.this).
                                        getString(R.string.order_failed), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, "Volley " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        updateStatusRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(activity, updateStatusRequest, "updateOrderStatus");
    }

    private void updateOrder(EntityDominosRequest request) {

        linearReconc.setVisibility(View.GONE);
        linearOrder.setVisibility(View.VISIBLE);
        updateOrderStatus = true;

        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        CrewProfile crewProfile = crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId());
        InventoryManager inventoryManager = InventoryManager.getInstance();
        for (EntityDominosRequest.Data data : request.getData()) {
            inventoryManager.soldItems(crewProfiles.getLogdInCartNo(), Integer.parseInt(data.getId()), data.getQuantity(), crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()));
        }
        DominosAPI.getInstance().removeRequest(request);
        requestMap.remove(selectedSeat);
        selectedSeat = null;
        orderCompleteSeat.add((String) ((TextView) lastSelectedClickedSeat).getText());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lastSelectedClickedSeat = null;
                updateSeatSelection(recyclerViewSeats);
            }
        }, 1500);

        ((BaseActivity) (FragmentUtil.getActivity(this))).txtViewCurrentTripPointsCount.setText(pref.getInt(SharedPrefsKeys.POINTS_EARNED, 0) + "");
        ((BaseActivity) (FragmentUtil.getActivity(this))).txtCrewProfilePoints.setText(crewProfile.getPoints() + "");
    }

    @Override
    public void onClick(View v) {
        int quantity = 0;
        int i = v.getId();
        if (i == R.id.linsnxt) {
            if (DominosAPI.getInstance().checkMapIsEmpty()) {
                showNoOrderAddedToast();
                return;
            }

            for (EntityDominosRequest request : DominosAPI.getInstance().getOrderMapValues()) {
                if (request.getPrice() != 0) {
                    isOrderedItem = true;
                    break;
                }
            }

            if (!isOrderedItem) {
                showNoOrderAddedToast();
                return;
            }

            /*for (String key : orderItems.getMapOfDataList().keySet()) {
                for (final OrderItems.DataItem dataItem : orderItems.getMapOfDataList().get(key)) {

                    quantity += dataItem.getLeftQuantity();

                }
            }
            if (quantity != 0) {
                return;
            }*/

            relMain.setVisibility(View.GONE);
            linearReconc.setVisibility(View.VISIBLE);
            refreshBill();

        } else if (i == R.id.linsbackk) {
            relMain.setVisibility(View.VISIBLE);
            linearReconc.setVisibility(View.GONE);

        } else if (i == R.id.linnextt) {
            /*linearReconc.setVisibility(View.GONE);
            linearOrder.setVisibility(View.VISIBLE);*/
            onOrderComplete();

        } else if (i == R.id.relscan) {
            Intent intentScan = new Intent(FragmentUtil.getActivity(OrdersFragment.this), ZxingScannerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SELECTED_SEAT_KEY, selectedSeat);
            bundle.putBoolean(Constants.ORDERS_SCREEN_KEY, true);
            bundle.putSerializable(Constants.ORDER_ITEMS_KEY, orderItems);
            intentScan.putExtras(bundle);
            startActivityForResult(intentScan, 201);
            // FragmentUtil.getActivity(this).finish();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            orderItems = (OrderItems) data.getSerializableExtra(Constants.ORDER_ITEMS_KEY);
            ordersScreen = data.getBooleanExtra(Constants.ORDERS_SCREEN_KEY, false);
            selectedSeatNo = data.getStringExtra(Constants.SELECTED_SEAT_KEY);
            refreshPassengerDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNoOrderAddedToast() {
        Toast.makeText(FragmentUtil.getActivity(this), FragmentUtil.getResources(this).getString(R.string.no_order_added_toast), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedItemPosition = position;
        adapter.setSelectedPos(position);
        refreshPassengerDetails();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!ordersScreen) {
            loadPendingOrder();
        } else {
            fetchReqDisSeats();
        }
    }

    private void loadPendingOrder() {
        DominosAPI.getInstance().clearOrders();
        final Activity activity = FragmentUtil.getActivity(this);
        final CrewProfiles crewProfiles = CrewProfiles.getInstance();
        String url = "http://192.168.43.1:8080/api/getAllPendingOrderByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("cartNumber", crewProfiles.getLogdInCrewMemId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                PendingOrders pendingOrders = gson.fromJson(response.toString(), PendingOrders.class);
                for (EntityDominosRequest entityDominosRequest : pendingOrders.getEntityDominosRequestList()) {
                    createNewRequest(entityDominosRequest, false);
                }

                if (request != null) {
                    DominosAPI.getInstance().addValueInOrderMap(request.getKey(), request);
                }
                fetchReqDisSeats();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(activity, jsonObjectRequest, "getAllPendingOrderByCart");
    }

    private void fetchReqDisSeats() {
        fetchRequests();
        lastSelectedClickedSeat = null;
        updateSeats(recyclerViewSeats);
    }

    public void createNewRequest(EntityDominosRequest entityDominosRequest, boolean isFromNotify) {
        DominosAPI dominosAPI = DominosAPI.getInstance();
        for (EntityDominosRequest.Data data : entityDominosRequest.getData()) {
            entityDominosRequest = dominosAPI.addRequestData(data, entityDominosRequest, false);
        }

        //it will be null if data list is empty
        if (entityDominosRequest != null) {
            entityDominosRequest.setOrderId(entityDominosRequest.getOrderId());
            entityDominosRequest.setUsername(entityDominosRequest.getUsername());
            dominosAPI.addRequest(entityDominosRequest.getMsisdn(), entityDominosRequest);
        }
        if (isFromNotify) {
            EntityDominosRequest finalEntityDominosRequest = entityDominosRequest;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (OrdersFragment.orderCompleteSeat.contains(finalEntityDominosRequest.getMsisdn())) {
                        OrdersFragment.orderCompleteSeat.remove(finalEntityDominosRequest.getMsisdn());
                    }
                    fetchReqDisSeats();
                }
            });
        }
    }

    @Override
    public void onOrderReceived(EntityDominosRequest entityDominosRequest, boolean isFromNotify) {
        createNewRequest(entityDominosRequest, isFromNotify);
    }

    public static class OrderItems implements Serializable {

        private List<EntityDominosRequest> requestList = new ArrayList<>();
        private Map<String, List<DataItem>> mapOfDataList = new HashMap<>();

        public void addItemInList(EntityDominosRequest request) {
            if (!requestList.contains(request)) {
                requestList.add(request);
            }

            for (final EntityDominosRequest.Data data : request.getData()) {
                String key = data.getId() + "_" + request.isPrebooked();
                List<DataItem> dataList = mapOfDataList.get(key);
                if (dataList == null) {
                    dataList = new ArrayList<>();
                }
                mapOfDataList.put(key, dataList);
                dataList.add(new DataItem(data));
            }
        }

        public List<EntityDominosRequest> getRequestList() {
            return requestList;
        }

        public Map<String, List<DataItem>> getMapOfDataList() {
            return mapOfDataList;
        }


        public static class DataItem implements Serializable {
            private int leftQuantity;
            private EntityDominosRequest.Data data;

            public DataItem(EntityDominosRequest.Data data) {
                this.data = data;
                leftQuantity = getLeftQuantity();
            }

            public int getLeftQuantity() {
                return leftQuantity;
            }

            public void setLeftQuantity(int leftQuantity) {
                this.leftQuantity = leftQuantity;
            }

            public EntityDominosRequest.Data getData() {
                return data;
            }
        }
    }

}
