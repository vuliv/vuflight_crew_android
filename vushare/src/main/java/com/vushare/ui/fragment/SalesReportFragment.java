package com.vushare.ui.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfiles;
import com.vushare.model.salesreport.SaleReportResponse;
import com.vushare.model.salesreport.SalesItemDetail;
import com.vushare.ui.adapter.SalesReprtRecyclerViewAdapter;
import com.vushare.ui.view.CircleImageView;
import com.vushare.utility.Constants;
import com.vushare.utility.ConversionUtil;
import com.vushare.utility.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SalesReportFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = SalesReportFragment.class.getSimpleName();
    private RelativeLayout lnrLytPerishable, lnrLytNonPerishable, lnrLytMerchandise, lnrLytDutyFree, lnrLytDryStore, lnrLytCrewFood;
    private ImageView imgPerishable, imgNonPerishable, imgMerchandise, imgDutyFree, imgDryStore, imgCrewFood;
    private TextView txtPerishable, txtNonPerishable, txtMerchandise, txtDutyFree, txtDryStore, txtCrewFood;
    private TextView txtPerishableCount, txtNonPerishableCount, txtMerchandiseCount, txtDutyFreeCount, txtDryStoreCount, txtCrewFoodCount;
    private FoodItemType selectedFoodItemType = FoodItemType.Perishable;
    private RecyclerView rvSalesReport;
    private CircleImageView imgProfileImage;
    private TextView tvProfileName;
    private SalesReprtRecyclerViewAdapter mealItemsRecyclerViewAdapter;
    // private Map<FoodItemType, List<CartInventory.Inventory>> cartInventoryMap = new HashMap<>();
    private Map<FoodItemType, List<SalesItemDetail>> cartInventoryMap = new HashMap<>();


    public enum FoodItemType {
        Perishable, NonPerishable, Merchandise, DutyFree, DryStore, CrewFood;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sales_report, container, false);
        initComponents(view);
        setCrewDetails();
        getSalesReport();
        return view;
    }

    private void getSalesReport() {
        final Activity activity = FragmentUtil.getActivity(this);
        final CrewProfiles crewProfiles = CrewProfiles.getInstance();
        String url = "http://192.168.43.1:8080/api/getALLSalesReportByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("cartNumber", crewProfiles.getLogdInCartNo());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                SaleReportResponse saleReportResponse = gson.fromJson(response.toString(), SaleReportResponse.class);

                List<SalesItemDetail> salesItemDetailList = saleReportResponse.getSaleReport();
                List<SalesItemDetail> perishableList = new ArrayList<>();
                List<SalesItemDetail> merchandiseList = new ArrayList<>();

                for (SalesItemDetail salesItemDetail : salesItemDetailList) {
                    if (salesItemDetail.getType().equals("MEAL")) {
                        perishableList.add(salesItemDetail);
                    } else {
                        merchandiseList.add(salesItemDetail);
                    }
                }


                cartInventoryMap.put(FoodItemType.Perishable, perishableList);
                cartInventoryMap.put(FoodItemType.Merchandise, merchandiseList);

                setCounts();
                selectFoodItemType(FoodItemType.Perishable);
                rvSalesReport.setAdapter(mealItemsRecyclerViewAdapter);
                //  cartInventoryMap

            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(activity, jsonObjectRequest, "getPassengerCrewDetailByCart");


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mealItemsRecyclerViewAdapter == null) {
            //   fillCartInventoryMap();
            setCounts();
            mealItemsRecyclerViewAdapter = new SalesReprtRecyclerViewAdapter(this);
            selectFoodItemType(FoodItemType.Perishable);
        }
        rvSalesReport.setAdapter(mealItemsRecyclerViewAdapter);
    }

    private void setCrewDetails() {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        tvProfileName.setText(crewProfiles.getLoginMemName());
        Bitmap bitmap = ConversionUtil.decodeFile(crewProfiles.getSelectedProfileImgUri(FragmentUtil.getActivity(SalesReportFragment.this)), Constants.REQUIRED_SIZE);
        if (bitmap != null) {
            imgProfileImage.setImageBitmap(bitmap);
        } else {
            imgProfileImage.setImageResource(R.drawable.profile_avtaar);
        }

    }

    private void initComponents(View view) {
        rvSalesReport = view.findViewById(R.id.rvSalesReport);
        rvSalesReport.setHorizontalScrollBarEnabled(false);

        LinearLayoutManager salesReportLlm = new LinearLayoutManager(FragmentUtil.getActivity(SalesReportFragment.this));
        salesReportLlm.setOrientation(LinearLayoutManager.VERTICAL);
        rvSalesReport.setLayoutManager(salesReportLlm);
        rvSalesReport.setItemAnimator(new DefaultItemAnimator());

        lnrLytPerishable = view.findViewById(R.id.lnrLytPerishable);
        lnrLytPerishable.setOnClickListener(this);
        imgPerishable = view.findViewById(R.id.imgPerishable);
        txtPerishable = view.findViewById(R.id.txtPerishable);
        txtPerishableCount = view.findViewById(R.id.txtPerishableCount);

        /*lnrLytNonPerishable = (RelativeLayout) view.findViewById(R.id.lnrLytNonPerishable);
        lnrLytNonPerishable.setOnClickListener(this);
        imgNonPerishable = (ImageView) view.findViewById(R.id.imgNonPerishable);
        txtNonPerishable = (TextView) view.findViewById(R.id.txtNonPerishable);
        txtNonPerishableCount = (TextView) view.findViewById(R.id.txtNonPerishableCount);
*/
        lnrLytMerchandise = view.findViewById(R.id.lnrLytMerchandise);
        lnrLytMerchandise.setOnClickListener(this);
        imgMerchandise = view.findViewById(R.id.imgMerchandise);
        txtMerchandise = view.findViewById(R.id.txtMerchandise);
        txtMerchandiseCount = view.findViewById(R.id.txtMerchandiseCount);

      /*  lnrLytDutyFree = (RelativeLayout) view.findViewById(R.id.lnrLytDutyFree);
        lnrLytDutyFree.setOnClickListener(this);
        imgDutyFree = (ImageView) view.findViewById(R.id.imgDutyFree);
        txtDutyFree = (TextView) view.findViewById(R.id.txtDutyFree);
        txtDutyFreeCount = (TextView) view.findViewById(R.id.txtDutyFreeCount);

        lnrLytDryStore = (RelativeLayout) view.findViewById(R.id.lnrLytDryStore);
        lnrLytDryStore.setOnClickListener(this);
        imgDryStore = (ImageView) view.findViewById(R.id.imgDryStore);
        txtDryStore = (TextView) view.findViewById(R.id.txtDryStore);
        txtDryStoreCount = (TextView) view.findViewById(R.id.txtDryStoreCount);

        lnrLytCrewFood = (RelativeLayout) view.findViewById(R.id.lnrLytCrewFood);
        lnrLytCrewFood.setOnClickListener(this);
        imgCrewFood = (ImageView) view.findViewById(R.id.imgCrewFood);
        txtCrewFood = (TextView) view.findViewById(R.id.txtCrewFood);
        txtCrewFoodCount = (TextView) view.findViewById(R.id.txtCrewFoodCount);
*/
        imgProfileImage = (CircleImageView) view.findViewById(R.id.imgCrewImage);
        tvProfileName = (TextView) view.findViewById(R.id.tvProfileName);

        lnrLytPerishable.performClick();
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.lnrLytPerishable) {
            if (selectedFoodItemType != FoodItemType.Perishable) {
                changeBackGroundOnclick(FoodItemType.Perishable, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.Perishable);
            }

        }/* else if (i == R.id.lnrLytNonPerishable) {
            if (selectedFoodItemType != FoodItemType.NonPerishable) {
                changeBackGroundOnclick(FoodItemType.NonPerishable, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.NonPerishable);
            }

        }*/ else if (i == R.id.lnrLytMerchandise) {
            if (selectedFoodItemType != FoodItemType.Merchandise) {
                changeBackGroundOnclick(FoodItemType.Merchandise, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.Merchandise);
            }

        } /*else if (i == R.id.lnrLytDutyFree) {
            if (selectedFoodItemType != FoodItemType.DutyFree) {
                changeBackGroundOnclick(FoodItemType.DutyFree, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.DutyFree);
            }

        } else if (i == R.id.lnrLytDryStore) {
            if (selectedFoodItemType != FoodItemType.DryStore) {
                changeBackGroundOnclick(FoodItemType.DryStore, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.DryStore);
            }

        } else if (i == R.id.lnrLytCrewFood) {
            if (selectedFoodItemType != FoodItemType.CrewFood) {
                changeBackGroundOnclick(FoodItemType.CrewFood, true);
                changeBackGroundOnclick(selectedFoodItemType, false);
                selectFoodItemType(FoodItemType.CrewFood);
            }

        }*/
    }

    private void changeBackGroundOnclick(FoodItemType foodItemType, boolean isSelected) {
        switch (foodItemType) {
            case Perishable:
                if (isSelected) {
                    lnrLytPerishable.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgPerishable.setImageResource(R.drawable.inventory_perishable_selected);
                    txtPerishable.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                    txtPerishableCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytPerishable.setBackgroundResource(R.drawable.rounded_edittext);
                    imgPerishable.setImageResource(R.drawable.inventory_perishable);
                    txtPerishable.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                    txtPerishableCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                }
                break;
         /*   case NonPerishable:
                if (isSelected) {
                    lnrLytNonPerishable.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgNonPerishable.setImageResource(R.drawable.inventory_non_perishable_selected);
                    txtNonPerishable.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                    txtNonPerishableCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytNonPerishable.setBackgroundResource(R.drawable.rounded_edittext);
                    imgNonPerishable.setImageResource(R.drawable.inventory_non_perishable);
                    txtNonPerishable.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                    txtNonPerishableCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                }
                break;*/
            case Merchandise:
                if (isSelected) {
                    lnrLytMerchandise.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgMerchandise.setImageResource(R.drawable.inventory_merchandise_selected);
                    txtMerchandise.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                    txtMerchandiseCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytMerchandise.setBackgroundResource(R.drawable.rounded_edittext);
                    imgMerchandise.setImageResource(R.drawable.inventory_merchandise);
                    txtMerchandise.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                    txtMerchandiseCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                }
                break;
            /*case DutyFree:
                if (isSelected) {
                    lnrLytDutyFree.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgDutyFree.setImageResource(R.drawable.inventory_duty_free_selected);
                    txtDutyFree.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                    txtDutyFreeCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytDutyFree.setBackgroundResource(R.drawable.rounded_edittext);
                    imgDutyFree.setImageResource(R.drawable.inventory_duty_free);
                    txtDutyFree.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                    txtDutyFreeCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                }
                break;
            case DryStore:
                if (isSelected) {
                    lnrLytDryStore.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgDryStore.setImageResource(R.drawable.inventory_dish_selected);
                    txtDryStore.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                    txtDryStoreCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytDryStore.setBackgroundResource(R.drawable.rounded_edittext);
                    imgDryStore.setImageResource(R.drawable.inventory_dish);
                    txtDryStore.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                    txtDryStoreCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                }
                break;
            case CrewFood:
                if (isSelected) {
                    lnrLytCrewFood.setBackgroundResource(R.drawable.rounded_edittext_blue);
                    imgCrewFood.setImageResource(R.drawable.inventory_tray_selected);
                    txtCrewFood.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                    txtCrewFoodCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorWhite));
                } else {
                    lnrLytCrewFood.setBackgroundResource(R.drawable.rounded_edittext);
                    imgCrewFood.setImageResource(R.drawable.inventory_tray);
                    txtCrewFood.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                    txtCrewFoodCount.setTextColor(FragmentUtil.getResources(SalesReportFragment.this).getColor(R.color.colorBlue));
                }
                break;*/
        }

    }

    private void setCounts() {
        for (FoodItemType foodItemType : cartInventoryMap.keySet()) {
            List<SalesItemDetail> inventories = cartInventoryMap.get(foodItemType);
            if (inventories != null) {
                switch (foodItemType) {

                    case Perishable:
                        txtPerishableCount.setText("" + inventories.size());
                        break;

                /*    case NonPerishable:
                        txtNonPerishableCount.setText("" + inventories.size());
                        break;
*/
                    case Merchandise:
                        txtMerchandiseCount.setText("" + inventories.size());
                        break;
/*
                    case DutyFree:
                        txtDutyFreeCount.setText("" + inventories.size());
                        break;

                    case DryStore:
                        txtDryStoreCount.setText("" + inventories.size());
                        break;

                    case CrewFood:
                        txtCrewFoodCount.setText("" + inventories.size());
                        break;
  */
                }
            }
        }
    }

  /*  private void fillCartInventoryMap() {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        Map<String, List<CartInventory.Inventory>> inventoryMap = InventoryManager.getInstance().getInventoryMap(crewProfiles.getLogdInCartNo());
        cartInventoryMap.put(FoodItemType.Perishable, inventoryMap.get(CartInventory.ITEM_TYPE_MEAL));
        cartInventoryMap.put(FoodItemType.Merchandise, inventoryMap.get(CartInventory.ITEM_TYPE_ECOM));
    }*/

    private void selectFoodItemType(FoodItemType foodItemType) {
        selectedFoodItemType = foodItemType;
        mealItemsRecyclerViewAdapter.setList(cartInventoryMap.get(foodItemType));
    }
}
