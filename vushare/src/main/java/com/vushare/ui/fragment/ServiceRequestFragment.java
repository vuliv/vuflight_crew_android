package com.vushare.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.vucast.callback.IClickCallback;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfiles;
import com.vushare.model.salesreport.ErrorResponse;
import com.vushare.model.services.ServiceRequestModel;
import com.vushare.model.services.ServiceResponse;
import com.vushare.ui.adapter.services.RecyclerAdapterServices;
import com.vushare.ui.view.CircleImageView;
import com.vushare.utility.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ServiceRequestFragment extends BaseFragment implements IClickCallback {
    private static final String TAG = ServiceRequestFragment.class.getSimpleName();

    RecyclerView rv_service_request;
    RecyclerAdapterServices recyclerAdapterServices;
    ArrayList<ServiceRequestModel> serviceRequestModels = new ArrayList<>();

    TextView tvProfileName;
    CircleImageView imgCrewImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_request, container, false);
        init(view);
        getAllServiceRequest();
        return view;
    }

    private void init(View view) {
        tvProfileName = view.findViewById(R.id.tvProfileName);
        imgCrewImage = view.findViewById(R.id.imgCrewImage);

        rv_service_request = view.findViewById(R.id.rv_service_request);
        rv_service_request.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerAdapterServices = new RecyclerAdapterServices(this, serviceRequestModels);
        rv_service_request.setAdapter(recyclerAdapterServices);
    }

    private void getAllServiceRequest() {
        final Activity activity = FragmentUtil.getActivity(this);
        final CrewProfiles crewProfiles = CrewProfiles.getInstance();
        String url = "http://192.168.43.1:8080/api/getAllActiveServicesByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("crewId", crewProfiles.getLogdInCrewMemId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                ServiceResponse serviceResponse = gson.fromJson(response.toString(), ServiceResponse.class);
                serviceRequestModels = (ArrayList<ServiceRequestModel>) serviceResponse.getServiceList();
                recyclerAdapterServices.addList(serviceRequestModels);
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(activity, jsonObjectRequest, "getPassengerCrewDetailByCart");


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void click(Object object) {
        ServiceRequestModel serviceRequestModel = (ServiceRequestModel) object;
        sendRequestCompleted(serviceRequestModel);
    }


    private void sendRequestCompleted(ServiceRequestModel serviceRequestModel) {
        Gson gson = new Gson();
        String url = "http://192.168.43.1:8080/api/updateServiceRequestStatus";
        Log.i(TAG, "sendRequestCompleted" + serviceRequestModel.toString());
        String jsonRequest = gson.toJson(serviceRequestModel, ServiceRequestModel.class);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse" + response);
                        ErrorResponse errorResponse = gson.fromJson(response, ErrorResponse.class);
                        if (errorResponse.getStatusCode().equals("200")) {
                            Toast.makeText(getActivity(), errorResponse.getErrorMessage(), Toast.LENGTH_SHORT).show();

                            serviceRequestModels.remove(serviceRequestModel);
                            recyclerAdapterServices.notifyDataSetChanged();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonRequest == null ? null : jsonRequest.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };
        NetworkController.addToRequestQueue(getActivity(), stringRequest, "tagPost");
    }

}
