package com.vushare.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vushare.R;
import com.vushare.ui.fragment.OnBoardSalesFragment;
import com.vushare.utility.FragmentUtil;

public class OnBoardSalesActivity extends BaseActivity {

    private OnBoardSalesFragment onBoardSalesFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment();
    }

    private void initializeFragment() {
        onBoardSalesFragment = (OnBoardSalesFragment) getFragmentByTag(FragmentUtil.getTag(OnBoardSalesFragment.class));
        if (onBoardSalesFragment == null) {
            onBoardSalesFragment = new OnBoardSalesFragment();
            onBoardSalesFragment.setArguments(getIntent().getExtras());
            addFragment(R.id.fragmentContainer, onBoardSalesFragment, FragmentUtil.getTag(onBoardSalesFragment), false);
        }
    }
}
