package com.vushare.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vushare.R;
import com.vushare.ui.fragment.CartViewFragment;
import com.vushare.utility.FragmentUtil;

public class CartViewActivity extends BaseActivity{

    private CartViewFragment cartViewFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cartViewFragment = (CartViewFragment) getFragmentByTag(FragmentUtil.getTag(CartViewFragment.class));
        initializeFragment();
        showBackIcon();
        displayCrewMember();
    }

    private void initializeFragment() {
        if (cartViewFragment == null) {
            cartViewFragment = new CartViewFragment();
            cartViewFragment.setArguments(getIntent().getExtras());
            addFragment(R.id.fragmentContainer, cartViewFragment, FragmentUtil.getTag(cartViewFragment), false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (cartViewFragment != null) {
            removeFragment(cartViewFragment);
            cartViewFragment = new CartViewFragment();
            cartViewFragment.setArguments(intent.getExtras());
            addFragment(R.id.fragmentContainer, cartViewFragment, FragmentUtil.getTag(cartViewFragment), false);
        }
    }
}
