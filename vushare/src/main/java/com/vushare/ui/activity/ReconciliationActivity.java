package com.vushare.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vushare.R;
import com.vushare.ui.fragment.ReconciliationFragment;
import com.vushare.utility.FragmentUtil;

public class ReconciliationActivity extends BaseActivity {

    private ReconciliationFragment reconciliationFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment();
        showBackIcon();
        displayCrewMember();
    }

    private void initializeFragment() {
        reconciliationFragment = (ReconciliationFragment) getFragmentByTag(FragmentUtil.getTag(ReconciliationFragment.class));
        if (reconciliationFragment == null) {
            reconciliationFragment = new ReconciliationFragment();
            reconciliationFragment.setArguments(getIntent().getExtras());
            addFragment(R.id.fragmentContainer, reconciliationFragment, FragmentUtil.getTag(reconciliationFragment), false);
        }
    }
}