package com.vushare.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.vucast.activity.ServerActivity;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.service.DataService;
import com.vuliv.network.service.SharedPrefController;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.FileSelectController;
import com.vushare.entity.EntityFileHost;
import com.vushare.ui.adapter.ViewPagerAdapter;
import com.vushare.ui.fragment.MusicsFragment;
import com.vushare.ui.fragment.PhotosFragment;
import com.vushare.ui.fragment.VideosFragment;
import com.vushare.utility.Constants;
import com.vushare.utility.Utility;
import com.vushare.utility.Utils;

import java.util.ArrayList;

import static com.vucast.activity.ClientActivity.SERVER_SSID;
import static com.vucast.constants.Constants.REQUEST_CODE_CLIENT_START;

public class FileChooseActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String SCREEN_TAB = "ScreenTab";

    private ViewPagerAdapter mViewPagerAdapter;
    private ViewPager mViewPager;
    private TedPermission mTedPermission;
    private TextView mSelectedView, mSendView;
    private int mTabIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_choose);
        if (!Utility.isPermissionGranted(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
           /* mTedPermission = new TedPermission(this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage(getString(R.string.permission_denied_message))
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE);
            mTedPermission.check();*/

            TedPermission.with(this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage(getString(R.string.permission_denied_message))
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .check();
        } else {
            mTabIndex = getIntent().getIntExtra(SCREEN_TAB, 0);
            init();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.file_choose_send) {
            int size = FileSelectController.getInstance(this).getSize();
            if (size > 0) {
                if (Utils.isModifySystemPermissionGranted(FileChooseActivity.this)) {
                    Intent intent;
                    if (Constants.SELF_SERVER) {
                        intent = new Intent(getApplicationContext(), ShareActivity.class);
                        startActivity(intent);
                    } else {
                        intent = new Intent(getApplicationContext(), FindNearbySenderWifiActivity.class);
                        ArrayList<EntityFileHost> filesToHost = FileSelectController.getInstance(FileChooseActivity.this).getFilesToHost();
                        ArrayList<EntityMediaDetail> entityMediaDetailArrayList = new ArrayList<>();
                        for (EntityFileHost entityFileHost : filesToHost) {
                            EntityMediaDetail entityMediaDetail = new EntityMediaDetail();
                            entityMediaDetail.setPath(entityFileHost.getHostPath());
                            entityMediaDetail.setTitle(entityFileHost.getName());
                            entityMediaDetail.setType(entityFileHost.getType());
                            entityMediaDetail.setDuration(entityFileHost.getDuration());
                            entityMediaDetail.setAudioId(entityFileHost.getAudioId());
                            entityMediaDetail.setVideoId(entityFileHost.getVideoId());
                            entityMediaDetail.setActualSize(entityFileHost.getActualSize());
                            if (com.vucast.constants.Constants.TYPE_MUSIC.equals(entityFileHost.getType()) ||
                                    com.vucast.constants.Constants.TYPE_VIDEO.equals(entityFileHost.getType()) ||
                                    com.vucast.constants.Constants.TYPE_PHOTO.equals(entityFileHost.getType())) {
                                entityMediaDetailArrayList.add(entityMediaDetail);
                            }
                        }
                        DataService.getInstance().setContent(entityMediaDetailArrayList);
                        Intent serverIntent = new Intent(FileChooseActivity.this, ServerActivity.class);
                        serverIntent.putExtra(SERVER_SSID, SharedPrefController.getUserName(FileChooseActivity.this) + "-" +
                                SharedPrefController.getUserAvatar(FileChooseActivity.this) + "-" + Constants.VUSHARE_HOTSPOT_SUFFIX);
                        startActivityForResult(serverIntent, REQUEST_CODE_CLIENT_START);
                    }
                }
            } else {
                Toast.makeText(this, "Please select a file", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.file_choose_toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        mViewPagerAdapter.addFrag(FolderFragment.newInstance(), getString(R.string.file));
        mViewPagerAdapter.addFrag(PhotosFragment.newInstance(), getString(R.string.photos));
        mViewPagerAdapter.addFrag(VideosFragment.newInstance(), getString(R.string.videos));
        mViewPagerAdapter.addFrag(MusicsFragment.newInstance(), getString(R.string.music));

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.file_choose_pager);
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setCurrentItem(mTabIndex);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.file_choose_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        mSelectedView = (TextView) findViewById(R.id.file_choose_selected);
        mSendView = (TextView) findViewById(R.id.file_choose_send);

        mSendView.setEnabled(false);
        mSendView.setAlpha(0.2F);
        mSelectedView.setEnabled(false);
        mSelectedView.setAlpha(0.2F);

        mSelectedView.setOnClickListener(this);
        mSendView.setOnClickListener(this);

        FileSelectController.getInstance(this).setCallbacks(mCallback);
    }

    public Fragment getCurrentFragment() {
        return mViewPagerAdapter.getCurrentFragment(mViewPager.getCurrentItem());
    }

    /**
     * Permission listener
     */
    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            init();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        }
    };

    /**
     * File selection callbacks
     */
    ICallback mCallback = new ICallback() {
        @Override
        public void onSuccess(Object object) {
            int size = FileSelectController.getInstance(FileChooseActivity.this).getSize();
            if (size <= 0) {
                mSelectedView.setText("Selected");
                mSendView.setEnabled(false);
                mSendView.setAlpha(0.2F);
                mSelectedView.setEnabled(false);
                mSelectedView.setAlpha(0.2F);
            } else {
                mSelectedView.setText("Selected (" + object + ")");
                mSendView.setEnabled(true);
                mSendView.setAlpha(1.0F);
                mSelectedView.setEnabled(true);
                mSelectedView.setAlpha(1.0F);
            }
        }

        @Override
        public void onFailure(Object msg) {
        }
    };
}
