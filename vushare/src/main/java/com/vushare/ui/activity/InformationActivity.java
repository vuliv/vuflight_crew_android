package com.vushare.ui.activity;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.vuliv.network.service.SharedPrefController;
import com.vuliv.network.activity.ParentActivity;
import com.vuliv.network.receiver.DeviceAdminReceiver;
import com.vuliv.network.server.DataController;
import com.vushare.R;
import com.vushare.service.OverlayService;
import com.vushare.utility.Constants;
import com.vushare.utility.StringUtils;
import com.vushare.utility.Utility;
import com.vushare.utility.Utils;

import java.util.ArrayList;

public class InformationActivity extends ParentActivity {

    // Auto complete edit text's arrays
    String[] sourceArray = {"S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"};
    String[] destinationArray = {"D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10"};
    String[] seatStartArray = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
    String[] seatEndArray = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
    private boolean showDrawOverAppsPermission;
    private TedPermission mTedPermission;

    // Views
    private AutoCompleteTextView etTripStart, etTripEnd, etSeatStart, etSeatEnd;
    private LinearLayout mSourceLayout, mDestinationLayout, mSeatsLayout;
    private EditText etVehicleNumber;
    private Button btnSave;
    private ImageView ivUnlock;
    private DevicePolicyManager mDevicePolicyManager;
    private ComponentName mComponentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        init();

        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mComponentName = DeviceAdminReceiver.getComponentName(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.IS_SHUTTLE) {
            // Do nothing
        } else {
            if (showDrawOverAppsPermission && Utility.checkDrawOverAppsPermissions(this)) {
                save();
            }
            showDrawOverAppsPermission = false;
        }
    }

    /**
     * Find all the views and populate them if necessary
     */
    private void init() {
        mSourceLayout = (LinearLayout) findViewById(R.id.ll_trip_start);
        mDestinationLayout = (LinearLayout) findViewById(R.id.ll_trip_end);
        mSeatsLayout = (LinearLayout) findViewById(R.id.ll_seat_number);

        ivUnlock = (ImageView) findViewById(R.id.iv_unlock);
        ivUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUnlockDialog();
            }
        });

        if (Constants.IS_SHUTTLE) {
            mSourceLayout.setVisibility(View.VISIBLE);
            mDestinationLayout.setVisibility(View.VISIBLE);
            mSeatsLayout.setVisibility(View.VISIBLE);
            ivUnlock.setVisibility(View.VISIBLE);
        } else {
            mSourceLayout.setVisibility(View.INVISIBLE);
            mDestinationLayout.setVisibility(View.INVISIBLE);
            mSeatsLayout.setVisibility(View.INVISIBLE);
            ivUnlock.setVisibility(View.GONE);
        }

        String vehicleName = SharedPrefController.getVehicleName(this);
        etVehicleNumber = (EditText) findViewById(R.id.et_vehicle_number);
        if (!StringUtils.isEmpty(vehicleName)) {
            etVehicleNumber.setText(vehicleName);
        }

        ArrayAdapter<String> adapterTripStart = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, sourceArray);
        etTripStart = (AutoCompleteTextView) findViewById(R.id.et_trip_start);
        etTripStart.setThreshold(1);
        etTripStart.setAdapter(adapterTripStart);
        String tripSource = SharedPrefController.getTripSource(this);
        if (!StringUtils.isEmpty(tripSource)) {
            etTripStart.setText(tripSource);
        }

        ArrayAdapter<String> adapterTripDestination = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, destinationArray);
        etTripEnd = (AutoCompleteTextView) findViewById(R.id.et_trip_end);
        etTripEnd.setThreshold(1);
        etTripEnd.setAdapter(adapterTripDestination);
        String tripDestination = SharedPrefController.getTripDestination(this);
        if (!StringUtils.isEmpty(tripDestination)) {
            etTripEnd.setText(tripDestination);
        }

        ArrayAdapter<String> adapterSeatStart = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, seatStartArray);
        etSeatStart = (AutoCompleteTextView) findViewById(R.id.et_seat_number_start);
        etSeatStart.setThreshold(1);
        etSeatStart.setAdapter(adapterSeatStart);
        String seatStart = SharedPrefController.getSeatStart(this);
        if (!StringUtils.isEmpty(seatStart)) {
            etSeatStart.setText(seatStart);
        }

        ArrayAdapter<String> adapterSeatEnd = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, seatEndArray);
        etSeatEnd = (AutoCompleteTextView) findViewById(R.id.et_seat_number_end);
        etSeatEnd.setThreshold(1);
        etSeatEnd.setAdapter(adapterSeatEnd);
        String seatEnd = SharedPrefController.getSeatEnd(this);
        if (!StringUtils.isEmpty(seatEnd)) {
            etSeatEnd.setText(seatEnd);
        }

        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isModifySystemPermissionGranted(InformationActivity.this)) {
                    if (!com.vushare.utility.Utility.isPermissionGranted(InformationActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                       /* mTedPermission = new TedPermission(InformationActivity.this)
                                .setPermissionListener(permissionlistener)
                                .setDeniedMessage(getString(R.string.permission_denied_message))
                                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE);
                        mTedPermission.check();*/
                        TedPermission.with(InformationActivity.this)
                                .setPermissionListener(permissionlistener)
                                .setDeniedMessage(getString(R.string.permission_denied_message))
                                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                                .check();
                    } else {
                        permissionlistener.onPermissionGranted();
                    }
                }
            }
        });
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            save();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        }
    };

    /**
     * Save fields
     */
    private void save() {
        // Validate fields
        final String vehicleName = etVehicleNumber.getText().toString().trim();
        if (StringUtils.isEmpty(vehicleName)) {
            Toast.makeText(InformationActivity.this,
                    getString(R.string.vehicle_number_error), Toast.LENGTH_SHORT).show();
            return;
        }
        final String source = etTripStart.getText().toString().trim();
        final String destination = etTripEnd.getText().toString().trim();
        String seatStartValue = etSeatStart.getText().toString().trim();
        String seatEndValue = etSeatEnd.getText().toString().trim();
        if (StringUtils.isEmpty(seatStartValue)) {
            seatStartValue = "0";
        }
        if (StringUtils.isEmpty(seatEndValue)) {
            seatEndValue = "0";
        }
        final String seatStart = seatStartValue;
        final String seatEnd = seatEndValue;

        if (Constants.IS_SHUTTLE) {
            if (StringUtils.isEmpty(source)) {
                Toast.makeText(InformationActivity.this,
                        getString(R.string.source_error), Toast.LENGTH_SHORT).show();
                return;
            }
            if (StringUtils.isEmpty(destination)) {
                Toast.makeText(InformationActivity.this,
                        getString(R.string.destination_error), Toast.LENGTH_SHORT).show();
                return;
            }
            if (StringUtils.isEmpty(seatStart) || StringUtils.isEmpty(seatEnd)) {
                Toast.makeText(InformationActivity.this,
                        getString(R.string.seat_number_error), Toast.LENGTH_SHORT).show();
                return;
            }
        }
        Toast.makeText(InformationActivity.this, R.string.registration_in_process, Toast.LENGTH_SHORT).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean registrationStatus = false;
                if (!Constants.IS_SHUTTLE && vehicleName.equals(SharedPrefController.getVehicleName(InformationActivity.this))) {
                    // Don't register
                    registrationStatus = true;
                } else {
                    registrationStatus = DataController.getInstance().registerUser(InformationActivity.this, vehicleName, source,
                            destination, Integer.parseInt(seatStart), Integer.parseInt(seatEnd));
                }
                if (registrationStatus) {
                    // Save all the details in Shared Prefs
                    SharedPrefController.setProfileCreated(InformationActivity.this, true);
                    SharedPrefController.setVehicleName(InformationActivity.this, vehicleName);
                    if (Constants.IS_SHUTTLE) {
                        SharedPrefController.setTripSource(InformationActivity.this, source);
                        SharedPrefController.setTripDestination(InformationActivity.this, destination);
                        SharedPrefController.setSeatStart(InformationActivity.this, seatStart);
                        SharedPrefController.setSeatEnd(InformationActivity.this, seatEnd);

                        // Go to Home activity
                        startActivity(new Intent(InformationActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        if (Utility.checkDrawOverAppsPermissions(InformationActivity.this)) {
                            Intent intent = new Intent(InformationActivity.this, OverlayService.class);
                            if (Utility.isOverlayServiceRunning(InformationActivity.this, OverlayService.class)) {
                                stopService(intent);
                            }
                            startService(intent);
                            finish();
                        } else {
                            showDrawOverAppsPermission = Utility.checkDrawOverlayPermission(InformationActivity.this);
                        }
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(InformationActivity.this, R.string.registration_failed, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    private void showUnlockDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Admin Password");
        alertDialogBuilder.setCancelable(false);

        int margin = (int) getResources().getDimension(R.dimen.dp_large);

        final EditText editText = new EditText(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        params.leftMargin = margin;
        params.rightMargin = margin;
        editText.setLayoutParams(params);
        alertDialogBuilder.setView(editText);
        alertDialogBuilder.setPositiveButton("Unlock", null);
        alertDialogBuilder.setNegativeButton("Remove Owner", null);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        final String password = editText.getText().toString().trim();
                        if (password.equals(com.vushare.utility.Constants.VUSHARE_UNLOCK_PASSWORD)) {
                            Utility.unlockDevice(InformationActivity.this, mDevicePolicyManager, mComponentName);
                        } else {
                            Toast.makeText(InformationActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                        }
                        alertDialog.dismiss();

                    }
                });

                Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        final String password = editText.getText().toString().trim();
                        if (password.equals(com.vushare.utility.Constants.VUSHARE_UNLOCK_PASSWORD)) {
                            Utility.removeOwner(InformationActivity.this, mDevicePolicyManager);
                        } else {
                            Toast.makeText(InformationActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                        }
                        alertDialog.dismiss();
                    }
                });
            }
        });

        alertDialog.show();
    }
}
