package com.vushare.ui.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.TheAplication;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.vucast.constants.Constants;
import com.vucast.utility.DominosAPI;
import com.vuliv.network.receiver.DeviceAdminReceiver;
import com.vuliv.network.server.DataController;
import com.vuliv.network.server.TrackingController;
import com.vushare.R;
import com.vushare.server.ShareService;
import com.vushare.service.AlarmService;
import com.vushare.ui.adapter.RecyclerAdapterOrder;
import com.vushare.utility.Utility;
import com.vushare.utility.Utils;

import java.util.ArrayList;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = HomeActivity.class.getCanonicalName();
    private Button mSend, mReceive;
    private LinearLayout mPhotosLayout, mVideosLayout, mMusicLayout, mFilesLayout;
    private ImageView mUserImage, mUnlock, mInvite;
    private TedPermission mTedPermission;
    private DevicePolicyManager mDevicePolicyManager;
    private ComponentName mComponentName;
    private RecyclerView rvOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        AlarmService.scheduleAlarm(this);
        TrackingController.getInstance().sendTracking(HomeActivity.this);
        DataController.getInstance().insertData(HomeActivity.this);
//        startService(new Intent(HomeActivity.this, DownloadService.class));
        init();
        rvOrder.setLayoutManager(new LinearLayoutManager(this));

        new Thread() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            rvOrder.setAdapter(new RecyclerAdapterOrder(HomeActivity.this, DominosAPI.getInstance().getOrderMapKeySet()));
                        }
                    });
                } catch (Exception e) {
                    Log.e(TAG, "listing clients countdown interrupted", e);
                }
            }
        }.start();
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mComponentName = DeviceAdminReceiver.getComponentName(this);
    }

    @Override
    protected void onResume() {
        if (isServiceRunning(ShareService.class)) {
            mSend.setVisibility(View.GONE);
            mReceive.setVisibility(View.VISIBLE);
        } else {
            mSend.setVisibility(View.VISIBLE);
            mReceive.setVisibility(View.GONE);
        }
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.UPDATE_UI);
        LocalBroadcastManager.getInstance(TheAplication.instance).registerReceiver(broadcastReceiver, intentFilter);
//        if (DominosAPI.getInstance().orderMap.size() > 0) {
//            rvOrder.setVisibility(View.VISIBLE);
//            rvOrder.getAdapter().notifyDataSetChanged();
//        } else {
//            rvOrder.setVisibility(View.GONE);
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(TheAplication.instance).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.home_send) {
            if (Utils.isModifySystemPermissionGranted(HomeActivity.this)) {
                if (!Utility.isPermissionGranted(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    /*mTedPermission = new TedPermission(HomeActivity.this)
                            .setPermissionListener(permissionlistener)
                            .setDeniedMessage(getString(R.string.permission_denied_message))
                            .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE);
                    mTedPermission.check();*/

                    TedPermission.with(this)
                            .setPermissionListener(permissionlistener)
                            .setDeniedMessage(getString(R.string.permission_denied_message))
                            .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE)
                            .check();
                } else {
                    permissionlistener.onPermissionGranted();
                }
            }
        } else if (i == R.id.home_receive) {
            int hour = com.vuliv.network.utility.Utility.getTimeForMomentOfDay();

            if (isServiceRunning(ShareService.class)/* &&
                !(TIME_HOTSPOT_ON_MORNING_EXACT <= hour && hour <= TIME_HOTSPOT_OFF_MORNING_EXACT) &&
                !(TIME_HOTSPOT_ON_EVENING_EXACT <= hour && hour <= TIME_HOTSPOT_OFF_EVENING_EXACT)*/) {
                stopSharingFiles();
                mSend.setVisibility(View.VISIBLE);
                mReceive.setVisibility(View.GONE);
            }
        } else if (i == R.id.home_user_image) {
            startActivity(new Intent(this, InformationActivity.class));
        } else if (i == R.id.home_unlock) {
            showUnlockDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case com.vushare.utility.Constants.PERMISSION_REQ_CODE_READ_PHONE_STATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //loadInventory();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void init() {
        rvOrder = (RecyclerView) findViewById(R.id.rv_order);

        mSend = (Button) findViewById(R.id.home_send);
        mReceive = (Button) findViewById(R.id.home_receive);

        mPhotosLayout = (LinearLayout) findViewById(R.id.home_photos);
        mVideosLayout = (LinearLayout) findViewById(R.id.home_videos);
        mMusicLayout = (LinearLayout) findViewById(R.id.home_music);
        mFilesLayout = (LinearLayout) findViewById(R.id.home_files);

        mUserImage = (ImageView) findViewById(R.id.home_user_image);
        mUnlock = (ImageView) findViewById(R.id.home_unlock);
        mInvite = (ImageView) findViewById(R.id.home_invite);

        mSend.setOnClickListener(this);
        mReceive.setOnClickListener(this);
        mPhotosLayout.setOnClickListener(this);
        mVideosLayout.setOnClickListener(this);
        mMusicLayout.setOnClickListener(this);
        mFilesLayout.setOnClickListener(this);
        mUserImage.setOnClickListener(this);
        mUnlock.setOnClickListener(this);
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            TrackingController.getInstance().sendTracking(HomeActivity.this);
            int hour = com.vuliv.network.utility.Utility.getTimeForMomentOfDay();

            if (!isServiceRunning(ShareService.class)/* &&
                ((TIME_HOTSPOT_ON_MORNING_EXACT <= hour && hour <= TIME_HOTSPOT_OFF_MORNING_EXACT) ||
                (TIME_HOTSPOT_ON_EVENING_EXACT <= hour && hour <= TIME_HOTSPOT_OFF_EVENING_EXACT))*/) {
                mSend.setVisibility(View.GONE);
                mReceive.setVisibility(View.VISIBLE);
                enableAp(false);
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        }
    };

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void showUnlockDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Admin Password");
        alertDialogBuilder.setCancelable(true);

        int margin = (int) getResources().getDimension(R.dimen.dp_large);

        final EditText editText = new EditText(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        params.leftMargin = margin;
        params.rightMargin = margin;
        editText.setLayoutParams(params);
        alertDialogBuilder.setView(editText);
        alertDialogBuilder.setPositiveButton("Unlock", null);
        alertDialogBuilder.setNegativeButton("Remove Owner", null);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        final String password = editText.getText().toString().trim();
                        if (password.equals(com.vushare.utility.Constants.VUSHARE_UNLOCK_PASSWORD)) {
                            mUserImage.setVisibility(View.VISIBLE);
                            Utility.unlockDevice(HomeActivity.this, mDevicePolicyManager, mComponentName);
                        } else {
                            Toast.makeText(HomeActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                        }
                        alertDialog.dismiss();

                    }
                });

                Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        final String password = editText.getText().toString().trim();
                        if (password.equals(com.vushare.utility.Constants.VUSHARE_UNLOCK_PASSWORD)) {
                            Utility.removeOwner(HomeActivity.this, mDevicePolicyManager);
                        } else {
                            Toast.makeText(HomeActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                        }
                        alertDialog.dismiss();
                    }
                });
            }
        });

        alertDialog.show();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rvOrder.setVisibility(View.VISIBLE);
//            rvOrder.getAdapter().notifyDataSetChanged();
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                rvOrder.setAdapter(new RecyclerAdapterOrder(HomeActivity.this, DominosAPI.getInstance().getOrderMapKeySet()));
                            }
                        });
                    } catch (Exception e) {
                        Log.e(TAG, "listing clients countdown interrupted", e);
                    }
                }
            }.start();
        }
    };
}
