package com.vushare.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.thanosfisherman.wifiutils.WifiUtils;
import com.thanosfisherman.wifiutils.wifiConnect.ConnectionSuccessListener;
import com.thanosfisherman.wifiutils.wifiScan.ScanResultsListener;
import com.vuliv.network.service.SharedPrefController;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.server.ShareService;
import com.vushare.ui.fragment.WelcomeFragment;
import com.vushare.utility.Constants;
import com.vushare.utility.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 10/23/2017.
 */

public class LauncherActivity extends BaseActivity {

    private static final String TAG = LauncherActivity.class.getSimpleName();
    private boolean connectedToVuFlight;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        checkUserPermission();

    }


    private void scanWifi() {
        WifiUtils.withContext(this).scanWifi(new ScanResultsListener() {
            @Override
            public void onScanResults(@NonNull List<ScanResult> scanResults) {
                ScanResult result = null;
                for (ScanResult scanResult : scanResults) {
                    // if (scanResult.SSID.startsWith("VuFlightServer")) {
                    if (scanResult.SSID.contains(Constants.WIFI_NAME)) {
                        scanResult.level = WifiManager.calculateSignalLevel(scanResult.level, 5);
                        result = scanResult;
                        break;
                    }

                }
                if (result != null) {
                    WifiUtils.withContext(LauncherActivity.this)
                            .connectWith(result.SSID, result.BSSID)
                            .setTimeout(30000)
                            .onConnectionResult(new ConnectionSuccessListener() {
                                @Override
                                public void isSuccessful(boolean isSuccess) {
                                    // todo sumit agrawal has done some changes here
                                    startP2pSenderWatchService();

                                    connectedToVuFlight = true;
                                    if (CrewProfiles.getInstance().getLoginMemName() != null && !CrewProfiles.getInstance().getLoginMemName().isEmpty() && connectedToVuFlight) {
                                        //startActivity(new Intent(LauncherActivity.this, OnBoardSalesActivity.class));
                                        authenticateCrew();
                                    } else {
                                        if (CrewProfiles.getInstance().getLoginMemName() != null && !CrewProfiles.getInstance().getLoginMemName().isEmpty() && !connectedToVuFlight) {
                                            Toast.makeText(LauncherActivity.this, getString(R.string.connect_to_hotspot), Toast.LENGTH_LONG).show();
                                        }
                                        startActivity(new Intent(LauncherActivity.this, WelcomeActivity.class));
                                        finish();
                                    }

                                }
                            })
                            .start();
                }
            }
        }).start();
    }


    private void checkUserPermission() {
        if (!com.vushare.utility.Utility.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            TedPermission.with(this)
                    .setPermissionListener(permissionlistener)
                    //.setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                    .check();
        } else {
            permissionlistener.onPermissionGranted();
        }
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            scanWifi();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(LauncherActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }

    };

    private void authenticateCrew() {
        final Activity activity = this;
        String url = "http://192.168.43.1:8080/api/authenticateCrew";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("empId", CrewProfiles.getInstance().getLogdInCrewMemId());
            jsonObject.put("cartNumber", CrewProfiles.getInstance().getLogdInCartNo());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        JsonRequest<Boolean> authenticateCrewDetailsResponse = new JsonRequest<Boolean>(Request.Method.POST, url, jsonObject.toString(), new Response.Listener<Boolean>() {
            @Override
            public void onResponse(Boolean response) {
                if (response) {
                    startActivity(new Intent(LauncherActivity.this, OnBoardSalesActivity.class));
                    finish();
                } else {
                    Toast.makeText(activity, activity.getString(R.string.invalid_crew), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, activity.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Response<Boolean> parseNetworkResponse(NetworkResponse response) {
                Boolean parsed;
                try {
                    parsed = Boolean.valueOf(new String(response.data, HttpHeaderParser.parseCharset(response.headers)));
                } catch (UnsupportedEncodingException e) {
                    parsed = Boolean.valueOf(new String(response.data));
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        };


        authenticateCrewDetailsResponse.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(activity, authenticateCrewDetailsResponse, "authenticateCrew");
    }
}
