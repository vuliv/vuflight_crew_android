package com.vushare.ui.activity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.vuliv.network.service.SharedPrefController;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.entity.EntityAvatar;
import com.vushare.ui.adapter.RecyclerAdapterProfileAvatar;
import com.vushare.utility.StringUtils;

import java.util.ArrayList;

/**
 * Created by MB0000021 on 8/24/2017.
 */

public class ProfileActivity extends AppCompatActivity {

    private ImageView mUserImage;
    private EditText mUserName;
    private RecyclerView mAvatarRecyclerView;
    private ArrayList<EntityAvatar> mAvatars = new ArrayList<>();

    private int mSelectedPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
    }

    @Override
    public void onBackPressed() {
        String username = mUserName.getText().toString().trim();
        if (StringUtils.isEmpty(username)) {
            Toast.makeText(this, getString(R.string.profile_empty_name), Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            String name = mUserName.getText().toString().trim();
            if (StringUtils.isEmpty(name)) {
                Toast.makeText(this, getString(R.string.profile_empty_name), Toast.LENGTH_SHORT).show();
            } else {
                SharedPrefController.setUserAvatar(this, mSelectedPosition);
                SharedPrefController.setUserName(this, name);
                SharedPrefController.setProfileCreated(this, true);
                onBackPressed();
            }
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        toolbar.setTitle(getString(R.string.profile));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUserImage = (ImageView) findViewById(R.id.profile_user_image);
        mUserName = (EditText) findViewById(R.id.profile_user_name);
        mAvatarRecyclerView = (RecyclerView) findViewById(R.id.profile_avatar_recyclerview);
        mAvatarRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        TypedArray images = getResources().obtainTypedArray(R.array.avatar_images);

        int size = images.length();
        for (int i=0; i<size; i++) {
            EntityAvatar entityAvatar = new EntityAvatar();
            entityAvatar.setDrawable(images.getResourceId(i, -1));
            mAvatars.add(entityAvatar);
        }

        // Populated user details
        mUserName.setText(SharedPrefController.getUserName(this));
        int position = SharedPrefController.getUserAvatar(this);
        mUserImage.setImageResource(images.getResourceId(position, -1));

        RecyclerAdapterProfileAvatar adapter = new RecyclerAdapterProfileAvatar(this, mCallback, mAvatars);
        mAvatarRecyclerView.setAdapter(adapter);
    }

    // Avatar click callback
    private ICallback mCallback = new ICallback() {
        @Override
        public void onSuccess(Object object) {
            mSelectedPosition = (int) object;
            mUserImage.setImageResource(mAvatars.get(mSelectedPosition).getDrawable());
        }

        @Override
        public void onFailure(Object msg) {

        }
    };
}
