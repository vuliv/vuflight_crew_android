package com.vushare.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vushare.R;
import com.vushare.ui.fragment.SalesReportFragment;
import com.vushare.utility.FragmentUtil;

public class SalesReportActivity extends BaseActivity {

    private SalesReportFragment salesReportFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment();
        showBackIcon();
        displayCrewMember();
    }

    private void initializeFragment() {
        salesReportFragment = (SalesReportFragment) getFragmentByTag(FragmentUtil.getTag(SalesReportFragment.class));
        if (salesReportFragment == null) {
            salesReportFragment = new SalesReportFragment();
            salesReportFragment.setArguments(getIntent().getExtras());
            addFragment(R.id.fragmentContainer, salesReportFragment, FragmentUtil.getTag(salesReportFragment), false);
        }
    }
}