/*
 * Copyright 2017 Srihari Yachamaneni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vushare.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.net.wifi.WifiConfiguration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.vucast.activity.ServerActivity;
import com.vucast.constants.Constants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.service.DataService;
import com.vushare.R;
import com.vushare.controller.FileSelectController;
import com.vushare.controller.HotspotControl;
import com.vuliv.network.service.SharedPrefController;
import com.vushare.entity.EntityFileHost;
import com.vushare.server.ShareService;
import com.vushare.utility.Utility;
import com.vushare.utility.Utils;
import com.vushare.utility.WifiUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.vucast.activity.ClientActivity.SERVER_SSID;
import static com.vucast.constants.Constants.REQUEST_CODE_SERVER_START;
import static com.vushare.ui.activity.ShareActivity.ShareUIHandler.LIST_API_CLIENTS;
import static com.vushare.ui.activity.ShareActivity.ShareUIHandler.UPDATE_AP_STATUS;

public class ShareActivity extends AppCompatActivity {

    public static final String TAG = "ShareActivity";
    public static final String PREFERENCES_KEY_SHARED_FILE_PATHS = "vushare_shared_file_paths";

    private TextView mShareHotspotInfo, mSenderName;
    private Toolbar mToolbar;
    private ImageView mSenderImage;

    private ShareUIHandler mUiUpdateHandler;
    private BroadcastReceiver mServerUpdateListener;

    private HotspotControl mHotspotControl;
    private boolean isApEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        init();
        mHotspotControl.disable();
        registerReceiver(mServerUpdateListener,
                new IntentFilter(ShareService.ShareIntents.SHARE_SERVER_UPDATES_INTENT_ACTION));
        enableAp();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_SERVER_START:
                    resetSenderUi(true);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    private void init() {
        ArrayList<EntityFileHost> filesToHost = FileSelectController.getInstance(this).getFilesToHost();

        mShareHotspotInfo = (TextView) findViewById(R.id.share_hotspot_info);
        mSenderName = (TextView) findViewById(R.id.share_sender_name);
        mSenderName.setText(SharedPrefController.getUserName(this));
        mSenderImage = (ImageView) findViewById(R.id.share_sender_image);
        TypedArray images = getResources().obtainTypedArray(R.array.avatar_images);
        int position = SharedPrefController.getUserAvatar(this);
        mSenderImage.setImageResource(images.getResourceId(position, 0));

        mToolbar = (Toolbar) findViewById(R.id.share_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mHotspotControl = HotspotControl.getInstance(getApplicationContext());

        /*if (null == filesToHost || filesToHost.size() <= 0) {
            mFilesCount.setVisibility(View.GONE);
        } else {
            mFilesCount.setVisibility(View.VISIBLE);
            int filesSize = filesToHost.size();
            mFilesCount.setText(filesSize <= 1 ? filesSize + " " + getString(R.string.file) :
                    filesSize + " " + getString(R.string.files));
        }*/

        mServerUpdateListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (isFinishing() || null == intent)
                    return;
                int intentType = intent.getIntExtra(ShareService.ShareIntents.TYPE, 0);
                if (intentType == ShareService.ShareIntents.Types.FILE_TRANSFER_STATUS) {
                    String fileName = intent.getStringExtra(ShareService.ShareIntents.SHARE_SERVER_UPDATE_FILE_NAME);
                } else if (intentType == ShareService.ShareIntents.Types.AP_DISABLED_ACKNOWLEDGEMENT) {
                    resetSenderUi(false);
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        //If service is already running, change UI and display info for receiver
        if (Utils.isShareServiceRunning(getApplicationContext())) {
            refreshApData();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mServerUpdateListener)
            unregisterReceiver(mServerUpdateListener);
        if (null != mUiUpdateHandler)
            mUiUpdateHandler.removeCallbacksAndMessages(null);
        mUiUpdateHandler = null;
    }


    @Override
    public void onBackPressed() {
        stopSharingFiles();
    }

    private void enableAp() {
        startP2pSenderWatchService();
        refreshApData();
    }

    private void disableAp() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_STOP);
        startService(p2pServiceIntent);
        isApEnabled = false;
    }

    private void startP2pSenderWatchService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
//        p2pServiceIntent.putExtra(ShareService.EXTRA_FILE_PATHS, m_sharedFilePaths);
        if (null != getIntent()) {
//            p2pServiceIntent.putExtra(ShareService.EXTRA_PORT, getIntent().getIntExtra(ShareService.EXTRA_PORT, 0));
            p2pServiceIntent.putExtra(ShareService.EXTRA_SENDER_NAME, getIntent().getStringExtra(ShareService.EXTRA_SENDER_NAME));
        }
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_START);
        startService(p2pServiceIntent);
    }

    /**
     * Starts {@link ShareService} with intent action {@link ShareService#WIFI_AP_ACTION_START_CHECK} to make {@link ShareService} constantly check for Hotspot status. (Sometimes Hotspot tend to stop if stayed idle for long enough. So this check makes sure {@link ShareService} is only alive if Hostspot is enaled.)
     */
    private void startHostspotCheckOnService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_START_CHECK);
        startService(p2pServiceIntent);
    }

    /**
     * Calls methods - {@link ShareActivity#updateApStatus()} & {@link ShareActivity#listApClients()} which are responsible for displaying Hotpot information and Listing connected clients to the same
     */
    private void refreshApData() {
        if (null == mUiUpdateHandler)
            mUiUpdateHandler = new ShareUIHandler(this);
        updateApStatus();
        listApClients();
    }

    /**
     * Updates Hotspot configuration info like Name, IP if enabled.<br> Posts a message to {@link ShareUIHandler} to call itself every 1500ms
     */
    private void updateApStatus() {
        if (!HotspotControl.isSupported()) {
            mShareHotspotInfo.setText("Warning: Hotspot mode not supported!\n");
        }
        if (mHotspotControl.isEnabled()) {
            if (!isApEnabled) {
                isApEnabled = true;
                startHostspotCheckOnService();
            }
            WifiConfiguration config = mHotspotControl.getConfiguration();
            String ip = Build.VERSION.SDK_INT >= 23 ? WifiUtils.getHostIpAddress() : mHotspotControl.getHostIpAddress();
            if (TextUtils.isEmpty(ip))
                ip = "";
            else
                ip = ip.replace("/", "");
            mShareHotspotInfo.setText(getString(R.string.hotspot_info, config.SSID));
            ArrayList<EntityFileHost> filesToHost = FileSelectController.getInstance(ShareActivity.this).getFilesToHost();
            ArrayList<EntityMediaDetail> entityMediaDetailArrayList = new ArrayList<>();
            for (EntityFileHost entityFileHost : filesToHost) {
                EntityMediaDetail entityMediaDetail = new EntityMediaDetail();
                entityMediaDetail.setPath(entityFileHost.getHostPath());
                entityMediaDetail.setTitle(entityFileHost.getName());
                entityMediaDetail.setType(entityFileHost.getType());
                entityMediaDetail.setDuration(entityFileHost.getDuration());
                entityMediaDetail.setAudioId(entityFileHost.getAudioId());
                entityMediaDetail.setVideoId(entityFileHost.getVideoId());
                entityMediaDetail.setActualSize(entityFileHost.getActualSize());
                if (Constants.TYPE_MUSIC.equals(entityFileHost.getType()) ||
                        Constants.TYPE_VIDEO.equals(entityFileHost.getType()) ||
                        Constants.TYPE_PHOTO.equals(entityFileHost.getType())) {
                    entityMediaDetailArrayList.add(entityMediaDetail);
                }
            }
            DataService.getInstance().setContent(entityMediaDetailArrayList);
            Intent serverIntent = new Intent(ShareActivity.this, ServerActivity.class);
            serverIntent.putExtra(SERVER_SSID, config.SSID.replace("\"", ""));
            startActivityForResult(serverIntent, REQUEST_CODE_SERVER_START);
        } else if (null != mUiUpdateHandler) {
            mUiUpdateHandler.removeMessages(UPDATE_AP_STATUS);
            mUiUpdateHandler.sendEmptyMessageDelayed(UPDATE_AP_STATUS, 1500);
        }
    }

    /**
     * Calls {@link HotspotControl#getConnectedWifiClients(int, HotspotControl.WifiClientConnectionListener)} to get Clients connected to Hotspot.<br>
     * Constantly adds/updates receiver items
     * <br> Posts a message to {@link ShareUIHandler} to call itself every 1000ms
     */
    private synchronized void listApClients() {
        if (mHotspotControl == null) {
            return;
        }
        mHotspotControl.getConnectedWifiClients(2000,
                new HotspotControl.WifiClientConnectionListener() {
                    public void onClientConnectionAlive(final HotspotControl.WifiScanResult wifiScanResult) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }

                    @Override
                    public void onClientConnectionDead(final HotspotControl.WifiScanResult c) {
                        Log.e(TAG, "onClientConnectionDead: " + c.ip);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }

                    public void onWifiClientsScanComplete() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mUiUpdateHandler) {
                                    mUiUpdateHandler.removeMessages(LIST_API_CLIENTS);
                                    mUiUpdateHandler.sendEmptyMessageDelayed(LIST_API_CLIENTS, 1000);
                                }
                            }
                        });
                    }
                }

        );
    }

    private void resetSenderUi(boolean disableAP) {
        mUiUpdateHandler.removeCallbacksAndMessages(null);
        mShareHotspotInfo.setText("");
        if (disableAP)
            disableAp();
    }

    static class ShareUIHandler extends Handler {
        WeakReference<ShareActivity> mActivity;

        static final int LIST_API_CLIENTS = 100;
        static final int UPDATE_AP_STATUS = 101;

        ShareUIHandler(ShareActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ShareActivity activity = mActivity.get();
            if (null == activity || msg == null)
                return;
            if (msg.what == LIST_API_CLIENTS) {
                activity.listApClients();
            } else if (msg.what == UPDATE_AP_STATUS) {
                activity.updateApStatus();
            }
        }
    }

    private void stopSharingFiles() {
        String message = getString(R.string.share_stop_message);
        String positiveText = getString(R.string.share_stop_positive);
        String negativeText = getString(R.string.share_stop_negative);
        Utility.showAlertDialog(this, message, positiveText, negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Positive button clicked
                        resetSenderUi(true);
                        finish();
                    }
                }, null);
    }

}
