package com.vushare.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vuliv.network.service.SharedPrefController;
import com.vushare.R;
import com.vushare.service.OverlayService;
import com.vushare.utility.StringUtils;
import com.vushare.utility.Utility;

/**
 * Created by MB0000003 on 10-Oct-17.
 */

public class UberHomeActivity extends AppCompatActivity {

    private Button btnStart;
    private EditText etVehicleNumber;
    private EditText etName;
    private boolean showDrawOverAppsPermission;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_uber_information);
        initViews();
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });
    }

    private void initViews() {
        btnStart = (Button) findViewById(R.id.btnStart);
        etVehicleNumber = (EditText) findViewById(R.id.et_vehicle_number);
        etName = (EditText) findViewById(R.id.et_name);

        String vehicleName = SharedPrefController.getVehicleName(this);
        etVehicleNumber.setText(StringUtils.isEmpty(vehicleName) ? "" : vehicleName);
        String driverName = SharedPrefController.getUserNameDefaultEmpty(this);
        etName.setText(StringUtils.isEmpty(driverName) ? "" : driverName);
    }

    public void start() {

        String driverName = etName.getText().toString();
        String vehicleNumber = etVehicleNumber.getText().toString();

        if (!TextUtils.isEmpty(driverName) && !TextUtils.isEmpty(vehicleNumber)) {

            SharedPrefController.setUserName(this, driverName);
            SharedPrefController.setVehicleName(this, vehicleNumber);

            if (Utility.checkDrawOverAppsPermissions(this)) {
                Intent intent = new Intent(this, OverlayService.class);
                if (Utility.isOverlayServiceRunning(this, OverlayService.class)) {
                    stopService(intent);
                }
                startService(intent);
                finish();
            } else {
                showDrawOverAppsPermission = Utility.checkDrawOverlayPermission(this);
            }
        } else {
            Toast.makeText(this, getString(R.string.empty_details), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (showDrawOverAppsPermission && Utility.checkDrawOverAppsPermissions(this)) {
            start();
        }
        showDrawOverAppsPermission = false;
    }
}