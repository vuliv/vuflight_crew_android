package com.vushare.ui.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.vucast.utility.DominosAPI;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.ui.adapter.RecyclerAdapterChat;
import com.vushare.utility.Constants;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import vuflight.com.vuliv.chatlib.controller.ChatMessageController;
import vuflight.com.vuliv.chatlib.entity.EntityCrewChatMessage;
import vuflight.com.vuliv.chatlib.entity.EntityCrewChatStatus;
import vuflight.com.vuliv.chatlib.entity.EntityIpDetail;
import vuflight.com.vuliv.chatlib.entity.IpList;
import vuflight.com.vuliv.chatlib.observer.IChatMessageObserver;

public class CrewChatActivity extends AppCompatActivity implements IChatMessageObserver {

    TextView tv_crewName, tv_aapName;
    EditText et_userMessage;
    RecyclerView recyclerView;
    RecyclerAdapterChat adapterChat;
    HashMap<String, ArrayList<EntityCrewChatMessage>> entityCrewChatMessagesMap;

    String otherCrewIp = null;
    int empId;
    String crewName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing_chat);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tv_crewName = (TextView) findViewById(R.id.tv_crewName);
        tv_aapName = (TextView) findViewById(R.id.tv_aapName);

        empId = getIntent().getIntExtra(Constants.EMP_ID_KEY, 0);
        try {
            if (getIntent().getStringExtra("Notification").equals("True")) {
                tv_aapName.setVisibility(View.VISIBLE);
            } else {
                tv_aapName.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            tv_aapName.setVisibility(View.GONE);
        }

        for (CrewProfile crewProfile : CrewProfiles.getInstance().crewProfileList) {
            if (crewProfile.getEmpId() == empId) {
                crewName = crewProfile.getEmpName();
                tv_crewName.setText(crewName);
                break;
            }
        }

        entityCrewChatMessagesMap = DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewMeOtherChat);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        et_userMessage = (EditText) findViewById(R.id.et_userMessage);
        ImageView iv_sen = (ImageView) findViewById(R.id.iv_send);
        ImageView iv_cross = (ImageView) findViewById(R.id.iv_cross);
        iv_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_sen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!TextUtils.isEmpty(et_userMessage.getText().toString())) {
                        sendChatToCrew(et_userMessage.getText().toString());
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        ChatMessageController.getInstance().registerObserver(this);

        fetchIpToOther();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DominosAPI.TempPrefrenceForChatAndServices.getInstance().setCrewChatActivityVisible(true);
        DominosAPI.TempPrefrenceForChatAndServices.getInstance().setIsCrewchatActivityVisibleCrewName(crewName);
        if (entityCrewChatMessagesMap.get(crewName) != null) {
            adapterChat = new RecyclerAdapterChat(this, crewName);
            recyclerView.setAdapter(adapterChat);
            if (entityCrewChatMessagesMap.get(crewName).size() > 2) {
                recyclerView.scrollToPosition(entityCrewChatMessagesMap.get(crewName).size() - 1);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        DominosAPI.TempPrefrenceForChatAndServices.getInstance().setCrewChatActivityVisible(false);
        DominosAPI.TempPrefrenceForChatAndServices.getInstance().setIsCrewchatActivityVisibleCrewName("");
    }

    private void fetchIpToOther() {
        /*CrewProfiles crewProfiles = CrewProfiles.getInstance();
        final String id = String.valueOf(crewProfiles.getLogdInCrewMemId());*/
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http").encodedAuthority("192.168.43.1:8080").appendPath("api").appendPath("getIpList");

        String url = builder.toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        EntityIpDetail entityIpDetail = gson.fromJson(response.toString(), EntityIpDetail.class);
                        for (IpList ipList : entityIpDetail.getIpList()) {
                            if (ipList.getId().equals(String.valueOf(empId))) {
                                otherCrewIp = ipList.getIp();
                                break;
                            }
                        }
                    }
                }, new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CrewChatActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(this, jsonObjectRequest, "getIpList");

    }

    @Override
    protected void onDestroy() {
        ChatMessageController.getInstance().removeObserver(this);
        super.onDestroy();
    }

    void sendChatToCrew(String msg) throws UnsupportedEncodingException {
        EntityCrewChatMessage entityCrewChatMessage = new EntityCrewChatMessage(msg, true);
        CrewProfile crewProfile = CrewProfiles.getInstance().getCrewProfileByEmpID(CrewProfiles.getInstance().getLogdInCrewMemId());

        entityCrewChatMessage.setCrewName(crewProfile.getEmpName());
        entityCrewChatMessage.setCrewId(crewProfile.getEmpId());

        String time = Calendar.getInstance().get(Calendar.HOUR) + ":" + Calendar.getInstance().get(Calendar.MINUTE);
        entityCrewChatMessage.setCreatedAt(time);

        Gson gson = new Gson();
        String jsonString = gson.toJson(entityCrewChatMessage);

        Uri.Builder builder = new Uri.Builder();

        builder.scheme("http").encodedAuthority(otherCrewIp + ":9632").appendPath("vuflight").appendPath("crewChat")
                .appendQueryParameter("d", jsonString);


        String url = builder.toString();
        final EntityCrewChatMessage entityCrewChatMessage1 = entityCrewChatMessage;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();

                        EntityCrewChatStatus entityCrewChatStatus = gson.fromJson(response.toString(), EntityCrewChatStatus.class);
                        if (entityCrewChatStatus.hasReceived()) {
                            et_userMessage.setText("");
                            ArrayList<EntityCrewChatMessage> entityCrewChatMessages = DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap().get(crewName);
                            if (entityCrewChatMessages != null) {
                                entityCrewChatMessages.add(entityCrewChatMessage1);
                            } else {
                                entityCrewChatMessages = new ArrayList<>();
                                entityCrewChatMessages.add(entityCrewChatMessage1);
                            }
                            entityCrewChatMessagesMap.put(crewName, entityCrewChatMessages);
                            DominosAPI.TempPrefrenceForChatAndServices.getInstance().saveCrewMessagesMap(entityCrewChatMessagesMap);
                            entityCrewChatMessagesMap = null;
                            entityCrewChatMessagesMap = DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap();
                            if (adapterChat == null) {
                                adapterChat = new RecyclerAdapterChat(getApplicationContext(), crewName);
                                recyclerView.setAdapter(adapterChat);
                                if (entityCrewChatMessagesMap.get(crewName).size() > 2) {
                                    recyclerView.smoothScrollToPosition(entityCrewChatMessagesMap.get(crewName).size() - 1);
                                }
                            } else {
                                adapterChat.addUserMessage(DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap().get(crewName));
                            }
                        } else {
                            Toast.makeText(CrewChatActivity.this, "Something Went Wrong..Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        et_userMessage.setText("");
                        Toast.makeText(CrewChatActivity.this, "Sorry! Currently " + crewName + " is not LoggedIn..", Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        NetworkController.addToRequestQueue(this, jsonObjectRequest, "crewChat");

    }

    @Override
    public void onMessageReceived(final EntityCrewChatMessage entityCrewChatMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EntityCrewChatMessage entityCrewChatMessage1 = entityCrewChatMessage;
                if (DominosAPI.TempPrefrenceForChatAndServices.getInstance().getCrewChatActivityVisible() && DominosAPI.TempPrefrenceForChatAndServices.getInstance().getIsCrewchatActivityVisibleCrewName().equals(crewName)) {
                    entityCrewChatMessage1.setOtherMsg(false);
                    ArrayList<EntityCrewChatMessage> entityCrewChatMessages = DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap().get(crewName);
                    if (entityCrewChatMessages != null) {
                        entityCrewChatMessages.add(entityCrewChatMessage1);
                    } else {
                        entityCrewChatMessages = new ArrayList<>();
                        entityCrewChatMessages.add(entityCrewChatMessage1);
                    }
                    entityCrewChatMessagesMap.put(crewName, entityCrewChatMessages);
                    DominosAPI.TempPrefrenceForChatAndServices.getInstance().saveCrewMessagesMap(entityCrewChatMessagesMap);
                }
                entityCrewChatMessagesMap = null;
                entityCrewChatMessagesMap = DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap();
                if (entityCrewChatMessagesMap.get(crewName) != null) {
                    if (adapterChat == null) {
                        adapterChat = new RecyclerAdapterChat(getApplicationContext(), crewName);
                        recyclerView.setAdapter(adapterChat);
                        if (entityCrewChatMessagesMap.get(crewName).size() > 2) {
                            recyclerView.smoothScrollToPosition(entityCrewChatMessagesMap.get(crewName).size() - 1);
                        }
                    } else {
                        adapterChat.addUserMessage(DominosAPI.TempPrefrenceForChatAndServices.getInstance().loadCrewMessagesMap().get(crewName));
                    }
                }
            }
        });
    }
}
