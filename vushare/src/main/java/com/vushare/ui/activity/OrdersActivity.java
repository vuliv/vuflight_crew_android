package com.vushare.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vushare.R;
import com.vushare.ui.fragment.OrdersFragment;
import com.vushare.utility.FragmentUtil;

public class OrdersActivity extends BaseActivity {

    private OrdersFragment ordersFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment();
        displayCrewMember();
    }

    private void initializeFragment() {
        ordersFragment = (OrdersFragment) getFragmentByTag(FragmentUtil.getTag(OrdersFragment.class));
        if (ordersFragment == null) {
            ordersFragment = new OrdersFragment();
            ordersFragment.setArguments(getIntent().getExtras());
            addFragment(R.id.fragmentContainer, ordersFragment, FragmentUtil.getTag(ordersFragment), false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        removeFragment(ordersFragment);
        ordersFragment = new OrdersFragment();
        ordersFragment.setArguments(intent.getExtras());
        addFragment(R.id.fragmentContainer, ordersFragment, FragmentUtil.getTag(ordersFragment), false);
    }
}