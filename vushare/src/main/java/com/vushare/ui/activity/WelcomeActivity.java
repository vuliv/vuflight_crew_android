package com.vushare.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vushare.R;
import com.vushare.ui.fragment.WelcomeFragment;
import com.vushare.utility.FragmentUtil;

public class WelcomeActivity extends BaseActivity {

    private WelcomeFragment welcomeFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment();
        hideToolbar();
        lockDrawerLayout();
    }

    private void initializeFragment() {
        welcomeFragment = (WelcomeFragment) getFragmentByTag(FragmentUtil.getTag(WelcomeFragment.class));
        if (welcomeFragment == null) {
            welcomeFragment = new WelcomeFragment();
            welcomeFragment.setArguments(getIntent().getExtras());
            addFragment(R.id.fragmentContainer, welcomeFragment, FragmentUtil.getTag(welcomeFragment), false);
        }
    }
}