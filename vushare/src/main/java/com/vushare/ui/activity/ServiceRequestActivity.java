package com.vushare.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.vucast.callback.IClickCallback;
import com.vucast.utility.DominosAPI;
import com.vucast.utility.ServiceRequestObserver;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfiles;
import com.vushare.model.salesreport.ErrorResponse;
import com.vushare.model.services.ServiceRequestModel;
import com.vushare.model.services.ServiceResponse;
import com.vushare.ui.adapter.services.RecyclerAdapterServices;
import com.vushare.ui.fragment.SalesReportFragment;
import com.vushare.ui.fragment.ServiceRequestFragment;
import com.vushare.ui.view.CircleImageView;
import com.vushare.utility.FragmentUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ServiceRequestActivity extends AppCompatActivity implements IClickCallback, View.OnClickListener, ServiceRequestObserver {

    private static final String TAG = ServiceRequestFragment.class.getSimpleName();

    RecyclerView rv_service_request;
    RecyclerAdapterServices recyclerAdapterServices;
    ArrayList<ServiceRequestModel> serviceRequestModels = new ArrayList<>();

    TextView tvProfileName;
    CircleImageView imgCrewImage;

    ImageView iv_refresh;

    CrewProfiles crewProfiles = CrewProfiles.getInstance();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_service_request);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.title));
        init();
        getAllServiceRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DominosAPI.IS_SERVICE_REQUEST_ACTIVITY_VISIBLE = true;
        DominosAPI.serviceRequestObserver = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        DominosAPI.IS_SERVICE_REQUEST_ACTIVITY_VISIBLE = false;
        DominosAPI.serviceRequestObserver = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        tvProfileName = (TextView) findViewById(R.id.tvProfileName);
        imgCrewImage = (CircleImageView) findViewById(R.id.imgCrewImage);

        iv_refresh = (ImageView) findViewById(R.id.iv_refresh);
        iv_refresh.setOnClickListener(this);

        rv_service_request = (RecyclerView) findViewById(R.id.rv_service_request);
        rv_service_request.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapterServices = new RecyclerAdapterServices(this, serviceRequestModels);
        rv_service_request.setAdapter(recyclerAdapterServices);
    }

    private void getAllServiceRequest() {
        final CrewProfiles crewProfiles = CrewProfiles.getInstance();
        String url = "http://192.168.43.1:8080/api/getAllActiveServicesByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("crewId", crewProfiles.getLogdInCrewMemId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                ServiceResponse serviceResponse = gson.fromJson(response.toString(), ServiceResponse.class);
                serviceRequestModels = (ArrayList<ServiceRequestModel>) serviceResponse.getServiceList();
                recyclerAdapterServices.addList(serviceRequestModels);
                crewProfiles.setCrewRequestsCount(serviceRequestModels.size());
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(ServiceRequestActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(ServiceRequestActivity.this, jsonObjectRequest, "getPassengerCrewDetailByCart");


    }

    @Override
    public void click(Object object) {
        ServiceRequestModel serviceRequestModel = (ServiceRequestModel) object;
        sendRequestCompleted(serviceRequestModel);
    }


    private void sendRequestCompleted(ServiceRequestModel serviceRequestModel) {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        Gson gson = new Gson();
        String url = "http://192.168.43.1:8080/api/updateServiceRequestStatus";
        Log.i(TAG, "sendRequestCompleted" + serviceRequestModel.toString());
        String jsonRequest = gson.toJson(serviceRequestModel, ServiceRequestModel.class);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse" + response);
                        ErrorResponse errorResponse = gson.fromJson(response, ErrorResponse.class);
                        if (errorResponse.getStatusCode().equals("200")) {
                            Toast.makeText(ServiceRequestActivity.this, errorResponse.getErrorMessage(), Toast.LENGTH_SHORT).show();

                            serviceRequestModels.remove(serviceRequestModel);
                            crewProfiles.setCrewRequestsCount(crewProfiles.getCrewRequestsCount() - 1);
                            recyclerAdapterServices.notifyDataSetChanged();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return jsonRequest == null ? null : jsonRequest.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };
        NetworkController.addToRequestQueue(ServiceRequestActivity.this, stringRequest, "tagPost");
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(this, "Refreshing....", Toast.LENGTH_SHORT).show();
        getAllServiceRequest();
    }


    @Override
    public void onRequestReceived(String serviceRequestModel) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ServiceRequestModel serviceRequestModel1 = new Gson().fromJson(serviceRequestModel, ServiceRequestModel.class);
                serviceRequestModels.add(serviceRequestModel1);
                recyclerAdapterServices.notifyDataSetChanged();
                //   recyclerAdapterServices.addServiceRequest(serviceRequestModel1);

            }

        });
    }

    @Override
    public void onRequestNotify(int notify) {
        crewProfiles.setCrewRequestsCount(crewProfiles.getCrewRequestsCount() + notify);

    }
}