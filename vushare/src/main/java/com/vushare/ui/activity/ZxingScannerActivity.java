package com.vushare.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.TheAplication;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.vucast.utility.DominosAPI;
import com.vuliv.network.entity.EntityDominosRequest;
import com.vushare.R;
import com.vushare.controller.InventoryManager;
import com.vushare.controller.NetworkController;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.ui.fragment.OrdersFragment;
import com.vushare.utility.BundleKeys;
import com.vushare.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ZxingScannerActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    static final Integer CAMERA = 0x1;
    private static final String TAG = ZxingScannerActivity.class.getSimpleName();
    public static ArrayList barCodeDataList;
    ImageView iv_cart, ivPassanger;
    TextView itemCount;
    private ZXingScannerView mScannerView;
    private LinearLayout relitems, relseatmap, llCartParent, llPassangerParent, relOtherCrewDetails;
    private RelativeLayout sofaset1, sofaset2, relScannerToolBar;
    private Map<String, List<EntityDominosRequest>> requestMap;
    private String selectedSeat;
    private View lastSelectedClickedSeat;
    private TextView txtItemCounts, txtViewSeat;
    private ImageView btnFlipCamera;
    private int CAMERA_ID = 0;
    private EntityDominosRequest request;
    private Map<Integer, Integer> mapItemId = new HashMap<>();
    Map<Integer, View> map;
    private boolean ordersScreen;
    private OrdersFragment.OrderItems orderItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zxing_scanner);
        txtViewSeat = (TextView) findViewById(R.id.txtViewSeat);
        txtItemCounts = (TextView) findViewById(R.id.txtItemCounts);
        itemCount = (TextView) findViewById(R.id.tv_cartCount);
        iv_cart = (ImageView) findViewById(R.id.iv_cart);
        ivPassanger = (ImageView) findViewById(R.id.iv_pass);
        relitems = (LinearLayout) findViewById(R.id.relitems);

        llCartParent = (LinearLayout) findViewById(R.id.llCartParent);
        llCartParent.setSelected(false);
        llCartParent.setOnClickListener(onCartViewClickListener);

        llPassangerParent = (LinearLayout) findViewById(R.id.llPassangerParent);
        llPassangerParent.setSelected(true);
        llPassangerParent.setOnClickListener(onPassangerViewClickListener);

        sofaset1 = (RelativeLayout) findViewById(R.id.sofaset1);
        sofaset2 = (RelativeLayout) findViewById(R.id.sofaset2);
        relScannerToolBar = (RelativeLayout) findViewById(R.id.relativeLayout_toolbar_scanner);
        relOtherCrewDetails = (LinearLayout) findViewById(R.id.relothercrews);
        relseatmap = (LinearLayout) findViewById(R.id.relseatmap);
        sofaset1.setVisibility(View.INVISIBLE);
        sofaset2.setVisibility(View.INVISIBLE);
        relitems.setVisibility(View.GONE);
        btnFlipCamera = (ImageView) findViewById(R.id.ivFlipCamera);

        boolean isFromNavDrawer = getIntent().getBooleanExtra(BundleKeys.IS_FROM_NAV_DRAWER, false);
        if (!isFromNavDrawer) {
            relScannerToolBar.setVisibility(View.GONE);
        }

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);

        askForPermission(Manifest.permission.CAMERA, CAMERA);
        btnFlipCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScannerView.stopCamera();

                if (CAMERA_ID == 0) {
                    CAMERA_ID = 1;
                    mScannerView.startCamera(CAMERA_ID);
                    mScannerView.setFocusable(false);
                } else {
                    CAMERA_ID = 0;
                    mScannerView.startCamera(CAMERA_ID);
                    mScannerView.setFocusable(false);
                }
            }
        });
//        if (barCodeDataList == null) {
//            itemCount.setText("" + barCodeDataList.size());
//        }
        barCodeDataList = new ArrayList();
        barCodeDataList.clear();
        relitems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mapItemId.isEmpty()) {
                    makeServerCall();
                }
            }
        });
        relseatmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sofaset1.setVisibility(View.VISIBLE);
                sofaset2.setVisibility(View.VISIBLE);
            }
        });
        fetchRequests();
        if (getIntent().getExtras() != null) {
            selectedSeat = getIntent().getExtras().getString(Constants.SELECTED_SEAT_KEY);
        }
        updateSeatSelection(findViewById(R.id.sofaset1));
        updateSeatSelection(findViewById(R.id.sofaset2));

        ordersScreen = getIntent().getExtras().getBoolean(Constants.ORDERS_SCREEN_KEY);
        orderItems = (OrdersFragment.OrderItems) getIntent().getExtras().getSerializable(Constants.ORDER_ITEMS_KEY);
    }

    private void makeServerCall() {
        if (ordersScreen) {
           /* Intent intentScan = new Intent(ZxingScannerActivity.this, OrdersActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SELECTED_SEAT_KEY, selectedSeat);
            bundle.putBoolean(Constants.ORDERS_SCREEN_KEY, true);
            intentScan.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            bundle.putSerializable(Constants.ORDER_ITEMS_KEY, orderItems);
            intentScan.putExtras(bundle);
            startActivity(intentScan);
            finish();*/
            Intent intent = new Intent();
            intent.putExtra(Constants.SELECTED_SEAT_KEY, selectedSeat);
            intent.putExtra(Constants.ORDERS_SCREEN_KEY, true);
            intent.putExtra(Constants.ORDER_ITEMS_KEY, orderItems);
            setResult(201, intent);
            finish();
            //return;
        } else {
            final Activity activity = this;
            request.setMsisdn(selectedSeat);
            String encode = "";
            try {
                encode = URLEncoder.encode(new Gson().toJson(request, EntityDominosRequest.class), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String url = "http://192.168.43.1:8080/api/confirm?d=" + encode;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String status = "";
                            String orderID = "";
                            try {
                                status = response.getString("status");
                                orderID = response.getString("orderID");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (status.equals("200")) {
                                request.setOrderId(orderID);
                                DominosAPI.getInstance().addRequest(selectedSeat, request);

                                //to highlight seats for which orders are placed
                                List<EntityDominosRequest> requestList = requestMap.get(selectedSeat);
                                if (requestList == null) {
                                    requestList = new ArrayList<>();
                                    requestMap.put(selectedSeat, requestList);
                                }
                                requestList.add(request);
                                Intent intentScan = new Intent(ZxingScannerActivity.this, OrdersActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.SELECTED_SEAT_KEY, selectedSeat);
                                bundle.putSerializable(Constants.REQUEST_KEY, request);
                                intentScan.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intentScan.putExtras(bundle);
                                startActivity(intentScan);

                                //Send Broadcast
                                intentScan = new Intent();
                                intentScan.setAction(com.vucast.constants.Constants.UPDATE_ORDERED_ITEM);
                                bundle = new Bundle();
                                bundle.putSerializable(com.vucast.constants.Constants.UPDATED_REQUEST_ITEM, request);
                                intentScan.putExtras(bundle);
                                LocalBroadcastManager.getInstance(TheAplication.instance).sendBroadcast(intentScan);

                                finish();
                            } else {
                                Toast.makeText(activity, "Please try again", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO: Handle error
                            Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();
                            //progressBar.setVisibility(View.GONE);
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            NetworkController.addToRequestQueue(activity, jsonObjectRequest, "checkout");
        }
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(ZxingScannerActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ZxingScannerActivity.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(ZxingScannerActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(ZxingScannerActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            // Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
            mScannerView.setResultHandler(this);
            //for front camera pass 1 in startCamera();
            mScannerView.startCamera(CAMERA_ID);
            mScannerView.setFocusable(false);
//            mScannerView.setFocusableInTouchMode(true);

            // mScannerView.setAutoFocus(true);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                //Camera
                case 1:
                    mScannerView.setResultHandler(this);
                    mScannerView.startCamera(CAMERA_ID);
                    mScannerView.setFocusable(false);
                    break;
            }
            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera(CAMERA_ID);// Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        mScannerView.setFocusable(false);

    }

    @Override
    public void handleResult(Result result) {
        CartInventory.Inventory inventory = InventoryManager.getInstance().getInventoryByBarcode(CrewProfiles.getInstance().getLogdInCartNo(), result.getText());
        if (inventory != null) {

            //if user scan from order screen then the left quantity should decrease
            int count = mapItemId.containsKey(inventory.getItemId()) ? mapItemId.get(inventory.getItemId()) : 0;
            outerloop:
            if (ordersScreen) {
                boolean exist = false;
                for (Iterator<String> itr = DominosAPI.getInstance().getOrderMapKeySet().iterator(); itr.hasNext(); ) {
                    String key = itr.next();
                    if (key.contains(selectedSeat)) {
                        for (String keys : orderItems.getMapOfDataList().keySet()) {
                            for (OrdersFragment.OrderItems.DataItem dataItem : orderItems.getMapOfDataList().get(keys)) {
                                EntityDominosRequest.Data data = dataItem.getData();
                                if (inventory.getItemId() == Integer.parseInt(data.getId())) {
                                    if (dataItem.getLeftQuantity() > 0) {
                                        dataItem.setLeftQuantity(dataItem.getLeftQuantity() - 1);
                                        mapItemId.put(inventory.getItemId(), ++count);
                                        exist = true;
                                        break outerloop;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!exist) {
                    Toast.makeText(this, getString(R.string.no_quantity_left), Toast.LENGTH_SHORT).show();
                }

            } else {
                if (inventory.getQuantity() > count) {
                    EntityDominosRequest.Data data = new EntityDominosRequest.Data(String.valueOf(inventory.getItemId()), inventory.getItemName(), inventory.getItemDescription(), 1, inventory.getPrice(), inventory.getImageUrl(), inventory.getType(), inventory.isVeg());
                    request = DominosAPI.getInstance().addRequestData(data, request, true);
                    mapItemId.put(inventory.getItemId(), ++count);
                }
            }
            setTextCount();
        }

        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }

    private void setTextCount() {
        int count = 0;
        for (Integer i : mapItemId.values()) {
            count += i;
        }
        txtItemCounts.setText(count + " " + getResources().getString(R.string.items));

    }


    @Override
    public void onBackPressed() {
    /*    AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();*/
        finish();
    }

    private void updateSeatSelection(View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    updateSeatSelection(child);
                }

            } else if (v instanceof TextView) {
                if (requestMap.containsKey(((TextView) v).getText())) {
                    if (getIntent().getExtras().containsKey(Constants.SELECTED_SEAT_KEY)) {
                        if (selectedSeat.equals(((TextView) v).getText())) {
                            v.setBackgroundResource(R.drawable.icon_seat_highlight);
                            selectSeat(v);
                        } else {
                            v.setBackgroundResource(R.drawable.icon_seat_selected);
                        }
                    } else if (lastSelectedClickedSeat == null) {
                        // display 1st request as highlighted by default
                        v.setBackgroundResource(R.drawable.icon_seat_highlight);
                        selectSeat(v);

                    } else {
                        v.setBackgroundResource(R.drawable.icon_seat_selected);
                    }
                } else {
                    v.setBackgroundResource(R.drawable.icon_seat);
                }
                v.setOnClickListener(onSeatClickListener);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchRequests() {
        requestMap = new HashMap<>();
        for (EntityDominosRequest request : DominosAPI.getInstance().getOrderMapValues()) {
            String seatNo = request.getMsisdn();
            List<EntityDominosRequest> requestList = requestMap.get(seatNo);
            if (requestList == null) {
                requestList = new ArrayList<>();
                requestMap.put(seatNo, requestList);
            }
            requestList.add(request);
        }
    }

    private void selectSeat(View v) {
        lastSelectedClickedSeat = v;
        selectedSeat = (String) ((TextView) v).getText();
        txtViewSeat.setText(selectedSeat);
        txtViewSeat.setBackground(getResources().getDrawable(R.drawable.icon_seat_selected));
        txtItemCounts.setText("0" + " " + getResources().getString(R.string.items));
        relitems.setVisibility(View.VISIBLE);
        relitems.setClickable(true);

        mapItemId.clear();
    }

    View.OnClickListener onSeatClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == lastSelectedClickedSeat) {
                return;
            }
            v.setBackgroundResource(R.drawable.icon_seat_highlight);
            if (lastSelectedClickedSeat != null) {
                lastSelectedClickedSeat.setBackgroundResource(R.drawable.icon_seat_selected);
            }
            selectSeat(v);
            sofaset1.setVisibility(View.INVISIBLE);
            sofaset2.setVisibility(View.INVISIBLE);
            setTextCount();
        }
    };

    View.OnClickListener onPassangerViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            llCartParent.setSelected(false);
            llPassangerParent.setSelected(true);
            relOtherCrewDetails.setVisibility(View.GONE);
            relitems.setVisibility(View.GONE);
            relseatmap.setVisibility(View.VISIBLE);

        }
    };

    View.OnClickListener onCartViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            relOtherCrewDetails.removeAllViews();
            llCartParent.setSelected(true);
            llPassangerParent.setSelected(false);
            relseatmap.setVisibility(View.GONE);
            relitems.setVisibility(View.GONE);
            sofaset1.setVisibility(View.GONE);
            sofaset2.setVisibility(View.GONE);
            relOtherCrewDetails.setVisibility(View.VISIBLE);
            CrewProfiles crewProfiles = CrewProfiles.getInstance();
            List<CrewProfile> crewProfileList = crewProfiles.getCrewProfileList();
            map = new HashMap<>();
            for (int i = 0; i < crewProfileList.size(); i++) {
                if (crewProfileList.get(i).getEmpId() != crewProfiles.getLogdInCrewMemId()) {
                    setOtherCrewDetails(crewProfileList.get(i), i);
                }
            }

        }
    };


    private void setOtherCrewDetails(CrewProfile crewProfile, int i) {
        LayoutInflater inflater = LayoutInflater.from(ZxingScannerActivity.this);
        View crewDetailsView = inflater.inflate(R.layout.layout_other_crew_details_item, relOtherCrewDetails, false);
        final LinearLayout linearLayout = crewDetailsView.findViewById(R.id.llOtherCrewDetails);
        TextView tvCrewName = crewDetailsView.findViewById(R.id.tvCrewName);
        final ImageView ivIndicator = crewDetailsView.findViewById(R.id.ivIndicator);
        ivIndicator.setVisibility(View.GONE);
        ImageView ivCrewProfileImage = crewDetailsView.findViewById(R.id.ivCrewImage);
        linearLayout.setTag(i);
        map.put(i, ivIndicator);
        Glide.with(this)
                .load(crewProfile.getImageUrl())
                .into(ivCrewProfileImage)
                .onLoadFailed(getResources().getDrawable(R.drawable.cart_sonam));
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Map.Entry s : map.entrySet()) {
                    if (s.getKey() == v.getTag()) {
                        ((ImageView) s.getValue()).setVisibility(View.VISIBLE);
                    } else {
                        ((ImageView) s.getValue()).setVisibility(View.GONE);
                    }
                }
                relitems.setVisibility(View.VISIBLE);
                relitems.setClickable(false);
                txtViewSeat.setBackground(getResources().getDrawable(R.drawable.icon_cart_blue));
                txtViewSeat.setText("");
                txtItemCounts.setText("0" + " " + getResources().getString(R.string.items));
                mapItemId.clear();
            }
        });
        tvCrewName.setText(crewProfile.getEmpName());

        relOtherCrewDetails.addView(crewDetailsView);
    }

}
