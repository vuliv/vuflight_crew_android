package com.vushare.ui.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.TheAplication;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.vucast.constants.DataConstants;
import com.vucast.service.WebServerService;
import com.vucast.utility.DominosAPI;
import com.vucast.utility.NotificationBuilder;
import com.vucast.utility.ServiceRequestObserver;
import com.vuliv.network.activity.ParentActivity;
import com.vuliv.network.service.SharedPrefController;
import com.vushare.R;
import com.vushare.controller.HotspotControl;
import com.vushare.controller.InventoryManager;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.model.services.ServiceRequestModel;
import com.vushare.model.services.ServiceResponse;
import com.vushare.server.ShareService;
import com.vushare.ui.fragment.BaseFragment;
import com.vushare.ui.fragment.CartViewFragment;
import com.vushare.ui.fragment.OrdersFragment;
import com.vushare.ui.view.CircleImageView;
import com.vushare.utility.BundleKeys;
import com.vushare.utility.Constants;
import com.vushare.utility.ConversionUtil;
import com.vushare.utility.SharedPrefsKeys;
import com.vushare.utility.Utility;
import com.vushare.utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.vucast.constants.Constants.REQUEST_CODE_SERVER_START;
import static com.vushare.ui.activity.ShareActivity.ShareUIHandler.LIST_API_CLIENTS;
import static com.vushare.ui.activity.ShareActivity.ShareUIHandler.UPDATE_AP_STATUS;

/**
 * Created by user on 21-09-2017.
 */

public class BaseActivity extends ParentActivity implements View.OnClickListener, ServiceRequestObserver, NotificationBuilder, DrawerLayout.DrawerListener {
    public static Context context;
    public static final String TAG = "BaseActivity";
    public static final String PREFERENCES_KEY_SHARED_FILE_PATHS = "vushare_shared_file_paths";
    public static final int REQUIRED_SIZE = 512;

    private ShareUIHandler mUiUpdateHandler;
    private BroadcastReceiver mServerUpdateListener;

    private HotspotControl mHotspotControl;
    private boolean isApEnabled = false;
    private RelativeLayout toolbar;
    private ImageView imgViewNav, imgViewCancel, imgViewBack;
    public static TextView tv_service_req;
    private DrawerLayout drawer_layout;
    private RelativeLayout rlServiceRequests, rlPaMode, rlScan, rlManage, rlSalesReport, rltLytLogout, rltLytReconciliation, rlCrewProfile, iv_service_req;
    private TextView tv_pa_mode, tv_service_request_count;
    private String currentContentFragmentTag;
    private TextView txtCart, txtName;
    private TextView txtCrewName, txtCrewName1, txtCrewName2, txtCrewName3;
    private CircleImageView imgCrewImage, imgCrewImage1, imgCrewImage2, imgCrewImage3, imgCrewProfileImage;
    private RelativeLayout rltLytMemThird, rltLytMemSecond, rltLytMemOne;
    private LinearLayout lnrLytMemberOne, lnrNavigationHeader;
    public TextView txtViewCurrentTripPointsCount, txtCrewProfilePoints;
    private SharedPreferences pref;


    public BaseActivity() {
        pref = TheAplication.getInstance().getSharedPreferences(SharedPrefsKeys.SHARED_PREFERENCES_NAME, TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        context = BaseActivity.this;
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        imgViewNav = (ImageView) findViewById(R.id.imgViewNav);
        imgViewNav.setOnClickListener(this);
        txtCrewName = (TextView) findViewById(R.id.txtCrewName);
        txtCart = (TextView) findViewById(R.id.txtCart);
        txtName = (TextView) findViewById(R.id.txtName);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_layout.addDrawerListener(this);
        imgViewCancel = (ImageView) findViewById(R.id.imgViewCancel);
        imgViewCancel.setOnClickListener(this);
        imgViewBack = (ImageView) findViewById(R.id.imgViewBack);
        imgViewBack.setOnClickListener(this);
        rlPaMode = (RelativeLayout) findViewById(R.id.rlPaMode);
        tv_pa_mode = (TextView) findViewById(R.id.tv_pa_mode);
        rlPaMode.setOnClickListener(this);
        rlServiceRequests = (RelativeLayout) findViewById(R.id.rlServiceRequests);
        tv_service_request_count = (TextView) findViewById(R.id.tv_service_request_count);
        rlServiceRequests.setOnClickListener(this);
        rlScan = (RelativeLayout) findViewById(R.id.rlScan);
        rlScan.setOnClickListener(this);
        rlManage = (RelativeLayout) findViewById(R.id.rlManage);
        rlManage.setOnClickListener(this);
        rlSalesReport = (RelativeLayout) findViewById(R.id.rlSalesReport);
        rlSalesReport.setOnClickListener(this);
        rlCrewProfile = (RelativeLayout) findViewById(R.id.rlCrewProfile);
        rltLytLogout = (RelativeLayout) findViewById(R.id.rltLytLogout);
        rltLytLogout.setOnClickListener(this);
        rltLytReconciliation = (RelativeLayout) findViewById(R.id.rltLytReconciliation);
        rltLytReconciliation.setOnClickListener(this);

        imgCrewProfileImage = (CircleImageView) findViewById(R.id.circleImageViewProfileImage);
        imgCrewImage = (CircleImageView) findViewById(R.id.imgCrewImage);
        imgCrewImage1 = (CircleImageView) findViewById(R.id.imgCrewImage1);
        txtCrewName1 = (TextView) findViewById(R.id.txtCrewName1);
        txtCrewProfilePoints = (TextView) findViewById(R.id.txtViewTotalPointsCount);
        imgCrewImage2 = (CircleImageView) findViewById(R.id.imgCrewImage2);
        txtCrewName2 = (TextView) findViewById(R.id.txtCrewName2);
        imgCrewImage3 = (CircleImageView) findViewById(R.id.imgCrewImage3);
        txtCrewName3 = (TextView) findViewById(R.id.txtCrewName3);
        rltLytMemThird = (RelativeLayout) findViewById(R.id.rltLytMemThird);
        rltLytMemSecond = (RelativeLayout) findViewById(R.id.rltLytMemSecond);
        rltLytMemOne = (RelativeLayout) findViewById(R.id.rltLytMemOne);
        lnrLytMemberOne = (LinearLayout) findViewById(R.id.lnrLytMemberOne);
        lnrNavigationHeader = (LinearLayout) findViewById(R.id.lnrLytHeader);

        iv_service_req = (RelativeLayout) findViewById(R.id.iv_service_req);
        tv_service_req = (TextView) findViewById(R.id.tv_service_req);
        iv_service_req.setOnClickListener(this);

        lnrNavigationHeader.setOnClickListener(this);

        lnrLytMemberOne.setOnClickListener(this);
        rltLytMemOne.setOnClickListener(this);
        rltLytMemSecond.setOnClickListener(this);
        rltLytMemThird.setOnClickListener(this);

        txtViewCurrentTripPointsCount = (TextView) findViewById(R.id.txtViewCurrentTripPointsCount);

        init();
        /*Getting All Service Requests From server*/
        getAllServiceRequest();
        registerReceiver(mServerUpdateListener,
                new IntentFilter(ShareService.ShareIntents.SHARE_SERVER_UPDATES_INTENT_ACTION));
    }

    @Override
    public void notification(String group, String name, String message, int empId, String condition) {
        createNotification(group, name, message, empId, condition);
    }

    public void createNotification(String group, String name, String message, int crewId, String condition) {
        Intent intent = null;
        switch (condition) {
            case "ChatActivity":
                intent = new Intent(getApplicationContext(), CrewChatActivity.class);
                intent.putExtra(Constants.EMP_ID_KEY, crewId);
                intent.putExtra("Notification", "True");
                break;
            case "ServiceRequest":
                intent = new Intent(getApplicationContext(), ServiceRequestActivity.class);
                intent.putExtra("Notification", "True");
                break;
            case "OrdersByPassenger":
                intent = new Intent(getApplicationContext(), OrdersActivity.class);
                break;
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        int notiID = new Date().getSeconds();
        String GROUP = group;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(TheAplication.getInstance())
                .setSmallIcon(com.vucast.R.drawable.ic_vuscreen)
                .setContentTitle(name)
                .setContentText(message)
                .setGroup(GROUP).setGroupSummary(true).setPriority(Notification.PRIORITY_HIGH)
                .setAutoCancel(true).setContentIntent(pendingIntent).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notiID, mBuilder.build());
    }

    public void setCrewDetails() {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        txtName.setText(crewProfiles.getLoginMemName());
        Bitmap bitmap = ConversionUtil.decodeFile(crewProfiles.getSelectedProfileImgUri(BaseActivity.this), REQUIRED_SIZE);
        if (bitmap != null) {
            imgCrewProfileImage.setImageBitmap(bitmap);
        } else {
            RequestOptions requestOptions = new RequestOptions();
            CrewProfile crewProfile = crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId());
            if (crewProfile.getGender().equals("F")) {
                Glide.with(this)
                        .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                        .load(crewProfile.getImageUrl())
                        .into(imgCrewProfileImage);
            } else {
                Glide.with(this)
                        .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                        .load(crewProfile.getImageUrl())
                        .into(imgCrewProfileImage);
            }
        }
        txtCart.setText(crewProfiles.getLogdInCartNo());
        // txtCrewProfilePoints.setText("" + (crewProfiles.getCrewProfileByEmpID(crewProfiles.logdInCrewMemId)).getPoints());
        txtCrewProfilePoints.setText("" + (crewProfiles.getLoginCrewTotalPoints(this)));
        txtViewCurrentTripPointsCount.setText("" + pref.getInt(SharedPrefsKeys.POINTS_EARNED, 0));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_SERVER_START:
                    resetSenderUi(true);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    private void init() {
        mHotspotControl = HotspotControl.getInstance(getApplicationContext());

        mServerUpdateListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (isFinishing() || null == intent)
                    return;
                int intentType = intent.getIntExtra(ShareService.ShareIntents.TYPE, 0);
                if (intentType == ShareService.ShareIntents.Types.FILE_TRANSFER_STATUS) {
                    String fileName = intent.getStringExtra(ShareService.ShareIntents.SHARE_SERVER_UPDATE_FILE_NAME);
                } else if (intentType == ShareService.ShareIntents.Types.AP_DISABLED_ACKNOWLEDGEMENT) {
                    resetSenderUi(false);
                }
            }
        };
    }

    @Override
    public void onRequestReceived(String serviceRequestModel) {

    }

    @Override
    public void onRequestNotify(int notify) {
        CrewProfiles.getInstance().setCrewRequestsCount(CrewProfiles.getInstance().getCrewRequestsCount() + notify);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DominosAPI.serviceRequestObserver = (ServiceRequestObserver) context;
        DominosAPI.notificationBuilder = (NotificationBuilder) context;
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        CrewProfile crewProfile = crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId());
        if (crewProfile != null) {
            txtCrewProfilePoints.setText(crewProfile.getPoints() + "");
            txtViewCurrentTripPointsCount.setText(pref.getInt(SharedPrefsKeys.POINTS_EARNED, 0) + "");
        }
        //If service is already running, change UI and display info for receiver
        if (Utils.isShareServiceRunning(getApplicationContext())) {
            refreshApData();
        }
    }

    private void getAllServiceRequest() {
        final CrewProfiles crewProfiles = CrewProfiles.getInstance();
        String url = "http://192.168.43.1:8080/api/getAllActiveServicesByCart";
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("crewId", crewProfiles.getLogdInCrewMemId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                ServiceResponse serviceResponse = gson.fromJson(response.toString(), ServiceResponse.class);
                ArrayList<ServiceRequestModel> serviceRequestModels = (ArrayList<ServiceRequestModel>) serviceResponse.getServiceList();
                crewProfiles.setCrewRequestsCount(serviceRequestModels.size());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(BaseActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new
                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(BaseActivity.this, jsonObjectRequest, "getPassengerCrewDetailByCart");
    }

    private void paModeEnableDisable(TextView textView) {
        String url = "http://192.168.43.1:8080/api/paModeActivate";
        JSONObject jsonRequest = new JSONObject();
        try {
            if (CrewProfiles.getInstance().getPaMode()) {
                jsonRequest.put("pamode", false);

            } else {
                jsonRequest.put("pamode", true);
            }
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("StatusCode").equals("200")) {
                        if (CrewProfiles.getInstance().getPaMode()) {
                            CrewProfiles.getInstance().setPaMode(false);
                            textView.setText("PA Mode Enable");
                            Toast.makeText(context, "PA Mode Disable Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            CrewProfiles.getInstance().setPaMode(true);
                            textView.setText("PA Mode Disable");
                            Toast.makeText(context, "PA Mode Enable Successfully", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong!\nPlease try again later", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Something went wrong!\nPlease try again later", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(BaseActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(BaseActivity.this, jsonObjectRequest, "getPassengerCrewDetailByCart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mServerUpdateListener)
            unregisterReceiver(mServerUpdateListener);
        if (null != mUiUpdateHandler)
            mUiUpdateHandler.removeCallbacksAndMessages(null);
        mUiUpdateHandler = null;
    }

    public void enableAp(boolean isNougatDisclaimerShown) {
        startP2pSenderWatchService();
        refreshApData();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (!isNougatDisclaimerShown) {
                Utility.showAlertDialog(this, getString(R.string.nougat_disclaimer), "Set", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }, null);
                String hotspotName = SharedPrefController.getUserName(this) +
                        Constants.VUSHARE_HOTSPOT_SEPARATOR +
                        SharedPrefController.getUserAvatar(this) +
                        Constants.VUSHARE_HOTSPOT_SEPARATOR +
                        Constants.VUSHARE_HOTSPOT_SUFFIX;
                mHotspotControl.setHotspotName(hotspotName);
            }
        }
    }

    private void disableAp() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_STOP);
        startService(p2pServiceIntent);
        isApEnabled = false;
    }

    public void startP2pSenderWatchService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
//        p2pServiceIntent.putExtra(ShareService.EXTRA_FILE_PATHS, m_sharedFilePaths);
        if (null != getIntent()) {
//            p2pServiceIntent.putExtra(ShareService.EXTRA_PORT, getIntent().getIntExtra(ShareService.EXTRA_PORT, 0));
            p2pServiceIntent.putExtra(ShareService.EXTRA_SENDER_NAME, getIntent().getStringExtra(ShareService.EXTRA_SENDER_NAME));
        }
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_START);
        startService(p2pServiceIntent);
    }

    /**
     * Starts {@link ShareService} with intent action {@link ShareService#WIFI_AP_ACTION_START_CHECK} to make {@link ShareService} constantly check for Hotspot status. (Sometimes Hotspot tend to stop if stayed idle for long enough. So this check makes sure {@link ShareService} is only alive if Hostspot is enaled.)
     */
    private void startHostspotCheckOnService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_START_CHECK);
        startService(p2pServiceIntent);
    }

    /**
     * Calls methods - {@link ShareActivity#updateApStatus()} & {@link ShareActivity#listApClients()} which are responsible for displaying Hotpot information and Listing connected clients to the same
     */
    private void refreshApData() {
        if (null == mUiUpdateHandler)
            mUiUpdateHandler = new ShareUIHandler(this);
        updateApStatus();
        listApClients();
    }

    /**
     * Updates Hotspot configuration info like Name, IP if enabled.<br> Posts a message to {@link ShareActivity.ShareUIHandler} to call itself every 1500ms
     */
    private void updateApStatus() {
        if (!HotspotControl.isSupported()) {

        }
        if (mHotspotControl.isEnabled()) {
            if (!isApEnabled) {
                isApEnabled = true;
                startHostspotCheckOnService();
            }
            DataConstants.IP_ADDRESS = com.vucast.utility.Utility.getIp(this);
            WebServerService.getInstance().startServer(this, null);
            /*WifiConfiguration config = mHotspotControl.getConfiguration();
            String ip = Build.VERSION.SDK_INT >= 23 ? WifiUtils.getHostIpAddress() : mHotspotControl.getHostIpAddress();
            if (TextUtils.isEmpty(ip))
                ip = "";
            else
                ip = ip.replace("/", "");
            mShareHotspotInfo.setText(getString(R.string.hotspot_info, config.SSID));
            ArrayList<EntityFileHost> filesToHost = FileSelectController.getInstance(ShareActivity.this).getFilesToHost();
            ArrayList<EntityMediaDetail> entityMediaDetailArrayList = new ArrayList<>();
            for (EntityFileHost entityFileHost : filesToHost) {
                EntityMediaDetail entityMediaDetail = new EntityMediaDetail();
                entityMediaDetail.setPath(entityFileHost.getHostPath());
                entityMediaDetail.setTitle(entityFileHost.getName());
                entityMediaDetail.setType(entityFileHost.getType());
                entityMediaDetail.setDuration(entityFileHost.getDuration());
                entityMediaDetail.setAudioId(entityFileHost.getAudioId());
                entityMediaDetail.setVideoId(entityFileHost.getVideoId());
                entityMediaDetail.setActualSize(entityFileHost.getActualSize());
                if (Constants.TYPE_MUSIC.equals(entityFileHost.getType()) ||
                        Constants.TYPE_VIDEO.equals(entityFileHost.getType()) ||
                        Constants.TYPE_PHOTO.equals(entityFileHost.getType())) {
                    entityMediaDetailArrayList.add(entityMediaDetail);
                }
            }
            DataService.getInstance().setContent(entityMediaDetailArrayList);
            Intent serverIntent = new Intent(ShareActivity.this, ServerActivity.class);
            serverIntent.putExtra(SERVER_SSID, config.SSID.replace("\"", ""));
            startActivityForResult(serverIntent, REQUEST_CODE_SERVER_START);*/
        } else if (null != mUiUpdateHandler) {
            mUiUpdateHandler.removeMessages(UPDATE_AP_STATUS);
            mUiUpdateHandler.sendEmptyMessageDelayed(UPDATE_AP_STATUS, 1500);
        }
    }

    /**
     * Calls {@link HotspotControl#getConnectedWifiClients(int, HotspotControl.WifiClientConnectionListener)} to get Clients connected to Hotspot.<br>
     * Constantly adds/updates receiver items
     * <br> Posts a message to {@link ShareActivity.ShareUIHandler} to call itself every 1000ms
     */
    private synchronized void listApClients() {
        if (mHotspotControl == null) {
            return;
        }
        mHotspotControl.getConnectedWifiClients(2000,
                new HotspotControl.WifiClientConnectionListener() {
                    public void onClientConnectionAlive(final HotspotControl.WifiScanResult wifiScanResult) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }

                    @Override
                    public void onClientConnectionDead(final HotspotControl.WifiScanResult c) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }

                    public void onWifiClientsScanComplete() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mUiUpdateHandler) {
                                    mUiUpdateHandler.removeMessages(LIST_API_CLIENTS);
                                    mUiUpdateHandler.sendEmptyMessageDelayed(LIST_API_CLIENTS, 1000);
                                }
                            }
                        });
                    }
                }

        );
    }

    private void resetSenderUi(boolean disableAP) {
        if (mUiUpdateHandler != null) mUiUpdateHandler.removeCallbacksAndMessages(null);
        if (disableAP)
            disableAp();
    }


    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

        if (CrewProfiles.getInstance().getCrewRequestsCount() > 0) {
            tv_service_request_count.setText("" + CrewProfiles.getInstance().getCrewRequestsCount());
        } else {
            tv_service_request_count.setText("");

        }

        if (CrewProfiles.getInstance().getPaMode()) {
            tv_pa_mode.setText("PA Mode Disable");
        } else {
            tv_pa_mode.setText("PA Mode Enable");
        }
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    static class ShareUIHandler extends Handler {
        WeakReference<BaseActivity> mActivity;

        static final int LIST_API_CLIENTS = 100;
        static final int UPDATE_AP_STATUS = 101;

        ShareUIHandler(BaseActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            BaseActivity activity = mActivity.get();
            if (null == activity || msg == null)
                return;
            if (msg.what == LIST_API_CLIENTS) {
                activity.listApClients();
            } else if (msg.what == UPDATE_AP_STATUS) {
                activity.updateApStatus();
            }
        }

    }

    public void stopSharingFiles() {
        resetSenderUi(true);
        WebServerService.getInstance().stopServer();
    }

    @Override
    public void onClick(View v) {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        int i = v.getId();
        if (i == R.id.iv_service_req) {
            Intent intent = new Intent(this, ServiceRequestActivity.class);
            startActivity(intent);
        } else if (i == R.id.imgViewNav) {
            drawer_layout.openDrawer(GravityCompat.START);
        } else if (i == R.id.imgViewCancel) {
            closeNavDrawer();
        } else if (i == R.id.imgViewBack) {
            onBackPressed();
        } else if (i == R.id.rlPaMode) {
            paModeEnableDisable(tv_pa_mode);
            closeNavDrawer();
        } else if (i == R.id.rlServiceRequests) {
            Intent intent = new Intent(this, ServiceRequestActivity.class);
            startActivity(intent);
            closeNavDrawer();
        } else if (i == R.id.rlScan) {
            Intent intent = new Intent(BaseActivity.this, ZxingScannerActivity.class);
            intent.putExtra(BundleKeys.IS_FROM_NAV_DRAWER, true);
            startActivity(intent);
            closeNavDrawer();
        } else if (i == R.id.rlManage) {
            startCartViewActivity(crewProfiles.getLogdInCartNo(), crewProfiles.getLogdInCrewMemId());
        } else if (i == R.id.rlSalesReport) {
            startActivity(new Intent(this, SalesReportActivity.class));
            closeNavDrawer();
        } else if (i == R.id.lnrLytHeader) {
            startActivity(new Intent(this, ProfileAndPrizeEligiblityActivity.class));
            closeNavDrawer();
        } else if (i == R.id.rltLytLogout) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.logout);
            builder.setMessage(R.string.exit_message);
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    CrewProfiles.getInstance().clearPreferance(BaseActivity.this);
                    Intent intent = new Intent(BaseActivity.this, WelcomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    dialog.dismiss();
                    InventoryManager.getInstance().clear();
                    DominosAPI.getInstance().clearOrders();
                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        } else if (i == R.id.rltLytReconciliation) {
            startActivity(new Intent(this, ReconciliationActivity.class));
            closeNavDrawer();
        } else if (i == R.id.lnrLytMemberOne) {
            startCartViewActivity(crewProfiles.getLogdInCartNo(), crewProfiles.getLogdInCrewMemId());
        } else if (i == R.id.rltLytMemOne) {
            startCartViewActivity(crewProfiles.getOtherCrewProfileList().get(0).getCartNumber(), crewProfiles.getOtherCrewProfileList().get(0).getEmpId());
        } else if (i == R.id.rltLytMemSecond) {
            startCartViewActivity(crewProfiles.getOtherCrewProfileList().get(1).getCartNumber(), crewProfiles.getOtherCrewProfileList().get(1).getEmpId());
        } else if (i == R.id.rltLytMemThird) {
            startCartViewActivity(crewProfiles.getOtherCrewProfileList().get(2).getCartNumber(), crewProfiles.getOtherCrewProfileList().get(2).getEmpId());
        }
    }

    public void setSelectedCrewOnTB(String cartNo, int empId) {
        if (cartNo.equals(CrewProfiles.getInstance().getLogdInCartNo())) {
            lnrLytMemberOne.setBackgroundColor(Color.parseColor("#f5cabe"));
        } else {
            lnrLytMemberOne.setBackgroundColor(Color.TRANSPARENT);
        }

        if (cartNo.equals(CrewProfiles.getInstance().getOtherCrewProfileList().get(0).getCartNumber())) {
            rltLytMemOne.setBackgroundColor(Color.parseColor("#f5cabe"));
        } else {
            rltLytMemOne.setBackgroundColor(Color.TRANSPARENT);
        }

        if (cartNo.equals(CrewProfiles.getInstance().getOtherCrewProfileList().get(1).getCartNumber())) {
            rltLytMemSecond.setBackgroundColor(Color.parseColor("#f5cabe"));
        } else {
            rltLytMemSecond.setBackgroundColor(Color.TRANSPARENT);
        }

        if (cartNo.equals(CrewProfiles.getInstance().getOtherCrewProfileList().get(2).getCartNumber())) {
            rltLytMemThird.setBackgroundColor(Color.parseColor("#f5cabe"));
        } else {
            rltLytMemThird.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void startCartViewActivity(String cartNo, int empId) {
        Intent intent = new Intent(this, CartViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.CART_NO_KEY, cartNo);
        bundle.putInt(Constants.EMP_ID_KEY, empId);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        closeNavDrawer();
    }

    private void closeNavDrawer() {
        drawer_layout.closeDrawer(GravityCompat.START);
    }

    public void addFragment(int containerViewId, BaseFragment baseFragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, baseFragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
        currentContentFragmentTag = tag;
    }

    protected BaseFragment getFragmentByTag(String fragmentTag) {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(fragmentTag);
    }

    public void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void showBackIcon() {
        imgViewNav.setVisibility(View.GONE);
        imgViewBack.setVisibility(View.VISIBLE);
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void lockDrawerLayout() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void displayCrewMember() {
        setCrewDetails();
        List<CrewProfile> crewProfileList = CrewProfiles.getInstance().getCrewProfileList();
        List<CrewProfile> otherCrewProfileList = CrewProfiles.getInstance().getOtherCrewProfileList();
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        RequestOptions requestOptions = new RequestOptions();
        switch (crewProfileList.size()) {
            case 1:
                lnrLytMemberOne.setVisibility(View.VISIBLE);
                if (crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                }

                txtCrewName.setText(crewProfiles.getLoginMemName());
                break;
            case 2:
                lnrLytMemberOne.setVisibility(View.VISIBLE);
                rltLytMemOne.setVisibility(View.VISIBLE);

                if (crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                }
                txtCrewName.setText(crewProfiles.getLoginMemName());

                if (crewProfileList.get(0).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(otherCrewProfileList.get(0).getImageUrl())
                            .into(imgCrewImage1);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(otherCrewProfileList.get(0).getImageUrl())
                            .into(imgCrewImage1);
                }
                txtCrewName1.setText(otherCrewProfileList.get(0).getEmpName());
                break;
            case 3:
                lnrLytMemberOne.setVisibility(View.VISIBLE);
                rltLytMemOne.setVisibility(View.VISIBLE);
                rltLytMemSecond.setVisibility(View.VISIBLE);

                if (crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                }
                txtCrewName.setText(crewProfiles.getLoginMemName());

                if (crewProfileList.get(0).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(otherCrewProfileList.get(0).getImageUrl())
                            .into(imgCrewImage1);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(otherCrewProfileList.get(0).getImageUrl())
                            .into(imgCrewImage1);
                }
                txtCrewName1.setText(otherCrewProfileList.get(0).getEmpName());

                if (crewProfileList.get(1).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(otherCrewProfileList.get(1).getImageUrl())
                            .into(imgCrewImage2);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(otherCrewProfileList.get(1).getImageUrl())
                            .into(imgCrewImage2);
                }
                txtCrewName2.setText(otherCrewProfileList.get(1).getEmpName());
                break;
            case 4:
                lnrLytMemberOne.setVisibility(View.VISIBLE);
                rltLytMemOne.setVisibility(View.VISIBLE);
                rltLytMemSecond.setVisibility(View.VISIBLE);
                rltLytMemThird.setVisibility(View.VISIBLE);

                if (crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getImageUrl())
                            .into(imgCrewImage);
                }
                txtCrewName.setText(crewProfiles.getLoginMemName());

                if (crewProfileList.get(0).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(otherCrewProfileList.get(0).getImageUrl())
                            .into(imgCrewImage1);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(otherCrewProfileList.get(0).getImageUrl())
                            .into(imgCrewImage1);
                }
                txtCrewName1.setText(otherCrewProfileList.get(0).getEmpName());

                if (crewProfileList.get(1).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(otherCrewProfileList.get(1).getImageUrl())
                            .into(imgCrewImage2);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(otherCrewProfileList.get(1).getImageUrl())
                            .into(imgCrewImage2);
                }
                txtCrewName2.setText(otherCrewProfileList.get(1).getEmpName());

                if (crewProfileList.get(2).getGender().equals("F")) {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                            .load(otherCrewProfileList.get(2).getImageUrl())
                            .into(imgCrewImage3);
                } else {
                    Glide.with(this)
                            .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                            .load(otherCrewProfileList.get(2).getImageUrl())
                            .into(imgCrewImage3);
                }

                txtCrewName3.setText(otherCrewProfileList.get(2).getEmpName());
                break;
        }

    }

    public void removeFragment(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(baseFragment);
        fragmentTransaction.commitAllowingStateLoss();
        //getSupportFragmentManager().executePendingTransactions();
    }
}
