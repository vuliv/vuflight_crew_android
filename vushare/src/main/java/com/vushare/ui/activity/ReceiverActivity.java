/*
 * Copyright 2017 Srihari Yachamaneni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vushare.ui.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;
import com.vucast.activity.ClientActivity;
import com.vucast.constants.DataConstants;
import com.vucast.entity.EntityWiFiDetail;
import com.vucast.service.DetectServer;
import com.vushare.R;
import com.vushare.callback.ICallback;
import com.vushare.controller.APIController;
import com.vushare.controller.DownloadController;
import com.vushare.entity.EntityFileHost;
import com.vushare.ui.adapter.RecyclerAdapterReceiverFiles;
import com.vushare.ui.adapter.RecyclerAdapterSendersList;
import com.vushare.utility.Constants;
import com.vushare.utility.Utility;
import com.vushare.utility.Utils;
import com.vushare.utility.WifiUtils;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static com.vucast.activity.ClientActivity.SERVER_SSID;
import static com.vucast.constants.Constants.REQUEST_CODE_CLIENT_START;
//import static com.vushare.ui.activity.ReceiverActivity.WifiTasksHandler.SCAN_FOR_WIFI_RESULTS;
//import static com.vushare.ui.activity.ReceiverActivity.WifiTasksHandler.WAIT_FOR_CONNECT_ACTION_TIMEOUT;
//import static com.vushare.ui.activity.ReceiverActivity.WifiTasksHandler.WAIT_FOR_RECONNECT_ACTION_TIMEOUT;
import static com.vushare.utility.WifiUtils.connectToOpenHotspot;

/**
 * Controls
 */
public class ReceiverActivity extends AppCompatActivity {

    public static final String TAG = "ReceiverActivity";
    private static final Long SYNCTIME = 800L;
    private static final String LASTCONNECTEDTIME = "LASTCONNECTEDTIME";

    Toolbar mToolbar;
    private TextView mSizeSent, mSizeLeft, mConnectionStatus, mSenderTitle, mSenderSubtitle;
    private ProgressBar mFileProgress;
    private RecyclerView mRecyclerView, mSenderChooseRecyclerView;
    private RelativeLayout mFrontLayout, mSenderChooseLayout;
    private ImageView mConnectionImage, mSenderImage;

    private APIController mApiController;
    private int mCurrentIndex = 0;
    private long mDownloadedBytes = 0;
    private long totalFileSize = 0;
    private ArrayList<EntityFileHost> mFilesToDownload = new ArrayList<>();
//    private ArrayList<String> mVuShareNetworks = new ArrayList<>();

    private ThinDownloadManager mDownloadManager;

    private RecyclerAdapterReceiverFiles mFilesAdapter;
    private RecyclerAdapterSendersList mSendersAdapter;

    private long updatedTime;
    private WifiManager mWifiManager;
    private SharedPreferences mSharedPreferences;
//    private WifiScanner mWifiScanReceiver;
//    private WifiScanner mWifiChangesReceiver;

//    private WifiTasksHandler mWifiScanHandler;
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 100;

    private String mConnectedSSID, mSenderSSID;
    private boolean mAreOtherNetworksDisabled = false;

    private TypedArray mAvatars;

    private Handler downloadHandler;
    private Runnable downloadRunnable = new Runnable() {
        @Override
        public void run() {
            startDownload();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        init();
    }

    @TargetApi(23)
    private boolean checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
            );
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkLocationAccess()) {
                        startSenderScan();
                    }
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        Utility.showAlertDialog(this, getString(R.string.location_deny_message),
                                getString(R.string.location_deny_positive),
                                getString(R.string.location_deny_negative),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        checkLocationPermission();
                                    }
                                }, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        finish();
                                    }
                                });
                    } else {
                        Utility.showAlertDialog(this, getString(R.string.location_message),
                                getString(R.string.settings),
                                getString(R.string.action_cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        try {
                                            Intent intent = new Intent();
                                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                                            intent.setData(uri);
                                            startActivity(intent);
                                        } catch (ActivityNotFoundException anf) {
                                            Toast.makeText(getApplicationContext(), "Settings activity not found", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                });
                    }
                }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startSenderScan();
        /*boolean isConnectedToVuSHareWifi = WifiUtils.isWifiConnectedToVuShareAccessPoint(getApplicationContext());
        if (isConnectedToVuSHareWifi) {
            unRegisterForScanResults();
            String ssid = mWifiManager.getConnectionInfo().getSSID();
            Log.d(TAG, "wifi is connected/connecting to VuShare ap, ssid: " + ssid);
            mConnectedSSID = ssid;
            showProgressScreen(ssid);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        unRegisterForScanResults();
//        unRegisterForNetworkStateChanges();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mFilesToDownload != null) mFilesToDownload.clear();
        if (mDownloadManager != null) mDownloadManager.cancelAll();
        DownloadController.getInstance(this).clearFilesToDownload();
        if (downloadHandler != null && downloadRunnable != null)
            downloadHandler.removeCallbacks(downloadRunnable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        stopReceivingFiles();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_CLIENT_START:
//                    disableReceiverMode();
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * API response callback
     */
    private ICallback mCallback = new ICallback() {
        @Override
        public void onSuccess(Object object) {
            Gson gson = new Gson();
            ArrayList<EntityFileHost> filesList = gson.fromJson(String.valueOf(object),
                    new TypeToken<ArrayList<EntityFileHost>>() {
                    }.getType());
            // Store file to download for future reference
            mFilesToDownload.addAll(filesList);
            Log.i("Download", "onSuccess: " + mFilesToDownload.size());
            DownloadController.getInstance(ReceiverActivity.this).setFilesToDownload(filesList);
            // Calculate total size of all the files to download
            getTotalFileSize();
            // Download first file
            downloadFile(mCurrentIndex);
            // Update adapter
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSenderSubtitle.setText(mFilesToDownload.size() + " file, total " +
                            Utility.getFileSize(totalFileSize));
                    mFilesAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onFailure(Object msg) {
            Log.i("Download", "onFailure, retry");
            if (mFilesToDownload.size() == 0)
                mApiController.downloadDataFromSender(Constants.FILES_URL, mCallback);
        }
    };

    private void init() {
        mFilesToDownload = DownloadController.getInstance(this).getFilesToDownload();
        mAvatars = getResources().obtainTypedArray(R.array.avatar_images);
        downloadHandler = new Handler();

        mToolbar = (Toolbar) findViewById(R.id.receiver_toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        updatedTime = System.currentTimeMillis();
        mApiController = new APIController();
        mDownloadManager = new ThinDownloadManager();

        mConnectionStatus = (TextView) findViewById(R.id.receiver_hotspot_info);
        mFrontLayout = (RelativeLayout) findViewById(R.id.receiver_front_layout);
        mConnectionImage = (ImageView) findViewById(R.id.receiver_connection_image);
        mConnectionImage.setBackgroundResource(R.drawable.connection_anim);
        // Get the background, which has been compiled to an AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) mConnectionImage.getBackground();
        // Start the animation (looped playback by default).
        frameAnimation.start();

        mSenderChooseLayout = (RelativeLayout) findViewById(R.id.sender_choose_layout);
        mSenderChooseRecyclerView = (RecyclerView) findViewById(R.id.sender_choose_recyclerview);
        mSenderChooseRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mSenderImage = (ImageView) findViewById(R.id.list_item_imageview);
        mSenderTitle = (TextView) findViewById(R.id.list_item_title);
        mSenderSubtitle = (TextView) findViewById(R.id.list_item_subtitle);

        mSizeSent = (TextView) findViewById(R.id.receiver_size_sent);
        mSizeLeft = (TextView) findViewById(R.id.receiver_size_left);
        mFileProgress = (ProgressBar) findViewById(R.id.receiver_file_progress);
        mRecyclerView = (RecyclerView) findViewById(R.id.receiver_files_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mFilesAdapter = new RecyclerAdapterReceiverFiles(this, mFilesToDownload);
        mRecyclerView.setAdapter(mFilesAdapter);

        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        mWifiScanHandler = new WifiTasksHandler(this);
        if (!mWifiManager.isWifiEnabled())
            mWifiManager.setWifiEnabled(true);
    }

    /**
     * Start scan for sender hotspot
     * @return
     */
    private boolean startSenderScan() {
        new  Thread(new Runnable() {
            @Override
            public void run() {
                DetectServer.getInstance().scanDevices(ReceiverActivity.this);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSendersAdapter = new RecyclerAdapterSendersList(ReceiverActivity.this, DetectServer.getInstance().getWiFiDetails(), mAvatars);
                        mSenderChooseRecyclerView.setAdapter(mSendersAdapter);
                    }
                });
            }
        }).start();

//        if (Utils.getTargetSDKVersion(getApplicationContext()) >= 23
//                &&
//                // if targetSdkVersion >= 23
//                //      Get Wifi Scan results method needs GPS to be ON and COARSE location permission
//                !checkLocationPermission())
//            return false;
//        checkLocationAccess();
//        registerAndScanForWifiResults();
//        registerForNetworkStateChanges();
        return true;
    }

    /**
     * Disable receiver mode
     *//*
    private void disableReceiverMode() {
        if (!TextUtils.isEmpty(mConnectedSSID)) {
            if (mAreOtherNetworksDisabled)
                WifiUtils.removeVuShareWifiAndEnableOthers(mWifiManager, mConnectedSSID);
            else
                WifiUtils.removeWifiNetwork(mWifiManager, mConnectedSSID);
        }
        mWifiScanHandler.removeMessages(WAIT_FOR_CONNECT_ACTION_TIMEOUT);
        mWifiScanHandler.removeMessages(WAIT_FOR_RECONNECT_ACTION_TIMEOUT);
        unRegisterForScanResults();
        unRegisterForNetworkStateChanges();
    }*/
/*
    *//**
     * Register and scan for wifi connections available
     *//*
    private void registerAndScanForWifiResults() {
        if (null == mWifiScanReceiver)
            mWifiScanReceiver = new WifiScanner();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mWifiScanReceiver, intentFilter);
        mConnectionStatus.setText(getString(R.string.hotspot_scan));
        startWifiScan();
    }

    *//**
     * Register for change in network state
     *//*
    private void registerForNetworkStateChanges() {
        if (null == mWifiChangesReceiver)
            mWifiChangesReceiver = new WifiScanner();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(mWifiChangesReceiver, intentFilter);
    }

    private void unRegisterForScanResults() {
        stopWifiScan();
        try {
            if (null != mWifiScanReceiver)
                unregisterReceiver(mWifiScanReceiver);
        } catch (Exception e) {
            Log.e(TAG, "exception while un-registering wifi changes.." + e.getMessage());
        }
    }

    private void unRegisterForNetworkStateChanges() {
        try {
            if (null != mWifiChangesReceiver)
                unregisterReceiver(mWifiChangesReceiver);
        } catch (Exception e) {
            Log.e(TAG, "exception while un-registering NW changes.." + e.getMessage());
        }
    }

    private void startWifiScan() {
        mWifiScanHandler.removeMessages(SCAN_FOR_WIFI_RESULTS);
        mWifiScanHandler.sendMessageDelayed(mWifiScanHandler.obtainMessage(SCAN_FOR_WIFI_RESULTS), 500);
    }

    private void stopWifiScan() {
        if (null != mWifiScanHandler)
            mWifiScanHandler.removeMessages(SCAN_FOR_WIFI_RESULTS);
    }*/

    private boolean checkLocationAccess() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.e(TAG, "GPS not enabled..");
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.please_enable_location)
                .setCancelable(false)
                .setPositiveButton(R.string.share_stop_positive, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.share_stop_negative, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    public void resetSenderSearch() {
        startSenderScan();
    }

    /**
     * Connect to Wifi with given SSID
     * @param entityWiFiDetail
     */
    public void connectToWifi(EntityWiFiDetail entityWiFiDetail) {
        DataConstants.IP_ADDRESS = entityWiFiDetail.getIpAddress();
        Intent clientIntent = new Intent(ReceiverActivity.this, ClientActivity.class);
        clientIntent.putExtra(SERVER_SSID, entityWiFiDetail.getName() + "-" + entityWiFiDetail.getAvatarId() + "-" + Constants.VUSHARE_HOTSPOT_SUFFIX);
        startActivityForResult(clientIntent, REQUEST_CODE_CLIENT_START);

        /*showConnectingScreen();
        WifiInfo info = mWifiManager.getConnectionInfo();
        unRegisterForScanResults();
        boolean resetWifiScan;
        if (info.getSSID().equals(ssid)) {
            Log.d(TAG, "Already connected to VuShare, add sender Files listing fragment");
            resetWifiScan = false;
            showProgressScreen(ssid);
        } else {
            mConnectionStatus.setText(getString(R.string.hotspot_info_2, ssid));
            resetWifiScan = !connectToOpenHotspot(mWifiManager, ssid, false);
            Log.e(TAG, "connection attempt to VuShare wifi is " + (!resetWifiScan ? "success!!!" : "FAILED..!!!"));
        }
        //if wap isnt successful, start wifi scan
        if (resetWifiScan) {
            mConnectionStatus.setText(getString(R.string.hotspot_scan));
            startSenderScan();
        } else {
            Message message = mWifiScanHandler.obtainMessage(WAIT_FOR_CONNECT_ACTION_TIMEOUT);
            message.obj = ssid;
            mWifiScanHandler.sendMessageDelayed(message, 7000);
        }*/
    }

/*
    private class WifiScanner extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION) && !WifiUtils.isWifiConnectedToVuShareAccessPoint(getApplicationContext())) {
                List<ScanResult> mScanResults = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE)).getScanResults();
                boolean foundVuSHareWifi = false;
                for (ScanResult result : mScanResults)
                    if (WifiUtils.isVuShareSSID(result.SSID) && WifiUtils.isOpenWifi(result)) {
                        Log.d(TAG, "signal level: " + result.level);
//                        connectToWifi(result.SSID);
                        String ssid = result.SSID;
                        if (!mVuShareNetworks.contains(ssid)) {
                            mVuShareNetworks.add(ssid);
                            showVuShareWifi(result.SSID);
                        }
                        foundVuSHareWifi = true;
                    }
                if (!foundVuSHareWifi) {
                    Log.e(TAG, "No VuShare wifi found, starting scan again!!");
                    startWifiScan();
                }
            } else if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo netInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (ConnectivityManager.TYPE_WIFI == netInfo.getType()) {
                    WifiInfo info = mWifiManager.getConnectionInfo();
                    SupplicantState supState = info.getSupplicantState();
                    Log.d(TAG, "NETWORK_STATE_CHANGED_ACTION, ssid: " + info.getSSID() + ", ap ip: " + WifiUtils.getAccessPointIpAddress(getApplicationContext()) + ", sup state: " + supState);
                    if (null == mSharedPreferences)
                        mSharedPreferences = getSharedPreferences(
                                getPackageName(), Context.MODE_PRIVATE);
                    if (WifiUtils.isVuShareSSID(info.getSSID())) {
                        if (System.currentTimeMillis() - mSharedPreferences.getLong(LASTCONNECTEDTIME, 0) >= SYNCTIME && supState.equals(SupplicantState.COMPLETED)) {
                            mConnectedSSID = info.getSSID();
                            mWifiScanHandler.removeMessages(WAIT_FOR_CONNECT_ACTION_TIMEOUT);
                            mWifiScanHandler.removeMessages(WAIT_FOR_RECONNECT_ACTION_TIMEOUT);
                            final String ip = WifiUtils.getAccessPointIpAddress(getApplicationContext());
                            mSharedPreferences.edit().putLong(LASTCONNECTEDTIME, System.currentTimeMillis()).commit();
                            Log.d(TAG, "client connected to VuShare hot spot. AP ip address: " + ip);
                            showProgressScreen(info.getSSID());
                        }
                    }
                }
            }
        }
    }
*/

    protected void gotoWifiSettings() {
        try {
            startActivity(new Intent(
                    Settings.ACTION_WIFI_SETTINGS));
        } catch (ActivityNotFoundException anf) {
            Toast.makeText(this, "No Wifi listings feature found on this device", Toast.LENGTH_SHORT).show();
        }
    }

/*
    static class WifiTasksHandler extends Handler {
        static final int SCAN_FOR_WIFI_RESULTS = 100;
        static final int WAIT_FOR_CONNECT_ACTION_TIMEOUT = 101;
        static final int WAIT_FOR_RECONNECT_ACTION_TIMEOUT = 102;
        private WeakReference<ReceiverActivity> mActivity;

        WifiTasksHandler(ReceiverActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final ReceiverActivity activity = mActivity.get();
            if (null == activity)
                return;
            switch (msg.what) {
                case SCAN_FOR_WIFI_RESULTS:
                    if (null != activity.mWifiManager)
                        activity.mWifiManager.startScan();
                    break;
                case WAIT_FOR_CONNECT_ACTION_TIMEOUT:
                    Log.e(TAG, "cant connect to sender's hotspot by increasing priority, try the dirty way..");
                    activity.mAreOtherNetworksDisabled = connectToOpenHotspot(activity.mWifiManager, (String) msg.obj, true);
                    Message m = obtainMessage(WAIT_FOR_RECONNECT_ACTION_TIMEOUT);
                    m.obj = msg.obj;
                    sendMessageDelayed(m, 6000);
                    break;
                case WAIT_FOR_RECONNECT_ACTION_TIMEOUT:
                    if (WifiUtils.isWifiConnectedToVuShareAccessPoint(activity) || activity.isFinishing())
                        return;
                    Log.e(TAG, "Even the dirty hack couldn't do it, prompt user to chose it fromWIFI settings..");
                    activity.disableReceiverMode();
                    Utility.showAlertDialog(activity, activity.getString(R.string.hotspot_timeout_error, msg.obj),
                            activity.getString(R.string.settings),
                            activity.getString(R.string.action_cancel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    activity.gotoWifiSettings();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    activity.finish();
                                }
                            });
                    break;
            }
        }
    }
*/

    /**
     * SHow dialog before stop receiving files
     */
/*    private void stopReceivingFiles() {
        Utility.showAlertDialog(this, getString(R.string.receive_stop_message),
                getString(R.string.share_stop_positive),
                getString(R.string.share_stop_negative),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        disableReceiverMode();
                        // TODO: Cancel all downloads
                        finish();
                    }
                }, null);
    }*/

    /**
     * Download file at this index
     * @param index
     */
    private void downloadFile(int index) {
        if (mFilesToDownload.size() > index) {
            EntityFileHost entityFileHost = mFilesToDownload.get(index);
            downloadFile(entityFileHost);
        }
    }

    /**
     * Download this file
     * @param entityFileHost
     */
    private void downloadFile(final EntityFileHost entityFileHost) {
        Log.i("Download", "downloadFile");
        String hostPath = entityFileHost.getHostPath();
        String fileName = hostPath.substring(hostPath.lastIndexOf('/') + 1, hostPath.length());
        final String savedPath = Constants.SAVE_FILE_PATH + "/" + entityFileHost.getType() + "/" + fileName;

        File folder = new File(Constants.SAVE_FILE_PATH + "/" + entityFileHost.getType());
        if (!folder.exists())
            folder.mkdir();

        Uri downloadUri = Uri.parse(Constants.DOWNLOAD_FILE_URL + entityFileHost.getHostPath().replace(" ", "+"));
        final Uri destinationUri = Uri.parse(savedPath);

        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri)
                .setPriority(DownloadRequest.Priority.HIGH)
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        Log.i("Download", "Complete: " + entityFileHost.getName());
                        // Set file's actual size and downloaded size
                        entityFileHost.setDownloadedSize(entityFileHost.getActualSize());
                        mDownloadedBytes += entityFileHost.getActualSize();
                        mCurrentIndex++;

                        // Scan Android database
                        Utility.scanFile(ReceiverActivity.this, savedPath);

                        // Update size views
                        updateFileSizeViews(mDownloadedBytes, true);

                        // Download next file
                        downloadFile(mCurrentIndex);
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes,
                                           long downloadedBytes, int progress) {
                        // Set file progress
                        entityFileHost.setDownloadedSize(downloadedBytes);
                        // Calculate total progress
                        long sent = mDownloadedBytes + downloadedBytes;

                        // Update size views
                        updateFileSizeViews(sent, false);
                    }
                });

        // Updated file
        entityFileHost.setDownloadId(Long.valueOf(downloadRequest.getDownloadId()));
        entityFileHost.setSavedPath(savedPath);

        mDownloadManager.add(downloadRequest);
    }

    /**
     * Get total size of all the files to download
     */
    private void getTotalFileSize() {
        ArrayList<EntityFileHost> filesToDownload = DownloadController.getInstance(this).getFilesToDownload();
        for (EntityFileHost entityFileHost : filesToDownload) {
            totalFileSize += entityFileHost.getActualSize();
        }
        updateFileSizeViews(0, false);
    }

    /**
     * Update file sent anf left size
     * @param sent
     */
    private void updateFileSizeViews(final long sent, boolean forceUpdate) {
        if (!forceUpdate) {
            if (Math.abs(System.currentTimeMillis() - updatedTime) < 100) return;
        }
        updatedTime = System.currentTimeMillis();
        final long progress = (long) ((sent * 100) / totalFileSize);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String sizeSent = Utility.getFileSize(sent);
                String sizeLeft = Utility.getFileSize(totalFileSize - sent);
                mSizeSent.setText(sizeSent);
                mSizeLeft.setText(sizeLeft);
                mFileProgress.setProgress((int) progress);
                mFilesAdapter.notifyDataSetChanged();

                Log.i("Download", "Send: " + sizeSent + ", Left: " + sizeLeft + ", Progress: " + progress);
            }
        });
    }

    /**
     * Show available VuShare Wifi on the radar
     * @param ssid
     */
    private void showVuShareWifi(String ssid) {
        mSendersAdapter.notifyDataSetChanged();
    }

    /**
     * Show Wifi connecting screen
     */
    private void showConnectingScreen() {
        mSenderChooseLayout.setVisibility(View.GONE);
        mFrontLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Show download screen after connection is established
     * @param ssid
     */
    private void showProgressScreen(String ssid) {
        // Return if sender info is not available
        String[] senderInfo = WifiUtils.getSenderInfoFromSSID(ssid);
        if (null == senderInfo) {
            Log.e("Download", "Sender info not available");
            return;
        }

        // Return if already started the download process
        if (ssid.equals(this.mSenderSSID))
            return;

        this.mSenderSSID = ssid;
        hideFrontLayout();
    }

    /**
     * Hide fron connection layout
     */
    private void hideFrontLayout() {
        if (downloadHandler != null && downloadRunnable != null)
            downloadHandler.postDelayed(downloadRunnable, 2000);
    }

    /**
     *
     */
    private void startDownload() {
        Animation topOutAnim = AnimationUtils.loadAnimation(ReceiverActivity.this, R.anim.top_out);
        topOutAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
//                if (mSenderChooseLayout.isShown()) mSenderChooseLayout.setVisibility(View.GONE);
//                if (mFrontLayout.isShown()) mFrontLayout.setVisibility(View.GONE);
//                String[] senderInfo = WifiUtils.getSenderInfoFromSSID(mSenderSSID);
//                if (senderInfo.length > 0) {
//                    mSenderTitle.setText(senderInfo[0] + " Sending...");
//                }
//                if (senderInfo.length > 1) {
//                    mSenderImage.setImageResource(mAvatars.getResourceId(Integer.parseInt(senderInfo[1]), 0));
//                    mSenderImage.setVisibility(View.VISIBLE);
//                }
//
//                Log.i("Download", "showProgressScreen: " + Constants.FILES_URL);
//                // Call api to fetch files to download
//                mApiController.downloadDataFromSender(Constants.FILES_URL, mCallback);

                Intent clientIntent = new Intent(ReceiverActivity.this, ClientActivity.class);
                clientIntent.putExtra(SERVER_SSID, mSenderSSID.replace("\"", ""));
                startActivityForResult(clientIntent, REQUEST_CODE_CLIENT_START);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        mConnectionImage.startAnimation(topOutAnim);
    }
}
