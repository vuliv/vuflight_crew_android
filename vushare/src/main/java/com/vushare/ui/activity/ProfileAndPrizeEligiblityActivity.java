package com.vushare.ui.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.TheAplication;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.vushare.R;
import com.vushare.controller.NetworkController;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.model.Prizes;
import com.vushare.ui.adapter.RecyclerAdapterPrizes;
import com.vushare.utility.SharedPrefsKeys;
import com.vushare.volley.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Harsh on 17-Aug-18.
 */
public class ProfileAndPrizeEligiblityActivity extends BaseActivity {

    private TextView txtCrewName,txtTotalPoints,txtCurrentTripPoints;
    private ImageView ivCrewImage,imgBackBtn;
    private RecyclerView rvCrewPrizes;
    private RecyclerAdapterPrizes recyclerAdapterPrizes;
    private SharedPreferences pref;

    public ProfileAndPrizeEligiblityActivity() {
        pref = TheAplication.getInstance().getSharedPreferences(SharedPrefsKeys.SHARED_PREFERENCES_NAME, TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_and_prize_eligiblity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        txtCrewName = (TextView) findViewById(R.id.txtCrewProfileName);
        txtCurrentTripPoints = (TextView) findViewById(R.id.txtCurrentTripPoints);
        txtTotalPoints = (TextView) findViewById(R.id.txtTotalPoints);
        ivCrewImage = (ImageView) findViewById(R.id.ivCrewProfileImage);
        imgBackBtn = (ImageView) findViewById(R.id.ivBackBtn);
        imgBackBtn.setOnClickListener(this);
        rvCrewPrizes = (RecyclerView) findViewById(R.id.rvPrizes);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCrewPrizes.setLayoutManager(linearLayoutManager);

        setCrewProfileDetails();
    }

    private void setCrewProfileDetails() {
        CrewProfiles crewProfiles = CrewProfiles.getInstance();
        CrewProfile crewProfile = crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId());
        txtCrewName.setText(crewProfile.getEmpName());
        txtTotalPoints.setText(crewProfile.getPoints()+"");
        txtCurrentTripPoints.setText("" + pref.getInt(SharedPrefsKeys.POINTS_EARNED, 0));
        RequestOptions requestOptions = new RequestOptions();
        if (crewProfiles.getCrewProfileByEmpID(crewProfiles.getLogdInCrewMemId()).getGender().equals("F")) {
            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.female))
                    .load(crewProfile.getImageUrl())
                    .into(ivCrewImage);
        }else{
            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions.placeholder(R.drawable.male))
                    .load(crewProfile.getImageUrl())
                    .into(ivCrewImage);
        }

        recyclerAdapterPrizes = new RecyclerAdapterPrizes(ProfileAndPrizeEligiblityActivity.this,crewProfile);
        rvCrewPrizes.setAdapter(recyclerAdapterPrizes);

        getPrizes();
    }

    private void getPrizes() {
        String url = "http://192.168.43.1:8080/api/getAllRedeemProduct";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ArrayList<Prizes> prizesArrayList = new ArrayList<>();
                Gson gson = new Gson();
                try {
                    for (int i=0;i<response.length();i++)
                    {
                        JSONObject jsonObject = response.getJSONObject(i);
                        prizesArrayList.add(gson.fromJson(jsonObject.toString(),Prizes.class));
                    }
                    recyclerAdapterPrizes.setList(prizesArrayList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        NetworkController.addToRequestQueue(ProfileAndPrizeEligiblityActivity.this, jsonArrayRequest, "getAllRedeemProduct");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.ivBackBtn)
        {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void setTotalPoints(int points) {
        txtTotalPoints.setText(points+"");
    }
}
