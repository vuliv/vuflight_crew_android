package com.vushare.entity;

/**
 * Created by user on 14-12-2017.
 */

public class EntityUserMac {
    String ip;
    String mac;

    public EntityUserMac(String ip, String mac) {
        this.ip = ip;
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return ip + " " + mac;
    }
}
