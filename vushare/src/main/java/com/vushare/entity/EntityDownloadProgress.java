package com.vushare.entity;

/**
 * Created by MX0002681 on 10-11-2015.
 */
public class EntityDownloadProgress {

    int progress;           //TODO: between 0-100
    String downloadSpeedWithUnit;   //TODO: kb/s, mb/s, gb/s
    float downloadSpeed;            // TODO: in kb
    long remainingTime;     //TODO: in seconds
    long fileSize;
    long downloadedSize;

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getDownloadSpeedWithUnit() {
        return downloadSpeedWithUnit;
    }

    public void setDownloadSpeedWithUnit(String downloadSpeed) {
        this.downloadSpeedWithUnit = downloadSpeed;
    }

    public long getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public float getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(float downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public long getDownloadedSize() {
        return downloadedSize;
    }

    public void setDownloadedSize(long downloadedSize) {
        this.downloadedSize = downloadedSize;
    }
}
