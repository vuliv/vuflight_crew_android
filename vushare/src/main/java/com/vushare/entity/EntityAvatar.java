package com.vushare.entity;

/**
 * Created by MB0000021 on 8/24/2017.
 */

public class EntityAvatar {

    private int drawable;

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }
}
