package com.vushare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Harsh on 17-Aug-18.
 */
public class Prizes {

    @SerializedName("itemId")
    @Expose
    private int itemId;
    @SerializedName("itenName")
    @Expose
    private String itenName;
    @SerializedName("itemDescription")
    @Expose
    private String itemDescription;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItenName() {
        return itenName;
    }

    public void setItenName(String itenName) {
        this.itenName = itenName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
