package com.vushare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CrewProfile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("flightNumber")
    @Expose
    private String flightNo;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("empName")
    @Expose
    private String empName;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("cartNumber")
    @Expose
    private String cartNumber;
    @SerializedName("empId")
    @Expose
    private int empId;
    @SerializedName("seatRange")
    @Expose
    private String seatRange;
    @SerializedName("passengerCount")
    @Expose
    private PassengerCount passengerCount;
    @SerializedName("crewDetailList")
    @Expose
    public List<CrewProfile> crewDetailList;
    @SerializedName("inventoryCount")
    @Expose
    private List<InventoryCountDetail> inventoryCount;

    public List<InventoryCountDetail> getInventoryCount() {
        return inventoryCount;
    }

    public void setInventoryCount(List<InventoryCountDetail> inventoryCount) {
        this.inventoryCount = inventoryCount;
    }


    public List<InventoryCountDetail> getPreOrderCount() {
        return preOrderCount;
    }

    public void setPreOrderCount(List<InventoryCountDetail> preOrderCount) {
        this.preOrderCount = preOrderCount;
    }

    @SerializedName("preOrderCount")
    @Expose
    private List<InventoryCountDetail> preOrderCount;


    private String crewName;
    private String cartNo;
    private CartInventory cartInventory;

    public void setCrewProfile(String crewName, int empId, String flightNo, String cartNo, String seatRange) {
        this.crewName = crewName;
        this.empId = empId;
        this.flightNo = flightNo;
        this.cartNo = cartNo;
        this.seatRange = seatRange;
    }

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getCartNo() {
        return cartNo;
    }

    public void setCartNo(String cartNo) {
        this.cartNo = cartNo;
    }

    public String getSeatRange() {
        return seatRange;
    }

    public void setSeatRange(String seatRange) {
        this.seatRange = seatRange;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public CartInventory getCartInventory() {
        return cartInventory;
    }

    public void setCartInventory(CartInventory cartInventory) {
        this.cartInventory = cartInventory;
    }

    public String getCartNumber() {
        return cartNumber;
    }

    public void setCartNumber(String cartNumber) {
        this.cartNumber = cartNumber;
    }

    public PassengerCount getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(PassengerCount passengerCount) {
        this.passengerCount = passengerCount;
    }

    public List<CrewProfile> getCrewDetailList() {
        return crewDetailList;
    }

    public void setCrewDetailList(List<CrewProfile> crewDetailList) {
        this.crewDetailList = crewDetailList;
    }
}
