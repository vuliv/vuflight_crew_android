package com.vushare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MB0000004 on 03-Sep-18.
 */

public class InventoryCountDetail {
    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("veg")
    @Expose
    private String veg;

    @SerializedName("nonVeg")
    @Expose
    private String nonVeg;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVeg() {
        return veg;
    }

    public void setVeg(String veg) {
        this.veg = veg;
    }

    public String getNonVeg() {
        return nonVeg;
    }

    public void setNonVeg(String nonVeg) {
        this.nonVeg = nonVeg;
    }
}
