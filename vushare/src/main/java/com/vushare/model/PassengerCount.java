package com.vushare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerCount {
    @SerializedName("male")
    @Expose
    private int male;
    @SerializedName("females")
    @Expose
    private int females;
    @SerializedName("children")
    @Expose
    private int children;
    @SerializedName("seniors")
    @Expose
    private int seniors;
    @SerializedName("specialCare")
    @Expose
    private int specialCare;
    @SerializedName("total")
    @Expose
    private int total;

    public Integer getMale() {
        return male;
    }

    public void setMale(Integer male) {
        this.male = male;
    }

    public Integer getFemales() {
        return females;
    }

    public void setFemales(Integer females) {
        this.females = females;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public Integer getSeniors() {
        return seniors;
    }

    public void setSeniors(Integer seniors) {
        this.seniors = seniors;
    }

    public Integer getSpecialCare() {
        return specialCare;
    }

    public void setSpecialCare(Integer specialCare) {
        this.specialCare = specialCare;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
