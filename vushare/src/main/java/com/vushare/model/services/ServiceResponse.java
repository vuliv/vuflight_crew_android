package com.vushare.model.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vushare.model.salesreport.ErrorResponse;

import java.util.List;

public class ServiceResponse extends ErrorResponse {

    @SerializedName("serviceList")
    @Expose
    private List<ServiceRequestModel> serviceList = null;

    public List<ServiceRequestModel> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ServiceRequestModel> serviceList) {
        this.serviceList = serviceList;
    }
}