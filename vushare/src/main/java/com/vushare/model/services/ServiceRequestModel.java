package com.vushare.model.services;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MB0000004 on 12-Sep-18.
 */

public class ServiceRequestModel {

    @SerializedName("serviceId")
    String serviceId;

    @SerializedName("msisdn")
    String msisdn;

    @SerializedName("serviceType")
    String serviceRequest;

    @SerializedName("hasAccepted")
    boolean hasAccepted;


    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(String serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public boolean isHasAccepted() {
        return hasAccepted;
    }

    public void setHasAccepted(boolean hasAccepted) {
        this.hasAccepted = hasAccepted;
    }


}
