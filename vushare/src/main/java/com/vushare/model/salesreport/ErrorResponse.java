package com.vushare.model.salesreport;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MB0000004 on 26-Jul-18.
 */

public class ErrorResponse {
    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }

    @SerializedName("StatusCode")
    String StatusCode;

    @SerializedName("ErrorMessage")
    String ErrorMessage;
}
