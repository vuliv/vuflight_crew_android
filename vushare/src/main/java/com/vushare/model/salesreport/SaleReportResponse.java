package com.vushare.model.salesreport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SaleReportResponse extends ErrorResponse {

    @SerializedName("saleReport")
    @Expose
    private List<SalesItemDetail> saleReport = null;

    public List<SalesItemDetail> getSaleReport() {
        return saleReport;
    }

    public void setSaleReport(List<SalesItemDetail> saleReport) {
        this.saleReport = saleReport;
    }

}