package com.vushare.model;

import android.content.SharedPreferences;

import com.TheAplication;
import com.google.gson.annotations.SerializedName;
import com.vushare.utility.SharedPrefsKeys;

import java.util.ArrayList;

/**
 * Created by Ankur on 7/5/2018.
 */

public class CartInventory {

    private static final String TAG = CartInventory.class.getSimpleName();
    public static final String ITEM_TYPE_MEAL = "MEAL";
    public static final String ITEM_TYPE_ECOM = "Ecom";

    @SerializedName("flightNumber")
    private
    String flightNumber;

    @SerializedName("cartNumber")
    private
    String cartNumber;

    @SerializedName("inventory")
    private
    ArrayList<Inventory> inventory;

    public static class Inventory {
        @SerializedName("quantity")
        int quantity;
        @SerializedName("quantitySold")
        int quantitySold;
        @SerializedName("itemId")
        int itemId;
        @SerializedName("itemName")
        String itemName;
        @SerializedName("itemDescription")
        String itemDescription;
        @SerializedName("price")
        int price;
        @SerializedName("imageUrl")
        String imageUrl;
        @SerializedName("barcode")
        String barcode;
        @SerializedName("type")
        String type;
        @SerializedName("videoUrl")
        String videoUrl;
        @SerializedName("veg")
        boolean veg;

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        int points;


        public SharedPreferences getPref() {
            return TheAplication.getInstance().getSharedPreferences(SharedPrefsKeys.SHARED_PREFERENCES_NAME, TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getQuantitySold() {
            if (quantitySold == 0) {
                quantitySold = getPref().getInt(SharedPrefsKeys.SOLD_ITEM + "_" + itemId, 0);
            }
            return quantitySold;
        }

        public void setQuantitySold(int quantitySold, CrewProfile crewProfile) {
            int earnedPoints = getPref().getInt(SharedPrefsKeys.POINTS_EARNED, 0);
            this.quantitySold = quantitySold;
            SharedPreferences.Editor editor = getPref().edit();
            editor.putInt(SharedPrefsKeys.SOLD_ITEM + "_" + itemId, quantitySold);
            // earnedPoints += points;
            earnedPoints += (points * quantitySold);            // Changed By Sumit Agrawal [Earned Points by Item ID = (Quantity of Item ID * points of a Single Item ID)]
            editor.putInt(SharedPrefsKeys.POINTS_EARNED, earnedPoints);
            crewProfile.setPoints(crewProfile.getPoints() + points);
            editor.commit();
        }

        public void updateQuantityAfterSell(int soldQuantity, CrewProfile crewProfile) {
            this.quantity -= soldQuantity;
            setQuantitySold(getQuantitySold() + soldQuantity, crewProfile);
        }

        public int getItemId() {
            return itemId;
        }

        public String getItemName() {
            return itemName;
        }

        public String getItemDescription() {
            return itemDescription;
        }

        public int getPrice() {
            return price;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public String getBarcode() {
            return barcode;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public boolean isVeg() {
            return veg;
        }

        public void setVeg(boolean veg) {
            this.veg = veg;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Inventory)) {
                return false;
            }

            if (getItemId() == ((Inventory) obj).getItemId()) {
                return true;
            }
            return false;
        }
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getCartNumber() {
        return cartNumber;
    }

    public ArrayList<Inventory> getInventory() {
        return inventory;
    }
}
