package com.vushare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vuliv.network.entity.EntityDominosRequest;

import java.util.List;

public class PendingOrders {
    @SerializedName("pendingOrder")
    @Expose
    private List<EntityDominosRequest> entityDominosRequestList = null;

    public List<EntityDominosRequest> getEntityDominosRequestList() {
        return entityDominosRequestList;
    }

    public void setEntityDominosRequestList(List<EntityDominosRequest> entityDominosRequestList) {
        this.entityDominosRequestList = entityDominosRequestList;
    }
}
