package com.vushare.model;

import android.app.Activity;
import android.content.SharedPreferences;

import com.TheAplication;
import com.vucast.utility.DominosAPI;
import com.vushare.utility.SharedPrefsKeys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CrewProfiles {

    private static CrewProfiles crewProfiles;
    public Map<Integer, CrewProfile> crewProfileMap = new HashMap<>();
    public List<CrewProfile> otherCrewProfileList = new ArrayList<>();
    public int logdInCrewMemId;
    public String seatRange;
    public List<CrewProfile> crewProfileList;
    public String cartNo, loginMemName, profileImgUri;
    private SharedPreferences pref;
    private String flightNo;

    private CrewProfiles() {
        pref = TheAplication.getInstance().getSharedPreferences(SharedPrefsKeys.SHARED_PREFERENCES_NAME, TheAplication.getInstance().getApplicationContext().MODE_PRIVATE);
    }

    public static CrewProfiles getInstance() {

        if (crewProfiles == null) {
            synchronized (CrewProfiles.class) {
                if (crewProfiles == null) {
                    crewProfiles = new CrewProfiles();
                }
            }
        }
        return crewProfiles;
    }

    public CrewProfile getCrewProfileByEmpID(int empId) {
        return crewProfileMap.get(empId);
    }

    public void addCrewProfile(CrewProfile crewProfile) {
        crewProfileMap.put(crewProfile.getEmpId(), crewProfile);
    }

    public CrewProfile getCrewProfileMemById(int logdCrewMemInId) {
        return crewProfileMap.get(logdInCrewMemId);
    }

    public void setLogdInCrewMemId(int logdInCrewMemId) {
        this.logdInCrewMemId = logdInCrewMemId;
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(SharedPrefsKeys.LOGIN_CREW_ID, this.logdInCrewMemId);
        editor.commit();
    }

    public void setPaMode(boolean pamode) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(SharedPrefsKeys.IS_PA_MODE_ENABLE, pamode);
        editor.commit();
    }

    public boolean getPaMode() {

        return pref.getBoolean(SharedPrefsKeys.IS_PA_MODE_ENABLE, false);
    }

    public void setCrewRequestsCount(int requestsCount) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(SharedPrefsKeys.REQUEST_COUNT, requestsCount);
        editor.commit();
    }

    public int getCrewRequestsCount() {

        return pref.getInt(SharedPrefsKeys.REQUEST_COUNT, 0);
    }

    public int getLogdInCrewMemId() {
        if (logdInCrewMemId == 0) {
            logdInCrewMemId = pref.getInt(SharedPrefsKeys.LOGIN_CREW_ID, 0);
        }
        return logdInCrewMemId;
    }

    public List<CrewProfile> getCrewProfileList() {
        return crewProfileList;
    }

    public void setCrewProfileList(List<CrewProfile> crewProfileList) {
        this.crewProfileList = crewProfileList;
    }

    public String getLogdInCartNo() {
        if (cartNo == null) {
            cartNo = pref.getString(SharedPrefsKeys.LOGIN_CREW_CART_NO, null);
        }
        return cartNo;
    }

    public void setLogdInMemCartNo(String cartNo) {
        this.cartNo = cartNo;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPrefsKeys.LOGIN_CREW_CART_NO, this.cartNo);
        editor.commit();
    }

    public String getLoginMemName() {
        if (loginMemName == null) {
            loginMemName = pref.getString(SharedPrefsKeys.LOGIN_CREW_NAME, null);
        }
        return loginMemName;
    }

    public void setLoginMemName(String loginMemName) {
        this.loginMemName = loginMemName;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPrefsKeys.LOGIN_CREW_NAME, this.loginMemName);
        editor.commit();
    }


    public void setLoginCrewTotalPoints(int totalPoints) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(SharedPrefsKeys.POINTS_TOTAL, totalPoints);
        editor.commit();

    }

    public int getLoginCrewTotalPoints(Activity context) {
        return pref.getInt(SharedPrefsKeys.POINTS_TOTAL, 0);
    }


    public void setLoginCrewSeatRange(String seatRange) {
        this.seatRange = seatRange;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPrefsKeys.LOGIN_CREW_SEAT_RANGE, this.seatRange);
        editor.commit();

    }

    public String getLoginCrewSeatRange(Activity context) {
        if (seatRange == null) {
            seatRange = pref.getString(SharedPrefsKeys.LOGIN_CREW_SEAT_RANGE, null);
        }
        return seatRange;
    }

    public String getSelectedProfileImgUri(Activity context) {
        if (profileImgUri == null) {
            profileImgUri = pref.getString(SharedPrefsKeys.PROFILE_IMAGE_URI, null);
        }
        return profileImgUri;
    }

    public void updateSelectedProfileImgUri(Activity context, String profileImgUri) {
        this.profileImgUri = profileImgUri;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPrefsKeys.PROFILE_IMAGE_URI, this.profileImgUri);
        editor.commit();
    }

    public void clearPreferance(Activity context) {
        loginMemName = null;
        cartNo = null;
        logdInCrewMemId = 0;
        flightNo = null;
        otherCrewProfileList.clear();
        crewProfileMap.clear();
        crewProfileList.clear();
        DominosAPI.TempPrefrenceForChatAndServices.getInstance().clearPreferance();
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public void setFlightMemName(String flightNo) {
        this.flightNo = flightNo;
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPrefsKeys.LOGIN_CREW_FLIGHT_NO, this.flightNo);
        editor.commit();
    }

    public String getFlightNo() {
        if (flightNo == null) {
            flightNo = pref.getString(SharedPrefsKeys.LOGIN_CREW_FLIGHT_NO, null);
        }
        return flightNo;
    }

    public List<CrewProfile> getOtherCrewProfileList() {
        return otherCrewProfileList;
    }

    public void addOtherCrewProfileList(CrewProfile crewProfile) {
        otherCrewProfileList.add(crewProfile);
    }


}
