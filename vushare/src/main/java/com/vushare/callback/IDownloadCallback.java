package com.vushare.callback;

import com.vushare.entity.EntityDownloadProgress;

public interface IDownloadCallback {
	void onPreExecute();
	void onSuccess(String path, Object object);
	void onFailure(Object object);
	void showProgress(EntityDownloadProgress progress);
	void cancelled(Object object);
	void alreadyProgress();
}
