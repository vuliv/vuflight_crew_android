package com.vushare.callback;

/**
 * Created by Nitesh Khatri on 1/5/2017.
 */

public interface ICallback<T, U> {
    void onSuccess(T object);
    void onFailure(U msg);
}
