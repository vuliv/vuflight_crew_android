package com.vushare.service;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.j256.ormlite.android.apptools.OrmLiteBaseService;
import com.vucast.constants.DataConstants;
import com.vucast.entity.EntityMediaDetail;
import com.vucast.service.DataService;
import com.vuliv.network.service.SharedPrefController;
import com.vucast.service.WebServerService;
import com.vuliv.network.database.DatabaseHandler;
import com.vushare.R;
import com.vushare.controller.HotspotControl;
import com.vushare.controller.MediaController;
import com.vushare.server.ShareService;
import com.vushare.ui.activity.ShareActivity;
import com.vushare.utility.Constants;
import com.vushare.utility.Utility;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.TreeMap;

import static com.vushare.service.BaseService.ShareUIHandler.LIST_API_CLIENTS;
import static com.vushare.service.BaseService.ShareUIHandler.UPDATE_AP_STATUS;

/**
 * Created by User on 10/16/2017.
 */

public abstract class BaseService extends OrmLiteBaseService<DatabaseHandler> {

    private ShareUIHandler mUiUpdateHandler;
    private BroadcastReceiver mServerUpdateListener;
    private Intent intent;

    private HotspotControl mHotspotControl;
    private boolean isApEnabled = false;

    public abstract void hotspotStarted();

    public abstract void hotspotStarting();

    public abstract void hotspotStopped();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.intent = intent;
        init();
        mHotspotControl.disable();
        registerReceiver(mServerUpdateListener,
                new IntentFilter(ShareService.ShareIntents.SHARE_SERVER_UPDATES_INTENT_ACTION));
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mServerUpdateListener)
            unregisterReceiver(mServerUpdateListener);
        if (null != mUiUpdateHandler)
            mUiUpdateHandler.removeCallbacksAndMessages(null);
        mUiUpdateHandler = null;
        stopFileSharing();
    }

    public void startFileSharing() {
        TedPermission mTedPermission;
        if (!com.vushare.utility.Utility.isPermissionGranted(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            /*mTedPermission = new TedPermission(this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage(getString(R.string.permission_denied_message))
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE);
            mTedPermission.check();
*/
            TedPermission.with(this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage(getString(R.string.permission_denied_message))
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .check();

        } else {
            permissionlistener.onPermissionGranted();
        }
    }

    private void init() {
        mHotspotControl = HotspotControl.getInstance(getApplicationContext());

        mServerUpdateListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (null == intent)
                    return;
                int intentType = intent.getIntExtra(ShareService.ShareIntents.TYPE, 0);
                if (intentType == ShareService.ShareIntents.Types.FILE_TRANSFER_STATUS) {
                    String fileName = intent.getStringExtra(ShareService.ShareIntents.SHARE_SERVER_UPDATE_FILE_NAME);
                } else if (intentType == ShareService.ShareIntents.Types.AP_DISABLED_ACKNOWLEDGEMENT) {
                    resetSenderUi(false);
                }
            }
        };
    }

    private void startHotspot() {
        mHotspotControl.disable();
        startP2pSenderWatchService();
        refreshApData();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Toast.makeText(this, "Please turn ON Hotspot manually", Toast.LENGTH_SHORT).show();
            Utility.showAlertDialog(this, getString(R.string.nougat_disclaimer), "Set", null, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
                    intent.setComponent(cn);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity( intent);
                }
            }, null);
            String hotspotName = SharedPrefController.getUserName(this) +
                    Constants.VUSHARE_HOTSPOT_SEPARATOR +
                    SharedPrefController.getUserAvatar(this) +
                    Constants.VUSHARE_HOTSPOT_SEPARATOR +
                    Constants.VUSHARE_HOTSPOT_SUFFIX;
            mHotspotControl.setHotspotName(hotspotName);
        }
    }

    private void disableAp() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_STOP);
        startService(p2pServiceIntent);
        isApEnabled = false;
    }

    private void startP2pSenderWatchService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        if (null != intent) {
            p2pServiceIntent.putExtra(ShareService.EXTRA_SENDER_NAME, intent.getStringExtra(ShareService.EXTRA_SENDER_NAME));
        }
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_START);
        startService(p2pServiceIntent);
    }

    /**
     * Starts {@link ShareService} with intent action {@link ShareService#WIFI_AP_ACTION_START_CHECK} to make {@link ShareService} constantly check for Hotspot status. (Sometimes Hotspot tend to stop if stayed idle for long enough. So this check makes sure {@link ShareService} is only alive if Hostspot is enaled.)
     */
    private void startHostspotCheckOnService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareService.class);
        p2pServiceIntent.setAction(ShareService.WIFI_AP_ACTION_START_CHECK);
        startService(p2pServiceIntent);
    }

    /**
     * Calls methods - {@link ShareActivity#updateApStatus()} & {@link ShareActivity#listApClients()} which are responsible for displaying Hotpot information and Listing connected clients to the same
     */
    private void refreshApData() {
        if (null == mUiUpdateHandler)
            mUiUpdateHandler = new BaseService.ShareUIHandler(this);
        updateApStatus();
        listApClients();
    }

    /**
     * Updates Hotspot configuration info like Name, IP if enabled.<br> Posts a message to {@link ShareActivity.ShareUIHandler} to call itself every 1500ms
     */
    private void updateApStatus() {
        if (!HotspotControl.isSupported()) {

        }
        if (mHotspotControl.isEnabled()) {
            if (!isApEnabled) {
                isApEnabled = true;
                startHostspotCheckOnService();
            }
            DataConstants.IP_ADDRESS = com.vucast.utility.Utility.getIp(this);
            WebServerService.getInstance().startServer(this, null);
            hotspotStarted();
        } else if (null != mUiUpdateHandler) {
            mUiUpdateHandler.removeMessages(UPDATE_AP_STATUS);
            mUiUpdateHandler.sendEmptyMessageDelayed(UPDATE_AP_STATUS, 1500);
        }
    }

    /**
     * Calls {@link HotspotControl#getConnectedWifiClients(int, HotspotControl.WifiClientConnectionListener)} to get Clients connected to Hotspot.<br>
     * Constantly adds/updates receiver items
     * <br> Posts a message to {@link ShareActivity.ShareUIHandler} to call itself every 1000ms
     */
    private synchronized void listApClients() {
        if (mHotspotControl == null) {
            return;
        }
        mHotspotControl.getConnectedWifiClients(2000,
                new HotspotControl.WifiClientConnectionListener() {
                    public void onClientConnectionAlive(final HotspotControl.WifiScanResult wifiScanResult) {
                    }

                    @Override
                    public void onClientConnectionDead(final HotspotControl.WifiScanResult c) {
                    }

                    public void onWifiClientsScanComplete() {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
                        if (null != mUiUpdateHandler) {
                            mUiUpdateHandler.removeMessages(LIST_API_CLIENTS);
                            mUiUpdateHandler.sendEmptyMessageDelayed(LIST_API_CLIENTS, 1000);
                        }
//                            }
//                        });
                    }
                }

        );
    }

    private void resetSenderUi(boolean disableAP) {
        if (mUiUpdateHandler != null) {
            mUiUpdateHandler.removeCallbacksAndMessages(null);
        }
        if (disableAP) {
            disableAp();
        }
    }

    static class ShareUIHandler extends Handler {
        WeakReference<BaseService> mService;

        static final int LIST_API_CLIENTS = 100;
        static final int UPDATE_AP_STATUS = 101;

        ShareUIHandler(BaseService service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            BaseService service = mService.get();
            if (null == service || msg == null)
                return;
            if (msg.what == LIST_API_CLIENTS) {
                service.listApClients();
            } else if (msg.what == UPDATE_AP_STATUS) {
                service.updateApStatus();
            }
        }
    }

    public void stopFileSharing() {
        resetSenderUi(true);
        WebServerService.getInstance().stopServer();
    }

    /**
     * Permission callbacks
     */
    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            TreeMap<String, ArrayList<EntityMediaDetail>> videosFolderMap =
                    MediaController.getInstance(BaseService.this).getVideosFolderMap();
            DataService.getInstance().setFolderContent(videosFolderMap);
            hotspotStarting();
            startHotspot();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        }
    };
}
