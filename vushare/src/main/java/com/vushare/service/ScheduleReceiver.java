package com.vushare.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.vushare.ui.activity.LauncherActivity;

/**
 * Created by user on 08-10-2017.
 */

public class ScheduleReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmService.scheduleAlarm(context);
        Intent launchIntent = new Intent(context, LauncherActivity.class);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(launchIntent);
        Log.i("RestartReceiver", "launch");
    }
}
