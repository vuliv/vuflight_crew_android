package com.vushare.service;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.IBinder;
import android.text.Html;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vucast.constants.DataConstants;
import com.vuliv.network.service.SharedPrefController;
import com.vuliv.network.server.DataController;
import com.vuliv.network.server.TrackingController;
import com.vushare.R;
import com.vushare.ui.activity.InformationActivity;
import com.vushare.utility.Constants;

/**
 * Created by Idoideas on 17/09/2017.
 */

public class OverlayService extends BaseService implements View.OnClickListener {

    WindowManager windowManager;
    View view;
    private View mFloatCrossLayout;            // Cross layout
    private LinearLayout mRootLayout;
    private RelativeLayout llBottom;
    private TextView mStartButton, mStopButton;
    private LinearLayout mLoadingDots;

    private Intent intent;
    private WindowManager.LayoutParams rootLayoutParams;
    private WindowManager.LayoutParams mFloatCrossLayoutParams;
    private GestureDetector gestureDetector;
    private LinearLayout llCrossLayout;
    private LinearLayout llRootLayout;
    private ImageView ivCross;
    private ImageView btnFloat;
    private TextView tvDuration;
    private TextView tvSuggest;
    private ImageButton mSettings, mInvite;
    private boolean layoutIntersect;
    private boolean startTimer;
    private long timerDuration;
    private boolean timerInitialized;

    @Override
    public void onCreate() {
        super.onCreate();
        initComponents();

        AlarmService.scheduleAlarm(this);
        TrackingController.getInstance().sendTracking(this);
        DataController.getInstance().insertData(this);
    }

    @Override
    public void hotspotStarted() {
        mStartButton.setVisibility(View.GONE);
        mLoadingDots.setVisibility(View.GONE);
        mStopButton.setVisibility(View.VISIBLE);
        mInvite.setVisibility(View.VISIBLE);
        mSettings.setVisibility(View.VISIBLE);

        startTimer();

        Toast.makeText(this, getString(R.string.ask_to_join, SharedPrefController.getUserName(this)),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hotspotStarting() {
        mStartButton.setVisibility(View.GONE);
        mLoadingDots.setVisibility(View.VISIBLE);
    }

    @Override
    public void hotspotStopped() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        this.intent = intent;
        init();
        setListener();
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        removeView();
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btnFloat) {
            if (llBottom.isShown()) {
                llBottom.setVisibility(View.GONE);
            } else {
                llBottom.setVisibility(View.VISIBLE);
            }
        } else if (i == R.id.button_start) {
            TrackingController.getInstance().startStop(this, true);
            startFileSharing();
            if (llBottom.isShown()) {
                llBottom.setVisibility(View.GONE);
            }
        } else if (i == R.id.button_stop) {
            TrackingController.getInstance().startStop(this, false);
            stopFileSharing();
            mStartButton.setVisibility(View.VISIBLE);
            mStopButton.setVisibility(View.GONE);
            mInvite.setVisibility(View.GONE);
            mSettings.setVisibility(View.GONE);
            tvSuggest.setVisibility(View.GONE);
            stopTimer();
        } else if (i == R.id.overlay_settings) {
            Intent intent = new Intent(this, InformationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (i == R.id.overlay_invite) {
            if (tvSuggest.isShown()) {
                tvSuggest.setVisibility(View.GONE);
            } else {
                tvSuggest.setVisibility(View.VISIBLE);
                String hotspotName = SharedPrefController.getUserName(this) + "-" +
                        SharedPrefController.getUserAvatar(this) + "-" +
                        Constants.VUSHARE_HOTSPOT_SUFFIX;
                tvSuggest.setText(Html.fromHtml(String.format(getResources().getString(R.string.ask_friend),
                        hotspotName,
                        "http://" + DataConstants.IP_ADDRESS + ":" + com.vucast.constants.Constants.SERVER_PORT + "/invite.html")));
            }
        }
    }

    private void initComponents() {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = View.inflate(getApplicationContext(), R.layout.service_overlay, null);
        mFloatCrossLayout = layoutInflater.inflate(R.layout.floating_cross_layout, null);
        gestureDetector = new GestureDetector(getApplicationContext(), new GestureListener());

        mFloatCrossLayoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);
        mFloatCrossLayoutParams.gravity = Gravity.BOTTOM | Gravity.CENTER;

        rootLayoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        rootLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
        rootLayoutParams.y = 0;
        rootLayoutParams.x = 0;
        windowManager.addView(mFloatCrossLayout, mFloatCrossLayoutParams);
        windowManager.addView(view, rootLayoutParams);
    }

    public void removeView() {
        if (view != null && windowManager != null) {
            windowManager.removeView(view);
        }
        if (mFloatCrossLayout != null) {
            windowManager.removeView(mFloatCrossLayout);
        }
    }

    public void init() {
        mRootLayout = (LinearLayout) view.findViewById(R.id.overlay_root_layout);
        llBottom = (RelativeLayout) view.findViewById(R.id.llBottom);
        mStartButton = (TextView) view.findViewById(R.id.button_start);
        mStopButton = (TextView) view.findViewById(R.id.button_stop);
        btnFloat = (ImageView) view.findViewById(R.id.btnFloat);
        tvDuration = (TextView) view.findViewById(R.id.tvDuration);
        tvSuggest = (TextView) view.findViewById(R.id.tvSuggest);
        mSettings = (ImageButton) view.findViewById(R.id.overlay_settings);
        mInvite = (ImageButton) view.findViewById(R.id.overlay_invite);

        mLoadingDots = (LinearLayout) view.findViewById(R.id.overlay_loading_dots);
        llCrossLayout = (LinearLayout) mFloatCrossLayout.findViewById(R.id.llCrossLayout);
        llRootLayout = (LinearLayout) mFloatCrossLayout.findViewById(R.id.llRootLayout);
        ivCross = (ImageView) mFloatCrossLayout.findViewById(R.id.ivCross);
    }

    private void setListener() {
        btnFloat.setOnClickListener(this);
        mStartButton.setOnClickListener(this);
        mStopButton.setOnClickListener(this);
        mSettings.setOnClickListener(this);
        mInvite.setOnClickListener(this);
        btnFloat.setOnTouchListener(onTouchListener);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (llBottom.isShown()) {
                llBottom.setVisibility(View.GONE);
            } else {
                llBottom.setVisibility(View.VISIBLE);
            }
            return super.onSingleTapConfirmed(e);
        }
    }

    private boolean isViewOverlapping(View secondView, int x, int y) {

        final int[] location = new int[2];

        ivCross.getLocationInWindow(location);
        Rect rect1 = new Rect(location[0], location[1], location[0] + ivCross.getWidth(), location[1] + ivCross.getHeight());

        secondView.getLocationInWindow(location);
        Rect rect2 = new Rect(x, y, x + secondView.getWidth(), y + secondView.getHeight());

        if (rect2.intersect(rect1)) {
            layoutIntersect = true;
            return true;
        } else {
            layoutIntersect = false;
            return false;
        }
    }

    /**
     * View on touch listener
     */
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        private int initialX;
        private int initialY;
        private float initialTouchX;
        private float initialTouchY;

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            //GESTURES LISTENER
            gestureDetector.onTouchEvent(event);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = rootLayoutParams.x;
                    initialY = rootLayoutParams.y;
                    initialTouchX = event.getRawX();
                    initialTouchY = event.getRawY();
                    return true;
                case MotionEvent.ACTION_UP:
                    llRootLayout.setVisibility(View.GONE);
                    if (layoutIntersect) {
                        stopSelf();
                    }
                    return true;
                case MotionEvent.ACTION_CANCEL:
                    llRootLayout.setVisibility(View.GONE);
                case MotionEvent.ACTION_MOVE:
                    llRootLayout.setVisibility(View.VISIBLE);
                    rootLayoutParams.x = initialX + (int) (event.getRawX() - initialTouchX);
                    rootLayoutParams.y = initialY + (int) (event.getRawY() - initialTouchY);
                    isViewOverlapping(btnFloat, rootLayoutParams.x, rootLayoutParams.y);
                    windowManager.updateViewLayout(view, rootLayoutParams);
                    return true;
            }
            return false;
        }
    };

    private void initializeTimer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (startTimer) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    DataConstants.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timerDuration = timerDuration + 1000;
                            tvDuration.setText(getDurationString());
                        }
                    });
                }
            }
        }).start();
        timerInitialized = true;
    }

    private void startTimer(){
        startTimer = true;
            initializeTimer();
    }

    private String getDurationString() {
        int sec = (int) (timerDuration / 1000);
        int second = sec % 60;
        int minute = sec / 60;
        if (minute >= 60) {
            int hour = minute / 60;
            minute %= 60;
            return hour + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
        }
        return minute + ":" + (second < 10 ? "0" + second : second);
    }

    private void stopTimer() {
        startTimer = false;
    }
}