package com.vushare.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.vuliv.network.service.DownloadService;
import com.vushare.server.ShareService;

import java.util.Calendar;

/**
 * Created by user on 24-10-2017.
 */

public class AlarmService {

    private static final String TAG = AlarmService.class.getCanonicalName();

    private static int UNIQUE_ID_SYNC_CONTENT_MORNING = 1;
    private static int UNIQUE_ID_SYNC_CONTENT_EVENING = 2;

    private static int UNIQUE_ID_HOTSPOT_ON_MORNING = 3;
    private static int UNIQUE_ID_HOTSPOT_ON_EVENING = 4;

    private static int UNIQUE_ID_HOTSPOT_OFF_MORNING = 5;
    private static int UNIQUE_ID_HOTSPOT_OFF_EVENING = 6;

    private static int UNIQUE_ID_TEMP_SYNC_CONTENT_1 = 7;
    private static int UNIQUE_ID_TEMP_SYNC_CONTENT_2 = 8;
    private static int UNIQUE_ID_TEMP_SYNC_CONTENT_3 = 9;
    private static int UNIQUE_ID_TEMP_SYNC_CONTENT_4 = 10;
    private static int UNIQUE_ID_TEMP_SYNC_CONTENT_5 = 11;
    private static int UNIQUE_ID_TEMP_SYNC_CONTENT_6 = 12;

    /**
     * TIME Constants
     */
    private static int TIME_SYNC_CONTENT_MORNING = 4; // 4AM
    private static int TIME_SYNC_CONTENT_EVENING = 15;// 3PM

    public static int TIME_HOTSPOT_ON_MORNING = 6;  // 6AM
    public static int TIME_HOTSPOT_ON_EVENING = 17; // 5PM

    public static int TIME_HOTSPOT_OFF_MORNING = 11;// 11AM
    public static int TIME_HOTSPOT_OFF_EVENING = 21;// 9PM

    public static int TIME_HOTSPOT_ON_MORNING_EXACT = 600;  // 6AM
    public static int TIME_HOTSPOT_ON_EVENING_EXACT = 1700; // 5PM

    public static int TIME_HOTSPOT_OFF_MORNING_EXACT = 1100;// 11AM
    public static int TIME_HOTSPOT_OFF_EVENING_EXACT = 2100;// 9PM

    public static void scheduleAlarm(Context context) {
        setSyncContentAlarm(context);
        setHotspotOnAlarm(context);
        setHotspotOffAlarm(context);
        setTempSyncContentAlarm(context);
    }

    public static void setSyncContentAlarm (Context context) {
        Intent intent = new Intent(context, DownloadService.class);

        setHotspotAlarm(context, TIME_SYNC_CONTENT_MORNING, UNIQUE_ID_SYNC_CONTENT_MORNING, intent, "SYNC_CONTENT_MORNING", 0);
        setHotspotAlarm(context, TIME_SYNC_CONTENT_EVENING, UNIQUE_ID_SYNC_CONTENT_EVENING, intent, "SYNC_CONTENT_EVENING", 0);
    }

    public static void setTempSyncContentAlarm (Context context) {
        Intent intent = new Intent(context, DownloadService.class);

        setHotspotAlarm(context, TIME_SYNC_CONTENT_MORNING,           UNIQUE_ID_TEMP_SYNC_CONTENT_1, intent, "TEMP_SYNC_CONTENT_1", 30);
        setHotspotAlarm(context, TIME_SYNC_CONTENT_MORNING + 1, UNIQUE_ID_TEMP_SYNC_CONTENT_2, intent, "TEMP_SYNC_CONTENT_2", 0);
        setHotspotAlarm(context, TIME_SYNC_CONTENT_MORNING + 1, UNIQUE_ID_TEMP_SYNC_CONTENT_3, intent, "TEMP_SYNC_CONTENT_3", 30);
        setHotspotAlarm(context, TIME_SYNC_CONTENT_EVENING,           UNIQUE_ID_TEMP_SYNC_CONTENT_4, intent, "TEMP_SYNC_CONTENT_4", 30);
        setHotspotAlarm(context, TIME_SYNC_CONTENT_EVENING + 1, UNIQUE_ID_TEMP_SYNC_CONTENT_5, intent, "TEMP_SYNC_CONTENT_5", 0);
        setHotspotAlarm(context, TIME_SYNC_CONTENT_EVENING + 1, UNIQUE_ID_TEMP_SYNC_CONTENT_6, intent, "TEMP_SYNC_CONTENT_6", 30);
    }

    public static void setHotspotOnAlarm (Context context) {
        Intent intent = new Intent(context, ShareService.class);
        intent.setAction(ShareService.WIFI_AP_ACTION_START);

        setHotspotAlarm(context, TIME_HOTSPOT_ON_MORNING, UNIQUE_ID_HOTSPOT_ON_MORNING, intent, "HOTSPOT_ON_MORNING", 0);
        setHotspotAlarm(context, TIME_HOTSPOT_ON_EVENING, UNIQUE_ID_HOTSPOT_ON_EVENING, intent, "HOTSPOT_ON_EVENING", 0);
    }

    public static void setHotspotOffAlarm (Context context) {
        Intent intent = new Intent(context, ShareService.class);
        intent.setAction(ShareService.WIFI_AP_ACTION_STOP);

        setHotspotAlarm(context, TIME_HOTSPOT_OFF_MORNING, UNIQUE_ID_HOTSPOT_OFF_MORNING, intent, "HOTSPOT_OFF_MORNING", 0);
        setHotspotAlarm(context, TIME_HOTSPOT_OFF_EVENING, UNIQUE_ID_HOTSPOT_OFF_EVENING, intent, "HOTSPOT_OFF_EVENING", 0);
    }

    private static void setHotspotAlarm (Context context, int hour, int requestCode, Intent intent, String tag, int minute) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar morningCalendar = Calendar.getInstance();
        morningCalendar.set(Calendar.HOUR_OF_DAY, hour);
        morningCalendar.set(Calendar.MINUTE, minute);
        morningCalendar.set(Calendar.SECOND, 0);

        PendingIntent oldPI = PendingIntent.getService(context.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_NO_CREATE);

        if (System.currentTimeMillis() < morningCalendar.getTimeInMillis()) {
            if (oldPI == null) {
                Log.wtf(TAG, tag + " : Set alarm for today :" + morningCalendar.getTimeInMillis());
//                oldPI.cancel();
                PendingIntent pendingIntent = PendingIntent.getService(context.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, morningCalendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            } else {
                Log.wtf(TAG, tag + " : Alarm Already Set");
            }
        } else if (oldPI == null) {
            morningCalendar.add(Calendar.DAY_OF_YEAR, 1);
            Log.wtf(TAG, tag + " : Set alarm for tomorrow :" + morningCalendar.getTimeInMillis());
            PendingIntent pendingIntent = PendingIntent.getService(context.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, morningCalendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        } else {
            Log.wtf(TAG, tag + " : No need to set alarm already done");
        }
    }
}
