/*
 * Copyright 2017 Srihari Yachamaneni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vushare.server;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteBaseService;
import com.vucast.service.WebServerService;
import com.vuliv.network.database.DatabaseHandler;
import com.vuliv.network.server.TrackingController;
import com.vushare.R;
import com.vushare.controller.FileSelectController;
import com.vushare.controller.HotspotControl;
import com.vuliv.network.service.SharedPrefController;
import com.vushare.ui.activity.ShareActivity;
import com.vushare.utility.Constants;

import java.lang.ref.WeakReference;
import java.net.BindException;

import static com.vushare.server.ShareService.ShareIntents.SHARE_CLIENT_IP;
import static com.vushare.server.ShareService.ShareIntents.SHARE_SERVER_UPDATES_INTENT_ACTION;
import static com.vushare.server.ShareService.ShareIntents.SHARE_SERVER_UPDATE_FILE_NAME;
import static com.vushare.server.ShareService.ShareIntents.SHARE_SERVER_UPDATE_TEXT;
import static com.vushare.server.ShareService.ShareIntents.SHARE_TRANSFER_PROGRESS;
import static com.vushare.server.ShareService.ShareIntents.TYPE;
import static com.vushare.server.ShareService.ShareIntents.Types.AP_DISABLED_ACKNOWLEDGEMENT;
import static com.vushare.server.ShareService.ShareIntents.Types.FILE_TRANSFER_STATUS;

/**
 * Manages Hotspot configuration using an instance of {@link HotspotControl} and also deals lifecycle of {@link HttpServer}
 * Created by Sri on 18/12/16.
 */
public class ShareService extends OrmLiteBaseService<DatabaseHandler> {

    private static final String TAG = "ShareService";

    private WifiManager mWifiManager;
    private HotspotControl mHotspotControl;
    // private ShareServer mFileServer;
    private BroadcastReceiver mNotificationStopActionReceiver;
    private HotspotChecker mHotspotCheckHandler;

    private static final int SHARE_SERVICE_NOTIFICATION_ID = 100001;
    public static final String WIFI_AP_ACTION_START = "wifi_ap_start";
    public static final String WIFI_AP_ACTION_STOP = "wifi_ap_stop";
    public static final String WIFI_AP_ACTION_START_CHECK = "wifi_ap_check";

    public static final String EXTRA_SENDER_NAME = "sender_name";

    private static final int AP_ALIVE_CHECK = 100;
    private static final int AP_START_CHECK = 101;
//    Handler fetchConnections = new Handler();
//    Runnable fetchConnectionsRunnable = new Runnable() {
//        @Override
//        public void run() {
//            listApClients();
//        }
//    };

    public static class ShareIntents {
        public static final String TYPE = "type";
        public static final String SHARE_SERVER_UPDATES_INTENT_ACTION = "share_server_updates_intent_action";
        public static final String SHARE_SERVER_UPDATE_TEXT = "share_server_update_text";
        public static final String SHARE_SERVER_UPDATE_FILE_NAME = "share_server_file_name";
        public static final String SHARE_CLIENT_IP = "share_client_ip";
        public static final String SHARE_TRANSFER_PROGRESS = "share_transfer_progress";

        public class Types {
            public static final int FILE_TRANSFER_STATUS = 1000;
            public static final int AP_ENABLED_ACKNOWLEDGEMENT = 1001;
            public static final int AP_DISABLED_ACKNOWLEDGEMENT = 1002;
        }
    }

    public ShareService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        TrackingController.getInstance().startStop(this, true);
        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mHotspotControl = HotspotControl.getInstance(getApplicationContext());
       /* mNotificationStopActionReceiver = new BroadcastReceiver() {
        mNotificationStopActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (null != intent && WIFI_AP_ACTION_STOP.equals(intent.getAction()))
                    disableHotspotAndStop();
            }
        };
        };*/

        registerReceiver(mNotificationStopActionReceiver, new IntentFilter(WIFI_AP_ACTION_STOP));
        //Start a foreground with message saying 'Initiating Hotspot'. Message is later updated using SHARE_SERVICE_NOTIFICATION_ID
        startForeground(SHARE_SERVICE_NOTIFICATION_ID, getVuShareNotification(getString(R.string.hotspot_init), false));
       // mHotspotCheckHandler = new HotspotChecker(this);
        WebServerService.getInstance().startServer(this, null);
//        fetchConnections.postDelayed(fetchConnectionsRunnable, 2000);
    }

    protected NotificationCompat.Action getStopAction() {
        Intent intent = new Intent(WIFI_AP_ACTION_STOP);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_close_clear_cancel, "Stop", pendingIntent).build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null != intent) {
            String action = intent.getAction();
            switch (action) {
                case WIFI_AP_ACTION_START:
                   /* if (!mHotspotControl.isEnabled()) {
                        startFileHostingServer(Constants.SERVER_PORT);
                    }*/
//                    sendAcknowledgementBroadcast(AP_ENABLED_ACKNOWLEDGEMENT);
                    break;
                case WIFI_AP_ACTION_STOP:
                    disableHotspotAndStop();
                    break;
                case WIFI_AP_ACTION_START_CHECK:
                    if (null != mHotspotControl && mHotspotControl.isEnabled()) {
                        //starts a handler in loop to check Hotspot check. Service kills itself when Hotspot is no more alive
                        if (null == mHotspotCheckHandler)
                            mHotspotCheckHandler = new HotspotChecker(this);
                        else mHotspotCheckHandler.removeMessages(AP_ALIVE_CHECK);
                        mHotspotCheckHandler.sendEmptyMessageDelayed(100, 3000);
                    }
                    break;
            }
        }
        return START_NOT_STICKY;
    }

    private void disableHotspotAndStop() {
        Log.d(TAG, "p2p service stop action received..");
        TrackingController.getInstance().startStop(this, false);
        if (null != mHotspotControl)
            mHotspotControl.disable();
        mWifiManager.setWifiEnabled(true);
        if (null != mHotspotCheckHandler)
            mHotspotCheckHandler.removeCallbacksAndMessages(null);
      //  stopFileTasks();
        stopForeground(true);
        sendAcknowledgementBroadcast(AP_DISABLED_ACKNOWLEDGEMENT);
        stopSelf();
    }

    /**
     * Show vushare hotspot notification
     * @param text
     * @param addStopAction
     * @return
     */
    private Notification getVuShareNotification(String text, boolean addStopAction) {
        Intent notificationIntent = new Intent(getApplicationContext(),
                ShareActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this);
        builder/*.setContentIntent(pendingIntent)
                */.setSmallIcon(R.drawable.ic_vuscreen)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(getString(R.string.app_name))
                .setTicker(text)
                .setContentText(text);
        if (addStopAction)
            builder.addAction(getStopAction());
        return builder.build();
    }

    /**
     * Starts {@link ShareServer} and then enables Hotspot with assigned port being part of encoded SSID.
     */

/*
    private void startFileHostingServer(int port) {
        mFileServer = new ShareServer(getApplicationContext(),
                FileSelectController.getInstance(this).getFilesToHost(), port);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mFileServer.start();

                    String hotspotName = SharedPrefController.getUserName(ShareService.this) +
                            Constants.VUSHARE_HOTSPOT_SEPARATOR +
                            SharedPrefController.getUserAvatar(ShareService.this) +
                            Constants.VUSHARE_HOTSPOT_SEPARATOR +
                            Constants.VUSHARE_HOTSPOT_SUFFIX;
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                        mHotspotControl.enableVuShareHotspot(hotspotName, mFileServer.getListeningPort());
                    }
                    mHotspotCheckHandler.sendEmptyMessage(AP_START_CHECK);
                } catch (BindException e) {
                    e.printStackTrace();
                    Log.e(TAG, "exception in starting file server: " + e.getMessage());
                } catch (Exception e) {
                    Log.e(TAG, "exception in starting file server: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }).start();
    }
*/
    public static String DEFAULT_SENDER_NAME = "Sender.";

    public static String generateP2PSpuulName() {
        String androidId = Settings.Secure.ANDROID_ID;
        if (TextUtils.isEmpty(androidId))
            androidId = "";
        else
            androidId = androidId.length() <= 3 ? androidId : androidId.substring(androidId.length() - 3, androidId.length());
        return DEFAULT_SENDER_NAME + androidId;
    }
/*

   /* private void stopFileTasks() {
        try {
            if (null != mFileServer && mFileServer.isAlive()) {
                Log.d(TAG, "stopping server..");
                mFileServer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "exception in stopping file server: " + e.getMessage());
        }
    }
*/

    private void sendAcknowledgementBroadcast(int type) {
        Intent updateIntent = new Intent(SHARE_SERVER_UPDATES_INTENT_ACTION);
        updateIntent.putExtra(TYPE, type);
        sendBroadcast(updateIntent);
    }

    private void sendTransferStatusBroadcast(String ip, int progress, String updateText, String fileName) {
        Intent updateIntent = new Intent(SHARE_SERVER_UPDATES_INTENT_ACTION);
        updateIntent.putExtra(TYPE, FILE_TRANSFER_STATUS);
        updateIntent.putExtra(SHARE_SERVER_UPDATE_TEXT, updateText);
        updateIntent.putExtra(SHARE_SERVER_UPDATE_FILE_NAME, fileName);
        updateIntent.putExtra(SHARE_CLIENT_IP, ip);
        updateIntent.putExtra(SHARE_TRANSFER_PROGRESS, progress);
        sendBroadcast(updateIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mHotspotControl && mHotspotControl.isEnabled()) {
            mHotspotControl.disable();
            mWifiManager.setWifiEnabled(true);
          //  stopFileTasks();
        }
        if (null != mHotspotCheckHandler)
            mHotspotCheckHandler.removeCallbacksAndMessages(null);
        if (null != mNotificationStopActionReceiver)
            unregisterReceiver(mNotificationStopActionReceiver);

//        try {
//            fetchConnections.removeCallbacks(fetchConnectionsRunnable);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Updates notification text saying 'Hotspot Enabled' along with STOP action.
     * Since there isn't an easy way to update Foreground notification, notifying a new notification with same ID as earlier would do the trick.
     * <br>Ref: <a href="http://stackoverflow.com/a/20142620/862882">http://stackoverflow.com/a/20142620/862882</a>
     */
    private void updateForegroundNotification() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(SHARE_SERVICE_NOTIFICATION_ID, getVuShareNotification(getString(R.string.hotspot_enabled), true));
    }

    private static class HotspotChecker extends Handler {
        WeakReference<ShareService> service;

        HotspotChecker(ShareService senderService) {
            this.service = new WeakReference<>(senderService);
        }

        @Override
        public void handleMessage(Message msg) {
            ShareService senderService = service.get();
            if (null == senderService)
                return;
            if (msg.what == AP_ALIVE_CHECK) {
                if (null == senderService.mHotspotControl || !senderService.mHotspotControl.isEnabled()) {
                    Log.e(TAG, "hotspot isnt active, close this service");
                    senderService.disableHotspotAndStop();
                } else sendEmptyMessageDelayed(AP_ALIVE_CHECK, 3000);
            } else if (msg.what == AP_START_CHECK && null != senderService.mHotspotControl) {
                if (senderService.mHotspotControl.isEnabled())
                    senderService.updateForegroundNotification();
                else {
                    removeMessages(AP_START_CHECK);
                    sendEmptyMessageDelayed(AP_START_CHECK, 800);
                }
            }
        }
    }
    /**
     * Calls {@link HotspotControl#getConnectedWifiClients(int, HotspotControl.WifiClientConnectionListener)} to get Clients connected to Hotspot.<br>
     * Constantly adds/updates receiver items
     * <br> Posts a message to {@link ShareActivity.ShareUIHandler} to call itself every 1000ms
     */
/*
    private synchronized void listApClients() {
        if (mHotspotControl == null) {
            return;
        }
        mHotspotControl.getConnectedWifiClients(2000,
                new HotspotControl.WifiClientConnectionListener() {
                    public void onClientConnectionAlive(final HotspotControl.WifiScanResult wifiScanResult) {
                        Log.wtf(TAG, wifiScanResult.toString());
                    }

                    @Override
                    public void onClientConnectionDead(final HotspotControl.WifiScanResult wifiScanResult) {
                        Log.wtf(TAG, wifiScanResult.toString());
                    }

                    public void onWifiClientsScanComplete() {
                        fetchConnections.postDelayed(fetchConnectionsRunnable, 2000);
                    }
                }

        );
    }
*/
}
