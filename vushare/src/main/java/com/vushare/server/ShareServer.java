/*
 * Copyright 2017 Srihari Yachamaneni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vushare.server;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vushare.entity.EntityFileHost;
import com.vushare.ui.activity.ShareActivity;
import com.vushare.utility.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A Tiny Http Server extended from {@link NanoHTTPD}. Once started, serves selected files from {@link ShareActivity} on an assigned PORT.
 * <p>
 * Created by Sri on 18/12/16.
 */

class ShareServer extends NanoHTTPD {

    private static final String TAG = "ShareServer";

    private static final String MIME_JSON = "application/json";

    private ArrayList<EntityFileHost> mFilesToHost;
    private Context mContext;
    private FileInputStream mFileInputStream;

    public ShareServer(String host_name, int port) {
        super(host_name, port);
    }

    public ShareServer(Context context, ArrayList<EntityFileHost> filesToBeHosted) {
        this(null, 0);
        mContext = context;
        mFilesToHost = filesToBeHosted;
    }

    public ShareServer(Context context, ArrayList<EntityFileHost> filesToBeHosted, int port) {
        this(null, port);
        mContext = context;
        mFilesToHost = filesToBeHosted;
    }

    @Override
    public void stop() {
        super.stop();
    }

    @Override
    public Response serve(IHTTPSession session) {
        Response response = null;
        try {
            String url = session.getUri();
            if (StringUtils.isEmpty(url)) return null;

            if (url.startsWith("/api")) {
                url = url.replace("/api", "");
                if (StringUtils.isEmpty(url)) {
                    response = createFilePathsResponse();
                }
            } else if (url.startsWith("/download")) {
                url = url.replace("/download", "");
                response = createFileDownloadResponse(session, url);
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return response;
    }

    /**
     * Create file listing response
     * @return
     */
    private Response createFilePathsResponse() {
        Gson gson = new Gson();
        String responseString = gson.toJson(mFilesToHost,
                new TypeToken<ArrayList<EntityFileHost>>() {
                }.getType());
        return new NanoHTTPD.Response(Response.Status.OK, MIME_JSON, responseString);
    }

    private Response createFileDownloadResponse(IHTTPSession session, String fileName) {
        String range = null;
        for (String key : session.getHeaders().keySet()) {
            if ("range".equals(key)) {
                range = session.getHeaders().get(key);
            }
        }
        try {
            if (range == null) {
                return getFullResponse("", fileName);
            } else {
                return getPartialResponse("", range, fileName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Response(Response.Status.NOT_FOUND, MIME_PLAINTEXT, "File not found");
    }

    private Response getFullResponse(String mimeType, String fileName) throws FileNotFoundException {
        cleanupStreams();
        mFileInputStream = new FileInputStream(fileName);
        return new Response(Response.Status.OK, mimeType, mFileInputStream);
    }

    private void cleanupStreams() {
        try {
            mFileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Response getPartialResponse(String mimeType, String rangeHeader, String fileName) throws IOException {
        File file = new File(fileName);
        String rangeValue = rangeHeader.trim().substring("bytes=".length());
        long fileLength = file.length();
        long start, end;
        if (rangeValue.startsWith("-")) {
            end = fileLength - 1;
            start = fileLength - 1 - Long.parseLong(rangeValue.substring("-".length()));
        } else {
            String[] range = rangeValue.split("-");
            start = Long.parseLong(range[0]);
            end = range.length > 1 ? Long.parseLong(range[1]) : fileLength - 1;
        }
        if (end > fileLength - 1) {
            end = fileLength - 1;
        }
        if (start <= end) {
            long contentLength = end - start + 1;
            cleanupStreams();
            mFileInputStream = new FileInputStream(file);
            //noinspection ResultOfMethodCallIgnored
            mFileInputStream.skip(start);
            Response response = new Response(Response.Status.PARTIAL_CONTENT, mimeType, mFileInputStream);
            response.addHeader("Content-Length", contentLength + "");
            response.addHeader("Content-Range", "bytes " + start + "-" + end + "/" + fileLength);
            response.addHeader("Content-Type", mimeType);
            return response;
        } else {
            return new Response(Response.Status.RANGE_NOT_SATISFIABLE, "text/html", rangeHeader);
        }
    }

}
