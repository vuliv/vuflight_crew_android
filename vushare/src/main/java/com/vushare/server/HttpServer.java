package com.vushare.server;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vushare.entity.EntityFileHost;
import com.vushare.utility.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class HttpServer extends NanoHTTPD {

    public static final int SERVER_PORT = 7070;

    private Context mContext;
    private ArrayList<EntityFileHost> mFilesToHost;

    final String MIME_JSON = "application/json",
    MIME_PNG = "image/png";
    private FileInputStream fileInputStream;

    public HttpServer(int port, Context context, ArrayList<EntityFileHost> filesToHost) {
        super(null, port);
        mContext = context;
        this.mFilesToHost = filesToHost;
    }

    @Override
    public Response serve(IHTTPSession session) {
        Response response = null;
        try {
            String url = session.getUri();
            if (StringUtils.isEmpty(url)) return null;

            if (url.startsWith("/api")) {
                url = url.replace("/api", "");
                if (StringUtils.isEmpty(url)) {
                    response = createFilePathsResponse();
                }
            } else if (url.startsWith("/download")) {
                url = url.replace("/download", "");
                //url = /storage/enulated/0/sdgsdg.mp4/png
                response = createFileDownloadResponse(session, url);
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return response;
    }

    /**
     * Create file listing response
     * @return
     */
    private Response createFilePathsResponse() {
        Gson gson = new Gson();
        String responseString = gson.toJson(mFilesToHost,
                new TypeToken<ArrayList<EntityFileHost>>() {
                }.getType());
        return new NanoHTTPD.Response(Response.Status.OK, MIME_JSON, responseString);
    }

    /**
     * Create file download response
     * @param url
     * @return
     */
    private Response createFileDownloadResponse(String url) {
        try {
            File f1 = new File(url);
            InputStream in = new FileInputStream(f1);
            return new Response(Response.Status.OK, "", in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Response createFileDownloadResponse(IHTTPSession session, String fileName) {
        String range = null;
        for (String key : session.getHeaders().keySet()) {
            if ("range".equals(key)) {
                range = session.getHeaders().get(key);
            }
        }
        try {
            if (range == null) {
                return getFullResponse("", fileName);
            } else {
                return getPartialResponse("", range, fileName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Response(Response.Status.NOT_FOUND, MIME_PLAINTEXT, "File not found");
    }

    private Response getFullResponse(String mimeType, String fileName) throws FileNotFoundException {
        cleanupStreams();
        fileInputStream = new FileInputStream(fileName);
        return new Response(Response.Status.OK, mimeType, fileInputStream);
    }

    private void cleanupStreams() {
        try {
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Response getPartialResponse(String mimeType, String rangeHeader, String fileName) throws IOException {
        File file = new File(fileName);
        String rangeValue = rangeHeader.trim().substring("bytes=".length());
        long fileLength = file.length();
        long start, end;
        if (rangeValue.startsWith("-")) {
            end = fileLength - 1;
            start = fileLength - 1 - Long.parseLong(rangeValue.substring("-".length()));
        } else {
            String[] range = rangeValue.split("-");
            start = Long.parseLong(range[0]);
            end = range.length > 1 ? Long.parseLong(range[1]) : fileLength - 1;
        }
        if (end > fileLength - 1) {
            end = fileLength - 1;
        }
        if (start <= end) {
            long contentLength = end - start + 1;
            cleanupStreams();
            fileInputStream = new FileInputStream(file);
            //noinspection ResultOfMethodCallIgnored
            fileInputStream.skip(start);
            Response response = new Response(Response.Status.PARTIAL_CONTENT, mimeType, fileInputStream);
            response.addHeader("Content-Length", contentLength + "");
            response.addHeader("Content-Range", "bytes " + start + "-" + end + "/" + fileLength);
            response.addHeader("Content-Type", mimeType);
            return response;
        } else {
            return new Response(Response.Status.RANGE_NOT_SATISFIABLE, "text/html", rangeHeader);
        }
    }
}