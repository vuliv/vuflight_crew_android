package com.vushare.controller;

import com.vuliv.network.server.DataController;
import com.vushare.model.CartInventory;
import com.vushare.model.CrewProfile;
import com.vushare.model.CrewProfiles;
import com.vushare.utility.ConversionUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ankur on 7/5/2018.
 */

public class InventoryManager {

    private static final String TAG = InventoryManager.class.getSimpleName();
    private static InventoryManager inventoryManager;

    private Map<String, Map<String, List<CartInventory.Inventory>>> inventoryMap;

    private InventoryManager() {
        inventoryMap = new HashMap<>();
    }

    public static InventoryManager getInstance() {
        if (inventoryManager == null) {
            synchronized (InventoryManager.class) {
                if (inventoryManager == null) {
                    inventoryManager = new InventoryManager();
                }
            }
        }
        return inventoryManager;
    }

    public void addInventory(CartInventory cartInventory, String cartNo, int empId) {
        if (cartInventory.getInventory() != null) {
            //Map<String, List<CartInventory.Inventory>> typeInventoryMap = inventoryMap.get(cartNo);
            Map<String, List<CartInventory.Inventory>> typeInventoryMap = new HashMap<>();
            inventoryMap.put(cartNo, typeInventoryMap);

            for (CartInventory.Inventory inventory : cartInventory.getInventory()) {
                String[] types = inventory.getType().split("-");
                inventory.setType(types[0]);
                if (types.length == 2) {
                    inventory.setVeg(types[1].equalsIgnoreCase("VEG"));
                }

                List<CartInventory.Inventory> typeInventoryList = typeInventoryMap.get(inventory.getType());
                if (typeInventoryList == null) {
                    typeInventoryList = new ArrayList<>();
                    typeInventoryMap.put(inventory.getType(), typeInventoryList);
                }

                if (typeInventoryList.contains(inventory)) {

                    // same item is found then just add quantity in existing item
                    int i = typeInventoryList.indexOf(inventory);
                    CartInventory.Inventory existingInventory = typeInventoryList.get(i);
                    existingInventory.setQuantity(existingInventory.getQuantity() + inventory.getQuantity());

                } else {
                    // new item is found, so add it
                    typeInventoryList.add(inventory);
                }
            }
        }

        if (CrewProfiles.getInstance().getLogdInCrewMemId() == empId) {
            DataController.getInstance().setDataResponse(ConversionUtil.toEntityDataResponse(this, false));
            DataController.getInstance().setProductResponse(ConversionUtil.toEntityDataResponse(this, true));
        }
    }

    public int getQuantity(String cartNo, String type, boolean veg) {
        int quantity = 0;
        Map<String, List<CartInventory.Inventory>> cartInventoryMap = inventoryMap.get(cartNo);
        List<CartInventory.Inventory> inventoryList = cartInventoryMap.get(type);
        if (inventoryList != null) {
            for (CartInventory.Inventory inventory: inventoryList) {
                if (inventory.isVeg() == veg) {
                    quantity += inventory.getQuantity();
                }
            }
        }
        return quantity;
    }

    public void soldItems(String cartNo, int itemId, int quantitySold, CrewProfile crewProfile) {
        Map<String, List<CartInventory.Inventory>> cartInventoryMap = inventoryMap.get(cartNo);
        for (String type: cartInventoryMap.keySet()) {
            for (CartInventory.Inventory inventory: cartInventoryMap.get(type)) {
                if (inventory.getItemId() == itemId) {

                    inventory.updateQuantityAfterSell(quantitySold,crewProfile);
                }
            }
        }
    }








    public Map<String, List<CartInventory.Inventory>> getInventoryMap(String cartNo) {
        if (!inventoryMap.containsKey(cartNo)) {
            return new HashMap<>();
        }
        return inventoryMap.get(cartNo);
    }

    public void clear(){
        inventoryMap.clear();
    }

    public CartInventory.Inventory getInventoryByBarcode(String cartNo, String barcodeText) {
        Map<String, List<CartInventory.Inventory>> cartInventoryMap = inventoryMap.get(cartNo);
        for (List<CartInventory.Inventory> list : cartInventoryMap.values()) {
            for (CartInventory.Inventory inventory : list) {
                if(barcodeText.equals(inventory.getBarcode())){
                    return inventory;
                }
            }
        }
        return null;
    }
}
