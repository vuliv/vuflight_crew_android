package com.vushare.controller;

import android.content.Context;

import com.vushare.callback.ICallback;
import com.vushare.entity.EntityDirectoryDetail;
import com.vushare.entity.EntityFileHost;
import com.vushare.entity.EntityMusic;
import com.vushare.entity.EntityPhoto;
import com.vushare.entity.EntityVideo;

import java.util.ArrayList;

/**
 * Created by MB0000021 on 8/14/2017.
 */

public class FileSelectController {

    private static FileSelectController instance = null;

    private ArrayList mSelected;
    private ICallback mCallback;

    public static FileSelectController getInstance (Context context) {
        if (instance == null) {
            instance = new FileSelectController();
        }
        return instance;
    }

    public FileSelectController() {
        mSelected = new ArrayList();
    }

    /**
     * Set callback for file selection
     * @param callback
     */
    public void setCallbacks(ICallback callback) {
        this.mCallback = callback;
    }

    /**
     * Check whether file is selected or not
     * @param file
     * @return
     */
    public boolean isFileSelected(Object file) {
        if (mSelected.contains(file))
            return true;
        else
            return false;
    }

    public void clearFiles() {
        if (mSelected != null) mSelected.clear();
    }

    /**
     * Add file in selected files list
     * @param file
     */
    public void addFile(Object file) {
        mSelected.add(file);
        mCallback.onSuccess(mSelected.size());
    }

    /**
     * Remove file from selected files list
     * @param file
     */
    public void removeFile(Object file) {
        if (mSelected.contains(file)) {
            mSelected.remove(file);
            mCallback.onSuccess(mSelected.size());
        }
    }

    public int getSize() {
        return mSelected.size();
    }

    public String[] getFilePaths() {
        int size = mSelected.size();
        String[] files = new String[size];
        for (int i=0; i<size; i++) {
            Object object = mSelected.get(i);
            if (object instanceof EntityPhoto) {
                files[i] = ((EntityPhoto) object).getPath();
            } else if (object instanceof EntityVideo) {
                files[i] = ((EntityVideo) object).getPath();
            } else if (object instanceof EntityDirectoryDetail) {
                files[i] = ((EntityDirectoryDetail) object).getPath();
            } else if (object instanceof EntityMusic) {
                files[i] = ((EntityMusic) object).getSongPath();
            }
        }
        return files;
    }

    public ArrayList<EntityFileHost> getFilesToHost() {
        ArrayList<EntityFileHost> filesToHost = new ArrayList<>();
        int size = mSelected.size();
        for (int i=0; i<size; i++) {
            EntityFileHost entityFileHost = new EntityFileHost();
            Object object = mSelected.get(i);
            if (object instanceof EntityPhoto) {
                entityFileHost.setName(((EntityPhoto) object).getTitle());
                entityFileHost.setHostPath(((EntityPhoto) object).getPath());
                entityFileHost.setActualSize(((EntityPhoto) object).getSize());
                entityFileHost.setType(MediaController.TYPE_PHOTO);
            } else if (object instanceof EntityVideo) {
                entityFileHost.setName(((EntityVideo) object).getTitle());
                entityFileHost.setHostPath(((EntityVideo) object).getPath());
                entityFileHost.setActualSize(((EntityVideo) object).getSize());
                entityFileHost.setDuration(((EntityVideo) object).getDuration());
                entityFileHost.setVideoId(((EntityVideo) object).getId());
                entityFileHost.setType(MediaController.TYPE_VIDEO);
            } else if (object instanceof EntityDirectoryDetail) {
                entityFileHost.setName(((EntityDirectoryDetail) object).getFileName());
                entityFileHost.setHostPath(((EntityDirectoryDetail) object).getPath());
                entityFileHost.setActualSize(((EntityDirectoryDetail) object).getSize());
                entityFileHost.setType(MediaController.TYPE_FILE);
            } else if (object instanceof EntityMusic) {
                entityFileHost.setName(((EntityMusic) object).getSongName());
                entityFileHost.setHostPath(((EntityMusic) object).getSongPath());
                entityFileHost.setActualSize(((EntityMusic) object).getSize());
                entityFileHost.setDuration(((EntityMusic) object).getDuration());
                entityFileHost.setAudioId(((EntityMusic) object).getAlbumId());
                entityFileHost.setType(MediaController.TYPE_MUSIC);
            }
            filesToHost.add(entityFileHost);
        }
        return filesToHost;
    }
}
