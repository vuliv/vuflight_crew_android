package com.vushare.controller;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.vushare.ui.activity.ShareActivity;
import com.vushare.utility.WifiUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

public class HotspotControl {

    private static final String TAG = "HotspotControl";
    private WifiConfiguration mDefaultWifiConfig;
    private int mServerPort;

    private static Method getWifiApConfiguration;
    private static Method getWifiApState;
    private static Method isWifiApEnabled;
    private static Method setWifiApEnabled;
    private static Method setWifiApConfiguration;

    private WifiManager mWifiManager;
    private String mDeviceName;

    private static HotspotControl instance = null;
    public static final String DEFAULT_DEVICE_NAME = "Unknown";

    static {
        for (Method method : WifiManager.class.getDeclaredMethods()) {
            switch (method.getName()) {
                case "getWifiApConfiguration":
                    getWifiApConfiguration = method;
                    break;
                case "getWifiApState":
                    getWifiApState = method;
                    break;
                case "isWifiApEnabled":
                    isWifiApEnabled = method;
                    break;
                case "setWifiApEnabled":
                    setWifiApEnabled = method;
                    break;
                case "setWifiApConfiguration":
                    setWifiApConfiguration = method;
                    break;
            }
        }
    }

    private HotspotControl(Context context) {
        mWifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        mDeviceName = WifiUtils.getDeviceName(mWifiManager);
    }

    public static HotspotControl getInstance(Context context) {
        if (instance == null) {
            instance = new HotspotControl(context);
        }
        return instance;
    }

    public boolean isEnabled() {
        Object result = invokeSilently(isWifiApEnabled, mWifiManager);
        if (result == null) {
            return false;
        }
        return (Boolean) result;
    }

    private static Object invokeSilently(Method method, Object receiver, Object... args) {
        try {
            return method.invoke(receiver, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            Log.e(TAG, "exception in invoking methods: " + e.getMessage());
        }
        return null;
    }

    public static boolean isSupported() {
        return (getWifiApState != null
                && isWifiApEnabled != null
                && setWifiApEnabled != null
                && getWifiApConfiguration != null);
    }

    public WifiConfiguration getConfiguration() {
        Object result = invokeSilently(getWifiApConfiguration, mWifiManager);
        if (result == null) {
            return null;
        }
        return (WifiConfiguration) result;
    }

    private boolean setHotspotEnabled(WifiConfiguration config, boolean enabled) {
        Object result = invokeSilently(setWifiApEnabled, mWifiManager, config, enabled);
        if (result == null) {
            return false;
        }
        return (Boolean) result;
    }

    private boolean setHotspotConfig(WifiConfiguration config) {
        Object result = invokeSilently(setWifiApConfiguration, mWifiManager, config);
        if (result == null) {
            return false;
        }
        return (Boolean) result;
    }

    /**
     * Enable hotspot
     * @param name
     * @param port
     * @return
     */
    public boolean enableVuShareHotspot(String name, int port) {
        mWifiManager.setWifiEnabled(false);
        mServerPort = port;
        mDefaultWifiConfig = getConfiguration();
        // Create new Open Wifi Configuration
//        WifiConfiguration wifiConf = new WifiConfiguration();
//        wifiConf.SSID = name;
//        wifiConf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        mWifiManager.addNetwork(wifiConf);
//
//        // Save this configuration
//        mWifiManager.saveConfiguration();
        return setHotspotEnabled(mDefaultWifiConfig, true);
    }

    public boolean setHotspotName(String newName) {
        return false;
       /* try {
            mDefaultWifiConfig = getConfiguration();
            Method getConfigMethod = mWifiManager.getClass().getMethod("getWifiApConfiguration");
            WifiConfiguration wifiConfig = (WifiConfiguration) getConfigMethod.invoke(mWifiManager);

            wifiConfig.SSID = newName;

            Method setConfigMethod = mWifiManager.getClass().getMethod("setWifiApConfiguration", WifiConfiguration.class);
            setConfigMethod.invoke(mWifiManager, wifiConfig);

            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }*/
    }

    /**
     * Enable hotspot with existing configuration
     * @return
     */
    public boolean enable() {
        mWifiManager.setWifiEnabled(false);
        return setHotspotEnabled(getConfiguration(), true);
    }

    /**
     * Disable hotspot
     * @return
     */
    public boolean disable() {
        //restore original hotspot config if available
        if (null != mDefaultWifiConfig)
            setHotspotConfig(mDefaultWifiConfig);
        mServerPort = 0;
        return setHotspotEnabled(mDefaultWifiConfig, false);
    }

    /**
     * Get server's listening port
     * @return
     */
    public int getShareServerListeningPort() {
        return mServerPort;
    }

    /**
     * Get server host ip address
     * @return
     */
    public String getHostIpAddress() {
        if (!isEnabled()) {
            return null;
        }
        Inet4Address inet4 = getInetAddress(Inet4Address.class);
        if (null != inet4)
            return inet4.toString();
        Inet6Address inet6 = getInetAddress(Inet6Address.class);
        if (null != inet6)
            return inet6.toString();
        return null;
    }

    private <T extends InetAddress> T getInetAddress(Class<T> addressType) {
        try {
            Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
            while (ifaces.hasMoreElements()) {
                NetworkInterface iface = ifaces.nextElement();

                if (!iface.getName().equals(mDeviceName)) {
                    continue;
                }

                Enumeration<InetAddress> addrs = iface.getInetAddresses();
                while (addrs.hasMoreElements()) {
                    InetAddress addr = addrs.nextElement();

                    if (addressType.isInstance(addr)) {
                        return addressType.cast(addr);
                    }
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "exception in fetching inet address: " + e.getMessage());
        }
        return null;
    }

    public static class WifiScanResult {
        public String ip;

        public WifiScanResult(String ipAddr) {
            this.ip = ipAddr;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this)
                return true;
            if (o == null || o.getClass() != this.getClass())
                return false;
            WifiScanResult v = (WifiScanResult) o;
            return ip.equals(v.ip);
        }
    }

    public List<WifiScanResult> getConnectedWifiClients(final int timeout,
                                                        final WifiClientConnectionListener listener) {
        List<WifiScanResult> wifiScanResults = getWifiClients();
        if (wifiScanResults == null) {
            listener.onWifiClientsScanComplete();
            return null;
        }
        final CountDownLatch latch = new CountDownLatch(wifiScanResults.size());
        ExecutorService es = Executors.newCachedThreadPool();
        for (final WifiScanResult c : wifiScanResults) {
            es.submit(new Runnable() {
                public void run() {
                    try {
                        InetAddress ip = InetAddress.getByName(c.ip);
                        if (ip.isReachable(timeout)) {
                            listener.onClientConnectionAlive(c);
                            Thread.sleep(1000);
                        } else listener.onClientConnectionDead(c);
                    } catch (IOException e) {
                        Log.e(TAG, "io exception while trying to reach client, ip: " + (c.ip));
                    } catch (InterruptedException ire) {
                        Log.e(TAG, "InterruptedException: " + ire.getMessage());
                    }
                    latch.countDown();
                }
            });
        }
        new Thread() {
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    Log.e(TAG, "listing clients countdown interrupted", e);
                }
                listener.onWifiClientsScanComplete();
            }
        }.start();
        return wifiScanResults;
    }

    public List<WifiScanResult> getWifiClients() {
        if (!isEnabled()) {
            return null;
        }
        List<WifiScanResult> result = new ArrayList<>();

        // Basic sanity checks
        Pattern macPattern = Pattern.compile("..:..:..:..:..:..");

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(" +");
                if (parts.length < 4 || !macPattern.matcher(parts[3]).find()) {
                    continue;
                }
                String ipAddr = parts[0];
                result.add(new WifiScanResult(ipAddr));
            }
        } catch (IOException e) {
            Log.e(TAG, "exception in getting clients", e);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "", e);
            }
        }

        return result;
    }

    public interface WifiClientConnectionListener {
        void onClientConnectionAlive(WifiScanResult c);

        void onClientConnectionDead(WifiScanResult c);

        void onWifiClientsScanComplete();
    }

}
