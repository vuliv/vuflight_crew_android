package com.vushare.controller;

import android.util.Log;

import com.vushare.callback.ICallback;
import com.vushare.utility.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by MB0000021 on 8/22/2017.
 */

public class APIController {

    /**
     * Download data from server
     * @param apiUrl
     * @param callback
     */
    public void downloadDataFromSender(final String apiUrl, final ICallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                InputStream is = null;
                try {
                    URL url = new URL(apiUrl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    // Starts the query
                    Log.i("Progress", "downloadDataFromSender1");
                    conn.connect();
                    Log.i("Progress", "downloadDataFromSender2");
                    conn.getResponseCode();
                    is = conn.getInputStream();
                    // Convert the InputStream into a string
                    String response = readIt(is);
                    Log.i("Progress", "Response: " + response);
                    callback.onSuccess(response);
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailure("");
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    /**
     * Read the response from input stream
     * @param stream
     * @return
     */
    private String readIt(InputStream stream) {
        Writer writer = new StringWriter();

        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(
                    new InputStreamReader(stream, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }
}
