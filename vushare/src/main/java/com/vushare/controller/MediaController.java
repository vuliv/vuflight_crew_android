package com.vushare.controller;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.vucast.entity.EntityMediaDetail;
import com.vushare.entity.EntityMusic;
import com.vushare.entity.EntityPhoto;
import com.vushare.entity.EntityVideo;
import com.vushare.utility.Utility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by MB0000021 on 8/9/2017.
 */

public class MediaController {

    public static final String TYPE_PHOTO = "Photo";
    public static final String TYPE_VIDEO = "Video";
    public static final String TYPE_MUSIC = "Music";
    public static final String TYPE_FILE = "File";

    public static MediaController instance = null;
    private final Context mContext;
    private ArrayList<EntityPhoto> mPhotos;
    private ArrayList<EntityVideo> mVideos;
    private TreeMap<String, ArrayList<EntityMediaDetail>> videosFolderMap;
    private ArrayList<EntityMusic> mMusics;

    public static MediaController getInstance (Context context) {
        if (instance == null) {
            instance = new MediaController(context);
        }
        return instance;
    }

    private MediaController(Context context) {
        this.mContext = context;
        mPhotos = new ArrayList<>();
        mVideos = new ArrayList<>();
        mMusics = new ArrayList<>();
        videosFolderMap = new TreeMap<>();
    }

    /**
     * Fetch all the photos from device
     */
    private void getAllPhotos() {
        if (Utility.isMarshmallow()) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.WIDTH,
                MediaStore.Images.Media.HEIGHT,
                MediaStore.Images.Media.SIZE
        };

        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + " = " + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

        Uri queryUri = MediaStore.Files.getContentUri("external");
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor cursor = contentResolver
                .query(queryUri, projection, selection, null, MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC");

        ArrayList<String> uniquePaths = new ArrayList<>();

        if (cursor != null) {
            int widthIndex = cursor.getColumnIndex(MediaStore.Images.Media.WIDTH);
            int heightIndex = cursor.getColumnIndex(MediaStore.Images.Media.HEIGHT);
            int sizeIndex = cursor.getColumnIndex(MediaStore.Images.Media.SIZE);
            int nameIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            int idIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
            int dateAddedIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATE_ADDED);
            int titleIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.TITLE);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);

            if (cursor.moveToFirst()) {
                do {
                    String path = cursor.getString(dataColumnIndex);

                    EntityPhoto entityPhoto = new EntityPhoto();
                    entityPhoto.setId(cursor.getLong(idIndex));
                    entityPhoto.setType(TYPE_PHOTO);
                    entityPhoto.setFolder(cursor.getString(nameIndex));
                    entityPhoto.setTitle(cursor.getString(titleIndex));
                    entityPhoto.setWidth(cursor.getLong(widthIndex));
                    entityPhoto.setHeight(cursor.getLong(heightIndex));
                    entityPhoto.setPath(path);
                    entityPhoto.setSize(cursor.getLong(sizeIndex));
                    entityPhoto.setModifiedTime(cursor.getLong(dateAddedIndex));
                    entityPhoto.setThumbnailId(cursor.getLong(idIndex));
                    entityPhoto.setSelected(false);

                    if (!uniquePaths.contains(path)) {
                        mPhotos.add(entityPhoto);
                        uniquePaths.add(path);
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    /**
     * Fetch all the videos from device
     */
    private void getAllVideos() {
        if (Utility.isMarshmallow()) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.WIDTH,
                MediaStore.Video.Media.HEIGHT,
                MediaStore.Video.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Video.Media.SIZE
        };

        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + " = " + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

        Uri queryUri = MediaStore.Files.getContentUri("external");
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor cursor = contentResolver
                .query(queryUri, projection, selection, null, MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC");

        ArrayList<String> uniquePaths = new ArrayList<>();

        if (cursor != null) {
            int idIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            int dateAddedIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATE_ADDED);
            int titleIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.TITLE);
            int durationColumnIndex = cursor.getColumnIndex(MediaStore.Video.Media.DURATION);
            int widthColumnIndex = cursor.getColumnIndex(MediaStore.Video.Media.WIDTH);
            int heightColumnIndex = cursor.getColumnIndex(MediaStore.Video.Media.HEIGHT);
            int bucketColumnIndex = cursor.getColumnIndex(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
            int sizeIndex = cursor.getColumnIndex(MediaStore.Images.Media.SIZE);

            if (cursor.moveToFirst()) {
                do {
                    String path = cursor.getString(dataColumnIndex);

                    EntityVideo entityVideo = new EntityVideo();
                    entityVideo.setId(cursor.getLong(idIndex));
                    entityVideo.setType(TYPE_VIDEO);
                    entityVideo.setFolder(cursor.getString(bucketColumnIndex));
                    entityVideo.setTitle(cursor.getString(titleIndex));
                    entityVideo.setWidth(cursor.getLong(widthColumnIndex));
                    entityVideo.setHeight(cursor.getLong(heightColumnIndex));
                    entityVideo.setDuration(cursor.getLong(durationColumnIndex));
                    entityVideo.setPath(path);
                    entityVideo.setSize(cursor.getLong(sizeIndex));
                    entityVideo.setModifiedTime(cursor.getLong(dateAddedIndex));
                    entityVideo.setThumbnailId(cursor.getLong(idIndex));
                    entityVideo.setSelected(false);
                    if (!videosFolderMap.containsKey(entityVideo.getFolder())) {
                        videosFolderMap.put(entityVideo.getFolder(), new ArrayList<EntityMediaDetail>());
                    }
                    EntityMediaDetail entityMediaDetail = new EntityMediaDetail();
                    entityMediaDetail.setPath(entityVideo.getPath());
                    entityMediaDetail.setTitle(entityVideo.getTitle());
                    entityMediaDetail.setType(entityVideo.getType());
                    entityMediaDetail.setDuration(entityVideo.getDuration());
                    entityMediaDetail.setVideoId(entityVideo.getId());
                    entityMediaDetail.setActualSize(entityVideo.getSize());
                    videosFolderMap.get(entityVideo.getFolder()).add(entityMediaDetail);
                    if (!uniquePaths.contains(path)) {
                        mVideos.add(entityVideo);
                        uniquePaths.add(path);
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    /**
     * Fetch all the music from device
     */
    private void getAllMusics() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(MediaStore.Audio.Media.IS_MUSIC);
        stringBuilder.append(" != 0 ");
        try {
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor c = mContext.getContentResolver().query(uri, null, stringBuilder.toString(), null,
                    "UPPER(" + MediaStore.Audio.Media.DATE_MODIFIED + ") DESC");
            ArrayList<String> uniqueSongs = new ArrayList<String>();
            c.moveToFirst();
            while (!c.isAfterLast()) {
                EntityMusic entityMusic = new EntityMusic();
                String title = c.getString(c.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String artist = c.getString(c.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String album = c.getString(c.getColumnIndex(MediaStore.Audio.Media.ALBUM));

                long duration = c.getLong(c.getColumnIndex(MediaStore.Audio.Media.DURATION));
                long size = c.getLong(c.getColumnIndex(MediaStore.Audio.Media.SIZE));
                String data = c.getString(c.getColumnIndex(MediaStore.Audio.Media.DATA));

                long albumId = c.getLong(c.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                long artistId = c.getLong(c.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID));
                long songId = c.getLong(c.getColumnIndex(MediaStore.Audio.Media._ID));
                int year = c.getInt(c.getColumnIndex(MediaStore.Audio.Media.YEAR));
                long dateModified = c.getLong(c.getColumnIndex(MediaStore.Audio.Media.DATE_MODIFIED));
                int trackNumber = c.getInt(c.getColumnIndex(MediaStore.Audio.Media.TRACK));
                String composer = c.getString(c.getColumnIndex(MediaStore.Audio.Media.COMPOSER));

                entityMusic.setSrNo(c.getPosition());
                entityMusic.setType(TYPE_MUSIC);

                entityMusic.setSongName(title);
                entityMusic.setAlbumName(album);
                entityMusic.setArtistName(artist);
                entityMusic.setYear(year);
                entityMusic.setDateModified(dateModified);
                entityMusic.setTrackNumber(trackNumber % 1000);
                entityMusic.setComposer(composer);

                entityMusic.setDuration(duration);
                entityMusic.setSize(size);
                entityMusic.setSongPath(data);

                entityMusic.setAlbumId(albumId);
                entityMusic.setSongId(songId);
                entityMusic.setArtistId(artistId);
                entityMusic.setSelected(false);

                if (!uniqueSongs.contains(data)) {
                    mMusics.add(entityMusic);
                    uniqueSongs.add(data);
                }
                c.moveToNext();
            }
            uniqueSongs.clear();
            uniqueSongs = null;
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<EntityPhoto> getPhotos() {
        if (mPhotos == null || mPhotos.size() <= 0) {
            getAllPhotos();
        }
        return mPhotos;
    }

    public ArrayList<EntityVideo> getVideos() {
        if (mVideos == null || mVideos.size() <= 0) {
            getAllVideos();
        }
        return mVideos;
    }

    public TreeMap<String, ArrayList<EntityMediaDetail>> getVideosFolderMap() {
        if (videosFolderMap == null || videosFolderMap.size() <= 0) {
            getAllVideos();
        }
        return videosFolderMap;
    }

    public ArrayList<EntityMusic> getMusics() {
        if (mMusics == null || mMusics.size() <= 0) {
            getAllMusics();
        }
        return mMusics;
    }
}
