package com.vushare.controller;

import android.content.Context;

import com.vushare.entity.EntityFileHost;

import java.util.ArrayList;

/**
 * Created by MB0000021 on 8/23/2017.
 */

public class DownloadController {

    private static DownloadController instance = null;
    private ArrayList<EntityFileHost> mFilesToDownload = new ArrayList<>();

    public static DownloadController getInstance (Context context) {
        if (instance == null) {
            instance = new DownloadController();
        }
        return instance;
    }

    public DownloadController() {}

    public void setFilesToDownload(ArrayList<EntityFileHost> filesToDownload) {
        this.mFilesToDownload = filesToDownload;
    }

    public ArrayList<EntityFileHost> getFilesToDownload() {
        return mFilesToDownload;
    }

    public void clearFilesToDownload() {
        mFilesToDownload.clear();
    }

}
