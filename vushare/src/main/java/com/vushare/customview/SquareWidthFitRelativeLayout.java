package com.vushare.customview;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;


public class SquareWidthFitRelativeLayout extends RelativeLayout {

    public SquareWidthFitRelativeLayout(Context context) {
        super(context);
    }


    public SquareWidthFitRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public SquareWidthFitRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
    	 super.onMeasure(widthMeasureSpec, heightMeasureSpec);
         setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
}
